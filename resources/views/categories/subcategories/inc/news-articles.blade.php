<div class="category-search-wrapper">
    @can('admin')
        <div class="category-create-button-wrapper">
            <a href="{{route('subcategories-posts-create', $subcategory->slug)}}">
                <button class="category-create-button">Create new post</button>
            </a>
        </div>
    @endcan
    <div class="category-search-button-wrapper">
        <input type="text" class="category-search-input">
        <button class="category-search-button">search</button>
    </div>
</div>
<div class="category-body-wrapper">
    <div class="category-subcategory-wrapper">
        <div class="category-subcategory-title">
            browse by speciality
        </div>
        <div class="category-subcategory-body">
            @foreach($subcategory->posts->sortByDesc('created_at')->where('is_deleted', 0) as $post)
                <div
                @if($subcategory->id == $item->id)
                     class="category-current-subcategory">
                @else
                    >
                @endif
                        <a href="{{route('subcategories-show', $item->slug)}}">
                            {{$item->name}}
                        </a>
                    </div>
            @endforeach
        </div>
    </div>
    <div class="category-post-wrapper">
        @foreach($subcategory->posts as $post)
            <div class="category-post-body">
                <div class="category-post-content">
                    <img class="category-post-body-image" src="{{$post->image}}" alt="">
                </div>
                <div class="category-post-sample">
                    <div class="category-post-sample-text">
                        <strong> Speciality:
                            @foreach($post->subcategories as $sub)
                                <span>
                                    <a href="{{route('subcategories-show', $sub->slug)}}">
                                        | {{$sub->name}} |
                                    </a>
                                </span>
                            @endforeach
                        </strong>
                        <br>
                        <strong> Attached users:
                            @foreach($post->users as $user)
                                <span>
                                    <a href="">
                                        | {{$user->name}} |
                                    </a>
                                </span>
                            @endforeach
                        </strong>
                        <br>
                        {{Str::limit($post->body, 250)}}
                    </div>
                    <div class="category-post-a-tag">
                        <a href="{{route('posts-show', $post->slug)}}">more...</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
