<div class="category-body-wrapper">
    <div class="event-wrapper">
        @can('admin')
            <div class="category-create-button-wrapper">
                <a href="{{route('subcategories-posts-create', $subcategory->slug)}}">
                    <button class="category-create-button">Create new post</button>
                </a>
            </div>
        @endcan
        <div class="event-title">
            <span>
                featured upcomming events
            </span>
        </div>
            @foreach($subcategory->posts->sortByDesc('created_at')->where('is_deleted', 0) as $post)
            <div class="category-event-post-wrapper">
                <div class="category-event-post-body">
                    <div class="category-event-post-content">
                        <img class="category-event-post-body-image" src="{{$post->image}}" alt="">
                    </div>
                    <div class="event-right-div">
                        <div class="event-right-date">
                            <div>
                            <span>
                                date: {{$post->start_date}}
                            </span>
                            </div>
                            <div>
                            <span>
                                speciality
                            </span>
                            </div>
                            <div class="event-speciality">
                            <span>
                                @foreach($post->subcategories as $sub)
                                    <a href="{{route('subcategories-show', $sub->slug)}}">
                                    <strong>
                                        {{$sub->name}}
                                    </strong>
                                    </a>
                                @endforeach
                            </span>
                            </div>
                        </div>
                        <div class="category-event-post-sample">
                            <div class="category-event-post-sample-text">
                                {{Str::limit($post->body, 250)}}
                            </div>
                            <div class="category-event-post-a-tag">
                                <a href="{{route('posts-show', $post->slug)}}">more...</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
    <div class="category-event-subcategory-wrapper">
        <div class="category-subcategory-title">
            browse by speciality
        </div>
            <div class="category-subcategory-body">
                @foreach($subcategory->category->subcategories as $item)
                    @if($subcategory->id == $item->id)
                        <div class="category-current-subcategory">
                    @else
                        <div>
                    @endif
                            <a href="{{route('subcategories-show', $item->slug)}}">
                                {{$item->name}}
                            </a>
                        </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
