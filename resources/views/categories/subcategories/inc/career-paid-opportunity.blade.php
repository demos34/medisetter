<div class="event-post-wrapper">
    <div class="ml-5">
        @can('admin')
            <div class="category-create-button-wrapper">
                <a href="{{route('categories-posts-create', $subcategory->category->slug)}}">
                    <button class="category-create-button">Create new post</button>
                </a>
            </div>
        @endcan
    </div>
    <div class="events">
        @foreach($subcategory->posts->sortByDesc('created_at')->where('is_deleted', 0) as $post)
            <div class="event-post">
                <div class="event-image-wrapper">
                    <img class="event-image" src="{{$post->image}}" alt="{{$post->image}}">
                </div>
                <div class="event-body-wrapper">
                    <div class="d-flex justify-content-between">
                        <div class="date-picker-career">
                            {{$post->created_at}}
                        </div>
                        <div class="date-picker-career">
                            Specialities:
                            @foreach($post->specialities as $speciality)
                                <strong class="text-uppercase">
                                    {{$speciality->en_name}}
                                </strong>
                            @endforeach
                        </div>
                    </div>
                    <div class="d-flex justify-content-between">
                        <div class="date-picker-career">
                            Subcategories:
                            @foreach($post->subcategories as $subcategory)
                                <strong class="text-uppercase">
                                    {{$subcategory->name}}
                                </strong>
                            @endforeach
                        </div>
                        <div class="date-picker-career">
                            @if($post->code_id !== NULL)
                                <strong>
                                    Code
                                </strong>:
                                <strong>
                                    {{$post->code->code}}
                                </strong>
                            @endif
                        </div>
                    </div>
                    <div class="p-1">
                        <span>
                            {{Str::limit($post->body, 140)}}
                        </span>
                    </div>
                    <div class="discussion-view-section">
                        <a href="{{route('posts-show', $post->slug)}}">
                            Read more
                        </a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>


{{--<div class="category-search-wrapper">--}}
{{--    @can('admin')--}}
{{--        <div class="category-create-button-wrapper">--}}
{{--            <a href="{{route('subcategories-posts-create', $subcategory->slug)}}">--}}
{{--                <button class="category-create-button">Create new post</button>--}}
{{--            </a>--}}
{{--        </div>--}}
{{--    @endcan--}}
{{--</div>--}}
{{--<div class="category-body-wrapper">--}}
{{--    <div class="career-wrapper">--}}
{{--        @foreach($subcategory->posts as $post)--}}
{{--            <div class="category-event-post-wrapper">--}}
{{--                <div class="category-event-post-body">--}}
{{--                    <div class="career-post-content">--}}
{{--                    <span>--}}
{{--                        {{Str::title($post->category->category)}}--}}
{{--                    </span>--}}
{{--                    </div>--}}
{{--                    <div class="career-post-content">--}}
{{--                    <span>--}}
{{--                        @foreach($post->subcategories as $sub)--}}
{{--                            <a href="{{route('subcategories-show', $sub->slug)}}">--}}
{{--                                <strong>--}}
{{--                                    {{Str::title($sub->name)}}--}}
{{--                                </strong>--}}
{{--                            </a>--}}
{{--                            <br>--}}
{{--                        @endforeach--}}
{{--                    </span>--}}
{{--                    </div>--}}
{{--                    <div class="event-right-div">--}}
{{--                        <div class="career-post-sample">--}}
{{--                            <div class="career-post-sample-text">--}}
{{--                                {{Str::limit($post->body, 250)}}--}}
{{--                            </div>--}}
{{--                            <div class="career-post-a-tag">--}}
{{--                                <a href="{{route('posts-show', $post->slug)}}">more...</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="career-post-content-right">--}}
{{--                    <span>--}}
{{--                        <u>CODE</u>--}}
{{--                    </span>--}}
{{--                        <br>--}}
{{--                        <span>--}}
{{--                        {{$post->code->code}}--}}
{{--                    </span>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endforeach--}}
{{--    </div>--}}
{{--</div>--}}
