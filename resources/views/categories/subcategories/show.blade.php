@section('spec')
    <spec :category="{{$subcategory->category}}"></spec>
@endsection

<x-app-layout>
    <div class="main-category-body">
        <div class="overflow-hidden">
            <div class="text-center mb-12">
                <strong class="category-title">
                    {{$subcategory->category->category}}
                </strong>
                <br>
                <strong class="subcategory-title">
                    {{$subcategory->name}}
                </strong>
            </div>
            @include('categories.subcategories.inc.' . $subcategory->category->slug)
        </div>
    </div>
</x-app-layout>
