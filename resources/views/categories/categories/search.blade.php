@section('spec')
    <spec :category="{{$category}}"></spec>
@endsection
@section('search')
    <search :category="{{$category}}" :user="{{Auth::user()}}"></search>
@endsection
<x-app-layout>
    <div class="main-category-body">
        <div class="w-100">
            @include('categories.categories.inc.search-' . $category->slug)
        </div>
    </div>
</x-app-layout>
