<x-app-layout>
    @guest()
    <section class="home-message">
        <div class="heading">
            <img class="home-logo" src="/storage/custom-nav/logo.png" alt="home-logo">
            <h2 class="home-heading">MediSetter</h2>
        </div>
        <p class="message-content">
            Sorry, you need to be logged in to view this page. If you already have an account, you can log in <a style="color: navajowhite; font-size: 1.1em; font-weight: bold" href="{{route('login')}}">here</a>.
        </p>
        <p class="message-content">
            Don't have an account? Register <a style="color: navajowhite; font-size: 1.1em; font-weight: bold" href="{{route('register')}}">here</a> and take advantage of the vast knowledge library that we provide.
        </p>
    </section>
    <div class="home-page">

        <div class="posts-grid home-posts-grid">
            <div class="home-category-container">
                <h5 class="home-subcategory-heading">News & Articles</h5>
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$newsPost->image}}" alt="{{$newsPost->image}}">
                    <h2>{{Str::limit($newsPost->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($newsPost->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button" href="{{route('posts-show', $newsPost->slug)}}">Continue</a>
                </article>
                <br>
            </div>

            <div class="home-category-container">
                <h5 class="home-subcategory-heading">Drug info & updates</h5>
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$drugPost->image}}" alt="{{$drugPost->image}}">
                    <h2>{{Str::limit($drugPost->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($drugPost->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button" href="{{route('posts-show', $drugPost->slug)}}">Continue</a>
                </article>
                <br>
            </div>

            <div class="home-category-container">
                <h5 class="home-subcategory-heading">Events</h5>
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$eventPost->image}}" alt="{{$eventPost->image}}">
                    <h2>{{Str::limit($eventPost->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($eventPost->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button" href="{{route('posts-show', $eventPost->slug)}}">Continue</a>
                </article>
                <br>
            </div>

            <div class="home-category-container">
                <h5 class="home-subcategory-heading">Discussion</h5>
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$discussionPost->image}}" alt="{{$discussionPost->image}}">
                    <h2>{{Str::limit($discussionPost->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($discussionPost->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button"
                       href="{{route('posts-show', $discussionPost->slug)}}">Continue</a>
                </article>
                <br>
            </div>

            <div class="home-category-container">
                <h5 class="home-subcategory-heading">Career & paid opportunities</h5>
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$careerPost->image}}" alt="{{$careerPost->image}}">
                    <h2>{{Str::limit($careerPost->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($careerPost->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button" href="{{route('posts-show', $careerPost->slug)}}">Continue</a>
                </article>
                <br>
            </div>
        </div>
    </div>
    @endcan
</x-app-layout>
