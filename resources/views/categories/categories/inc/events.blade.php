<div class="discussion">
    @if(Auth::check())
    @can('verify')
    <a class="post-create" href="{{route('discussion-posts-create', $category->slug)}}">
        <i class="fas fa-plus"></i>
    </a>
    @else
    <strong>Your account is not verified yet!</strong>
    @endif
    @else
    <strong>You must be <a href="{{route('login')}}">logged in</a> to write new post!</strong>
    @endif

    @foreach($category->posts->sortByDesc('created_at')->where('is_deleted', 0) as $post)
    <section class="discussion-post-single">
        <section class="post-author event-career-image-holder">
            <img class="event-career-avatar" src="{{$post->image}}" alt="">
        </section>


        <section class="post-discussion-content">

            <div class="top-data-events-career">
                <div class="left-end">
                    <span class="date-picker discussion-date">
                         Date: <strong>{{$post->created_at}}</strong>
                    </span>
                </div>


                <div class="right-end">
                    <div class="qualification">
                        Qualification:
                        @foreach($post->specialities as $speciality)
                        <strong class="text-uppercase">
                            {{$speciality->en_name}}
                        </strong>
                        @endforeach
                    </div>

                    <div class="code">
                        @if($post->code_id !== NULL)
                        <span>
                            Code
                        </span>:
                        <strong class="post-code">
                            {{$post->code->code}}
                        </strong>
                        @endif
                    </div>
                </div>
            </div>


            <br>

            <div class="post-preview">
                <br>
                <span class="discussion-post-body-boyd">
                    {{Str::limit($post->body, 100)}}
                </span>
            </div>


            <a class="show-button events-careers-button" href="{{route('posts-show', $post->slug)}}">
                <span>
                    Read more
                </span>
            </a>
        </section>
    </section>
    @endforeach
</div>

