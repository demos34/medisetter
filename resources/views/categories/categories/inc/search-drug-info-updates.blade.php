<div class="discussion">
    <div class="search-results-msg">
        <strong class="search-results-msg">
            Search results: {{$posts->count()}}
        </strong>
    </div>

    <div class="posts-grid">

        @foreach($posts->sortByDesc('created_at')->where('is_deleted', 0) as $post)
        <article class="post-card">
            <img class="post-card-image" src="{{$post->image}}" alt="{{$post->image}}">
            <h2>{{Str::limit($post->title, 30)}}</h2>
            <div class="post-card-content">
                <p>{{Str::limit($post->body, 150)}}</p>
            </div>
            <a class="show-button news-button" href="{{route('posts-show', $post->slug)}}">Continue</a>
        </article>
        @endforeach

    </div>
</div>
