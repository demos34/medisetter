<div class="discussion">
    @if(Auth::check())
    @can('verify')
    <a class="post-create" href="{{route('discussion-posts-create', $category->slug)}}">
        <i class="fas fa-plus"></i>
    </a>
    @else
    <strong>Your account is not verified yet!</strong>
    @endif
    @else
    <strong>You must be <a href="{{route('login')}}">logged in</a> to write new post!</strong>
    @endif

    @foreach($category->posts->where('is_deleted', 0)->sortByDesc('created_at') as $post)
    <section class="discussion-post-single">
        <section class="post-author">
            <a href="{{route('user-view', $post->author->user_id)}}" class="w-100">
                <img class="author-avatar" src="{{$post->author->user->profile->avatar}}" alt="">
            </a>
            <p class="author-name">{{$post->author->user->name}}</p>
            @if(implode(',', $post->author->user->roles()->get()->pluck('role')->toArray()) === 'Administrator')
            <p class="author-role">
                {{implode(',', $post->author->user->roles()->get()->pluck('role')->toArray())}}
            </p>
            @endif
        </section>


        <section class="post-discussion-content">
            <p class="discussion-action">
                @if($post->is_shared === 1)
                <strong>
                    {{$post->author->user->name}}
                </strong>
                shared a post of
                <strong>{{$post->share->post->author->user->name}}:</strong>
                @if($post->share->title != NULL)
                {{$post->share->title}}
                @endif
                </strong>
                @endif
            </p>

            <!--            <div class="discussion-post-body">-->
            <span class="date-picker discussion-date">
                Date: <strong>{{$post->created_at}}</strong>
            </span>

            <br>

            <div class="post-preview">
                <strong>
                    {{$post->title}}
                </strong>
                <br>
                <span class="discussion-post-body-boyd">
                    {{Str::limit($post->body, 100)}}
                </span>
            </div>

            <div class="bottom-discussion">
                <div class="post-reactions discussion-reactions">
                    <div class="comments">
                        <span><i class="far fa-comment-dots"></i></span>
                        <span>{{$post->comments->count()}}</span>
                    </div>
                    <div class="views">
                        <span><i class="far fa-eye"></i></span>
                        <span>{{$post->views->count()}}</span>
                    </div>
                    <div class="likes">
                        <span><i class="far fa-thumbs-up"></i></span>
                        <span>{{$post->postReactions->where('reaction_id', 1)->count()}}</span>
                    </div>
                </div>

                <a class="show-button" href="{{route('posts-show', $post->slug)}}">
                    <span>
                    Show Discussion
                    </span>
                </a>
            </div>

        </section>
    </section>
    @endforeach
</div>
<!--<div class="w-100 users-view-posts-wrapper">-->
<!--    @foreach($category->posts->sortByDesc('created_at') as $post)-->
<!--        <div class="mx-auto my-4 discussion-post">-->
<!--            <div class="discussion-author-wrapper">-->
<!--                <a href="{{route('user-view', $post->author->user_id)}}" class="w-100">-->
<!--                    <img class="discussion-author-avatar" src="{{$post->author->user->profile->avatar}}" alt="">-->
<!--                </a>-->
<!--                <p class="discussion-author-name">{{$post->author->user->name}}</p>-->
<!--                @if(implode(',', $post->author->user->roles()->get()->pluck('role')->toArray()) === 'Administrator')-->
<!--                    <p class="discussion-author-role">-->
<!--                        {{implode(',', $post->author->user->roles()->get()->pluck('role')->toArray())}}-->
<!--                    </p>-->
<!--                @endif-->
<!--            </div>-->
<!--            <div class="discussion-post-wrapper">-->
<!--                <div class="discussion-shared-section mx-auto text-center mt-2">-->
<!--                    @if($post->is_shared === 1)-->
<!--                        <strong>-->
<!--                            {{$post->author->user->name}}-->
<!--                        </strong>-->
<!--                        share post of-->
<!--                        <strong>{{$post->share->post->author->user->name}}:</strong>-->
<!--                        @if($post->share->title != NULL)-->
<!--                        {{$post->share->title}}-->
<!--                        @endif-->
<!--                        </strong>-->
<!---->
<!--                    @endif-->
<!--                </div>-->
<!--                <div class="discussion-post-body">-->
<!--                    <div class="d-flex justify-content-between">-->
<!--                        <div class="w-70 p-2">-->
<!--                                <span class="date-picker">-->
<!--                                DATE: <strong>{{$post->created_at}}</strong>-->
<!--                                </span>-->
<!--                        </div>-->
<!---->
<!--                        <div class="discussion-icons-wrapper">-->
<!--                            <div class="comments">-->
<!--                                <span><i class="material-icons-outlined activity-icon">comment</i></span>-->
<!--                                <span>{{$post->comments->count()}}</span>-->
<!--                            </div>-->
<!--                            <div class="views">-->
<!--                                <span><i class="material-icons-outlined activity-icon">remove_red_eye</i></span>-->
<!--                                <span>{{$post->views->count()}}</span>-->
<!--                            </div>-->
<!--                            <div class="likes">-->
<!--                                <span><i class="material-icons-outlined activity-icon">thumb_up_alt</i></span>-->
<!--                                <span>{{$post->postReactions->where('reaction_id', 1)->count()}}</span>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="p-2 discussion-post-title">-->
<!--                        <strong>-->
<!--                            {{$post->title}}-->
<!--                        </strong>-->
<!--                        <br>-->
<!--                        <span class="discussion-post-body-boyd">-->
<!--                                {{Str::limit($post->body, 100)}}-->
<!--                            </span>-->
<!--                    </div>-->
<!--                    <div class="discussion-view-section">-->
<!--                        <a href="{{route('posts-show', $post->slug)}}" class="w-100">-->
<!--                            view discussion-->
<!--                        </a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    @endforeach-->
<!--</div>-->
