<div class="discussion">

    @if(Auth::check())
    @can('verify')
    <a class="post-create" href="{{route('discussion-posts-create', $category->slug)}}">
        <i class="fas fa-plus"></i>
    </a>
    @else
    <strong>Your account is not verified yet!</strong>
    @endif
    @else
    <strong>You must be <a href="{{route('login')}}">logged in</a> to write new post!</strong>
    @endif

    <br>

    <div class="posts-grid">

        @foreach($category->posts->sortByDesc('created_at')->where('is_deleted', 0) as $post)
        <article class="post-card">
            <img class="post-card-image" src="{{$post->image}}" alt="{{$post->image}}">
            <h2>{{Str::limit($post->title, 30)}}</h2>
            <div class="post-card-content">
                <p>{{Str::limit($post->body, 150)}}</p>
            </div>
            <a class="show-button news-button" href="{{route('posts-show', $post->slug)}}">Continue</a>
        </article>
        @endforeach

    </div>

</div>


<!--<div class="event-post-wrapper">-->
<!--    <div class="ml-5">-->
<!--        @can('admin')-->
<!--            <div class="category-create-button-wrapper">-->
<!--                <a href="{{route('categories-posts-create', $category->slug)}}">-->
<!--                    <button class="category-create-button">Create new post</button>-->
<!--                </a>-->
<!--            </div>-->
<!--        @endcan-->
<!--    </div>-->
<!--</div>-->
<!--<div class="news-articles-wrapper">-->
<!--    @foreach($category->posts->sortByDesc('created_at')->where('is_deleted', 0) as $post)-->
<!--        <div class="news-articles-post-wrapper">-->
<!--            <div class="news-articles-image">-->
<!--                <img class="news-articles-image-image" src="{{$post->image}}" alt="{{$post->image}}">-->
<!--            </div>-->
<!--            <div class="news-articles-body-wrapper">-->
<!--                <div class="news-articles-title">-->
<!--                    {{$post->title}}-->
<!--                </div>-->
<!--                <div class="news-articles-body">-->
<!--                    {{Str::limit($post->body, 150)}}-->
<!--                </div>-->
<!--                <div class="news-articles-btn">-->
<!--                    <a href="{{route('posts-show', $post->slug)}}">-->
<!--                        <button class="btn news-articles-btn-continue">-->
<!--                            Continue-->
<!--                        </button>-->
<!--                    </a>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    @endforeach-->
<!--</div>-->
