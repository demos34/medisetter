@section('spec')
<spec :category="{{$category}}" :speciality="{{$speciality}}"></spec>
@endsection

<x-app-layout>
    @include('categories.spec.inc.' . $category->slug)
</x-app-layout>
