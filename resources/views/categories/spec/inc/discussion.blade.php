<div class="discussion">
    @foreach($speciality->posts->sortByDesc('created_at')->where('category_id', $category->id)->where('is_deleted', 0) as $post)
        <section class="discussion-post-single">
            <section class="post-author">
                <a href="{{route('user-view', $post->author->user_id)}}" class="w-100">
                    <img class="author-avatar" src="{{$post->author->user->profile->avatar}}" alt="">
                </a>
                <p class="author-name">{{$post->author->user->name}}</p>
                @if(implode(',', $post->author->user->roles()->get()->pluck('role')->toArray()) === 'Administrator')
                    <p class="author-role">
                        {{implode(',', $post->author->user->roles()->get()->pluck('role')->toArray())}}
                    </p>
                @endif
            </section>


            <section class="post-discussion-content">
                <p class="discussion-action">
                    @if($post->is_shared === 1)
                        <strong>
                            {{$post->author->user->name}}
                        </strong>
                        shared a post of
                        <strong>{{$post->share->post->author->user->name}}:</strong>
                        @if($post->share->title != NULL)
                        {{$post->share->title}}
                        @endif
                        </strong>
                    @endif
                </p>

                <!--            <div class="discussion-post-body">-->
                <span class="date-picker discussion-date">
                Date: <strong>{{$post->created_at}}</strong>
            </span>

                <br>

                <div class="post-preview">
                    <strong>
                        {{$post->title}}
                    </strong>
                    <br>
                    <span class="discussion-post-body-boyd">
                    {{Str::limit($post->body, 100)}}
                </span>
                </div>

                <div class="bottom-discussion">
                    <div class="post-reactions discussion-reactions">
                        <div class="comments">
                            <span><i class="far fa-comment-dots"></i></span>
                            <span>{{$post->comments->count()}}</span>
                        </div>
                        <div class="views">
                            <span><i class="far fa-eye"></i></span>
                            <span>{{$post->views->count()}}</span>
                        </div>
                        <div class="likes">
                            <span><i class="far fa-thumbs-up"></i></span>
                            <span>{{$post->postReactions->where('reaction_id', 1)->count()}}</span>
                        </div>
                    </div>

                    <a class="show-button" href="{{route('posts-show', $post->slug)}}">
                    <span>
                    Show Discussion
                    </span>
                    </a>
                </div>

            </section>
        </section>
    @endforeach
</div>
