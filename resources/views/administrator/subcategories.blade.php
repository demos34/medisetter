<x-app-layout>
    <div class="main-body">
        <div class="overflow-hidden">
            <a href="/admin"><button class="button-primary">back</button></a>
            <div class="main-body-title">
                <h2>
                    Categories
                </h2>
            </div>
            <div class="main-category-wrapper">
                <div class="main-category-table">
                    <table class="main-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Subcategory</th>
                            <th>Attached to category</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($subcategories as $subcategory)
                            <tr>
                                <td>
                                    {{$subcategory->id}}
                                </td>
                                <td>
                                    {{$subcategory->name}}
                                </td>
                                <td>
                                    {{$subcategory->category->category}}
                                </td>
                                <td>
                                    <form action="{{route('admin-subcategories-delete', $subcategory->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="confirm('Are you sure?')">
                                            X
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="category-create">
                    <label>
                        Add category
                    </label>
                    <form action="{{route('admin-subcategories-store')}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="card-header">
                            <label for="name">
                                Subcategory:
                            </label>
                        </div>
                        <div class="card-body">
                            <input id="name"
                                   type="text"
                                   class="form-control @error('name') is-invalid @enderror"
                                   name="name"
                                   value="{{ old('name') }}"
                                   required autocomplete="name" autofocus>
                            @error('name')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="card-header">
                            <label for="category">
                                For category:
                            </label>
                        </div>
                        <div class="card-radio">
                            <label class="container">News & Events
                                <input type="radio" checked="checked" name="radio" value="1">
                                <span class="checkmark"></span>
                            </label>
                            <label class="container">Career opportunity
                                <input type="radio" name="radio" value="2">
                                <span class="checkmark"></span>
                            </label>
                        </div>
                        <div class="flex justify-center">
                            <button type="submit" class="add-button">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</x-app-layout>
