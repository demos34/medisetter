<x-app-layout>
    <div class="main-body">
        <div class="overflow-hidden">
            <a href="/admin"><button class="button-primary">back</button></a>
            <div class="main-body-title">
                <h2>
                    Categories
                </h2>
            </div>
            <div class="main-category-wrapper">
                <div class="main-category-table">
                    <table class="main-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Category</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>
                                    {{$category->id}}
                                </td>
                                <td>
                                    {{$category->category}}
                                </td>
                                <td>
                                    <form action="{{route('admin-categories-delete', $category->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button onclick="confirm('Are you sure?')">
                                            X
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="category-create">
                    <label>
                        Add category
                    </label>
                    <form action="{{route('admin-categories-store')}}" method="post">
                        @csrf
                        @method('PUT')
                        <div class="card-header">
                            <label for="category">
                                Category:
                            </label>
                        </div>
                        <div class="card-body">
                                <input id="category"
                                       type="text"
                                       class="form-control @error('category') is-invalid @enderror"
                                       name="category"
                                       value="{{ old('category') }}"
                                       required autocomplete="bg_color" autofocus>
                                @error('category')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        <div class="flex justify-center">
                            <button type="submit" class="add-button">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</x-app-layout>
