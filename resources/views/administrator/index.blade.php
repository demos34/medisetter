<x-app-layout>
    <div class="main-body">
        <div class="overflow-hidden">
            <div class="main-body-title">
                <h2>
                    Admin panel
                </h2>
            </div>
            <div class="main-body-items">
                <a href="">
                    View all users
                </a>
            </div>
            <div class="main-body-items">
                <a href="{{route('admin-roles')}}">
                    View all roles
                </a>
            </div>
            <div class="main-body-items">
                <a href="{{route('admin-categories')}}">
                    View all categories
                </a>
            </div>
            <div class="main-body-items">
                <a href="{{route('admin-subcategories')}}">
                    View all subcategories
                </a>
            </div>
        </div>
    </div>
</x-app-layout>
