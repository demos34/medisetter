<x-app-layout>
    <div class="main-body">
        <div class="overflow-hidden">
            <div class="main-body-title">
                <h2>
                    Roles
                </h2>
            </div>
            <div class="main-body-items">
                <table class="main-table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Role</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($roles as $role)
                            <tr>
                                <td>
                                    {{$role->id}}
                                </td>
                                <td>
                                    {{$role->role}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</x-app-layout>
