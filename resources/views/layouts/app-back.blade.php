<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'MediSetter') }}</title>
        <link rel="icon" type="image/svg+xml" href="/storage/custom-nav/logo.png">

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        @yield('custom-css')
        @include('partials.custom-2')

        @livewireStyles

        <!-- Scripts -->
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.3/dist/alpine.js"></script>
    </head>
    <body class="font-sans antialiased" id="">
        <div class="min-h-screen">
            <!-- Page Heading -->
            <header class="bg-white">
                @if(isset(Auth::user()->name))
                    @livewire('navigation-dropdown')
                @endif
                <div class="max-w-7xl mx-auto">
{{--                    {{ $header }}--}}
                </div>
            </header>
            <!-- Page Content -->
            <main>
                <div id="app">
                    <category></category>
                @include('partials.alerts')
                    {{ $slot }}
                </div>
            </main>
        </div>

        @stack('modals')
        <script src="{!! asset('js/app.js') !!}"></script>
        @yield('custom-js')
        @livewireScripts
    </body>
</html>
