<x-app-layout>
    <div class="discussion">

        <div class="user-info">
            <div class="user-data">
                <p class="user-data-row">
                    <strong>Names:</strong> {{$user->name}}
                </p>

                <p class="user-data-row">
                    <strong>Followed by:</strong>
                    @if($user->followers())
                    {{$user->followers()->count()}}
                    @else
                    0
                    @endif
                </p>

                <p class="user-data-row">
                    <strong>Following:</strong>
                    @if(isset($user->follower))
                    {{$user->follower->users()->count()}}
                    @else
                    0
                    @endif
                </p>

                <p class="user-data-row">
                    <strong>Posts:</strong>
                    @if(isset($user->author->posts))
                    {{$user->author->posts->count()}}
                    @else
                    0
                    @endif
                </p>

                <div class="user-actions-container">
                    @can('admin')
                    <a href="{{route('add-contacts', $user->id)}}">
                        <i class="far fa-envelope"></i>
                    </a>
                    @endcan

                    <a href="{{route('user-follow', $user->id)}}">
                        @if(isset(Auth::user()->follower))
                        @if(Auth::user()->follower->users()->where('user_id', $user->id)->count() > 0)
                        <i class="fas fa-user"></i>
                        @else
                        <i class="fas fa-users"></i>
                        @endif
                        @else
                        <i class="fas fa-users"></i>
                        @endif
                    </a>
                </div>
            </div>

            <div class="user-avatar">
                <img src="{{$user->profile->avatar}}" alt="{{$user->name}}">
            </div>
        </div>

        @if(isset($user->author->posts))
        @foreach($user->author->posts->sortByDesc('created_at') as $post)
        <section class="discussion-post-single">
            <section class="post-author">
                <a href="{{route('user-view', $post->author->user_id)}}" class="w-100">
                    <img class="author-avatar" src="{{$post->author->user->profile->avatar}}" alt="">
                </a>
                <p class="author-name">{{$post->author->user->name}}</p>
                @if(implode(',', $post->author->user->roles()->get()->pluck('role')->toArray()) === 'Administrator')
                <p class="author-role">
                    {{implode(',', $post->author->user->roles()->get()->pluck('role')->toArray())}}
                </p>
                @endif
            </section>


            <section class="post-discussion-content">
                <p class="discussion-action">
                    @if($post->is_shared === 1)
                    <strong>
                        {{$post->author->user->name}}
                    </strong>
                    shared a post of
                    <strong>{{$post->share->post->author->user->name}}:</strong>
                    @if($post->share->title != NULL)
                    {{$post->share->title}}
                    @endif
                    </strong>
                    @endif
                </p>

                <!--            <div class="discussion-post-body">-->
                <span class="date-picker discussion-date">
                Date: <strong>{{$post->created_at}}</strong>
            </span>

                <br>

                <div class="post-preview">
                    <strong>
                        {{$post->title}}
                    </strong>
                    <br>
                    <span class="discussion-post-body-boyd">
                    {{Str::limit($post->body, 100)}}
                </span>
                </div>

                <div class="bottom-discussion">
                    <div class="post-reactions discussion-reactions">
                        <div class="comments">
                            <span><i class="far fa-comment-dots"></i></span>
                            <span>{{$post->comments->count()}}</span>
                        </div>
                        <div class="views">
                            <span><i class="far fa-eye"></i></span>
                            <span>{{$post->views->count()}}</span>
                        </div>
                        <div class="likes">
                            <span><i class="far fa-thumbs-up"></i></span>
                            <span>{{$post->postReactions->where('reaction_id', 1)->count()}}</span>
                        </div>
                    </div>

                    <a class="show-button" href="{{route('posts-show', $post->slug)}}">
                    <span>
                    Show Discussion
                    </span>
                    </a>
                </div>

            </section>
        </section>
        @endforeach
        @else
        <div class="text-center">
            <strong>
                {{$user->name}}
            </strong> has not post yet!
        </div>
        @endif
    </div>
</x-app-layout>
