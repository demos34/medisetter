@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"
        integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.js"
        integrity="sha512-zO8oeHCxetPn1Hd9PdDleg5Tw1bAaP0YmNvPY8CwcRyUk7d7/+nyElmFrB6f7vg4f7Fv4sui1mcep8RIEShczg=="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.min.js"
        integrity="sha512-SuxO9djzjML6b9w9/I07IWnLnQhgyYVSpHZx0JV97kGBfTIsUYlWflyuW4ypnvhBrslz1yJ3R+S14fdCWmSmSA=="
        crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.css"
      integrity="sha512-C7hOmCgGzihKXzyPU/z4nv97W0d9bv4ALuuEbSf6hm93myico9qa0hv4dODThvCsqQUmKmLcJmlpRmCaApr83g=="
      crossorigin="anonymous"/>
@endsection

@can('admin-enterprise')
    <div class="row my-5 w-100">
        <div class="col-md-3 ml-1">
            <div class="text-center mb-2">
                <h4>
                    <strong>
                        {{$post->title}}
                    </strong>
                </h4>
            </div>
            <div class="text-center mb-2">
                <h6>
                    <span>
                        Posted:
                    </span>
                    <strong>
                        {{$post->created_at}}
                    </strong>
                    <br>

                </h6>
            </div>
            <div class="text-center">
                <span>
                        Shared:
                    </span>
                <strong>
                    @if($post->shares->count() > 0)
                        {{$post->shares->count()}}
                    @else
                        0
                    @endif
                </strong>
            </div>
<!--            <div class="my-5">-->
<!--                <a href="{{route('analyze')}}">-->
<!--                    <button class="btn btn-outline-dark">-->
<!--                        Back-->
<!--                    </button>-->
<!--                </a>-->
<!--            </div>-->
        </div>
        <div class="col-md-8 m-auto">
            <div class="row w-100">
                <div class="col-md-4">
                    <span>total views:</span> @if($post->total !== NULL) <strong>{{$post->total->count}}</strong> @else
                        <strong>0</strong> @endif
                </div>
                <div class="col-md-4">
                    <span>In past 24 hours</span> @if($post->total !== NULL) <strong>{{$past24}}</strong> @else <strong>0</strong> @endif
                </div>
                <div class="col-md-4">
                    <span>In past week</span> @if($post->total !== NULL) <strong>{{$pastWeek}}</strong> @else
                        <strong>0</strong> @endif
                </div>
            </div>
            <div class="row w-100">
                <div class="col-md-4">
                    <span>total reactions:</span> @if($post->postReactions->count() > 0)
                        <strong>{{$post->postReactions->count()}}</strong> @else <strong>0</strong> @endif
                </div>
                <div class="col-md-4">
                    <span>In past 24 hours</span> @if($post->total !== NULL) <strong>{{$past24Reacts}}</strong> @else
                        <strong>0</strong> @endif
                </div>
                <div class="col-md-4">
                    <span>In past week</span> @if($post->total !== NULL) <strong>{{$pastWeekReacts}}</strong> @else
                        <strong>0</strong> @endif
                </div>
            </div>
            <div class="row w-100">
                <div class="col-md-4">
                    <span>likes:</span> @if($post->postReactions->count() > 0)
                        <strong>{{$post->postReactions->where('id', 1)->count()}}</strong> @else
                        <strong>0</strong> @endif
                </div>
                <div class="col-md-4">
                    <span>loves: </span> @if($post->postReactions->count() > 0)
                        <strong>{{$post->postReactions->where('id', 2)->count()}}</strong> @else
                        <strong>0</strong> @endif
                </div>
                <div class="col-md-4">
                    <span>support: </span> @if($post->postReactions->count() > 0)
                        <strong>{{$post->postReactions->where('id', 3)->count()}}</strong> @else
                        <strong>0</strong> @endif
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    @if(Auth::user()->author->id === $post->author->id)
        <div class="row my-5 w-100">
            <div class="col-md-3 ml-1">
                <div class="text-center mb-2">
                    <h4>
                        <strong>
                            Existing schemas:
                        </strong>
                    </h4>
                </div>
            </div>
            <div class="col-md-8 m-auto">
                <div class="row w-100">
                    <div class="col-md-2">
                        <strong>
                            Title:
                        </strong>
                    </div>
                    <div class="col-md-3">
                        <strong>
                            Start:
                        </strong>
                    </div>
                    <div class="col-md-3">
                        <strong>
                            End:
                        </strong>
                    </div>
                    <div class="col-md-2">
                        <strong>
                            Create:
                        </strong>
                    </div>
                    <div class="col-md-2">
                        <strong>
                            Details
                        </strong>
                    </div>
                    @foreach($analyzes as $analyze)
                        <div class="col-md-2 mt-2">
                                {{$analyze->title}}
                        </div>
                        <div class="col-md-3 mt-2">
                                {{$analyze->start}}
                        </div>
                        <div class="col-md-3 mt-2">
                                {{$analyze->end}}
                        </div>
                        <div class="col-md-2 mt-2">
                                {{$analyze->created_at}}
                        </div>
                        <div class="col-md-2 mt-2">
                            <a href="{{route('analyze-show', $analyze->id)}}">
                                <button class="btn btn-outline-primary">
                                    View
                                </button>
                            </a>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
        <div class="py-8">
            <div class="border-t border-gray-200"></div>
        </div>
        <div class="row my-5 w-100">
            <div class="col-md-3 ml-1">
                <div class="text-center mb-2">
                    <h4>
                        <strong>
                            Create new schema
                        </strong>
                    </h4>
                </div>
            </div>
            <div class="col-md-8 m-auto">
                <div class="text-center w-100">
                    <form action="{{route('analyze-create', $post->slug)}}" method="post" class="w-100">
                        @csrf
                        <div class="my-5">
                            <label for="name">
                                Schema name:
                            </label>
                            <br>
                            <input id="name"
                                   class="create-posts-title-input @error('name') is-invalid @enderror"
                                   name="name"
                                   value="{{ old('name') }}"
                                   required autocomplete="title">
                            @error('name')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                            @enderror
                        </div>
                        <div class="d-flex justify-content-between w-100">
                            <div class="w-50">
                                <label for="start_date">
                                    Start date
                                </label>
                                <br>
                                <input name="start_date" type="datetime-local" id="start_date" autocomplete>
                            </div>
                            <div class="w-50">
                                <label for="start_date">
                                    End date
                                </label>
                                <br>
                                <input name="end_date" type="datetime-local" id="start_date" autocomplete>
                            </div>
                        </div>
                        <div class="mx-auto my-5 text-center font-weight-bolder">
                            <h3 class="text-center">
                                Select age range!
                            </h3>
                            <label for="from_age">
                                From age:
                            </label>
                            <input name="from_age" type="number" id="from_age" autocomplete class="range-age">
                            <label for="to_age">
                                To age:
                            </label>
                            <input name="to_age" type="number" id="to_age" autocomplete class="range-age">
                        </div>
                        <div class="mx-auto my-5 text-center font-weight-bolder">
                            <h3 class="text-center">
                                Select Location!
                            </h3>
                            <label for="location">
                                Location:
                            </label>
                            <input name="location" type="text" id="location" autocomplete class="location-input">
                        </div>
                        <div>
                            <label for="speciality">
                                Qualification
                            </label>
                            <select class="select-css text-uppercase" name="speciality" id="speciality">
                                <option value="all">
                                    ---- All -----
                                </option>
                                @foreach($specialities as $speciality)
                                    <option value="{{$speciality->id}}">
                                        {{$speciality->$name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-5">
                            <label for="degrees">
                                Education
                            </label>
                            <select class="select-css text-uppercase" name="degrees" id="degrees">
                                <option value="all">
                                    ---- All -----
                                </option>
                                @foreach($degrees as $degree)
                                    <option value="{{$degree->id}}">
                                        {{$degree->$name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mx-auto text-center mt-5">
                            <button class="btn btn-outline-primary">
                                Create
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="py-8">
            <div class="border-t border-gray-200"></div>
        </div>
    @else
        <div class="row my-5 w-100">
            <div class="col-md-3 ml-1">
                <div class="text-center mb-2">
                    <h4>
                        <strong>
                            Foreign Existing schemas:
                        </strong>
                    </h4>
                </div>
            </div>
            <div class="col-md-8 m-auto">
                <div class="row w-100">
                    <div class="col-md-2">
                        <strong>
                            Title:
                        </strong>
                    </div>
                    <div class="col-md-3">
                        <strong>
                            Start:
                        </strong>
                    </div>
                    <div class="col-md-3">
                        <strong>
                            End:
                        </strong>
                    </div>
                    <div class="col-md-2">
                        <strong>
                            Create:
                        </strong>
                    </div>
                    <div class="col-md-2">
                        <strong>
                            Details
                        </strong>
                    </div>
                    @foreach($analyzes as $analyze)
                        <div class="col-md-2 mt-2">
                            {{$analyze->title}}
                        </div>
                        <div class="col-md-3 mt-2">
                            {{$analyze->start}}
                        </div>
                        <div class="col-md-3 mt-2">
                            {{$analyze->end}}
                        </div>
                        <div class="col-md-2 mt-2">
                            {{$analyze->created_at}}
                        </div>
                        <div class="col-md-2 mt-2">
                            <a href="{{route('analyze-show', $analyze->id)}}">
                                <button class="btn btn-outline-primary">
                                    View
                                </button>
                            </a>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>
        <div class="py-8">
            <div class="border-t border-gray-200"></div>
        </div>
        <div class="row my-5 w-100">
            <div class="col-md-3 ml-1">
                <div class="text-center mb-2">
                    <h4>
                        <strong>
                            Create new foreign schema
                        </strong>
                    </h4>
                </div>
            </div>
            <div class="col-md-8 m-auto">
                <div class="text-center w-100">
                    <form action="{{route('analyze-create', $post->slug)}}" method="post" class="w-100">
                        @csrf
                        <div class="my-5">
                            <label for="name">
                                Schema name:
                            </label>
                            <br>
                            <input id="name"
                                   class="create-posts-title-input @error('name') is-invalid @enderror"
                                   name="name"
                                   value="{{ old('name') }}"
                                   required autocomplete="title">
                            @error('name')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                            @enderror
                        </div>
                        <div class="d-flex justify-content-between w-100">
                            <div class="w-50">
                                <label for="start_date">
                                    Start date
                                </label>
                                <br>
                                <input name="start_date" type="datetime-local" id="start_date" autocomplete>
                            </div>
                            <div class="w-50">
                                <label for="start_date">
                                    End date
                                </label>
                                <br>
                                <input name="end_date" type="datetime-local" id="start_date" autocomplete>
                            </div>
                        </div>
                        <div class="mx-auto my-5 text-center font-weight-bolder">
                            <h3 class="text-center">
                                Select age range!
                            </h3>
                            <label for="from_age">
                                From age:
                            </label>
                            <input name="from_age" type="number" id="from_age" autocomplete class="range-age">
                            <label for="to_age">
                                To age:
                            </label>
                            <input name="to_age" type="number" id="to_age" autocomplete class="range-age">
                        </div>
                        <div class="mx-auto my-5 text-center font-weight-bolder">
                            <h3 class="text-center">
                                Select Location!
                            </h3>
                            <label for="location">
                                Location:
                            </label>
                            <input name="location" type="text" id="location" autocomplete class="location-input">
                        </div>
                        <div>
                            <label for="speciality">
                                Qualification
                            </label>
                            <select class="select-css text-uppercase" name="speciality" id="speciality">
                                <option value="all">
                                    ---- All -----
                                </option>
                                @foreach($specialities as $speciality)
                                    <option value="{{$speciality->id}}">
                                        {{$speciality->$name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="my-5">
                            <label for="degrees">
                                Education
                            </label>
                            <select class="select-css text-uppercase" name="degrees" id="degrees">
                                <option value="all">
                                    ---- All -----
                                </option>
                                @foreach($degrees as $degree)
                                    <option value="{{$degree->id}}">
                                        {{$degree->$name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mx-auto text-center mt-5">
                            <button class="btn btn-outline-primary">
                                Create
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="py-8">
            <div class="border-t border-gray-200"></div>
        </div>
    @endif
@endcan
