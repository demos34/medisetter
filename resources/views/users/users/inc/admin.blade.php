@can('admin')
    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    Roles:
                </h4>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="row">
                <div class="col-md-12">
                    <div class="">
                        @foreach($roles as $role)
                            <div class="admin-items">
                                <div class="text-uppercase">
                                    {{$role->role}}
                                    <hr>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>



    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    Posts:
                </h4>
            </div>
            <div class="my-5 mx-auto text-center">
                <strong>
                    Hard delete all of soft-deleted posts!
                </strong>
                <form action="{{route('admin-posts-hard-delete-all')}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="mt-4 btn btn-outline-danger" onclick="return confirm('Are you sure? This will destroy all soft-deleted posts permanently!')">
                        &times;
                    </button>
                </form>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="col-md-12 w-100 mx-auto">
            <h4>View all soft-deleted posts. These posts can be restored!</h4>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="admin-items-wrapper row mt-5 text-center">
                        <div class="col-lg-3 my-2">
                            <strong>
                                Title
                            </strong>
                        </div>
                        <div class="col-lg-2 my-2">
                            <strong>
                                Author
                            </strong>
                        </div>
                        <div class="col-lg-2 my-2">
                            <strong>
                                Restore
                            </strong>
                        </div>
                        <div class="col-lg-2 my-2">
                            <strong>
                                View
                            </strong>
                        </div>
                        <div class="col-lg-3 my-2">
                            <strong>
                                Hard delete (can't be restored)
                            </strong>
                        </div>
                        @foreach($deletedPosts as $deleted)
                            <div class="col-lg-3 my-2">
                                {{Str::limit($deleted->title, 20)}}
                            </div>
                            <div class="col-lg-2 my-2">
                                {{$deleted->author->user->name}}
                            </div>
                            <div class="col-lg-2">
                                <a href="{{ route('admin-posts-restore', $deleted->slug) }}">
                                    <button class="btn btn-outline-warning">
                                        Restore
                                    </button>
                                </a>
                            </div>
                            <div class="col-lg-2">
                                <a href="{{ route('admin-posts-deleted-show', $deleted->slug) }}">
                                    <button class="btn btn-outline-primary">
                                        Show
                                    </button>
                                </a>
                            </div>
                            <div class="col-lg-3">
                                <form action="{{route('admin-posts-hard-delete', $deleted->slug)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-outline-danger" onclick="return confirm('Are you sure? This will destroy permanently post!')">
                                        Hard Delete
                                    </button>
                                </form>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>






    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    Categories:
                </h4>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="row">
                <div class="col-md-8">
                    <div class="admin-items-wrapper">
                        @foreach($categories as $category)
                            <div class="admin-items my-2">
                                <div class="admin-item-div">
                                    {{$category->$cat}}
                                </div>
                                <div class="admin-item-btn">
                                    <a href="{{ route('admin-show-category', $category->slug) }}">
                                        <button class="btn btn-outline-primary">
                                            Show more..
                                        </button>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="text-uppercase text-center font-weight-bolder font-italic">
                        Add new category
                    </label>
                    <form action="{{route('admin-categories-store')}}" method="post">
                        @csrf
                        @method('PUT')
                        <div>
                            <label for="category">
                                English name:
                            </label>
                            <input id="category"
                                   type="text"
                                   class="form-control @error('category') is-invalid @enderror"
                                   name="category"
                                   value="{{ old('category') }}"
                                   required autocomplete="category">
                            @error('category')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mt-2">
                            <label for="vi_category">
                                Vietnamese name:
                            </label>
                            <input id="vi_category"
                                   type="text"
                                   class="form-control @error('vi_category') is-invalid @enderror"
                                   name="vi_category"
                                   value="{{ old('vi_category') }}"
                                   autocomplete="vi_category">
                            @error('vi_category')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="flex justify-center mt-3">
                            <button type="submit" class="btn btn-outline-primary">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    Subcategories:
                </h4>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="row">
                <div class="col-md-8">
                    <div class="admin-items-wrapper">
                        @foreach($subcategories as $subcategory)
                            <div class="admin-items my-2">
                                <div class="admin-item-div">
                                    {{$subcategory->$name}}
                                </div>
                                <div class="admin-item-btn">
                                    <a href="{{ route('admin-show-subcategory', $subcategory->slug) }}">
                                        <button class="btn btn-outline-primary">
                                            Show more..
                                        </button>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="text-uppercase text-center font-weight-bolder font-italic">
                        Add new subcategory
                    </label>
                    <form action="{{route('admin-subcategories-store')}}" method="post">
                        @csrf
                        @method('PUT')
                        <div>
                            <label for="name">
                                English name:
                            </label>
                            <input id="name"
                                   type="text"
                                   class="form-control @error('name') is-invalid @enderror"
                                   name="name"
                                   value="{{ old('name') }}"
                                   required>
                            @error('name')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mt-2">
                            <label for="vi_name">
                                Vietnamese name:
                            </label>
                            <input id="vi_name"
                                   type="text"
                                   class="form-control @error('vi_name') is-invalid @enderror"
                                   name="vi_name"
                                   value="{{ old('vi_name') }}"
                                   autocomplete="vi_category">
                            @error('vi_name')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div>

                            <label for="category">
                                For category:
                            </label>
                            <label class="container">Career opportunity
                                <input type="radio" checked name="radio" value="2" class="d-none">
                            </label>
                            <select class="select-css text-uppercase" name="category" id="category">
                                <option value="2" selected>
                                    Career opportunity
                                </option>
                            </select>
                        </div>
                        <div class="flex justify-center mt-3">
                            <button type="submit" class="btn btn-outline-primary">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    Qualifications:
                </h4>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="row">
                <div class="col-md-8">
                    <div class="admin-items-wrapper">
                        @foreach($specialities as $speciality)
                            <div class="admin-items my-2">
                                <div class="admin-item-div">
                                    {{$speciality->$name}}
                                </div>
                                <div class="admin-item-btn">
                                    <a href="{{ route('admin-show-speciality', $speciality->slug) }}">
                                        <button class="btn btn-outline-primary">
                                            Show more..
                                        </button>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="text-uppercase text-center font-weight-bolder font-italic">
                        Add new qualification
                    </label>
                    <form action="{{route('admin-specialities-store')}}" method="post">
                        @csrf
                        @method('PUT')
                        <div>
                            <label for="spec">
                                English name:
                            </label>
                            <input id="spec"
                                   type="text"
                                   class="form-control @error('spec') is-invalid @enderror"
                                   name="spec"
                                   value="{{ old('spec') }}"
                                   required>
                            @error('spec')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mt-2">
                            <label for="vi_spec">
                                Vietnamese name:
                            </label>
                            <input id="vi_spec"
                                   type="text"
                                   class="form-control @error('vi_spec') is-invalid @enderror"
                                   name="vi_spec"
                                   value="{{ old('vi_spec') }}"
                                   autocomplete="vi_spec">
                            @error('vi_spec')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="flex justify-center mt-3">
                            <button type="submit" class="btn btn-outline-primary">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    Universities:
                </h4>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="row">
                <div class="col-md-8">
                    <div class="admin-items-wrapper">
                        @foreach($universities as $university)
                            <div class="admin-items my-2">
                                <div class="admin-item-div">
                                    {{$university->$name}}
                                </div>
                                <div class="admin-item-btn">
                                    <a href="{{ route('admin-show-university', $university->slug) }}">
                                        <button class="btn btn-outline-primary">
                                            Show more..
                                        </button>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="text-uppercase text-center font-weight-bolder font-italic">
                        Add new University:
                    </label>
                    <form action="{{route('admin-universities-store')}}" method="post">
                        @csrf
                        @method('PUT')
                        <div>
                            <label for="uni">
                                English name:
                            </label>
                            <input id="uni"
                                   type="text"
                                   class="form-control @error('uni') is-invalid @enderror"
                                   name="uni"
                                   value="{{ old('uni') }}"
                                   required>
                            @error('uni')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mt-2">
                            <label for="vi_uni">
                                Vietnamese name:
                            </label>
                            <input id="vi_uni"
                                   type="text"
                                   class="form-control @error('vi_uni') is-invalid @enderror"
                                   name="vi_uni"
                                   value="{{ old('vi_uni') }}"
                                   autocomplete="vi_uni">
                            @error('vi_uni')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="flex justify-center  mt-3">
                            <button type="submit" class="btn btn-outline-primary">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    Education (Degree types):
                </h4>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="row">
                <div class="col-md-8">
                    <div class="admin-items-wrapper">
                        @foreach($degrees as $degree)
                            <div class="admin-items my-2">
                                <div class="admin-item-div">
                                    {{$degree->$name}}
                                </div>
                                <div class="admin-item-btn">
                                    <a href="{{ route('admin-show-degree', $degree->slug) }}">
                                        <button class="btn btn-outline-primary">
                                            Show more..
                                        </button>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="text-uppercase text-center font-weight-bolder font-italic">
                        Add new Degree type:
                    </label>
                    <form action="{{route('admin-degrees-store')}}" method="post">
                        @csrf
                        @method('PUT')
                        <div>
                            <label for="deg">
                                English name:
                            </label>
                            <input id="deg"
                                   type="text"
                                   class="form-control @error('deg') is-invalid @enderror"
                                   name="deg"
                                   value="{{ old('deg') }}"
                                   required>
                            @error('deg')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mt-2">
                            <label for="vi_deg">
                                Vietnamese name:
                            </label>
                            <input id="vi_deg"
                                   type="text"
                                   class="form-control @error('vi_deg') is-invalid @enderror"
                                   name="vi_deg"
                                   value="{{ old('vi_deg') }}"
                                   autocomplete="vi_deg">
                            @error('vi_deg')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="flex justify-center mt-3">
                            <button type="submit" class="btn btn-outline-primary">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    Codes:
                </h4>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="row">
                <div class="col-md-8">
                    <div class="admin-items-wrapper">
                        @foreach($codes as $code)
                            <div class="admin-items my-2">
                                <div class="admin-item-div">
                                    {{$code->code}}
                                </div>
                                <div class="admin-item-btn">
                                    <a href="{{ route('admin-show-code', $code->id) }}">
                                        <button class="btn btn-outline-primary">
                                            Show more..
                                        </button>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <label class="text-uppercase text-center font-weight-bolder font-italic">
                        Add new code:
                    </label>
                    <form action="{{route('admin-codes-store')}}" method="post">
                        @csrf
                        @method('PUT')
                        <div>
                            <label for="code">
                                Code:
                            </label>
                            <input id="code"
                                   type="text"
                                   class="form-control @error('code') is-invalid @enderror"
                                   name="code"
                                   value="{{ old('code') }}"
                                   required>
                            @error('code')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="flex justify-center mt-3">
                            <button type="submit" class="btn btn-outline-primary">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
@endcan

<!--<canvas id="myChart" width="400" height="400"></canvas>-->
<!--<script>-->
<!--    var ctx = document.getElementById('myChart').getContext('2d');-->
<!--    var myChart = new Chart(ctx, {-->
<!--        type: 'bar',-->
<!--        data: {-->
<!--            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],-->
<!--            datasets: [{-->
<!--                label: '# of Votes',-->
<!--                data: [12, 19, 3, 5, 2, 3],-->
<!--                backgroundColor: [-->
<!--                    'rgba(255, 99, 132, 0.2)',-->
<!--                    'rgba(54, 162, 235, 0.2)',-->
<!--                    'rgba(255, 206, 86, 0.2)',-->
<!--                    'rgba(75, 192, 192, 0.2)',-->
<!--                    'rgba(153, 102, 255, 0.2)',-->
<!--                    'rgba(255, 159, 64, 0.2)'-->
<!--                ],-->
<!--                borderColor: [-->
<!--                    'rgba(255, 99, 132, 1)',-->
<!--                    'rgba(54, 162, 235, 1)',-->
<!--                    'rgba(255, 206, 86, 1)',-->
<!--                    'rgba(75, 192, 192, 1)',-->
<!--                    'rgba(153, 102, 255, 1)',-->
<!--                    'rgba(255, 159, 64, 1)'-->
<!--                ],-->
<!--                borderWidth: 1-->
<!--            }]-->
<!--        },-->
<!--        options: {-->
<!--            scales: {-->
<!--                yAxes: [{-->
<!--                    ticks: {-->
<!--                        beginAtZero: true-->
<!--                    }-->
<!--                }]-->
<!--            }-->
<!--        }-->
<!--    });-->
<!--</script>-->
