@if(Auth::check())
    <div class="discussion">
        <div class="ml-5">
            <chat-app :user="{{auth()->user()}}"></chat-app>
        </div>
    </div>
@endif
