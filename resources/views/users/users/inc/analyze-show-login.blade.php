@can('admin')
    <div class="row my-5 w-100">
        <div class="col-md-3 ml-1">
            <div class="text-center mb-2">
                <h4>
                    <strong>
                        {{$analyze->title}}
                    </strong>
                </h4>
            </div>
            <div class="text-center mb-2">
                <h6>
                    <span>
                        From:
                    </span>
                    <strong>
                        {{$analyze->start}}
                    </strong>
                    <br>

                    <span>
                        To:
                    </span>
                    <strong>
                        {{$analyze->end}}
                    </strong>
                    <br>
                </h6>
            </div>
            <div class="my-2">
                <a href="{{route('analyze')}}">
                    <button class="btn btn-outline-dark">
                        Back
                    </button>
                </a>
            </div>
        </div>
        <div class="col-md-8 m-auto">
            <div class="row w-100">
                <div class="col-md-6">
                    <span> Users activity for period:</span> @if($users > 0) <strong>{{$users}}</strong> @else
                        <strong>0</strong> @endif
                </div>
                <div class="col-md-6 text-center">
                    <form action="{{route('analyze-delete-login', $analyze->id)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button onclick="return confirm('Are you sure?')" class="btn btn-outline-danger">
                            Delete
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
@endcan
