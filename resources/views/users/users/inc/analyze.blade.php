@section('custom-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"
        integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw=="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.js"
        integrity="sha512-zO8oeHCxetPn1Hd9PdDleg5Tw1bAaP0YmNvPY8CwcRyUk7d7/+nyElmFrB6f7vg4f7Fv4sui1mcep8RIEShczg=="
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.min.js"
        integrity="sha512-SuxO9djzjML6b9w9/I07IWnLnQhgyYVSpHZx0JV97kGBfTIsUYlWflyuW4ypnvhBrslz1yJ3R+S14fdCWmSmSA=="
        crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.css"
      integrity="sha512-C7hOmCgGzihKXzyPU/z4nv97W0d9bv4ALuuEbSf6hm93myico9qa0hv4dODThvCsqQUmKmLcJmlpRmCaApr83g=="
      crossorigin="anonymous"/>

<script>
    var myPosts = document.getElementById('myPosts').getContext('2d');
    var myPostsChart = new Chart(myPosts, {
        type: 'bar',
        data: {
            labels: ['Total Views', 'Total Reacts', 'Total Shares'],
            datasets: [{
                label: 'Amount',
                data: [{!! $totalViews !!}, {!! $totalReacts !!}, {!! $totalReacts !!}],
                backgroundColor: [
                    'rgba(39, 132, 252, 0.2)',
                    'rgba(39, 132, 252, 0.2)',
                    'rgba(39, 132, 252, 0.2)'
                ],
                borderColor: [
                    'rgba(39, 132, 252, 1)',
                    'rgba(39, 132, 252, 1)',
                    'rgba(39, 132, 252, 1)',
                ],
                borderWidth: 1
                }
            ]
        }
    });


    // var myTaggedPosts = document.getElementById('myTaggedPosts').getContext('2d');
    // var myTaggedPostsChart = new Chart(myTaggedPosts, {
    //     type: 'bar',
    //     data: {
    //         labels: ['Total Views', 'Total Reacts', 'Total Shares'],
    //         datasets: [{
    //             label: 'Amount',
    //             data: [{!! $totalViews !!}, {!! $totalReacts !!}, {!! $totalReacts !!}],
    //             backgroundColor: [
    //                 'rgba(39, 132, 252, 0.2)',
    //                 'rgba(39, 132, 252, 0.2)',
    //                 'rgba(39, 132, 252, 0.2)'
    //             ],
    //             borderColor: [
    //                 'rgba(39, 132, 252, 1)',
    //                 'rgba(39, 132, 252, 1)',
    //                 'rgba(39, 132, 252, 1)',
    //         ],
    //         borderWidth: 1
    //         }]
    //     }
    // });


    var usersStats = document.getElementById('userStats').getContext('2d');
    var userStatsChart = new Chart(usersStats, {
        type: 'bar',
        data: {
            labels: ['Total registered users', 'In past 24 hours', 'In last week'],
            datasets: [{
                label: 'Amount',
                data: [{!! $users->count() !!}, {!! $past24 !!}, {!! $pastWeek !!}],
    backgroundColor: [
        'rgba(39, 132, 252, 0.2)',
        'rgba(39, 132, 252, 0.2)',
        'rgba(39, 132, 252, 0.2)'
    ],
        borderColor: [
        'rgba(39, 132, 252, 1)',
        'rgba(39, 132, 252, 1)',
        'rgba(39, 132, 252, 1)',
    ],
        borderWidth: 1
    }]
    }
    });


    var usersActivityStats = document.getElementById('userActivityStats').getContext('2d');
    var userActivityStatsChart = new Chart(usersActivityStats, {
        type: 'bar',
        data: {
            labels: ['Active users last 24 hours', 'Active users last week', 'Active users last month'],
            datasets: [{
                label: 'Amount',
                data: [{!! $last24Logins !!}, {!! $lastWeekLogins !!}, {!! $pastMonth !!}],
                backgroundColor: [
                    'rgba(39, 132, 252, 0.2)',
                    'rgba(39, 132, 252, 0.2)',
                    'rgba(39, 132, 252, 0.2)'
                ],
                borderColor: [
                    'rgba(39, 132, 252, 1)',
                    'rgba(39, 132, 252, 1)',
                    'rgba(39, 132, 252, 1)',
                ],
                borderWidth: 1
                }]
            }
        });
</script>
@endsection

@can('admin-enterprise')
@can('admin')
<div class="discussion">
    <div>
        <div>
            <div class="text-center mb-2">
                <h4>
                    <strong>
                        My posts:
                    </strong>
                </h4>
            </div>
            <div class="my-3 text-center">
                <span>
                    Total posts:
                </span>
                <strong>
                    @if($posts !== NULL)
                    @if($posts->count() > 0) {{$posts->count()}} @else 0 @endif
                    @else
                    0
                    @endif
                </strong>
                <br>

                <span>
                    Total attached to analyzer:
                </span>
                <strong>
                    @if($posts !== NULL)
                    @if($posts->count() > 0) {{$posts->where('is_attached', 1)->count()}} @else 0 @endif
                    @else
                    0
                    @endif
                </strong>

                <br>

            </div>

            <div class="canvas-container" style="width: 80%; margin: 0 auto;">
                <canvas id="myPosts"></canvas>
            </div>

            <br>
            <br>

            <div class="mb-3 text-center">
                <a href="{{route('analyze-show-dettached')}}">
                    <button class="btn btn-outline-info">
                        View all detached posts
                    </button>
                </a>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            @if($posts !== NULL)
            @if($posts->count() > 0)
            <div class="row text-center analyze-posts-wrapper">
                <div class="col-sm-3">
                    <strong>
                        Title
                    </strong>
                </div>
                <div class="col-sm-2">
                    <strong>
                        Views
                    </strong>
                </div>
                <div class="col-sm-2">
                    <strong>
                        Reacts
                    </strong>
                </div>
                <div class="col-sm-1">
                    <strong>
                        Shares
                    </strong>
                </div>
                <div class="col-sm-2">
                    <strong>
                        Details
                    </strong>
                </div>
                <div class="col-sm-2">
                    <strong>
                        Detach
                    </strong>
                </div>
                @foreach($posts->where('is_attached', 1) as $post)
                <div class="col-sm-3 my-1">
                    <a href="{{route('posts-show', $post->slug)}}">
                        {{$post->title}}
                    </a>
                </div>
                <div class="col-sm-2 my-1">
                    @if($post->total === NULL)
                    0
                    @else
                    {{$post->total->count}}
                    @endif
                </div>
                <div class="col-sm-2 my-1">
                    @if($post->postReactions->count() > 0)
                    {{$post->postReactions->count()}}
                    @else
                    0
                    @endif
                </div>
                <div class="col-sm-1 my-1">
                    @if($post->shares->count() > 0)
                    {{$post->shares->count()}}
                    @else
                    0
                    @endif
                </div>
                <div class="col-sm-2 my-1">
                    @if($post->is_deleted === 1)
                    &times;
                    @else
                    <a href="{{route('analyze-details', $post->slug)}}">
                        <button class="btn btn-outline-primary">
                            Details
                        </button>
                    </a>
                    @endif
                </div>
                <div class="col-sm-2 my-1">
                    @if($post->is_deleted === 1)
                    &times;
                    @else
                    <a href="{{route('analyze-dettach', $post->slug)}}">
                        <button class="btn btn-outline-warning" onclick="return confirm('Are you sure?')">
                            Detach
                        </button>
                    </a>
                    @endif
                </div>
                @endforeach
            </div>
            @else
            <div class="text-center w-100">
                <strong>
                    You has not posts yet!
                </strong>
            </div>
            @endif
            @else
            <div class="text-center w-100">
                <strong>
                    You has not posts yet!
                </strong>
            </div>
            @endif
        </div>
    </div>

    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    @endcan


    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    <strong>
                        Posts where you are tagged:
                    </strong>
                </h4>
            </div>
        </div>

        <div class="col-md-9 m-auto">
            <div class="row text-center">
                @if($enterprise->count() > 0)
                <div class="col-sm-3">
                    <strong>
                        Title
                    </strong>
                </div>
                <div class="col-sm-2">
                    <strong>
                        Views
                    </strong>
                </div>
                <div class="col-sm-2">
                    <strong>
                        Reacts
                    </strong>
                </div>
                <div class="col-sm-2">
                    <strong>
                        Shares
                    </strong>
                </div>
                <div class="col-sm-3">
                    <strong>
                        Details
                    </strong>
                </div>
            </div>
            <div class="row text-center analyze-posts-wrapper">
                @foreach($enterprise as $post)
                <div class="col-sm-3 my-1">
                    <a href="{{route('posts-show', $post->slug)}}">
                        {{$post->title}}
                    </a>
                </div>
                <div class="col-sm-2 my-1">
                    @if($post->total === NULL)
                    0
                    @else
                    {{$post->total->count}}
                    @endif
                </div>
                <div class="col-sm-2 my-1">
                    @if($post->postReactions->count() > 0)
                    {{$post->postReactions->count()}}
                    @else
                    0
                    @endif
                </div>
                <div class="col-sm-2 my-1">
                    @if($post->shares->count() > 0)
                    {{$post->shares->count()}}
                    @else
                    0
                    @endif
                </div>
                <div class="col-sm-3 my-1">
                    @if($post->is_deleted === 1)
                    &times;
                    @else
                    <a href="{{route('analyze-details', $post->slug)}}">
                        <button class="btn btn-outline-primary">
                            Details
                        </button>
                    </a>
                    @endif
                </div>
                @endforeach
                @else
                <div class="text-center w-100">
                    <strong>
                        You are not tagged anywhere yet!
                    </strong>
                </div>
                @endif
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    @endcan
    @can('admin')
        <div style="text-align: center">
                <h4 style="margin: 0 auto;">
                    <strong>
                        Users:
                    </strong>
                </h4>
        </div>
        <br>

        <div class="canvas-container" style="width: 80%; margin: 0 auto;">
            <canvas id="userStats"></canvas>
        </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    <div>

            <div style="text-align: center">
                <h4 style="margin: 0 auto;">
                    <strong>
                        Users:
                    </strong>
                </h4>
            </div>
            <br>

            <div class="canvas-container" style="width: 80%; margin: 0 auto;">
                <canvas id="userActivityStats"></canvas>
            </div>


            <div class="my-3 w-100">
                <h5>
                    <strong>Existing Activity Schemas</strong>
                </h5>
            </div>
            <div class="row w-100">
                <div class="col-md-2">
                    <strong>
                        Title:
                    </strong>
                </div>
                <div class="col-md-3">
                    <strong>
                        Start:
                    </strong>
                </div>
                <div class="col-md-3">
                    <strong>
                        End:
                    </strong>
                </div>
                <div class="col-md-2">
                    <strong>
                        Create:
                    </strong>
                </div>
                <div class="col-md-2">
                    <strong>
                        Details
                    </strong>
                </div>
                @foreach($analyzes as $analyze)
                <div class="col-md-2 mt-2">
                    {{$analyze->title}}
                </div>
                <div class="col-md-3 mt-2">
                    {{$analyze->start}}
                </div>
                <div class="col-md-3 mt-2">
                    {{$analyze->end}}
                </div>
                <div class="col-md-2 mt-2">
                    {{$analyze->created_at}}
                </div>
                <div class="col-md-2 mt-2">
                    <a href="{{route('analyze-show-login', $analyze->id)}}">
                        <button class="btn btn-outline-primary">
                            View
                        </button>
                    </a>
                </div>
                @endforeach
            </div>

            <div class="w-100">
                <div class="my-5 w-100">
                    <h5>
                        <strong>Create new Schema</strong>
                    </h5>
                </div>
                <div class="text-center w-100">
                    <form action="{{route('analyze-create-login')}}" method="post" class="w-100">
                        @csrf
                        <div class="my-5">
                            <label for="name">
                                Schema name:
                            </label>
                            <br>
                            <input id="name"
                                   class="create-posts-title-input @error('name') is-invalid @enderror"
                                   name="name"
                                   value="{{ old('name') }}"
                                   required autocomplete="title">
                            @error('name')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="d-flex justify-content-between w-100">
                            <div class="w-50">
                                <label for="start_date">
                                    Start date
                                </label>
                                <br>
                                <input name="start_date" type="datetime-local" id="start_date" autocomplete>
                            </div>
                            <div class="w-50">
                                <label for="start_date">
                                    End date
                                </label>
                                <br>
                                <input name="end_date" type="datetime-local" id="start_date" autocomplete>
                            </div>
                        </div>
                        <div class="mx-auto text-center mt-5">
                            <button class="btn btn-outline-primary">
                                Create
                            </button>
                        </div>
                    </form>
                </div>
            </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
</div>
@endcan



