@can('admin')
    <div class="row my-5 w-100">
        <div class="col-md-4 ml-1 my-5">
            <div class="text-center mb-5">
                <h3>
                    {{$category->en_category}} // {{$category->vi_category}}
                </h3>
            </div>
            <div class="mb-5">
                <span>
                    Update or Delete this category! Be careful - you cannot bring it back
                </span>
            </div>
            <div class="text-center mt-5">
                <a href="{{route('admin-index', 'admin')}}">
                    <button class="btn btn-outline-dark">
                        Back
                    </button>
                </a>
            </div>
        </div>
        <div class="col-md-7 my-5 mx-auto">
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <span>Status:</span>
                        <strong>
                            @if($category->is_visible === true) Visible @else Not visible @endif
                        </strong>
                    </div>
                    <div>
                        <span>Change status:</span>
                        <a href="{{ route('admin-visible-category', $category->slug) }}">
                            <button class="btn btn-outline-warning">
                                Change to @if($category->is_visible === 1) Not visible @else Visible @endif
                            </button>
                        </a>
                    </div>
                    <div class="mt-5">
                        <span>Delete category:</span>
                            <form action="{{ route('admin-categories-delete', $category->slug) }}" method="post">
{{--                                @csrf--}}
                                @method('DELETE')
                                <button class="btn btn-outline-danger" onclick="return confirm('Are you sure?')">
                                    Delete
                                </button>
                            </form>
                    </div>
                </div>
                <div class="col-md-8">
                    <label class="text-uppercase text-center font-weight-bolder font-italic">
                        Change:
                    </label>
                    <form action="{{route('admin-categories-update', $category->slug)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div>
                            <label for="category">
                                English name:
                            </label>
                            <input id="category"
                                   type="text"
                                   class="form-control @error('category') is-invalid @enderror"
                                   name="category"
                                   value="@if(old('category')){{ old('category') }}@else{{$category->category}}@endif"
                                   required autocomplete="category">
                            @error('category')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mt-2">
                            <label for="vi_category">
                                Vietnamese name:
                            </label>
                            <input id="vi_category"
                                   type="text"
                                   class="form-control @error('vi_category') is-invalid @enderror"
                                   name="vi_category"
                                   value="@if(old('vi_category')){{ old('vi_category') }}@else{{$category->vi_category}}@endif"
                                   autocomplete="vi_category">
                            @error('vi_category')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="flex justify-center mt-3">
                            <button type="submit" class="btn btn-outline-primary">
                                Add
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
@endcan
