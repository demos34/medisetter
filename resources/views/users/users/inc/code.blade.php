@can('admin')
    <div class="row my-5 w-100">
        <div class="col-md-4 ml-1 my-5">
            <div class="text-center mb-5">
                <h3>
                    {{$code->code}}
                </h3>
            </div>
            <div class="mb-5">
                <span>
                    Update or Delete this Code! Be careful - you cannot bring it back
                </span>
            </div>
            <div class="text-center mt-5">
                <a href="{{route('admin-index', 'admin')}}">
                    <button class="btn btn-outline-dark">
                        Back
                    </button>
                </a>
            </div>
        </div>
        <div class="col-md-7 my-5 mx-auto">
            <div class="row">
                <div class="col-md-4">
                    <div class="mt-5">
                        <span>Delete code:</span>
                            <form action="{{ route('admin-codes-delete', $code->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-outline-danger" onclick="return confirm('Are you sure?')">
                                    Delete
                                </button>
                            </form>
                    </div>
                </div>
                <div class="col-md-8">
                    <label class="text-uppercase text-center font-weight-bolder font-italic">
                        Change:
                    </label>
                    <form action="{{route('admin-codes-update', $code->id)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div>
                            <label for="code">
                                Code:
                            </label>
                            <input id="code"
                                   type="text"
                                   class="form-control @error('code') is-invalid @enderror"
                                   name="code"
                                   value="@if(old('code')){{ old('code') }}@else{{$code->code}}@endif"
                                   required autocomplete="code">
                            @error('code')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="flex justify-center mt-3">
                            <button type="submit" class="btn btn-outline-primary">
                                Change
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
@endcan
