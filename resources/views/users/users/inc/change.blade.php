@can('admin')
    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    Users
                </h4>
            </div>
            <div>
                <p class="text-justify">
{{--                    If you want to change roles and permissions of users, click <a href="{{route('admin-change-roles')}}">here</a>--}}
                </p>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="row my-5">
                <div class="col-sm-4">Name</div>
                <div class="col-sm-4">Email</div>
                <div class="col-sm-4">Role</div>
                @foreach($users as $user)
                    <div class="col-sm-4 my-1">
                       {{$user->name}}
                    </div>
                    <div class="col-sm-4 my-1">
                        {{$user->email}}
                    </div>
                    <div class="col-sm-4 my-1">
                        {{implode(',', $user->roles()->get()->pluck('role')->toArray())}}
                        ->
                        <a href="{{ route('admin-change-roles', $user->id) }}">
                            Change
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
@endcan
