@can('admin')
    <div class="row my-5 w-100">
        <div class="col-md-4 ml-1 my-5">
            <div class="text-center mb-5">
                <h3>
                    {{$degree->en_name}} // {{$degree->vi_name}}
                </h3>
            </div>
            <div class="mb-5">
                <span>
                    Update or Delete this Degree type! Be careful - you cannot bring it back
                </span>
            </div>
            <div class="text-center mt-5">
                <a href="{{route('admin-index', 'admin')}}">
                    <button class="btn btn-outline-dark">
                        Back
                    </button>
                </a>
            </div>
        </div>
        <div class="col-md-7 my-5 mx-auto">
            <div class="row">
                <div class="col-md-4">
                    <div class="mt-5">
                        <span>Delete speciality:</span>
                            <form action="{{ route('admin-degrees-delete', $degree->slug) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-outline-danger" onclick="return confirm('Are you sure?')">
                                    Delete
                                </button>
                            </form>
                    </div>
                </div>
                <div class="col-md-8">
                    <label class="text-uppercase text-center font-weight-bolder font-italic">
                        Change:
                    </label>
                    <form action="{{route('admin-degrees-update', $degree->slug)}}" method="post">
                        @csrf
                        @method('PUT')
                        <div>
                            <label for="deg">
                                English name:
                            </label>
                            <input id="deg"
                                   type="text"
                                   class="form-control @error('deg') is-invalid @enderror"
                                   name="deg"
                                   value="@if(old('deg')){{ old('deg') }}@else{{$degree->en_name}}@endif"
                                   required autocomplete="deg">
                            @error('deg')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="mt-2">
                            <label for="vi_deg">
                                Vietnamese name:
                            </label>
                            <input id="vi_deg"
                                   type="text"
                                   class="form-control @error('vi_deg') is-invalid @enderror"
                                   name="vi_deg"
                                   value="@if(old('vi_deg')){{ old('vi_deg') }}@else{{$degree->vi_name}}@endif"
                                   autocomplete="vi_deg">
                            @error('vi_deg')
                            <br>
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="flex justify-center mt-3">
                            <button type="submit" class="btn btn-outline-primary">
                                Change
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
@endcan
