@can('admin')
    <div class="row my-5 w-100">
        <div class="col-md-12 m-auto text-center">
            <form action="{{route('users-search')}}" method="post">
                @csrf
                <div>
                    <label for="username">
                    </label>
                    <input id="username"
                           style="display: inline; width: 23%;"
                           type="text"
                           class="input-users @error('username') is-invalid @enderror"
                           name="username"
                           placeholder="Type name/names"
                           value="{{ old('username') }}"
                           required>
                    <button type="submit" class="btn-users">
                        Search
                    </button>
                    @error('username')
                    <br>
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </form>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    @if($users->where('is_verified', 0)->count() > 0)
        <div class="row my-5 w-100">
            <div class="col-md-2 ml-1">
                <div class="text-center mb-5">
                    <h4>
                        <strong>
                            Unverified users
                        </strong>
                    </h4>
                </div>
                <div class="text-center my-2">
                    <span>Total:</span>
                    <strong>{{$users->where('is_verified', 0)->count()}}</strong>
                </div>
                <div class="text-center mx-auto my-2">
                    <a href="{{route('verify-all-users')}}">
                        <button class="btn btn-outline-success">
                            Verify all
                        </button>
                    </a>
                </div>
            </div>
            <div class="col-md-9 m-auto">
                <div class="row">
                    <div class="col-sm-4">
                        <strong>
                            Name
                        </strong>
                    </div>
                    <div class="col-sm-4">
                        <strong>
                            Email
                        </strong>
                    </div>
                    <div class="col-sm-4">
                        <strong>
                            Role
                        </strong>
                    </div>
                </div>
                <div class="row admin-enterprise-users-wrapper">
                    @foreach($users->where('is_verified', 0) as $unUser)
                        <div class="col-sm-4 my-1">
                            {{$unUser->name}}
                        </div>
                        <div class="col-sm-4 my-1">
                            {{$unUser->email}}
                        </div>
                        <div class="col-sm-4 my-1">
                            {{implode(',', $unUser->roles()->get()->pluck('role')->toArray())}}
                            ->
                            <a href="{{ route('admin-view-one-user', $unUser->id) }}">
                                Details
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="py-8">
            <div class="border-t border-gray-200"></div>
        </div>
    @endif
    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    <strong>
                        Administrators
                    </strong>
                </h4>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="row">
                <div class="col-sm-4">
                    <strong>
                        Name
                    </strong>
                </div>
                <div class="col-sm-4">
                    <strong>
                        Email
                    </strong>
                </div>
                <div class="col-sm-4">
                    <strong>
                        Role
                    </strong>
                </div>
            </div>
            <div class="row admin-enterprise-users-wrapper">
                @foreach($admins as $admin)
                    <div class="col-sm-4 my-1">
                       {{$admin->name}}
                    </div>
                    <div class="col-sm-4 my-1">
                        {{$admin->email}}
                    </div>
                    <div class="col-sm-4 my-1">
                        {{implode(',', $admin->roles()->get()->pluck('role')->toArray())}}
                        ->
                        <a href="{{ route('admin-view-one-user', $admin->id) }}">
                            Details
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    <strong>
                        Enterprise users
                    </strong>
                </h4>
            </div>
            <div class="text-center my-2">
                <a href="{{route('admin-create-enterprise')}}">
                    <button class="btn btn-outline-info">Create new Enterprise user</button>
                </a>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="row">
                <div class="col-sm-4">
                    <strong>
                        Name
                    </strong>
                </div>
                <div class="col-sm-4">
                    <strong>
                        Email
                    </strong>
                </div>
                <div class="col-sm-4">
                    <strong>
                        Role
                    </strong>
                </div>
            </div>
            <div class="row admin-enterprise-users-wrapper">
                @foreach($enterprises as $value)
                    <div class="col-sm-4 my-1">
                       {{$value->name}}
                    </div>
                    <div class="col-sm-4 my-1">
                        {{$value->email}}
                    </div>
                    <div class="col-sm-4 my-1">
                        {{implode(',', $value->roles()->get()->pluck('role')->toArray())}}
                        ->
                        <a href="{{ route('admin-view-one-user', $value->id) }}">
                            Details
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    <strong>
                        Users
                    </strong>
                </h4>
            </div>
            <div>
                <p class="text-justify">
{{--                    If you want to change roles and permissions of users, click <a href="{{route('admin-change-roles')}}">here</a>--}}
                </p>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="row">
                <div class="col-sm-4">
                    <strong>
                        Name
                    </strong>
                </div>
                <div class="col-sm-4">
                    <strong>
                        Email
                    </strong>
                </div>
                <div class="col-sm-4">
                    <strong>
                        Role
                    </strong>
                </div>
            </div>
            <div class="row generic-users-wrapper">
                @foreach($users as $user)
                    <div class="col-sm-4 my-1">
                       {{$user->name}}
                    </div>
                    <div class="col-sm-4 my-1">
                        {{$user->email}}
                    </div>
                    <div class="col-sm-4 my-1">
                        {{implode(',', $user->roles()->get()->pluck('role')->toArray())}}
                        ->
                        <a href="{{ route('admin-view-one-user', $user->id) }}">
                            Details
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
@endcan
