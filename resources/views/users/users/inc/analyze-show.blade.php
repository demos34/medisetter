@can('admin-enterprise')
    <div class="row my-5 w-100">
        <div class="col-md-3 ml-1">
            <div class="text-center mb-2">
                <h4>
                    <strong>
                        {{$analyze->title}}
                    </strong>
                    <br>
                </h4>
                <h5>
                    <span>for post: </span>
                    <strong>
                        <a href="{{route('posts-show', $analyze->post->slug)}}">
                            {{$analyze->post->title}}
                        </a>
                    </strong>
                </h5>
            </div>
            <div class="text-center mb-2">
                <h6>
                    <span>
                        From:
                    </span>
                    <strong>
                        {{$analyze->start}}
                    </strong>
                    <br>

                    <span>
                        To:
                    </span>
                    <strong>
                        {{$analyze->end}}
                    </strong>
                    <br>
                </h6>
            </div>
            <div class="my-2 d-flex justify-content-between">
                <a href="{{route('analyze-details', $analyze->post->slug)}}">
                    <button class="btn btn-outline-dark">
                        Back
                    </button>
                </a>
                <form action="{{route('analyze-delete', $analyze->id)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button onclick="return confirm('Are you sure?')" class="btn btn-outline-danger">
                        Delete
                    </button>
                </form>
            </div>
        </div>
        <div class="col-md-8 m-auto">
            <div class="row w-100">
                <div class="col-md-4">
                    <span> views:</span> @if($views->count() > 0) <strong>{{$views->count()}}</strong> <span>/ from guests:</span> <strong>{{$views->where('user_id', NULL)->count()}}</strong> @else
                        <strong>0</strong> @endif
                </div>
                <div class="col-md-4">
                    <span>shares</span> @if($shares > 0) <strong>{{$shares}}</strong> @else <strong>0</strong> @endif
                </div>
                <div class="col-md-4">
                    <span>reactions</span> @if($reacts->count() > 0) <strong>{{$reacts->count()}}</strong> @else
                        <strong>0</strong> @endif
                </div>
            </div>
            <div class="row w-100 mt-5">
                <div class="col-md-4">
                    <span>likes:</span> @if($reacts->count() > 0)
                        <strong>{{$reacts->where('id', 1)->count()}}</strong> @else
                        <strong>0</strong> @endif
                </div>
                <div class="col-md-4">
                    <span>loves: </span> @if($reacts->count() > 0)
                        <strong>{{$reacts->where('id', 2)->count()}}</strong> @else
                        <strong>0</strong> @endif
                </div>
                <div class="col-md-4">
                    <span>support: </span> @if($reacts->count() > 0)
                        <strong>{{$reacts->where('id', 3)->count()}}</strong> @else
                        <strong>0</strong> @endif
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    <div class="row my-5 w-100">
        <div class="col-md-3 ml-1">
            <div class="text-center mb-2">
                <h4>
                    <strong>
                        Users in Age Range
                    </strong>
                </h4>
            </div>
            <div class="text-center mb-2">
                <h6>
                    <span>
                        From:
                    </span>
                    <strong>
                        {{$analyze->from_age}} Y
                    </strong>
                    <br>

                    <span>
                        To:
                    </span>
                    <strong>
                        {{$analyze->to_age}} Y
                    </strong>
                    <br>
                </h6>
            </div>
            <div class="my-3 text-center">
                <span>Total users in range: </span>
                <strong>{{$ageUsers->count()}}</strong>
            </div>
        </div>
        <div class="col-md-8 m-auto">
            <div class="admin-items-wrapper row mt-5 text-center">
                <div class="col-lg-4 my-2">
                    <strong>
                        Name
                    </strong>
                </div>
                <div class="col-lg-2 my-2">
                    <strong>
                        Age
                    </strong>
                </div>
                <div class="col-lg-2 my-2">
                    <strong>
                        Location
                    </strong>
                </div>
                <div class="col-lg-4 my-2">
                    <strong>
                        Education
                    </strong>
                </div>
                @foreach($ageUsers as $ageUser)
                    <div class="col-lg-4 my-2">
                        {{$ageUser->name}}
                    </div>
                    <div class="col-lg-2 my-2">
                        {{$ageUser->profile->age}}
                    </div>
                    <div class="col-lg-2">
                       {{$ageUser->profile->location}}
                    </div>
                    <div class="col-lg-4">
                        {{implode(',', $ageUser->degrees()->get()->pluck($name)->toArray())}}
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    <div class="row my-5 w-100">
        <div class="col-md-3 ml-1">
            <div class="text-center mb-2">
                <h4>
                    <strong>
                       Users living/working in location
                    </strong>
                </h4>
            </div>
            <div class="text-center mb-2">
                <h6>
                    <span>
                        Location:
                    </span>
                    <strong>
                        {{$analyze->location}}
                    </strong>
                </h6>
            </div>
            <div class="my-3 text-center">
                <span>Total users in range: </span>
                <strong>{{$locationUsers->count()}}</strong>
            </div>
        </div>
        <div class="col-md-8 m-auto">
            <div class="admin-items-wrapper row mt-5 text-center">
                <div class="col-lg-4 my-2">
                    <strong>
                        Name
                    </strong>
                </div>
                <div class="col-lg-2 my-2">
                    <strong>
                        Age
                    </strong>
                </div>
                <div class="col-lg-2 my-2">
                    <strong>
                        Location
                    </strong>
                </div>
                <div class="col-lg-4 my-2">
                    <strong>
                        Education
                    </strong>
                </div>
                @foreach($locationUsers as $locationUser)
                    <div class="col-lg-4 my-2">
                        {{$locationUser->name}}
                    </div>
                    <div class="col-lg-2 my-2">
                        {{$locationUser->profile->age}}
                    </div>
                    <div class="col-lg-2">
                       {{$locationUser->profile->location}}
                    </div>
                    <div class="col-lg-4">
                        {{implode(',', $locationUser->degrees()->get()->pluck($name)->toArray())}}
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    <div class="row my-5 w-100">
        <div class="col-md-3 ml-1">
            <div class="text-center mb-2">
                <h4>
                    <strong>
                       Users living/working in location and in age range
                    </strong>
                </h4>
            </div>
            <div class="text-center mb-2">
                <h6>
                    <span>
                        Location:
                    </span>
                    <strong>
                        {{$analyze->location}}
                    </strong>
                    <br>
                    <span>
                        From age:
                    </span>
                    <strong>
                        {{$analyze->from_age}} Y
                    </strong>
                    <br>
                    <span>
                        To age:
                    </span>
                    <strong>
                        {{$analyze->to_age}} Y
                    </strong>
                </h6>
            </div>
            <div class="my-3 text-center">
                <span>Total users in range: </span>
                <strong>{{$ageLocationUsers->count()}}</strong>
            </div>
        </div>
        <div class="col-md-8 m-auto">
            <div class="admin-items-wrapper row mt-5 text-center">
                <div class="col-lg-4 my-2">
                    <strong>
                        Name
                    </strong>
                </div>
                <div class="col-lg-2 my-2">
                    <strong>
                        Age
                    </strong>
                </div>
                <div class="col-lg-2 my-2">
                    <strong>
                        Location
                    </strong>
                </div>
                <div class="col-lg-4 my-2">
                    <strong>
                        Education
                    </strong>
                </div>
                @foreach($ageLocationUsers as $ageLocationUser)
                    <div class="col-lg-4 my-2">
                        {{$ageLocationUser->name}}
                    </div>
                    <div class="col-lg-2 my-2">
                        {{$ageLocationUser->profile->age}}
                    </div>
                    <div class="col-lg-2">
                       {{$ageLocationUser->profile->location}}
                    </div>
                    <div class="col-lg-4">
                        {{implode(',', $ageLocationUser->degrees()->get()->pluck($name)->toArray())}}
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>

@endcan
