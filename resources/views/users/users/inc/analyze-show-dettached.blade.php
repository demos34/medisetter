@can('admin-enterprise')
    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-2">
                <h4>
                    <strong>
                        Dettached posts:
                    </strong>
                </h4>
            </div>
            <div class="my-3 text-center">
                <span>
                    Total posts:
                </span>
                <br>
                <strong>
                    @if($posts->where('is_attached', 0)->count() > 0) {{$posts->where('is_attached', 0)->count()}} @else 0 @endif
                </strong>
            </div>
            <div class="mb-3 text-center">
                <a href="{{route('analyze')}}">
                    <button class="btn btn-outline-dark">
                        Back
                    </button>
                </a>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            @if($posts->count() > 0)
                <div class="row text-center analyze-posts-wrapper">
                    <div class="col-sm-3">
                        <strong>
                            Title
                        </strong>
                    </div>
                    <div class="col-sm-2">
                        <strong>
                            Views
                        </strong>
                    </div>
                    <div class="col-sm-2">
                        <strong>
                            Reacts
                        </strong>
                    </div>
                    <div class="col-sm-1">
                        <strong>
                            Shares
                        </strong>
                    </div>
                    <div class="col-sm-2">
                        <strong>
                            Details
                        </strong>
                    </div>
                    <div class="col-sm-2">
                        <strong>
                            Attach
                        </strong>
                    </div>
                    @foreach($posts->where('is_attached', 0) as $post)
                        <div class="col-sm-3 my-1">
                            <a href="{{route('posts-show', $post->slug)}}">
                                {{$post->title}}
                            </a>
                        </div>
                        <div class="col-sm-2 my-1">
                            @if($post->total === NULL)
                                0
                            @else
                                {{$post->total->count}}
                            @endif
                        </div>
                        <div class="col-sm-2 my-1">
                            @if($post->postReactions->count() > 0)
                                {{$post->postReactions->count()}}
                            @else
                                0
                            @endif
                        </div>
                        <div class="col-sm-1 my-1">
                            @if($post->shares->count() > 0)
                                {{$post->shares->count()}}
                            @else
                                0
                            @endif
                        </div>
                        <div class="col-sm-2 my-1">
                            @if($post->is_deleted === 1)
                                &times;
                            @else
                                <a href="{{route('analyze-details', $post->slug)}}">
                                    <button class="btn btn-outline-primary">
                                        Details
                                    </button>
                                </a>
                            @endif
                        </div>
                        <div class="col-sm-2 my-1">
                            @if($post->is_deleted === 1)
                                &times;
                            @else
                                <a href="{{route('analyze-attach', $post->slug)}}">
                                    <button class="btn btn-outline-success">
                                        Attach
                                    </button>
                                </a>
                            @endif
                        </div>
                    @endforeach
                </div>
            @else
                <div class="text-center w-100">
                    <strong>
                        You has not posts yet!
                    </strong>
                </div>
            @endif
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
@endcan
