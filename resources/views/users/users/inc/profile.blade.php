<div class="discussion">
    <form action="{{route('users-update', $page)}}" method="post" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="user-name-pic">
            <img class="profile-pic side-pic" src="{{Auth::user()->profile->avatar}}" alt="profile">
            <label for="image" class="new-image">
            <span class="material-icons">
                insert_photo
            </span>
            </label>
            <h3>User name</h3>
        </div>

        <br>
        <br>
        @can('admin-generic')

            <label for="name">
                Name:
            </label>
            <input id="name"
                   class="@error('name') is-invalid @enderror user-profile-age-input"
                   name="name"
                   value=@if(Auth::user()->name) @if(old('name')) "{{ old('name') }}" @else
            "{{Auth::user()->name}}" @endif @else "{{ old('name') }}" @endif>
        @error('age')
        <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <br>
            <label for="email">
                Email:
            </label>
            <input id="email"
                   class="@error('email') is-invalid @enderror user-profile-age-input"
                   name="email"
                   type="email"
                   value=@if(Auth::user()->email) @if(old('email')) "{{ old('email') }}" @else
            "{{Auth::user()->email}}" @endif @else "{{ old('email') }}" @endif>
        @error('email')
        <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <br>

            <label for="age">
                Age:
            </label>
            <input id="age"
                   class="@error('age') is-invalid @enderror user-profile-age-input"
                   name="age"
                   value=@if(Auth::user()->profile->age) @if(old('age')) "{{ old('age') }}" @else
            "{{Auth::user()->profile->age}}" @endif @else "{{ old('age') }}" @endif>
        @error('age')
        <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <br>
        <label for="location">
            Location (live where):
        </label>

        <input id="location"
               class="@error('im_from') is-invalid @enderror user-profile-age-input"
               name="im_from"
               value=@if(Auth::user()->profile->location) @if(old('im_from')) "{{ old('im_from') }}" @else
            "{{Auth::user()->profile->location}}" @endif @else "{{ old('age') }}" @endif>
        @error('im_from')

        <br>

        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

        @endcan

        <input type="file" class="form-control-file" id="image" name="image" hidden>
        @if($errors->has('image'))
            <strong>{{ $errors->first('image') }}</strong>
        @endif

        <br>
        @if(Auth::user()->profile->is_student === 1)
            <strong class="font-italic">
                I'm still a Student
            </strong>
            <br>
        @endif
        @if(isset(Auth::user()->otherUniversity))
            {{Auth::user()->otherUniversity->name}}
        @else
            @if(Auth::user()->universities->count() > 0)
                @foreach(Auth::user()->universities as $university)
                    {{$university->$name}}
                @endforeach
            @endif
        @endif
        @can('admin-generic')

            <br>
            @foreach(Auth::user()->degrees as $degree)
                {{$degree->$name}}
            @endforeach

            <hr class="w-50">
            <div class="w-100">

                <input name="is_student" type="checkbox" value="1" id="is-student"
                       @if(Auth::user()->profile->is_student == true)
                       checked
                    @endif>
                <label for="is-student">
                    <strong class="font-italic">
                        I'm still a student!
                    </strong>
                </label>
                <div class="w-100" id="other-university">
                    <label for="other-uni-input">
                        Type your University:
                    </label>
                    <br>
                    <input id="other-uni-input"
                           class="@error('other_university') is-invalid @enderror"
                           name="other_university"
                           value="@if(Auth::user()->otherUniversity) @if(old('other_university')) {{ old('other_university') }} @else {{Auth::user()->otherUniversity->name}} @endif @else {{ old('other_university') }} @endif">
                    @error('other_university')
                    <br>
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div id="universities-div" class="w-50">
                    <label for="universities">
                        Universities
                    </label>
                    <select class="select-css text-uppercase" name="university" id="universities">
                        <option value="null">
                            ---- Please select ----
                        </option>
                        @foreach($universities as $university)
                            <option value="{{$university->id}}"
                                    @if(implode(
                            ',', Auth::user()->universities()->get()->pluck('id')->toArray()) == $university->id)
                                    selected
                                @endif
                            >
                                {{$university->$name}}
                            </option>
                        @endforeach
                    </select>
                </div>
                <div id="other-div">
                    <input name="other_uni" type="checkbox" value="1" id="other-uni"
                           @if(Auth::user()->otherUniversity) checked @endif>
                    <label for="other-uni">Other</label>
                </div>
                <div id="degrees-div" class="w-50">
                    <label for="degrees">
                        Degree:
                    </label>
                    <select class="select-css text-uppercase" name="degrees" id="degrees">
                        <option value="null">
                            ---- Please select ----
                        </option>
                        @foreach($degrees as $degree)
                            <option value="{{$degree->id}}"
                                    @if(implode(
                            ',', Auth::user()->degrees()->get()->pluck('id')->toArray()) == $degree->id)
                                    selected
                                @endif
                            >
                                {{$degree->$name}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <hr class="w-50">
            <div class="w-100">
                <div class="w-50">
                    <label for="primary">
                        Select for primary qualification
                    </label>
                    <select class="select-css text-uppercase" name="primary" id="primary">
                        <option value="null">
                            ---- Please select ----
                        </option>
                        @foreach($specialities as $speciality)
                            <option value="{{$speciality->id}}"
                                    @if(Auth::user()->primary)
                                    @if(Auth::user()->primary->speciality_id == $speciality->id)
                                    selected
                                @endif
                                @endif
                            >
                                {{$speciality->$name}}
                            </option>
                        @endforeach
                    </select>
                </div>

                <div class="w-50">
                    <label for="secondary">
                        Select for secondary qualification:
                    </label>
                    <select class="select-css text-uppercase" name="secondary" id="secondary">
                        <option value="null">
                            ---- Please select ----
                        </option>
                        @foreach($specialities as $speciality)
                            <option value="{{$speciality->id}}"
                                    @if(Auth::user()->secondary)
                                    @if(Auth::user()->secondary->speciality_id == $speciality->id)
                                    selected
                                @endif
                                @endif
                            >
                                {{$speciality->$name}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <hr class="w-50">
            <div class="w-50">
                <div class="w-100">
                    <label for="organisation">
                        Your organisation:
                    </label>
                    <br>
                    <input id="organisation"
                           class="@error('organisation') is-invalid @enderror custom-input-css"
                           name="organisation"
                           value=@if(Auth::user()->organisation) @if(old('organisation')) "{{ old('organisation') }}" @else
                        "{{Auth::user()->organisation->name}}" @endif @else "{{ old('organisation') }}" @endif>
                    @error('organisation')
                    <br>
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="w-100">
                    <label for="organisation">
                        Location:
                    </label>
                    <br>
                    <input id="location"
                           class="@error('location') is-invalid @enderror custom-input-css"
                           name="location"
                           value=@if(Auth::user()->organisation) @if(old('location')) "{{ old('location') }}" @else
                        "{{Auth::user()->organisation->location}}" @endif @else "{{ old('location') }}" @endif>
                    @error('location')
                    <br>
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <br>
                <button type="submit" class="show-button">
                    Save
                </button>

            </div>
        @endcan
    </form>
</div>
@section('custom-js')
    <script src="{!! asset('js/jquery.js') !!}"></script>
    <script src="{!! asset('js/profile.js') !!}"></script>
    <script src="{!! asset('js/multiselect.min.js') !!}"></script>
    <script src="{!! asset('js/multiselect.min.js') !!}"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $(".mul-select").select2({
                placeholder: "Select", //placeholder
                tags: true,
                tokenSeparator: ['/', ',', ',', ""]
            })
        })
    </script>
    <script src="{!! asset('js/multiselect-dis.js') !!}"></script>
@endsection
