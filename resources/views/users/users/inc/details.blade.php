@can('admin')
    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-2">
                <img class="rounded-circle" src="{{$user->avatar}}" alt="{{$user->name}}">
            </div>
            <div class="text-center mb-2">
                <h4>
                    {{$user->name}}
                </h4>
            </div>
            <div class="text-center mb-3">
                {{implode(',', $user->roles()->get()->pluck('role')->toArray())}}
            </div>
            <div class="text-center mb-3">
                Email: <strong>
                    {{$user->email}}
                </strong>
            </div>
            <div class="my-2">
                <a href="{{route('add-contacts', $user->id)}}">
                    <button class="btn btn-outline-primary">
                        Send {{$user->name}} MSG
                    </button>
                </a>
            </div>
            <div class="text-center my-3">
                <a href="{{route('admin-view-users')}}">
                    <button class="btn btn-outline-dark">Back</button>
                </a>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="user-verify">
                <span>Status:</span>
                @if($user->is_verified == 0)
                    <span class="unverified">
                        Unverified
                    </span>
                        ->
                    <a href="{{route('admin-user-verify', $user->id)}}">
                        <button class="btn btn-outline-warning">Verify!!!</button>
                    </a>
                @else
                    <span class="verified">
                        Verified!
                    </span>
                @endif
            </div>
            <div class="w-100 my-5">
                <h4 class="text-center">
                    Change role
                </h4>
                <div class="row w-70 mx-auto">
                    <div class="col-lg-10">
                        <form action="{{route('admin-change-roles', $user->id)}}" method="post">
                            @csrf
                            <div class="text-center">
                                @foreach($roles as $role)
                                    <input type="radio" id="{{$role->id}}" name="role" value="{{$role->id}}" @if((int)implode(',', $user->roles()->get()->pluck('id')->toArray()) == $role->id) checked @endif>
                                    <label for="{{$role->id}}">{{$role->role}}</label><br>
                                @endforeach
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary">
                                    Change
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>

    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="my-4">
                <span>Age: </span>
                <strong>{{$user->profile->age}}</strong>
            </div>
            <div class="my-4">
                <span>Lives in: </span>
                <strong>{{$user->profile->location}}</strong>
            </div>
            <div class="my-4">
                <span>Is student: </span>
                @if($user->profile->is_student == 1)
                <strong>Yes</strong>
                @else
                <strong>No</strong>
                @endif
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="w-100 mx-auto my-3">
                <span>University: </span>
                @if(isset($user->otherUniversity))
                    <strong>
                        {{$user->otherUniversity->name}}
                    </strong>
                @else
                    @if($user->universities->count() > 0)
                        @foreach($user->universities as $university)
                            <strong>
                                {{$university->en_name}};
                            </strong>
                        @endforeach
                    @else
                        <strong>Not set yet!</strong>
                    @endif
                @endif
            </div>
            <div class="w-100 mx-auto my-3">
                <span>Education: </span>
                @if($user->degrees->count() > 0)
                    @foreach($user->degrees as $degree)
                        <strong>
                            {{$degree->en_name}};
                        </strong>
                    @endforeach
                @else
                    <strong>Not set yet!</strong>
                @endif
            </div>
            <div class="w-100 mx-auto my-3">
                <span>Primary qualification: </span>
                @if($user->primary)
                    <strong>
                        {{$user->primary->speciality->en_name}};
                    </strong>
                @else
                    <strong>Not set yet!</strong>
                @endif
            </div>
            <div class="w-100 mx-auto my-3">
                <span>Secondary qualification: </span>
                @if($user->secondary)
                    <strong>
                        {{$user->secondary->speciality->en_name}};
                    </strong>
                @else
                    <strong>Not set yet!</strong>
                @endif
            </div>
            <div class="w-100 mx-auto my-3">
                <span>Organisation: </span>
                @if(isset($user->organisation))
                        <strong>
                            {{$user->organisation->name}},
                            {{$user->organisation->location}}.
                        </strong>
                @else
                    <strong class="font-italic">
                        Not set yet!
                    </strong>
                @endif
            </div>
        </div>
    </div>


    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>


    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="user-view-info-followed">
                Followed by:
                @if($user->followers())
                    {{$user->followers()->count()}}
                @else
                    0
                @endif
            </div>
            <div class="user-view-info-following">
                Following:
                @if(isset($user->follower))
                    {{$user->follower->users()->count()}}
                @else
                    0
                @endif
            </div>
            <div class="user-view-info-following">
                Posts:
                @if(isset($user->author->posts))
                    {{$user->author->posts->count()}}
                @else
                    0
                @endif
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="w-100 users-view-posts-wrapper">
                @if(isset($user->author->posts))
                    @foreach($user->author->posts->sortByDesc('created_at') as $post)
                        <div class="mx-auto my-4 discussion-post">
                            <div class="discussion-author-wrapper">
                                <a href="{{route('user-view', $post->author->user_id)}}" class="w-100">
                                    <img class="discussion-author-avatar" src="{{$post->author->user->profile->avatar}}"
                                         alt="avatar">
                                </a>
                                <p class="discussion-author-name">{{$post->author->user->name}}</p>
                                @if(implode(',', $post->author->user->roles()->get()->pluck('role')->toArray()) === 'Administrator')
                                    <p class="discussion-author-role">
                                        {{implode(',', $post->author->user->roles()->get()->pluck('role')->toArray())}}
                                    </p>
                                @endif
                            </div>
                            <div class="discussion-post-wrapper">
                                <div class="discussion-shared-section mx-auto text-center mt-2">
                                    @if($post->is_shared === 1)
                                        <strong>
                                            {{$post->author->user->name}}
                                        </strong>
                                        share post of
                                        <strong>{{$post->share->post->author->user->name}}:</strong>
                                        @if($post->share->title != NULL)
                                        {{$post->share->title}}
                                        @endif
                                        </strong>

                                    @endif
                                </div>
                                <div class="discussion-post-body">
                                    <div class="d-flex justify-content-between">
                                        <div class="w-70 p-2">
                                <span class="date-picker">
                                DATE: <strong>{{$post->created_at}}</strong>
                                </span>
                                        </div>

                                        <div class="discussion-icons-wrapper">
                                            <div class="comments">
                                                <span><i class="material-icons-outlined activity-icon">comment</i></span>
                                                <span>{{$post->comments->count()}}</span>
                                            </div>
                                            <div class="views">
                                            <span><i
                                                    class="material-icons-outlined activity-icon">remove_red_eye</i></span>
                                                <span>{{$post->views->count()}}</span>
                                            </div>
                                            <div class="likes">
                                            <span><i
                                                    class="material-icons-outlined activity-icon">thumb_up_alt</i></span>
                                                <span>{{$post->postReactions->where('reaction_id', 1)->count()}}</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="p-2">
                                        <strong>
                                            {{$post->title}}
                                        </strong>
                                        <br>
                                        <span>
                                {{Str::limit($post->body, 100)}}
                            </span>
                                    </div>
                                    <div class="discussion-view-section">
                                        <a href="{{route('posts-show', $post->slug)}}">
                                            view discussion
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="text-center">
                        <strong>
                            {{$user->name}}
                        </strong> has not post yet!
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
@endcan
