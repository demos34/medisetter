<x-app-layout>
    <div class="users-wrapper">
        <div class="row w-100">
            <div class="col-md-2">
                <div class="left-side-menu text-center">
                    <span class="text-uppercase text-center my-2">
                        <a href="{{route('users')}}" class="text-decoration-none">
                            Dashboard
                        </a>
                    </span>
                    <ul class="text-left m-3">
                        @can('admin')
                            <li class="my-3">
                                <a href="{{route('admin-index', 'admin')}}" class="custom-dash-links">
                                    Admin panel
                                </a>
                            </li>
                            <li class="my-3">
                                <a href="{{route('admin-view-users')}}" class="custom-dash-links">
                                    Users
                                </a>
                            </li>
                            <li class="my-3">
                                <a href="{{route('categories-posts-create', 'news-articles')}}" class="custom-dash-links">
                                    Write new post
                                </a>
                            </li>
                        @endcan
                        <li class="my-3">
                            <a href="{{route('users-page', 'user')}}" class="custom-dash-links">
                                User information
                            </a>
                        </li>
                        <li class="my-3">
                            <a href="{{route('users-page', 'profile')}}" class="custom-dash-links">
                                Profile information
                            </a>
                        </li>
                        <li class="my-3">
                            <a href="{{route('msg')}}" class="custom-dash-links notification">
                                Messages
                                <chat-counter :user="{{auth()->user()}}" :to="{{auth()->user()->to}}"></chat-counter>
                            </a>
                        </li>
                        @can('admin-enterprise')
                            <li class="my-3">
                                <a href="{{route('analyze', 'analyze')}}" class="custom-dash-links">
                                    Analyze
                                </a>
                            </li>
                        @endcan
                    </ul>
                </div>
            </div>
            <div class="col-md-9">
                @include('users.users.inc.' . $page)
            </div>
        </div>
    </div>
</x-app-layout>
