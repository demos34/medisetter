<x-app-layout>
    <div class="main-category-body">
            <div class="ml-5">
                <chat-app :user="{{auth()->user()}}"></chat-app>
            </div>
    </div>
</x-app-layout>
