<x-app-layout>
    <div class="discussion">
        @can('admin')
            <div class="m-5 text-center">
                <strong>
                    <h2>All events (excluded my events) - only for Administrators</h2>
                </strong>
            </div>
            <div class="row my-5 w-100">
                <div class="col-md-2 ml-1">
                    <div class="text-center mb-5">
                        <h4>
                            Upcoming Events
                        </h4>
                    </div>
                </div>
                <div class="col-md-9 m-auto">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row my-events-scroll">
                                <div class="col-md-3 my-3">
                                    <strong>
                                        Post title
                                    </strong>
                                </div>
                                <div class="col-md-2 my-3 text-left">
                                    <strong>
                                        Author
                                    </strong>
                                </div>
                                <div class="col-md-3 my-3">
                                    <strong>
                                        Event start date
                                    </strong>
                                </div>
                                <div class="col-md-2 my-3 text-center">
                                    <strong>Interested</strong>
                                </div>
                                <div class="col-md-2 my-3 text-center">
                                    <strong>Participants</strong>
                                </div>
                                @if($allUpcomingEvents->count() > 0)
                                    @foreach($allUpcomingEvents as $interested)
                                        <div class="col-md-3 my-1">
                                            <strong>
                                                {{$interested->event->post->title}}
                                            </strong>
                                        </div>
                                        <div class="col-md-2 my-1 text-left">
                                            <strong>
                                                {{$interested->author->user->name}}
                                            </strong>
                                        </div>
                                        <div class="col-md-3 my-1">
                                            <strong>
                                                {{$interested->event->start_date}}
                                            </strong>
                                        </div>
                                        <div class="col-md-2 my-1 text-center">
                                            <strong>
                                                {{$interested->event->interesteds->count()}}
                                            </strong>
                                            @if($interested->event->interesteds->count() > 0)
                                                <a href="{{route('users-list-events', ['interested', $interested->slug])}}">(View
                                                    list)</a>
                                            @endif
                                        </div>
                                        <div class="col-md-2 my-1 text-center">
                                            <strong>
                                                {{$interested->event->participates->count()}}
                                            </strong>
                                            @if($interested->event->participates->count() > 0)
                                                <a href="{{route('users-list-events', ['participate', $interested->slug])}}">(View
                                                    list)</a>
                                            @endif
                                        </div>
                                        <hr>
                                    @endforeach
                                @else
                                    <div class="col-md-12 text-center">
                                        <strong>No upcoming events!</strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="py-8">
                <div class="border-t border-gray-200"></div>
            </div>
            <div class="row my-5 w-100">
                <div class="col-md-2 ml-1">
                    <div class="text-center mb-5">
                        <h4>
                            Past Events
                        </h4>
                    </div>
                </div>
                <div class="col-md-9 m-auto">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row my-events-scroll">
                                <div class="col-md-3 my-3">
                                    <strong>
                                        Post title
                                    </strong>
                                </div>
                                <div class="col-md-2 my-3 text-left">
                                    <strong>
                                        Author
                                    </strong>
                                </div>
                                <div class="col-md-3 my-3">
                                    <strong>
                                        Event start date
                                    </strong>
                                </div>
                                <div class="col-md-2 my-3 text-center">
                                    <strong>Interested</strong>
                                </div>
                                <div class="col-md-2 my-3 text-center">
                                    <strong>Participants</strong>
                                </div>
                                @if($allPastEvents->count() > 0)
                                    @foreach($allPastEvents as $interested)
                                        <div class="col-md-3 my-1">
                                            <strong>
                                                {{$interested->event->post->title}}
                                            </strong>
                                        </div>
                                        <div class="col-md-2 my-1 text-left">
                                            <strong>
                                                {{$interested->author->user->name}}
                                            </strong>
                                        </div>
                                        <div class="col-md-3 my-1">
                                            <strong>
                                                {{$interested->event->start_date}}
                                            </strong>
                                        </div>
                                        <div class="col-md-2 my-1 text-center">
                                            <strong>
                                                {{$interested->event->interesteds->count()}}
                                            </strong>
                                            @if($interested->event->interesteds->count() > 0)
                                                <a href="{{route('users-list-events', ['interested', $interested->slug])}}">(View
                                                    list)</a>
                                            @endif
                                        </div>
                                        <div class="col-md-2 my-1 text-center">
                                            <strong>
                                                {{$interested->event->participates->count()}}
                                            </strong>
                                            @if($interested->event->participates->count() > 0)
                                                <a href="{{route('users-list-events', ['participate', $interested->slug])}}">(View
                                                    list)</a>
                                            @endif
                                        </div>
                                        <hr>
                                    @endforeach
                                @else
                                    <div class="col-md-12 text-center">
                                        <strong>No past events!</strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="py-8">
                <div class="border-t border-gray-200"></div>
            </div>

            <div class="m-5 text-center">
                <strong>
                    <h2>Events, which I've posted:</h2>
                </strong>
            </div>
            <div class="row my-5 w-100">
                <div class="col-md-2 ml-1">
                    <div class="text-center mb-5">
                        <h4>
                            Upcoming Events
                        </h4>
                    </div>
                </div>
                <div class="col-md-9 m-auto">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row my-events-scroll">
                                <div class="col-md-4 my-3">
                                    <strong>
                                        Post title
                                    </strong>
                                </div>
                                <div class="col-md-3 my-3">
                                    <strong>
                                        Event start date
                                    </strong>
                                </div>
                                <div class="col-md-3 my-3 text-center">
                                    <strong>Interested</strong>
                                </div>
                                <div class="col-md-2 my-3 text-center">
                                    <strong>Participants</strong>
                                </div>
                                @if($postedUpcomingEvents !== NULL)
                                    @if($postedUpcomingEvents->count() > 0)
                                        @foreach($postedUpcomingEvents as $interested)
                                            <div class="col-md-4 my-1">
                                                <strong>
                                                    {{$interested->event->post->title}}
                                                </strong>
                                            </div>
                                            <div class="col-md-3">
                                                <strong>
                                                    {{$interested->event->start_date}}
                                                </strong>
                                            </div>
                                            <div class="col-md-3 my-1 text-center">
                                                <strong>
                                                    {{$interested->event->interesteds->count()}}
                                                </strong>
                                                @if($interested->event->interesteds->count() > 0)
                                                    <a href="{{route('users-list-events', ['interested', $interested->slug])}}">(View
                                                        list)</a>
                                                @endif
                                            </div>
                                            <div class="col-md-2 my-1 text-center">
                                                <strong>
                                                    {{$interested->event->participates->count()}}
                                                </strong>
                                                @if($interested->event->participates->count() > 0)
                                                    <a href="{{route('users-list-events', ['participate', $interested->slug])}}">(View
                                                        list)</a>
                                                @endif
                                            </div>
                                            <hr>
                                        @endforeach
                                    @else
                                        <div class="col-md-12 text-center">
                                            <strong>No upcoming events!</strong>
                                        </div>
                                    @endif
                                @else
                                    <div class="col-md-12 text-center">
                                        <strong>No upcoming events!</strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="py-8">
                <div class="border-t border-gray-200"></div>
            </div>

            <div class="row my-5 w-100">
                <div class="col-md-2 ml-1">
                    <div class="text-center mb-5">
                        <h4>
                            Past Events
                        </h4>
                    </div>
                </div>
                <div class="col-md-9 m-auto">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row my-events-scroll">
                                <div class="col-md-4 my-3">
                                    <strong>
                                        Post title
                                    </strong>
                                </div>
                                <div class="col-md-3 my-3">
                                    <strong>
                                        Event start date
                                    </strong>
                                </div>
                                <div class="col-md-3 my-3 text-center">
                                    <strong>Interested</strong>
                                </div>
                                <div class="col-md-2 my-3 text-center">
                                    <strong>Participants</strong>
                                </div>
                                @if($postedPastEvents !== NULL)
                                    @if($postedPastEvents->count() > 0)
                                        @foreach($postedPastEvents as $interested)
                                            <div class="col-md-4 my-1">
                                                <strong>
                                                    {{$interested->event->post->title}}
                                                </strong>
                                            </div>
                                            <div class="col-md-3">
                                                <strong>
                                                    {{$interested->event->start_date}}
                                                </strong>
                                            </div>
                                            <div class="col-md-3 my-1 text-center">
                                                <strong>
                                                    {{$interested->event->interesteds->count()}}
                                                </strong>
                                                @if($interested->event->interesteds->count() > 0)
                                                    <a href="{{route('users-list-events', ['interested', $interested->slug])}}">(View
                                                        list)</a>
                                                @endif
                                            </div>
                                            <div class="col-md-2 my-1 text-center">
                                                <strong>
                                                    {{$interested->event->participates->count()}}
                                                </strong>
                                                @if($interested->event->participates->count() > 0)
                                                    <a href="{{route('users-list-events', ['participate', $interested->slug])}}">(View
                                                        list)</a>
                                                @endif
                                            </div>
                                            <hr>
                                        @endforeach
                                    @else
                                        <div class="col-md-12 text-center">
                                            <strong>No upcoming events!</strong>
                                        </div>
                                    @endif
                                @else
                                    <div class="col-md-12 text-center">
                                        <strong>No upcoming events!</strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="py-8">
                <div class="border-t border-gray-200"></div>
            </div>
        @endcan
        <div class="m-5 text-center">
            <strong>
                Events, which I'm subscribed in:
            </strong>
        </div>
        <div class="row my-5 w-100">
            <div class="col-md-2 ml-1">
                <div class="text-center mb-5">
                    <h4>
                        Upcoming Events
                    </h4>
                    <br>
                    <h5>
                        where I'm subscribed as "Interested"
                    </h5>
                </div>
            </div>
            <div class="col-md-9 m-auto">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row my-events-scroll">
                            <div class="col-md-5 my-3">
                                <strong>
                                    Post title
                                </strong>
                            </div>
                            <div class="col-md-2 my-3">
                                <strong>
                                    Event start date
                                </strong>
                            </div>
                            <div class="col-md-5 my-3 text-center">
                                <strong>Actions</strong>
                            </div>
                            @if($upcomingInts->count() > 0)
                                @foreach($upcomingInts as $interested)
                                    <div class="col-md-5 my-1">
                                        <strong>
                                            {{$interested->event->post->title}}
                                        </strong>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>
                                            {{$interested->event->start_date}}
                                        </strong>
                                    </div>
                                    <div class="col-md-3 my-1">
                                        <a href="{{route('posts-show', $interested->event->post->slug)}}">
                                            <button class="btn btn-primary">
                                                View event
                                            </button>
                                        </a>
                                    </div>
                                    <div class="col-md-2 my-1">
                                        <a href="{{route('posts-event-participate', $interested->event->post->slug)}}">
                                            <button class="btn btn-success">
                                                Participate
                                            </button>
                                        </a>
                                    </div>
                                    <hr>
                                @endforeach
                            @else
                                <div class="col-md-12 text-center">
                                    <strong>No upcoming events!</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-8">
            <div class="border-t border-gray-200"></div>
        </div>

        <div class="row my-5 w-100">
            <div class="col-md-2 ml-1">
                <div class="text-center mb-5">
                    <h4>
                        Upcoming Events
                    </h4>
                    <br>
                    <h5>
                        where I'm subscribed as "Participant"
                    </h5>
                </div>
            </div>
            <div class="col-md-9 m-auto">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row my-events-scroll">
                            <div class="col-md-4 offset-1 my-3">
                                <strong>
                                    Post title
                                </strong>
                            </div>
                            <div class="col-md-2 my-3">
                                <strong>
                                    Event start date
                                </strong>
                            </div>
                            <div class="col-md-4 offset-1 my-3 text-center">
                                <strong>Action</strong>
                            </div>
                            @if($upcomingParts->count() > 0)
                                @foreach($upcomingParts as $interested)
                                    <div class="col-md-4 offset-1 my-1">
                                        <strong>
                                            {{$interested->event->post->title}}
                                        </strong>
                                    </div>
                                    <div class="col-md-2">
                                        <strong>
                                            {{$interested->event->start_date}}
                                        </strong>
                                    </div>
                                    <div class="col-md-4 offset-1 my-1">
                                        <a href="{{route('posts-event-participate', $interested->event->post->slug)}}">
                                            <button class="btn btn-warning">
                                                Unsubscribe
                                            </button>
                                        </a>
                                    </div>
                                    <hr>
                                @endforeach
                            @else
                                <div class="col-md-12 text-center">
                                    <strong>No upcoming events!</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-8">
            <div class="border-t border-gray-200"></div>
        </div>

        <div class="row my-5 w-100">
            <div class="col-md-2 ml-1">
                <div class="text-center mb-5">
                    <h4>
                        Past Events
                    </h4>
                    <br>
                    <h5>
                        where I was subscribed as "Interested"
                    </h5>
                </div>
            </div>
            <div class="col-md-9 m-auto">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row my-events-scroll">
                            <div class="col-md-4 offset-2 my-3">
                                <strong>
                                    Post title
                                </strong>
                            </div>
                            <div class="col-md-3 my-3">
                                <strong>
                                    Event start date
                                </strong>
                            </div>
                            <div class="col-md-3 my-3">
                                <strong>
                                    Event end date
                                </strong>
                            </div>
                            @if($pastInts->count() > 0)
                                @foreach($pastInts as $interested)
                                    <div class="col-md-4 offset-2 my-1">
                                        <strong>
                                            {{$interested->event->post->title}}
                                        </strong>
                                    </div>
                                    <div class="col-md-3 my-1">
                                        <strong>
                                            {{$interested->event->start_date}}
                                        </strong>
                                    </div>
                                    <div class="col-md-3 my-1">
                                        <strong>
                                            {{$interested->event->end_date}}
                                        </strong>
                                    </div>
                                    <hr>
                                @endforeach
                            @else
                                <div class="col-md-12 text-center">
                                    <strong>No past events!</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-8">
            <div class="border-t border-gray-200"></div>
        </div>
        <div class="row my-5 w-100">
            <div class="col-md-2 ml-1">
                <div class="text-center mb-5">
                    <h4>
                        Past Events
                    </h4>
                    <br>
                    <h5>
                        where I was subscribed as "Participant"
                    </h5>
                </div>
            </div>
            <div class="col-md-9 m-auto">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row my-events-scroll">
                            <div class="col-md-6 offset-1 my-4">
                                <strong>
                                    Post title
                                </strong>
                            </div>
                            <div class="col-md-4 offset-1 my-4">
                                <strong>
                                    Event start date
                                </strong>
                            </div>
                            <hr>
                            @if($pastParts->count() > 0)
                                @foreach($pastParts as $interested)
                                    <div class="col-md-6 offset-1 my-1">
                                        <strong>
                                            {{$interested->event->post->title}}
                                        </strong>
                                    </div>
                                    <div class="col-md-4 offset-1 my-1">
                                        {{$interested->event->start_date}}
                                    </div>
                                    <hr>
                                @endforeach
                            @else
                                <div class="col-md-12 text-center">
                                    <strong>No past events!</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-8">
            <div class="border-t border-gray-200"></div>
        </div>
    </div>
</x-app-layout>
