<x-app-layout>
    <div class="discussion">
        @can('admin')
            <div class="m-5 text-center">
                <strong>
                    <h2>All posts in Career & Paid opportunities (excluded my posts) - only for Administrators</h2>
                </strong>
            </div>
            <div class="row my-5 w-100">
                <div class="col-md-2 ml-1">
                    <div class="text-center mb-5">
                        <h4>
                            Career & Paid opportunities
                        </h4>
                    </div>
                </div>
                <div class="col-md-9 m-auto">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row my-events-scroll">
                                <div class="col-md-4 my-3">
                                    <strong>
                                        Post title
                                    </strong>
                                </div>
                                <div class="col-md-2 my-3 text-left">
                                    <strong>
                                        Author
                                    </strong>
                                </div>
                                <div class="col-md-3 my-3 text-center">
                                    <strong>Interested</strong>
                                </div>
                                <div class="col-md-3 my-3 text-center">
                                    <strong>Participants</strong>
                                </div>
                                @if($allCareers !== NULL)
                                    @if($allCareers->count() > 0)
                                        @foreach($allCareers as $interested)
                                            <div class="col-md-4 my-1">
                                                <strong>
                                                    {{$interested->title}}
                                                </strong>
                                            </div>
                                            <div class="col-md-2 my-1 text-left">
                                                <strong>
                                                    {{$interested->author->user->name}}
                                                </strong>
                                            </div>
                                            <div class="col-md-3 my-1 text-center">
                                                <strong>
                                                    {{$interested->careerInteresteds->count()}}
                                                </strong>
                                                @if($interested->careerInteresteds->count() > 0)
                                                    <a href="{{route('users-list-events', ['career-interested', $interested->slug])}}">(View
                                                        list)</a>
                                                @endif
                                            </div>
                                            <div class="col-md-3 my-1 text-center">
                                                <strong>
                                                    {{$interested->careerParticipates->count()}}
                                                </strong>
                                                @if($interested->careerParticipates->count() > 0)
                                                    <a href="{{route('users-list-events', ['career-participate', $interested->slug])}}">(View
                                                        list)</a>
                                                @endif
                                            </div>
                                            <hr>
                                        @endforeach
                                    @else
                                        <div class="col-md-12 text-center">
                                            <strong>No posts to show!</strong>
                                        </div>
                                    @endif
                                @else
                                    <div class="col-md-12 text-center">
                                        <strong>No posts to show!</strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="py-8">
                <div class="border-t border-gray-200"></div>
            </div>

            <div class="m-5 text-center">
                <strong>
                    <h2>Posts, which I've posted:</h2>
                </strong>
            </div>
            <div class="row my-5 w-100">
                <div class="col-md-2 ml-1">
                    <div class="text-center mb-5">
                        <h4>
                            Career & Paid opportunities posts
                        </h4>
                    </div>
                </div>
                <div class="col-md-9 m-auto">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row my-events-scroll">
                                <div class="col-md-6 my-3">
                                    <strong>
                                        Post title
                                    </strong>
                                </div>
                                <div class="col-md-3 my-3 text-center">
                                    <strong>Interested</strong>
                                </div>
                                <div class="col-md-3 my-3 text-center">
                                    <strong>Participants</strong>
                                </div>
                                @if($me->author !== NULL)
                                    @if($me->author->posts->where('category_id', 6)->count() > 0)
                                        @foreach($me->author->posts->where('category_id', 6) as $interested)
                                            <div class="col-md-6 my-1">
                                                <strong>
                                                    {{$interested->title}}
                                                </strong>
                                            </div>
                                            <div class="col-md-3 my-1 text-center">
                                                <strong>
                                                    {{$interested->careerInteresteds->count()}}
                                                </strong>
                                                @if($interested->careerInteresteds->count() > 0)
                                                    <a href="{{route('users-list-events', ['career-interested', $interested->slug])}}">(View
                                                        list)</a>
                                                @endif
                                            </div>
                                            <div class="col-md-3 my-1 text-center">
                                                <strong>
                                                    {{$interested->careerParticipates->count()}}
                                                </strong>
                                                @if($interested->careerParticipates->count() > 0)
                                                    <a href="{{route('users-list-events', ['career-participate', $interested->slug])}}">(View
                                                        list)</a>
                                                @endif
                                            </div>
                                            <hr>
                                        @endforeach
                                    @else
                                        <div class="col-md-12 text-center">
                                            <strong>No posts to show!</strong>
                                        </div>
                                    @endif
                                @else
                                    <div class="col-md-12 text-center">
                                        <strong>No posts to show!</strong>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="py-8">
                <div class="border-t border-gray-200"></div>
            </div>
        @endcan
        <div class="m-5 text-center">
            <strong>
                Posts, which I'm subscribed in:
            </strong>
        </div>
        <div class="row my-5 w-100">
            <div class="col-md-2 ml-1">
                <div class="text-center mb-5">
                    <h4>
                        Career & Paid opportunities posts
                    </h4>
                    <br>
                    <h5>
                        where I'm subscribed as "Interested"
                    </h5>
                </div>
            </div>
            <div class="col-md-9 m-auto">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row my-events-scroll">
                            <div class="col-md-5 offset-1 my-3">
                                <strong>
                                    Post title
                                </strong>
                            </div>
                            <div class="col-md-6 my-3 text-center">
                                <strong>Actions</strong>
                            </div>
                            @if($me->careerInteresteds->count() > 0)
                                @foreach($me->careerInteresteds as $interested)
                                    <div class="col-md-5 offset-1 my-1">
                                        <strong>
                                            {{$interested->post->title}}
                                        </strong>
                                    </div>
                                    <div class="col-md-3 text-center my-1">
                                        <a href="{{route('posts-show', $interested->post->slug)}}">
                                            <button class="btn btn-primary">
                                                View post
                                            </button>
                                        </a>
                                    </div>
                                    <div class="col-md-3 text-center my-1">
                                        <a href="{{route('posts-career-participate', $interested->post->slug)}}">
                                            <button class="btn btn-success">
                                                Participate
                                            </button>
                                        </a>
                                    </div>
                                    <hr>
                                @endforeach
                            @else
                                <div class="col-md-12 text-center">
                                    <strong>No posts to show!</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-8">
            <div class="border-t border-gray-200"></div>
        </div>
        <div class="row my-5 w-100">
            <div class="col-md-2 ml-1">
                <div class="text-center mb-5">
                    <h4>
                        Career & Paid opportunities posts
                    </h4>
                    <br>
                    <h5>
                        where I'm subscribed as "Participant"
                    </h5>
                </div>
            </div>
            <div class="col-md-9 m-auto">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row my-events-scroll">
                            <div class="col-md-5 offset-1 my-3">
                                <strong>
                                    Post title
                                </strong>
                            </div>
                            <div class="col-md-5 offset-1 my-3 text-center">
                                <strong>Action</strong>
                            </div>
                            @if($me->careerParticipates->count() > 0)
                                @foreach($me->careerParticipates as $interested)
                                    <div class="col-md-5 offset-1 my-1">
                                        <strong>
                                            {{$interested->post->title}}
                                        </strong>
                                    </div>
                                    <div class="col-md-5 offset-1 my-1 text-center">
                                        <a href="{{route('posts-career-participate', $interested->post->slug)}}">
                                            <button class="btn btn-warning">
                                                Unsubscribe
                                            </button>
                                        </a>
                                    </div>
                                    <hr>
                                @endforeach
                            @else
                                <div class="col-md-12 text-center">
                                    <strong>No posts to show!</strong>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="py-8">
            <div class="border-t border-gray-200"></div>
        </div>
    </div>
</x-app-layout>
