<x-app-layout>
    <div class="users-wrapper my-5">
        <div class="my-5 mx-auto text-center w-50">
            <p>
                <strong>
                    Thank you for registering on our platform!
                </strong>
            </p>
            <p>
                <strong>
                    Your account if currently under review and you will receive an email confirmation once it is verified!
                </strong>
            </p>
            <p>
                <strong>
                    In the mean time, please click <a href="{{route('users-page', 'profile')}}">here</a> to complete your profile!
                </strong>
            </p>
        </div>
    </div>
</x-app-layout>
