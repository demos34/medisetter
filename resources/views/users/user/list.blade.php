<x-app-layout>
    @can('admin')
    <div class="row my-5 w-100">
        <div class="col-md-2 ml-1">
            <div class="text-center mb-5">
                <h4>
                    {{$post->title}}
                </h4>
                <br>
                <h5>
                    ({{$type}})
                </h5>
                <br>
                @if($post->event != NULL)
                <span>Start:</span>
                <strong>{{$post->event->start_date}}</strong>
                <br>
                <span>End:</span>
                <strong>{{$post->event->end_date}}</strong>
                <br>
                @endif
                <span>
                    Total:
                </span>
                <strong>
                    {{$users->count()}}
                </strong>
            </div>
        </div>
        <div class="col-md-9 m-auto">
            <div class="row">
                <div class="col-md-12">
                    <div class="row my-events-users-scroll">
                        <div class="col-md-5 offset-1 my-3 text-center">
                            <strong>
                                User's name
                            </strong>
                        </div>
                        <div class="col-md-5 offset-1 my-3 text-center">
                            <strong>
                                email
                            </strong>
                        </div>
                        @if($users->count() > 0)
                            @foreach($users as $user)
                                <div class="col-md-5 offset-1 my-1 text-center">
                                    <strong>
                                        {{$user->user->name}}
                                    </strong>
                                </div>
                                <div class="col-md-5 offset-1 my-1 text-center">
                                    <strong>
                                        {{$user->user->email}}
                                    </strong>
                                </div>
                            @endforeach
                        @else
                            <div class="col-md-12 text-center">
                                <strong>No subscribed users for this event!</strong>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="py-8">
        <div class="border-t border-gray-200"></div>
    </div>
    @endcan
</x-app-layout>
