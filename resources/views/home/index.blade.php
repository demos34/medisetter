<x-app-layout>
    <div class="home-page">
        <h2 class="posts-type-home">
            Recommended for you
        </h2>

        <div class="posts-grid home-posts-grid">
            <div class="home-category-container">
                <h5 class="home-subcategory-heading">News & Articles</h5>
                @foreach($newsPosts as $newsPost)
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$newsPost->image}}" alt="{{$newsPost->image}}">
                    <h2>{{Str::limit($newsPost->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($newsPost->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button" href="{{route('posts-show', $newsPost->slug)}}">Continue</a>
                </article>
                <br>
                @endforeach
            </div>

            <div class="home-category-container">
                <h5 class="home-subcategory-heading">Drug info & updates</h5>
                @foreach($drugPosts as $drugPost)
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$drugPost->image}}" alt="{{$drugPost->image}}">
                    <h2>{{Str::limit($drugPost->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($drugPost->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button" href="{{route('posts-show', $drugPost->slug)}}">Continue</a>
                </article>
                <br>
                @endforeach
            </div>

            <div class="home-category-container">
                <h5 class="home-subcategory-heading">Events</h5>
                @foreach($eventPosts as $eventPost)
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$eventPost->image}}" alt="{{$eventPost->image}}">
                    <h2>{{Str::limit($eventPost->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($eventPost->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button" href="{{route('posts-show', $eventPost->slug)}}">Continue</a>
                </article>
                <br>
                @endforeach
            </div>

            <div class="home-category-container">
                <h5 class="home-subcategory-heading">Discussion</h5>
                @foreach($discussionPosts as $discussionPost)
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$discussionPost->image}}" alt="{{$discussionPost->image}}">
                    <h2>{{Str::limit($discussionPost->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($discussionPost->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button"
                       href="{{route('posts-show', $discussionPost->slug)}}">Continue</a>
                </article>
                <br>
                @endforeach
            </div>

            <div class="home-category-container">
                <h5 class="home-subcategory-heading">Career & paid opportunities</h5>
                @foreach($careerPosts as $careerPost)
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$careerPost->image}}" alt="{{$careerPost->image}}">
                    <h2>{{Str::limit($careerPost->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($careerPost->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button" href="{{route('posts-show', $careerPost->slug)}}">Continue</a>
                </article>
                <br>
                @endforeach
            </div>
        </div>

        <h2 class="posts-type-home">
            Our last posts
        </h2>

        <div class="posts-grid home-posts-grid">
            <div class="home-category-container">
                <h5 class="home-subcategory-heading">News & Articles</h5>
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$newsLast->image}}" alt="{{$newsLast->image}}">
                    <h2>{{Str::limit($newsLast->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($newsLast->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button" href="{{route('posts-show', $newsLast->slug)}}">Continue</a>
                </article>
                <br>
            </div>

            <div class="home-category-container">
                <h5 class="home-subcategory-heading">Drug info & updates</h5>
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$drugLast->image}}" alt="{{$drugLast->image}}">
                    <h2>{{Str::limit($drugLast->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($drugLast->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button" href="{{route('posts-show', $drugLast->slug)}}">Continue</a>
                </article>
                <br>
            </div>

            <div class="home-category-container">
                <h5 class="home-subcategory-heading">Events</h5>
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$eventLast->image}}" alt="{{$eventLast->image}}">
                    <h2>{{Str::limit($eventLast->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($eventLast->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button" href="{{route('posts-show', $eventLast->slug)}}">Continue</a>
                </article>
                <br>
            </div>

            <div class="home-category-container">
                <h5 class="home-subcategory-heading">Discussion</h5>
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$discussionLast->image}}" alt="{{$discussionLast->image}}">
                    <h2>{{Str::limit($discussionLast->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($discussionLast->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button"
                       href="{{route('posts-show', $discussionLast->slug)}}">Continue</a>
                </article>
                <br>
            </div>

            <div class="home-category-container">
                <h5 class="home-subcategory-heading">Career & paid opportunities</h5>
                <article class="post-card home-post-card">
                    <img class="post-card-image" src="{{$careerLast->image}}" alt="{{$careerLast->image}}">
                    <h2>{{Str::limit($careerLast->title, 30)}}</h2>
                    <div class="post-card-content">
                        <p>{{Str::limit($careerLast->body, 150)}}</p>
                    </div>
                    <a class="show-button news-button" href="{{route('posts-show', $careerLast->slug)}}">Continue</a>
                </article>
                <br>
            </div>
        </div>

    </div>
</x-app-layout>
