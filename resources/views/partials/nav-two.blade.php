<nav class="main-navbar">
    <div class="navbar-content">

        <a href="#">
            <img class="logo" src="/storage/custom-nav/logo.svg" alt="logo">
        </a>

        <ul class="user-links">
            <li>
                <a href="#" class="unverified-link">
                    <i class="fas fa-exclamation"></i>
                </a>
                <ul class="nav-dropdown user-link-options unverified-info">
                    <li>Unverified account</li>
                </ul>
            </li>
            <li>
                <a href="#">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li>
                <span class="user-notification">12</span>
                <a href="#" class="messages-link">
                    <i class="far fa-envelope"></i>
                </a>
                <ul class="nav-dropdown user-link-options-notification messages-info">
                    <li>
                        <a href="#">
                            <img
                                src="http://134.209.207.208/storage/users/2020-12-01-18-37-13-senior-doctor-laughing-to-camera-16276713.jpg"
                                alt="avatar">
                        </a>
                        <div class="user-data">
                            <p class="notification-text">Something happened</p>
                            <p class="notification-time">an hour ago</p>
                        </div>
                    </li>

                    <li>
                        <a href="#">
                            <img
                                src="http://134.209.207.208/storage/users/2020-12-01-18-37-13-senior-doctor-laughing-to-camera-16276713.jpg"
                                alt="avatar">
                        </a>
                        <div class="user-data">
                            <p class="notification-text">Something happened</p>
                            <p class="notification-time">an hour ago</p>
                        </div>
                    </li>

                    <li>
                        <a href="#">
                            <img
                                src="http://134.209.207.208/storage/users/2020-12-01-18-37-13-senior-doctor-laughing-to-camera-16276713.jpg"
                                alt="avatar">
                        </a>
                        <div class="user-data">
                            <p class="notification-text">Something happened</p>
                            <p class="notification-time">an hour ago</p>
                        </div>
                    </li>

                    <li>
                        <a href="#">
                            <img
                                src="http://134.209.207.208/storage/users/2020-12-01-18-37-13-senior-doctor-laughing-to-camera-16276713.jpg"
                                alt="avatar">
                        </a>
                        <div class="user-data">
                            <p class="notification-text">Something happened</p>
                            <p class="notification-time">an hour ago</p>
                        </div>
                    </li>

                    <li>
                        <a href="#">
                            <img
                                src="http://134.209.207.208/storage/users/2020-12-01-18-37-13-senior-doctor-laughing-to-camera-16276713.jpg"
                                alt="avatar">
                        </a>
                        <div class="user-data">
                            <p class="notification-text">Something happened</p>
                            <p class="notification-time">an hour ago</p>
                        </div>
                    </li>

                    <li>
                        <a href="#">
                            <img
                                src="http://134.209.207.208/storage/users/2020-12-01-18-37-13-senior-doctor-laughing-to-camera-16276713.jpg"
                                alt="avatar">
                        </a>
                        <div class="user-data">
                            <p class="notification-text">Something happened</p>
                            <p class="notification-time">an hour ago</p>
                        </div>
                    </li>
                </ul>
            </li>

            <li>
                <span class="user-notification">12</span>
                <a href="#" class="notification-link">
                    <i class="far fa-bell"></i>
                </a>

                <ul class="nav-dropdown user-link-options-notification notification-info">
                    <li>
                        <a href="#">
                            <img
                                src="http://134.209.207.208/storage/users/2020-12-01-18-37-13-senior-doctor-laughing-to-camera-16276713.jpg"
                                alt="avatar">
                        </a>
                        <div class="user-data">
                            <p class="notification-text">Something happened</p>
                            <p class="notification-time">an hour ago</p>
                        </div>
                    </li>

                    <li>
                        <a href="#">
                            <img
                                src="http://134.209.207.208/storage/users/2020-12-01-18-37-13-senior-doctor-laughing-to-camera-16276713.jpg"
                                alt="avatar">
                        </a>
                        <div class="user-data">
                            <p class="notification-text">Something happened</p>
                            <p class="notification-time">an hour ago</p>
                        </div>
                    </li>

                    <li>
                        <a href="#">
                            <img
                                src="http://134.209.207.208/storage/users/2020-12-01-18-37-13-senior-doctor-laughing-to-camera-16276713.jpg"
                                alt="avatar">
                        </a>
                        <div class="user-data">
                            <p class="notification-text">Something happened</p>
                            <p class="notification-time">an hour ago</p>
                        </div>
                    </li>

                    <li>
                        <a href="#">
                            <img
                                src="http://134.209.207.208/storage/users/2020-12-01-18-37-13-senior-doctor-laughing-to-camera-16276713.jpg"
                                alt="avatar">
                        </a>
                        <div class="user-data">
                            <p class="notification-text">Something happened</p>
                            <p class="notification-time">an hour ago</p>
                        </div>
                    </li>

                    <li>
                        <a href="#">
                            <img
                                src="http://134.209.207.208/storage/users/2020-12-01-18-37-13-senior-doctor-laughing-to-camera-16276713.jpg"
                                alt="avatar">
                        </a>
                        <div class="user-data">
                            <p class="notification-text">Something happened</p>
                            <p class="notification-time">an hour ago</p>
                        </div>
                    </li>

                    <li>
                        <a href="#">
                            <img
                                src="http://134.209.207.208/storage/users/2020-12-01-18-37-13-senior-doctor-laughing-to-camera-16276713.jpg"
                                alt="avatar">
                        </a>
                        <div class="user-data">
                            <p class="notification-text">Something happened</p>
                            <p class="notification-time">an hour ago</p>
                        </div>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#" class="options-link">
                    <i class="far fa-user"></i>
                </a>
                <ul class="nav-dropdown user-link-options-notification options-info">
                    <li><a href="#">Mykhaylo</a></li>
                    <li><a href="#">Admin panel</a></li>
                    <li><a href="#">Users</a></li>
                    <li><a href="#">Write new post</a></li>
                    <li><a href="#">Profile information</a></li>
                    <li><a href="#">Analyze</a></li>
                    <li><a href="#">Exit</a></li>
                </ul>
            </li>
            <li>
                <a href="#" class="lang-link">
                    <i class="fas fa-globe-asia"></i>
                </a>
                <ul class="nav-dropdown user-link-options lang-info">
                    <li><a href="#">EN</a></li>
                    <li><a href="#">VI</a></li>
                </ul>
            </li>
        </ul>

        <div class="desktop-links-row">
            <ul class="desktop-links">
                <li>
                    <a href="#">News & Articles</a>
                </li>
                <li>
                    <a href="#">Drug info & updates</a>
                </li>
                <li>
                    <a href="#">Events</a>
                </li>
                <li>
                    <a href="#" class="career">Career & paid opportunities</a>
                    <ul class="nav-dropdown menu-link-options career-menu">
                        <li><a href="#">Surveys</a></li>
                        <li><a href="#">Clinical studies</a></li>
                        <li><a href="#">Products previews</a></li>
                        <li><a href="#">Job</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#">Discussion</a>
                </li>
            </ul>
        </div>

        <div class="mobile-links-row">
            <a class="hamburger-link" href="#">
                <i class="fas fa-bars"></i>
            </a>
            <ul class="nav-dropdown hamburger-menu">
                <li><a href="#">News & articles</a></li>
                <li><a href="#">Drug info & updates</a></li>
                <li><a href="#">Events</a></li>
                <li><a href="#">Career & paid opportunities</a></li>
                <li><a href="#">Discussion</a></li>
            </ul>
        </div>

        <ul class="filters">
            <li class="speciality-filter">
                <a href="#" class="speciality-link">
                    <span>Speciality: </span>

                    <span class="speciality-type">All </span>
                </a>

                <ul class="nav-dropdown speciality-types">
                    <li><a href="#">Speciality 1</a></li>
                    <li><a href="#">Speciality 2</a></li>
                    <li><a href="#">Speciality 3</a></li>
                    <li><a href="#">Speciality 4</a></li>
                    <li><a href="#">Speciality 5</a></li>
                </ul>
            </li>

            <li class="search-bar">
                <input type="text" placeholder="Search article">
                <i class="fas fa-search"></i>
            </li>
        </ul>
    </div>
</nav>
