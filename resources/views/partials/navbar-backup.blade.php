<nav class="navbar navbar-expand-lg">
    <a class="logo" href="/">
        <img class="custom-nav-logo" src="/storage/custom-nav/logo.svg" alt="top-logo">
    </a>
    <button class="navbar-toggler navbar-toggler-mobile" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon">
            <img src="/storage/custom-nav/toggler.svg">
        </span>
    </button>
    <div class="custom-nav-tag">
        <div class="collapse navbar-collapse mobile-custom-nav" id="navbarNav">
            <div class="custom-nav-wrapper">
                <div class="custom-nav-icons">
                    @if(Auth::check())
                        @if(Auth::user()->is_verified == 0)
                            <div>
                                <small class="nav-unverified">
                                    Unverified account!
                                </small>
                            </div>
                        @endif
                        <div>
                            <a href="{{ route('home') }}" class="notification">
                                <span class="material-icons">
                                    home
                                </span>
                            </a>
                        </div>
                        <div>
                            <a href="{{ route('msg') }}" class="notification">
                                <span>
                                    <i class="material-icons-outlined profile-icon">email</i>
                                </span>
                                <chat-counter :user="{{auth()->user()}}" :to="{{auth()->user()->to}}"></chat-counter>
                            </a>
                        </div>
                        <div>
                            <notif :user="{{auth()->user()}}"></notif>
                        </div>
                        <div>
                            <div class="dropdown">
                                <a class="dropdown-toggle notification" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <span>
                                    <i class="material-icons-outlined profile-icon">person</i>
                                </span>
                                </a>
                                <div class="dropdown-menu lang-settings" aria-labelledby="navbarDropdown">
                                    <ul class="text-left m-3">
                                        <li class="my-3">
                                            <a href="{{route('users')}}" class="custom-dash-links">
                                                {{Auth::user()->name}}
                                            </a>
                                        </li>
                                        @can('admin')
                                            <li class="my-3">
                                                <a href="{{route('admin-index', 'admin')}}" class="custom-dash-links">
                                                    Admin panel
                                                </a>
                                            </li>
                                            <li class="my-3">
                                                <a href="{{route('admin-view-users')}}" class="custom-dash-links">
                                                    Users
                                                </a>
                                            </li>
                                            <li class="my-3">
                                                <a href="{{route('categories-posts-create', 'news-articles')}}" class="custom-dash-links">
                                                    Write new post
                                                </a>
                                            </li>
                                        @endcan
                                        <li class="my-3">
                                            <a href="{{route('users-page', 'user')}}" class="custom-dash-links">
                                                User information
                                            </a>
                                        </li>
                                        <li class="my-3">
                                            <a href="{{route('users-page', 'profile')}}" class="custom-dash-links">
                                                Profile information
                                            </a>
                                        </li>
                                        @can('admin-enterprise')
                                            <li class="my-3">
                                                <a href="{{route('analyze')}}" class="custom-dash-links">
                                                    Analyze
                                                </a>
                                            </li>
                                        @endcan
                                        <li class="my-3">
                                            <a class="custom-dash-links" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                                Exit
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div class="dropdown">
                                <a class="dropdown-toggle notification" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <span class="text-uppercase">
                                   {{App::getLocale()}}
                                </span>
                                </a>
                                <div class="dropdown-menu lang-settings" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="/lang/en">
                                        EN
                                    </a>
                                    <a class="dropdown-item" href="/lang/vi">
                                        VI
                                    </a>
                                </div>
                            </div>

                        </div>
                    @else
                        <div class="guest-login-register">
                            <ul class="navbar-nav mr-auto">
                                <li class="float-left" style="position:absolute; right: 50px; margin-top: -1.4em;">
                                    <a class="notification" href="{{route('login')}}">
                                        <span class="caret lang-span text-uppercase">Login</span>
                                    </a>
                                </li>
                                <li class="nav-item dropdown" style="position:absolute; right: 0; margin-top: -1em;">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        <span class="caret lang-span text-uppercase">{{App::getLocale()}}</span>
                                    </a>
                                    <div style="    position: absolute;margin-top: 3em;margin-left: -5em;" class="dropdown-menu lang-settings
                                    lang-guest" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="/lang/en">
                                            EN
                                        </a>
                                        <a class="dropdown-item" href="/lang/vi">
                                            VI
                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    @endif
                </div>
                @can('admin-generic')
                    <div class="custom-nav-category-vue-wrapper">
                        <category></category>
                    </div>
                @endcan
            </div>
        </div>
        <div class="custom-nav-icons-mobile">
            @if(Auth::check())
                <div>
                    <a href="{{ route('msg') }}" class="notification">
                                <span>
                                    <i class="material-icons-outlined profile-icon">email</i>
                                </span>
                        <chat-counter :user="{{auth()->user()}}" :to="{{auth()->user()->to}}"></chat-counter>
                    </a>
                </div>
                <div>
                    <notif :user="{{auth()->user()}}"></notif>
                </div>
                <div>
                    <div class="dropdown">
                        <a class="dropdown-toggle notification" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <span>
                                    <i class="material-icons-outlined profile-icon">person</i>
                                </span>
                        </a>
                        <div class="dropdown-menu lang-settings" aria-labelledby="navbarDropdown">
                            <ul class="text-left m-3">
                                <li class="my-3">
                                    <a href="{{route('users')}}" class="custom-dash-links">
                                        {{Auth::user()->name}}
                                    </a>
                                </li>
                                @can('admin')
                                    <li class="my-3">
                                        <a href="{{route('admin-index', 'admin')}}" class="custom-dash-links">
                                            Admin panel
                                        </a>
                                    </li>
                                    <li class="my-3">
                                        <a href="{{route('admin-view-users')}}" class="custom-dash-links">
                                            Users
                                        </a>
                                    </li>
                                    <li class="my-3">
                                        <a href="{{route('categories-posts-create', 'news-articles')}}" class="custom-dash-links">
                                            Write new post
                                        </a>
                                    </li>
                                @endcan
                                <li class="my-3">
                                    <a href="{{route('users-page', 'user')}}" class="custom-dash-links">
                                        User information
                                    </a>
                                </li>
                                <li class="my-3">
                                    <a href="{{route('users-page', 'profile')}}" class="custom-dash-links">
                                        Profile information
                                    </a>
                                </li>
                                @can('admin-enterprise')
                                    <li class="my-3">
                                        <a href="{{route('analyze')}}" class="custom-dash-links">
                                            Analyze
                                        </a>
                                    </li>
                                @endcan
                                <li class="my-3">
                                    <a class="custom-dash-links" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                        Exit
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="dropdown">
                        <a class="dropdown-toggle notification" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <span class="text-uppercase">
                                   {{App::getLocale()}}
                                </span>
                        </a>
                        <div class="dropdown-menu lang-settings" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/lang/en">
                                EN
                            </a>
                            <a class="dropdown-item" href="/lang/vi">
                                VI
                            </a>
                        </div>
                    </div>
                </div>
            @else
                <div class="guest-login-register">
                    <ul class="navbar-nav mr-auto">
                        <li class="float-left" style="position:absolute; right: 50px; margin-top: -1.4em;">
                            <a class="notification" href="{{route('login')}}">
                                <span class="caret lang-span text-uppercase">Login</span>
                            </a>
                        </li>
                        <li class="nav-item dropdown" style="position:absolute; right: 0; margin-top: -1em;">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                <span class="caret lang-span text-uppercase">{{App::getLocale()}}</span>
                            </a>
                            <div style="    position: absolute;margin-top: 3em;margin-left: -5em;" class="dropdown-menu lang-settings
                                    lang-guest" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/lang/en">
                                    EN
                                </a>
                                <a class="dropdown-item" href="/lang/vi">
                                    VI
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            @endif
        </div>
        @can('admin-generic')
            <div class="d-flex justify-content-between w-100 nav-speciality-wrapper">
                <div class="d-flex justify-content-start w-100">
                    @yield('spec')
                    @yield('search')
                </div>
            </div>
        @endcan
    </div>
</nav>
