<nav class="navbar navbar-expand-lg">
    <a href="/">
        <img class="custom-nav-logo" src="/storage/custom-nav/logo.png" alt="top-logo">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon">
            <img src="/storage/custom-nav/toggler.svg">
        </span>
    </button>
    <div class="custom-nav-tag">
        <div class="collapse navbar-collapse mobile-custom-nav" id="navbarNav">
            <div class="custom-nav-wrapper">
                <div class="custom-nav-icons">
                    @if(Auth::check())
                        <div>
                            <a href="{{ route('msg') }}" class="notification">
                              <span>
                                  <i class="material-icons-outlined profile-icon">email</i>
                              </span>
                                <span class="badge">3</span>
                            </a>
                        </div>
                        <div>
                            <a href="#" class="notification">
                                <span>
                                    <i class="material-icons-outlined profile-icon">notifications</i>
                                </span>
                                <span class="badge">3</span>
                            </a>
                        </div>
                       <div>
                           <a href="{{route('users')}}" class="notification">
                          <span>
                            <i class="material-icons-outlined profile-icon">person</i>
                          </span>
                           </a>
                       </div>
                        <div>
                            <a href="">
                                <img class="custom-nav-images" src="/storage/nav/settings-icon.png" alt="msg">
                            </a>
                        </div>
                        <ul class="ext-center my-auto ml-3">
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                        Exit
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        </ul>
                    @else
                        <div class="guest-login-register">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        <span class="caret lang-span text-uppercase">{{App::getLocale()}}</span>
                                    </a>
                                    <div class="dropdown-menu lang-settings" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="/lang/en">
                                            EN
                                        </a>
                                        <a class="dropdown-item" href="/lang/vi">
                                            VI
                                        </a>
                                    </div>
                                </li>
                            </ul>
                            <div>
                                <a href="{{ route('login') }}">
                                    <button class="text-uppercase">
                                        {{ __('nav.sign-in') }}
                                    </button>
                                </a>
                            </div>
                            <div>
                                <a href="{{ route('register') }}">
                                    <button class="text-uppercase">
                                        {{ __('nav.sign-up') }}
                                    </button>
                                </a>
                            </div>
                        </div>
                    @endif
                </div>
                <div>
                    <category></category>
                </div>
            </div>
        </div>
        <div class="d-flex justify-content-between w-100 my-5 nav-speciality-wrapper">
            <div class="d-flex justify-content-between w-50">
                <div class="w-25">
                    <img class="nav-specality-image" src="/storage/custom-nav/settings.svg" alt="settings-svg">
                </div>
                @yield('spec')
            </div>
            <div class="custom-nav-search-wrapper">
                <a href="">
                    <img class="custom-nav-search-image" src="/storage/nav/settings-icon.png" alt="search">
                </a>
            </div>
        </div>
    </div>
</nav>
