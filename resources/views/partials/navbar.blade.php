<nav class="main-navbar">
    <div class="navbar-content">
        <a href="{{route('home')}}">
            <img class="logo" src="/storage/custom-nav/logo.png" alt="logo">
        </a>

        <ul class="user-links">
            @if(Auth::check())
                @if(Auth::user()->is_verified == 0)
                    <li>
                        <a href="#" class="unverified-link">
                            <i class="fas fa-exclamation"></i>
                        </a>
                        <ul class="nav-dropdown user-link-options unverified-info">
                            <li>Unverified account</li>
                        </ul>
                    </li>
                @endif
                <li>
                    <a href="{{route('home')}}">
                        <i class="fas fa-home"></i>
                    </a>
                </li>
                <li>
                    <chat-counter :user="{{auth()->user()}}" :to="{{auth()->user()->to}}"></chat-counter>
                    <a href="{{route('msg')}}" class="messages-link">
                        <i class="far fa-envelope"></i>
                    </a>

                </li>
                    <notif :user="{{auth()->user()}}"></notif>
                <li>
                    <a href="#" class="options-link">
                        <i class="far fa-user"></i>
                    </a>
                    <ul class="nav-dropdown user-link-options-notification options-info navbar-options-dropdown">
                        <li><a href="{{route('users')}}">{{Auth::user()->name}}</a></li>
                        <li><a href="{{route('users-my-events')}}">My events</a></li>
                        <li><a href="{{route('users-my-career')}}">My Career & Paid opp</a></li>
                        @can('admin')
                        <li><a href="{{route('admin-index', 'admin')}}">Admin panel</a></li>
                        <li><a href="{{route('admin-view-users')}}">Users</a></li>
                        <li><a href="{{route('categories-posts-create', 'news-articles')}}">Write new post</a></li>
                        @endcan
                        <li><a href="{{route('users-page', 'user')}}">User information</a></li>
                        <li><a href="{{route('users-page', 'profile')}}">Profile information</a></li>
                        @can('admin-enterprise')
                        <li><a href="{{route('analyze')}}">Analyze</a></li>
                        @endcan
                        <li>
                            <a href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">Exit</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                @csrf
                            </form>
                        </li>
                        @can('dev')
                        <li>
                            <a href="{{route('new-layout')}}">
                                Layout
                            </a>
                        </li>
                        @endcan
                    </ul>
                </li>
            @endif
            @guest
                <li>
                    <a href="{{ route('login') }}">
                        <i class="fas fa-sign-in-alt"></i>
                    </a>
                </li>
                <li>
                    <a href="{{ route('register') }}">
                        <i class="fas fa-user-plus"></i>
                    </a>
                </li>
            @endguest
            <li>
                <a href="#" class="lang-link">
                    <i class="fas fa-globe-asia"></i>
                </a>
                <ul class="nav-dropdown user-link-options lang-info navbar-options-dropdown">
                    <li><a href="/lang/en">EN</a></li>
                    <li><a href="/lang/vi">VI</a></li>
                </ul>
            </li>
        </ul>

        <div class="desktop-links-row">
                <ul class="desktop-links">
                    <li>
                        <a href="/categories/news-articles">News & Articles</a>
                    </li>
                    <li>
                        <a href="/categories/drug-info-updates">Drug info & updates</a>
                    </li>
                    <li>
                        <a href="/categories/events">Events</a>
                    </li>
                    <li>
                        <a href="/categories/career-paid-opportunity" class="career">Career & paid opportunities</a>
{{--                        <ul class="nav-dropdown menu-link-options career-menu navbar-options-dropdown">--}}
{{--                            <li><a href="/subcategories/surveys-6">Surveys</a></li>--}}
{{--                            <li><a href="/subcategories/clinical-studies-6">Clinical studies</a></li>--}}
{{--                            <li><a href="/subcategories/products-preview-6">Products previews</a></li>--}}
{{--                            <li><a href="/subcategories/job-6">Job</a></li>--}}
{{--                        </ul>--}}
                    </li>
                    <li>
                        <a href="/categories/discussion">Discussion</a>
                    </li>
                </ul>
        </div>

        <div class="mobile-links-row">
            <a class="hamburger-link" href="#">
                <i class="fas fa-bars"></i>
            </a>
                <ul class="nav-dropdown hamburger-menu navbar-options-dropdown">
                    <li><a href="/categories/news-articles">News & articles</a></li>
                    <li><a href="/categories/drug-info-updates">Drug info & updates</a></li>
                    <li><a href="/categories/events">Events</a></li>
                    <li><a href="/categories/career-paid-opportunity">Career & paid opportunities</a></li>
                    <li><a href="/categories/discussion">Discussion</a></li>
                </ul>
        </div>
        @can('admin-generic')
            <ul class="filters">
                @yield('spec')
                @yield('search')
            </ul>
        @endcan
    </div>
</nav>
