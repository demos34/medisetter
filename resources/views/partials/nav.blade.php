<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        MediSetter
    </h2>
    <div class="custom-nav">
        <div class="custom-nav-sections">
            <div class="custom-dropdown">
                <a href="">
                    News & articles
                </a>
                <div class="custom-dropdown-content">
                    <p>Hello World!</p>
                </div>
            </div>
        </div>
        <div class="custom-nav-sections">
            <div>
                <a href="">
                    Drug info & updates
                </a>
            </div>

        </div>
        <div class="custom-nav-sections">
            <div>
                <a href="">
                    Test your knowledge
                </a>
            </div>

        </div>
        <div class="custom-nav-sections">
            <div>
                <a href="">
                    Events
                </a>
            </div>

        </div>
        <div class="custom-nav-sections">
            <div>
                <a href="">
                    Discussion
                </a>
            </div>

        </div>
        <div class="custom-nav-sections">
            <div>
                <a href="">
                    Career and paid opportunities
                </a>
            </div>
        </div>
    </div>
</x-slot>
