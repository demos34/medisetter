@extends('layouts.v2.v2')
@section('custom-css')
    <link rel="stylesheet" href="{{ asset('css/newview/layout.css') }}">
@endsection
@section('content')
    @if(Auth::check())
        <new-no-login></new-no-login>
        <div class="sub-nav">
            <div class="d-flex justify-content-between w-100 p-4">
                <div>
                    <img class="w-50" src="/storage/custom-nav/logo.png" alt="logo">
                </div>
                <div>
                    <a class="right-drop-link">
                        <i class="fas fa-bars"></i>
                    </a>
                    <ul class="nav-dropdown right-drop-link-menu">
                        <li class="right-drop-link-menu-header d-flex justify-content-between">
                            <a href="/new/log-in">
                                <i class="fas fa-sign-in-alt"></i>
                            </a>
                            <a class="lang-link">
                                <i class="fas fa-globe-asia"></i>
                            </a>
                            <ul class="nav-dropdown user-link-options lang-info navbar-options-dropdown">
                                <li><a href="/lang/en">EN</a></li>
                                <li><a href="/lang/vi">VI</a></li>
                            </ul>
                            <a href="/new/log-in">
                                <i class="fas fa-user-plus"></i>
                            </a>
                        </li>
                        <li class="right-drop-link-menu-header">
                            Categories
                        </li>
                        <li><a href="/categories/news-articles">News & articles</a></li>
                        <li><a href="/categories/drug-info-updates">Drug info & updates</a></li>
                        <li><a href="/categories/events">Events</a></li>
                        <li><a href="/categories/career-paid-opportunities">Career & paid opportunities</a></li>
                        <li><a href="/categories/discussion">Discussion</a></li>

                        <li class="right-drop-link-menu-header">
                            Help
                        </li>
                        <li><a href="#">Help & support</a></li>
                        <li><a href="#">Contact us</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="new-home-image">
            <img src="https://medisetter.com/en/images/banner-img.jpg?lan=en" alt="medisetter">
        </div>
        <div class="new-body-wrapper">
            <div class="new-body">
                <div class="new-posts">
                    <div class="new-posts-top">
                        Some kind of allert ->
                        <a href="">
                            <button class="btn btn-outline-danger">
                                Read more...
                            </button>
                        </a>
                    </div>
                    <div>
                        <span>
                            List of posts (down below) will be infinity scroll menu (like facebook)
                        </span>
                    </div>
                    <div class="new-posts-post">
                        <div class="new-post-category">
                            News & articles
                        </div>
                        <div class="new-post-title">
                            What is Lorem Ipsum?
                        </div>
                        <div class="new-post-image">
                            <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large" alt="post">
                        </div>
                        <div class="new-post-body">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.
                        </div>
                        <div class="new-post-date">
                            15 dec 2020
                        </div>
                        <div class="new-post-buttons">
                            <button class="btn btn-outline-primary">
                                Read more
                            </button>
                            <button>
                                share
                            </button>
                        </div>
                        <div class="new-post-footer">
                        </div>
                    </div>
                    <div class="new-posts-post">
                        <div class="new-post-category">
                            News & articles
                        </div>
                        <div class="new-post-title">
                            What is Lorem Ipsum?
                        </div>
                        <div class="new-post-image">
                            <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large" alt="post">
                        </div>
                        <div class="new-post-body">
                            Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type and scrambled it to make a type specimen book. It has survived not only five
                            centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
                            It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
                            passages, and more recently with desktop publishing software like Aldus PageMaker including
                            versions of Lorem Ipsum.
                        </div>
                        <div class="new-post-date">
                            15 dec 2020
                        </div>
                        <div class="new-post-buttons">
                            <button class="btn btn-outline-primary">
                                Read more
                            </button>
                            <button>
                                share
                            </button>
                        </div>
                        <div class="new-post-footer">
                        </div>
                    </div>
                </div>
            </div>
            <div class="new-newest">
                <div class="new-top-rated">
                    <div class="new-top-title">
                        <span>
                            Top rated by categories
                        </span>
                    </div>
                    <div class="new-top-post">
                        <div class="my-2">
                            <a href="">
                                <div class="new-top-post-category">
                                    News & articles
                                </div>
                                <div class="new-top-post-body">
                                    <div class="new-top-post-title">
                                        What is lorem ipsum?
                                    </div>
                                    <div class="new-top-post-image">
                                        <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                             alt="post">
                                    </div>
                                </div>
                                <div class="new-top-post-footer" />
                            </a>
                        </div>
                    </div>
                </div>
                <div class="new-top-post">
                    <div class="my-2">
                        <a href="">
                            <div class="new-top-post-category">
                                Drug info & updates
                            </div>
                            <div class="new-top-post-body">
                                <div class="new-top-post-title">
                                    What is lorem ipsum?
                                </div>
                                <div class="new-top-post-image">
                                    <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                         alt="post">
                                </div>
                            </div>
                            <div class="new-top-post-footer">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="new-top-post">
                    <div class="my-2">
                        <a href="">
                            <div class="new-top-post-category">
                                Events
                            </div>
                            <div class="new-top-post-body">
                                <div class="new-top-post-title">
                                    What is lorem ipsum?
                                </div>
                                <div class="new-top-post-image">
                                    <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                         alt="post">
                                </div>
                            </div>
                            <div class="new-top-post-footer">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="new-top-post">
                    <div class="my-2">
                        <a href="">
                            <div class="new-top-post-category">
                                Discussion
                            </div>
                            <div class="new-top-post-body">
                                <div class="new-top-post-title">
                                    What is lorem ipsum?
                                </div>
                                <div class="new-top-post-image">
                                    <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                         alt="post">
                                </div>
                            </div>
                            <div class="new-top-post-footer">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="new-top-post">
                    <div class="my-2">
                        <a href="">
                            <div class="new-top-post-category">
                                Career & paid opportunities
                            </div>
                            <div class="new-top-post-body">
                                <div class="new-top-post-title">
                                    What is lorem ipsum?
                                </div>
                                <div class="new-top-post-image">
                                    <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                         alt="post">
                                </div>
                            </div>
                            <div class="new-top-post-footer">
                            </div>
                        </a>
                    </div>
                </div>
            </div>

        </div>
        </div>
    @endif
@endsection
