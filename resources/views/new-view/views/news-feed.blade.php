@extends('layouts.v2.v2')
@section('custom-css')
    <link rel="stylesheet" href="{{ asset('css/newview/layout.css') }}">
@endsection
@section('content')
    @if(Auth::check())
        <new-nav :user="{{auth()->user()}}" :to="{{auth()->user()->to}}"></new-nav>
        <div class="sub-nav">
            <div class="d-flex justify-content-between w-100 p-4">
                <div>
                    <img class="w-50" src="/storage/custom-nav/logo.png" alt="logo">
                </div>
                <div>
                    <a class="right-drop-link">
                        <i class="fas fa-bars"></i>
                    </a>
                    <ul class="nav-dropdown right-drop-link-menu">
                        <li class="right-drop-link-menu-header d-flex justify-content-between">
                            <a href="#" class="options-link">
                                <i class="far fa-user"></i>
                            </a>
                            <ul class="nav-dropdown user-link-options-notification options-info navbar-options-dropdown">
                                <li><a href="">Username</a></li>
                                <li><a href="">Admin panel</a></li>
                                <li><a href="">Users</a></li>
                                <li><a href="">Write new post</a></li>
                                <li><a href="">User information</a></li>
                                <li><a href="">Profile information</a></li>
                                <li><a href="">Analyze</a></li>
                                <li>
                                    <a href=""
                                       onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();">Exit</a>
                                    <form id="logout-form" action="" method="POST">
                                        @csrf
                                    </form>
                                </li>
                                <li>
                                    <a href="">
                                        Layout
                                    </a>
                                </li>
                            </ul>
                            <a class="lang-link">
                                <i class="fas fa-globe-asia"></i>
                            </a>
                            <ul class="nav-dropdown user-link-options lang-info navbar-options-dropdown">
                                <li><a href="/lang/en">EN</a></li>
                                <li><a href="/lang/vi">VI</a></li>
                            </ul>
                            <a href="">
                                <i class="fas fa-home"></i>
                            </a>
                        </li>
                        <li class="right-drop-link-menu-header">
                            Categories
                        </li>
                        <li><a href="/categories/news-articles">News & articles</a></li>
                        <li><a href="/categories/drug-info-updates">Drug info & updates</a></li>
                        <li><a href="/categories/events">Events</a></li>
                        <li><a href="/categories/career-paid-opportunities">Career & paid opportunities</a></li>
                        <li><a href="/categories/discussion">Discussion</a></li>

                        <li class="right-drop-link-menu-header">
                            Help
                        </li>
                        <li><a href="#">Help & support</a></li>
                        <li><a href="#">Contact us</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="new-home-image">
            <img src="https://medisetter.com/en/images/banner-img.jpg?lan=en&__cf_chl_jschl_tk__=204819986b135d2ecdc47ed8e5f48f3dfae75609-1608207614-0-AXCwuzo7dgWF50s1Ng3Jam0OgM32nMm4a3wLWItcQC8SsNmCO_EbU8dJ8gfqMacdMzw6I69DutXjUhHQ81QnrnBEZzuhp2rqcjlu1l-TMEV_cvdJZF0uk_BnXU_jCZJDjjG1PaKtde5lmYhSBw9tG0tzyv5gdzkF6_j-7z_6MWCFtrhjEwQSIc0wIMxkCksFpdDw4lfqaXDHagUSeYLMroB0F1fnaISLX-JX_wMaMYMrqWBMKVJ4uakTn841rGmLilamkG8cQ5W95KY5ZeQaqTD-FYAnxPMLiaemA9cFqHvOazVo3lwHbhdcQX1Ri-ZVeccy6e9SVK0T-j7SSBl4_F3I_ivqbW26sldp5LGfwvNS" alt="medisetter">
        </div>
        <div class="new-newsfeed">
            <div class="new-newsfeed-profile-wrapper">
                <div class="new-newsfeed-profile">
                    <div class="new-newsfeed-profile-top">
                    </div>
                    <div class="new-newsfeed-profile-info">
                        <img class="new-newsfeed-profile-avatar" src="{{Auth::user()->avatar}}"
                             alt="{{Auth::user()->avatar}}">
                        <div class="new-newsfeed-profile-user">
                            <strong>
                                Username
                            </strong>
                            <br>
                            <strong>Administrator</strong>
                            <br>
                            <span>
                                Education: Specialist
                            </span>
                            <br>
                            <span>
                                Followed: 10
                            </span>
                            <br>
                            <span>
                                Following: 10
                            </span>
                        </div>
                        <div class="new-newsfeed-profile-user">
                            <span>
                                Total posts:
                            </span>
                            <br>
                            <strong>
                                50
                            </strong>
                        </div>
                    </div>
                </div>
            </div>
            <div class="new-newsfeed-news-wrapper">
                <div class="new-newsfeed-news">
                    <div class="new-newsfeed-news-rapid-post">
                        <form class="new-newsfeed-news-rapid-post-form">
                            @csrf
                            <input class="new-newsfeed-form-textarea mb-2" placeholder="Rapid title...">

                            <textarea type="text" name="post" class="new-newsfeed-form-textarea"
                                      placeholder="Start post here..."></textarea>
                            <div class="d-flex justify-content-between mx-auto w-80">
                                <div>
                                    <label for="video">
                                        <span>
                                            Video
                                        </span>
                                <span class="material-icons">
                                   live_tv
                                </span>
                                    </label>
                                    <input type="file" id="video" style="display: none"/>
                                </div>
                                <div>
                                    <label for="document" class="align-items-center text-center">
                                        <span>
                                            Document
                                        </span>
                                       <span class="material-icons">
                                            article
                                       </span>
                                    </label>
                                    <input type="file" id="document" style="display: none"/>
                                </div>
                                <div>
                                    <label for="image">

                                        <span>Image
                                        </span>
                                    <span class="material-icons">
                                        camera_enhance
                                    </span>
                                    </label>
                                    <input type="file" id="image" style="display: none"/>
                                </div>
                                <div>
                                        <span>
                                            Go!
                                        </span>
                                    <button class="new-newsfeed-button">
                                        <span class="material-icons">
                                            publish
                                        </span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="new-newsfeed-body-wrapper">
                        <span>
                            List of posts (down below) will be infinity scroll menu (like facebook)
                        </span>
                        <div class="new-newsfeed-body">
                            <div class="new-newsfeed-body-user">
                                <img src="{{Auth::user()->avatar}}" alt="{{Auth::user()->name}}">
                                <div>
                                    <strong>
                                        {{Auth::user()->name}}
                                    </strong>
                                    <br>
                                    <small>
                                        15 dec 2020
                                    </small>
                                </div>
                            </div>
                            <hr>
                            <div class="new-newsfeed-body-title">
                                What is Lorem Ipsum?
                            </div>
                            <div class="new-newsfeed-body-body p-3">
                                {{Str::limit('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 190)}}
                                <div class="text-right mr-3">
                                    <a href="">see more...</a>
                                </div>
                            </div>
                            <hr>
                            <div class="new-newsfeed-body-image">
                                <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                     alt="post">
                            </div>
                            <div class="new-newsfeed-body-counters my-2 p-3">
                                <div class="comments">
                                    <span><i class="material-icons-outlined activity-icon">comment</i></span>
                                    <span>5</span>
                                </div>
                                <div class="views">
                                    <span><i class="material-icons-outlined activity-icon">remove_red_eye</i></span>
                                    <span>22</span>
                                </div>
                                <div class="likes">
                                    <span><i class="material-icons-outlined activity-icon">thumb_up_alt</i></span>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="new-newsfeed-body">
                            <div class="new-newsfeed-body-user">
                                <img src="{{Auth::user()->avatar}}" alt="{{Auth::user()->name}}">
                                <div>
                                    <strong>
                                        {{Auth::user()->name}}
                                    </strong>
                                    <br>
                                    <small>
                                        15 dec 2020
                                    </small>
                                </div>
                            </div>
                            <hr>
                            <div class="new-newsfeed-body-title">
                                What is Lorem Ipsum?
                            </div>
                            <div class="new-newsfeed-body-body p-3">
                                {{Str::limit('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 190)}}
                                <div class="text-right mr-3">
                                    <a href="">see more...</a>
                                </div>
                            </div>
                            <hr>
                            <div class="new-newsfeed-body-image">
                                <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                     alt="post">
                            </div>
                            <div class="new-newsfeed-body-counters my-2 p-3">
                                <div class="comments">
                                    <span><i class="material-icons-outlined activity-icon">comment</i></span>
                                    <span>5</span>
                                </div>
                                <div class="views">
                                    <span><i class="material-icons-outlined activity-icon">remove_red_eye</i></span>
                                    <span>22</span>
                                </div>
                                <div class="likes">
                                    <span><i class="material-icons-outlined activity-icon">thumb_up_alt</i></span>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="new-newsfeed-body">
                            <div class="new-newsfeed-body-user">
                                <img src="{{Auth::user()->avatar}}" alt="{{Auth::user()->name}}">
                                <div>
                                    <strong>
                                        {{Auth::user()->name}}
                                    </strong>
                                    <br>
                                    <small>
                                        15 dec 2020
                                    </small>
                                </div>
                            </div>
                            <hr>
                            <div class="new-newsfeed-body-title">
                                What is Lorem Ipsum?
                            </div>
                            <div class="new-newsfeed-body-body p-3">
                                {{Str::limit('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 190)}}
                                <div class="text-right mr-3">
                                    <a href="">see more...</a>
                                </div>
                            </div>
                            <hr>
                            <div class="new-newsfeed-body-image">
                                <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                     alt="post">
                            </div>
                            <div class="new-newsfeed-body-counters my-2 p-3">
                                <div class="comments">
                                    <span><i class="material-icons-outlined activity-icon">comment</i></span>
                                    <span>5</span>
                                </div>
                                <div class="views">
                                    <span><i class="material-icons-outlined activity-icon">remove_red_eye</i></span>
                                    <span>22</span>
                                </div>
                                <div class="likes">
                                    <span><i class="material-icons-outlined activity-icon">thumb_up_alt</i></span>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                        <div class="new-newsfeed-body">
                            <div class="new-newsfeed-body-user">
                                <img src="{{Auth::user()->avatar}}" alt="{{Auth::user()->name}}">
                                <div>
                                    <strong>
                                        {{Auth::user()->name}}
                                    </strong>
                                    <br>
                                    <small>
                                        15 dec 2020
                                    </small>
                                </div>
                            </div>
                            <hr>
                            <div class="new-newsfeed-body-title">
                                What is Lorem Ipsum?
                            </div>
                            <div class="new-newsfeed-body-body p-3">
                                {{Str::limit('Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', 190)}}
                                <div class="text-right mr-3">
                                    <a href="">see more...</a>
                                </div>
                            </div>
                            <hr>
                            <div class="new-newsfeed-body-image">
                                <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                     alt="post">
                            </div>
                            <div class="new-newsfeed-body-counters my-2 p-3">
                                <div class="comments">
                                    <span><i class="material-icons-outlined activity-icon">comment</i></span>
                                    <span>5</span>
                                </div>
                                <div class="views">
                                    <span><i class="material-icons-outlined activity-icon">remove_red_eye</i></span>
                                    <span>22</span>
                                </div>
                                <div class="likes">
                                    <span><i class="material-icons-outlined activity-icon">thumb_up_alt</i></span>
                                    <span>8</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="new-newest">
                <div class="new-top-rated">
                    <div class="new-top-title">
                        <span>
                            Top rated by categories
                        </span>
                    </div>
                    <div class="new-top-post">
                        <div class="my-2">
                            <a href="">
                                <div class="new-top-post-category">
                                    News & articles
                                </div>
                                <div class="new-top-post-body">
                                    <div class="new-top-post-title">
                                        What is lorem ipsum?
                                    </div>
                                    <div class="new-top-post-image">
                                        <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                             alt="post">
                                    </div>
                                </div>
                                <div class="new-top-post-footer"/>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="new-top-post">
                    <div class="my-2">
                        <a href="">
                            <div class="new-top-post-category">
                                Drug info & updates
                            </div>
                            <div class="new-top-post-body">
                                <div class="new-top-post-title">
                                    What is lorem ipsum?
                                </div>
                                <div class="new-top-post-image">
                                    <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                         alt="post">
                                </div>
                            </div>
                            <div class="new-top-post-footer">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="new-top-post">
                    <div class="my-2">
                        <a href="">
                            <div class="new-top-post-category">
                                Events
                            </div>
                            <div class="new-top-post-body">
                                <div class="new-top-post-title">
                                    What is lorem ipsum?
                                </div>
                                <div class="new-top-post-image">
                                    <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                         alt="post">
                                </div>
                            </div>
                            <div class="new-top-post-footer">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="new-top-post">
                    <div class="my-2">
                        <a href="">
                            <div class="new-top-post-category">
                                Discussion
                            </div>
                            <div class="new-top-post-body">
                                <div class="new-top-post-title">
                                    What is lorem ipsum?
                                </div>
                                <div class="new-top-post-image">
                                    <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                         alt="post">
                                </div>
                            </div>
                            <div class="new-top-post-footer">
                            </div>
                        </a>
                    </div>
                </div>
                <div class="new-top-post">
                    <div class="my-2">
                        <a href="">
                            <div class="new-top-post-category">
                                Career & paid opportunities
                            </div>
                            <div class="new-top-post-body">
                                <div class="new-top-post-title">
                                    What is lorem ipsum?
                                </div>
                                <div class="new-top-post-image">
                                    <img src="https://pbs.twimg.com/media/Ct8ctaRVUAQeKRG?format=jpg&name=large"
                                         alt="post">
                                </div>
                            </div>
                            <div class="new-top-post-footer">
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
