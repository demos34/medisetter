@extends('layouts.v2.v2')
@section('custom-css')
    <link rel="stylesheet" href="{{ asset('css/newview/layout.css') }}">
@endsection
@section('content')
    @if(Auth::check())
    <new-nav :user="{{auth()->user()}}" :to="{{auth()->user()->to}}"></new-nav>
    @endif
    <div class="text-center font-weight-bolder my-5">
        <p>
            <a href="{{route('new-layout-logout')}}">
                Logout view
            </a>
        </p>
        <p>
            <a href="{{route('new-layout-news-feed')}}">
                Login view - news feed
            </a>
        </p>
{{--        <p>--}}
{{--            <a href="#">--}}
{{--                categories view - not hosted yet!--}}
{{--            </a>--}}
{{--        </p>--}}
    </div>
@endsection
