<div class="mx-auto my-4 discussion-post-post">
    <div class="discussion-author-wrapper">
        <a href="{{route('user-view', $post->author->user_id)}}" class="w-100">
        <img class="discussion-author-avatar" src="{{$post->author->user->profile->avatar}}" alt="">
        </a>
        <p class="discussion-author-name">{{$post->author->user->name}}</p>
        @if(implode(',', $post->author->user->roles()->get()->pluck('role')->toArray()) === 'Administrator')
            <p class="discussion-author-role">
                {{implode(',', $post->author->user->roles()->get()->pluck('role')->toArray())}}
            </p>
        @endif
        <div class="date-picker">
            DATE: <strong>{{$post->created_at}}</strong>
        </div>
        <div class="discussion-icons-wrapper-post">
            <div class="comments">
                <span><i class="material-icons-outlined activity-icon">comment</i></span>
                <span>{{$post->comments->count()}}</span>
            </div>
            <div class="views">
                <span><i class="material-icons-outlined activity-icon">remove_red_eye</i></span>
                <span>{{$post->views->count()}}</span>
            </div>
            <div class="likes">
                <span><i class="material-icons-outlined activity-icon">thumb_up_alt</i></span>
                <span>{{$post->postReactions->where('reaction_id', 1)->count()}}</span>
            </div>
        </div>

        @can('admin')
            <div class="d-flex justify-content-between mx-2">
                <div>
                    <a href="{{route('admin-posts-restore', $post->slug)}}">
                        <button class="btn btn-outline-warning">
                            Restore
                        </button>
                    </a>
                </div>
                <div>
                    <form action="{{route('admin-posts-hard-delete', $post->slug)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-outline-danger" onclick="return confirm('Are you sure? This will destroy permanently post!')">
                            Delete
                        </button>
                    </form>
                </div>
            </div>
        @endcan
    </div>
    <div class="discussion-post-wrapper">
        <div class="w-100 mx-auto text-center">
            <strong>Category:</strong> <strong class="text-uppercase">{{$post->category->en_category}}</strong>
        </div>
        <div class="news-articles-attached-users-specialities">
            <div>
                <strong>
                    @foreach($post->specialities as $speciality)
                        <span>
                            <a href="">
                                | {{$speciality->en_name}} |
                            </a>
                        </span>
                    @endforeach
                </strong>
            </div>
            <div>
                <strong>
                    @foreach($post->users as $user)
                        <span>
                            <a href="{{route('user-view', $user->id)}}">
                                | {{$user->name}} |
                            </a>
                        </span>
                    @endforeach
                </strong>
            </div>
            <div>
                @if($post->code_id !== NULL)
                    <strong>
                        <u>
                            CODE
                        </u>
                    </strong>
                    <br>
                    <strong>
                        {{$post->code->code}}
                    </strong>
                @endif
            </div>
        </div>
        <div>
            @if($post->event)
                <div class="row mx-auto px-5 my-3">
                    <div class="col-md-6">
                        <strong class="text-uppercase">
                            <u>
                                Start:
                            </u>
                        </strong>
                        <strong>
                            {{$post->event->start_date}}
                        </strong>
                    </div>
                    <div class="col-md-6">
                        <strong class="text-uppercase">
                            <u>
                                End:
                            </u>
                        </strong>
                        <strong>
                            {{$post->event->end_date}}
                        </strong>
                    </div>
                </div>
            @endif
        </div>

        <div class="d-flex justify-content-between px-5">
            <div class="p-1 w-50 text-justify">
                <strong>
                    Title:
                </strong>
            </div>
            <div class="w-25">
                @if($post->video === NULL)
                    <span class="material-icons">
                       live_tv
                    </span>
                @else
                <a href="{{$post->video}}" target="_blank">
                    <span class="material-icons">
                       live_tv
                    </span>
                </a>
                @endif
            </div>
            <div class="w-25">
                @if($post->document === NULL)
                    <span class="material-icons">
                        assignment
                    </span>
                @else
                <a href="{{$post->document}}" target="_blank">
                    <span class="material-icons">
                        assignment
                    </span>
                </a>
                @endif
            </div>
        </div>
        <div class="text-justify px-5 font-italic">
            <strong>
                {{$post->title}}
            </strong>
        </div>
        <div class="d-flex justify-content-center">
            <img src="{{$post->image}}" alt="{{$post->image}}">
        </div>
        <div class="discussion-post-body ">
            <div class="p-3">
                @foreach(explode(PHP_EOL, $post->body) as $paragraph)
                    <p>
                        {{$paragraph}}
                    </p>
                @endforeach
            </div>
            <div class="discussion-post-reaction">
                <div class="post-reaction-wrapper">
                        <div class="post-reaction">
                            @foreach($reacts as $react)
                                <a href="">
                                    <img src="{{$react->image}}" alt="{{$react->image}}">
                                </a>
                                <strong class="my-auto">
                                    {{$post->postReactions->where('reaction_id', $react->id)->count()}}
                                </strong>
                            @endforeach
                        </div>
                        <div class="share m-auto">
                            <button class="notification" data-modal-target="#modal">
                                <span class="material-icons">
                                    share
                                </span>
                            </button>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
