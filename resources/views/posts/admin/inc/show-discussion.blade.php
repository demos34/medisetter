<div class="mx-auto my-4 discussion-post-post">
    <div class="discussion-author-wrapper">
        <img class="discussion-author-avatar" src="{{$post->author->user->profile->avatar}}" alt="">
        <p class="discussion-author-name">{{$post->author->user->name}}</p>
        @if(implode(',', $post->author->user->roles()->get()->pluck('role')->toArray()) === 'Administrator')
            <p class="discussion-author-role">
                {{implode(',', $post->author->user->roles()->get()->pluck('role')->toArray())}}
            </p>
        @endif
        <div class="date-picker">
            DATE: <strong>{{$post->created_at}}</strong>
        </div>
        <div class="discussion-icons-wrapper-post">
            <div class="comments">
                <span><i class="material-icons-outlined activity-icon">comment</i></span>
                <span>{{$post->comments->count()}}</span>
            </div>
            <div class="views">
                <span><i class="material-icons-outlined activity-icon">remove_red_eye</i></span>
                <span>{{$post->views->count()}}</span>
            </div>
            <div class="likes">
                <span><i class="material-icons-outlined activity-icon">thumb_up_alt</i></span>
                <span>{{$post->postReactions->where('reaction_id', 1)->count()}}</span>
            </div>
        </div>
        @can('admin')
            <div class="d-flex justify-content-between mx-2">
                <div>
                    <a href="{{route('admin-posts-restore', $post->slug)}}">
                        <button class="btn btn-outline-warning">
                            Restore
                        </button>
                    </a>
                </div>
                <div>
                    <form action="{{route('admin-posts-hard-delete', $post->slug)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-outline-danger" onclick="return confirm('Are you sure? Whis will destroy permanently post!')">
                            Delete
                        </button>
                    </form>
                </div>
            </div>
        @endcan
    </div>
    <div class="discussion-post-wrapper">

        <div class="w-100 mx-auto text-center">
            <strong>Category:</strong> <strong class="text-uppercase">{{$post->category->en_category}}</strong>
        </div>
        <div class="discussion-shared-section mx-auto text-center mt-2">
            @if($post->is_shared === 1)
                <strong>
                    {{$post->author->user->name}}
                </strong>
                share post of
                <strong>{{$post->share->post->author->user->name}}:</strong>
                @if($post->share->title != NULL)
                {{$post->share->title}}
                @endif
                </strong>

            @endif
        </div>
        <div class="d-flex justify-content-between px-5">
            <div class="p-1 w-50 text-justify">
                <strong>
                    Title:
                </strong>
            </div>
            <div class="w-25">
                @if($post->video === NULL)
                    <span class="material-icons">
                       live_tv
                    </span>
                @else
                    <a href="{{$post->video}}" target="_blank">
                    <span class="material-icons">
                       live_tv
                    </span>
                    </a>
                @endif
            </div>
            <div class="w-25">
                @if($post->document === NULL)
                    <span class="material-icons">
                        assignment
                    </span>
                @else
                    <a href="{{$post->document}}" target="_blank">
                    <span class="material-icons">
                        assignment
                    </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="text-justify px-5 font-italic">
            <strong>
                {{$post->title}}
            </strong>
        </div>
        <div class="w-100">
            <img class="w-80" src="{{$post->image}}" alt="{{$post->image}}">
        </div>
        <div class="discussion-post-body">
            <div class="p-3">
                @foreach(explode(PHP_EOL, $post->body) as $paragraph)
                    <p>
                        {{$paragraph}}
                    </p>
                @endforeach
            </div>
            <div class="discussion-post-reaction">
                <div class="post-reaction-wrapper">
                    @if(Auth::check())
                        <div class="post-reaction">
                            @foreach($reacts as $react)
                                <a href="">
                                    <img src="{{$react->image}}" alt="{{$react->image}}">
                                </a>
                                <strong class="my-auto">
                                    {{$post->postReactions->where('reaction_id', $react->id)->count()}}
                                </strong>
                            @endforeach
                            <a href="#">
                                <img src="/storage/reactions/comments.svg" alt="comments">
                            </a>
                            <strong class="my-auto">
                                {{$post->comments()->count()}}
                            </strong>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
