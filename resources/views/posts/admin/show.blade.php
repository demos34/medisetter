<x-app-layout>
    <div class="main-category-body">
        <div>
            <div id="custom-overlay">
                @if($post->category->id == 5)
                    @include('posts.admin.inc.show-' . $post->category->slug)
                @else
                    @include('posts.admin.inc.show')
                @endif
            </div>
        </div>
    </div>
</x-app-layout>
