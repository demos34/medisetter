<x-app-layout>
    <div class="main-category-body">
        <div class="w-100">
            <div class="w-100">
                   <div class="w-100 text-center">
                       <strong class="category-title" id="{{$current->slug}}">
                           Create new post!
                       </strong>
                   </div>
                @include('posts.categories.inc.create-form')
            </div>
        </div>
    </div>
</x-app-layout>
