@section('custom-css')
<link rel="stylesheet" href="{{ asset('css/modal.css') }}">
@endsection
@section('spec')
<spec :category="{{$post->category}}"></spec>
@endsection
@section('search')

@endsection
<x-app-layout>
    <div class="main-category-body">
        <div id="modal-full-overlay">
            <div class="custom-modal" id="modal">
                <div class="custom-modal-header">
                    <div></div>
                    <button data-close-button class="custom-close-button">
                        &times;
                    </button>
                </div>
                <div class="custom-modal-body">
                    <form action="{{route('posts-share', $post->slug)}}"
                          method="post">
                        @csrf
                        @method('PATCH')
                        <label for="title">Say something about it:</label>
                        <br>
                        <input class="form-control my-2" id="title" name="title">
                        <div class="text-center my-2">
                            <button class="btn btn-primary">Share!</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div id="custom-overlay">
            @if($post->category->id == 5)
            @include('posts.categories.inc.show-' . $post->category->slug)
            @else
            @include('posts.categories.inc.show')
            @endif
        </div>
    </div>
</x-app-layout>
@section('custom-js')
<script src="{!! asset('js/modal.js') !!}"></script>
@endsection
