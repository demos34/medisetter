<x-app-layout>
    <div class="main-category-body">
        <div class="w-100">
            <div class="w-100">
                <div class="text-center mb-2">
                    <strong class="category-title" id="{{$current->slug}}">
                        Create new post!
                    </strong>
                </div>
                @include('posts.categories.inc.create-form')
            </div>
        </div>
    </div>
</x-app-layout>
@section('custom-js')
    <script src="{!! asset('js/jquery.js') !!}"></script>
    <script src="{!! asset('js/multiselect.min.js') !!}"></script>
    @can('admin')
        <script src="{!! asset('js/posts-create-ajax.js') !!}"></script>
        <script src="{!! asset('js/multiselect.min.js') !!}"></script>
        <script>
            $(document).ready(function () {
                $(".mul-select").select2({
                    placeholder: "Select", //placeholder
                    tags: true,
                    tokenSeparator: ['/', ',', ',', ""]
                })
            })
        </script>
    @endcan
    <script src="{!! asset('js/multiselect-dis.js') !!}"></script>
@endsection
