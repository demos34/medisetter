<x-app-layout>
    <div class="main-category-body">
        <div class="w-100">
            <div class="category-body-wrapper">
                <div class="w-80 d-flex justify-content-center">
                    <form action="{{route('categories-posts-update-author', $post->slug)}}" method="post" enctype="multipart/form-data" class="w-100 form-group create-posts-form">
                        @csrf
                        @method('PATCH')
                        <div class="w-100">
                            @can('admin')
                                <div class="post-create-wrapper">
                                    <div>
                                        <label for="categories">
                                            Categories
                                        </label>
                                        <select class="select-css text-uppercase" name="categories" id="categories">
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}" @if($category->id == $current->id) selected
                                                        id="selected-cat" @endif>
                                                    {{$category->$cat}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div>
                                        <label for="specialities">
                                            Qualifications
                                        </label>
                                        <select class="select-css mul-select" name="specialities[]" id="specialities"
                                                multiple="true">
                                            @foreach($specialities as $speciality)
                                                <option value="{{$speciality->id}}">
                                                    {{$speciality->$name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div>
                                        <label for="subcategories">
                                            Subcategories
                                        </label>
                                        <select class="select-css text-uppercase" name="subcategories" id="subcategories">
                                            <option value="0">
                                                ---- Please select Subcategory -----
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="post-create-wrapper">
                                    <div>
                                        <label for="users">
                                            Enterprise users
                                        </label>
                                        <select class="select-css mul-select" name="users[]" id="users" multiple="true">
                                            @foreach($users as $user)
                                                <option value="{{$user->id}}">
                                                    {{$user->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="d-none" id="codes">
                                        <label for="selected-codes">
                                            Code
                                        </label>
                                        <br>
                                        <input id="selected-codes"
                                               class="@error('code') is-invalid @enderror create-post-codes-inputs"
                                               name="code"
                                               value="@if($post->code_id !== NULL) @if(old('code')){{ old('code') }}@else{{$post->code->code}}@endif @else{{ old('code') }}@endif"
                                               autocomplete="code">
                                        @error('code')
                                        <br>
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="post-create-wrapper" id="event-dates">
                                    <div>
                                        <label for="start_date">
                                            Event start date
                                        </label>
                                        <br>
                                        <input name="" type="datetime-local" id="start_date" autocomplete>
                                    </div>
                                    <div>
                                        <label for="end_date">
                                            Event end date
                                        </label>
                                        <br>
                                        <input name="" type="datetime-local" id="end_date" autocomplete>
                                    </div>
                                </div>
                            @else
                                <div class="post-create-wrapper">
                                    <div>
                                        <label for="categories">
                                            Categories
                                        </label>
                                        <select class="select-css text-uppercase" name="categories" id="categories">
                                            <option value="{{$current->id}}" selected>
                                                {{$current->$cat}}
                                            </option>
                                        </select>
                                    </div>
                                    <div>
                                        <label for="specialities">
                                            Qualifications
                                        </label>
                                        <select class="select-css mul-select" name="specialities[]" id="specialities"
                                                multiple="multiple">
                                            @foreach($specialities as $speciality)
                                                <option value="{{$speciality->id}}">
                                                    {{$speciality->$name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endcan
                            <div class="create-posts-body">
                                <div class="create-posts-body-title">
                                    <strong>
                                        Edit this post
                                    </strong>
                                </div>
                                <div class="create-posts-inputs">
                                    <div>
                                        <img class="create-posts-body-image rounded-circle" src="{{Auth::user()->profile->avatar}}"
                                             alt="">
                                        <strong>
                                            {{Auth::user()->name}}
                                        </strong>
                                    </div>
                                    <div class="create-posts-textarea-wrapper">
                                        <div class="w-80 mx-auto">
                                            <label for="title">
                                                Post title:
                                            </label>
                                            <input id="title"
                                                   class="create-posts-title-input @error('title') is-invalid @enderror"
                                                   name="title"
                                                   value=@if(old('title')) "{{ old('title') }}" @else "{{$post->title}}" @endif
                                                   required autocomplete="title">
                                            @error('title')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                        <div class="">
                                            <label for="body">
                                            </label>
                                        </div>
                                        <div class="">
                            <textarea id="body"
                                      class="create-posts-textarea @error('body') is-invalid @enderror"
                                      name="body"
                                      required autocomplete="body">@if(old('body')){{ old('body') }}@else{{$post->body}}@endif</textarea>
                                            @error('body')
                                            <br>
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="create-posts-files-wrapper">
                                    <div class="create-posts-files">
                                        <div class="text-center">
                                            <label for="image">
                                                Attach image to post:
                                            </label>
                                            <input type="file" id="image" name="image" class="w-90">
                                        </div>
                                        <div class="text-center">
                                            <label for="youtube">
                                                Attach link to video (youtube):
                                            </label>
                                            <input type="text" id="youtube" name="youtube" class="w-90" value="{{$post->video}}">
                                        </div>
                                        <div class="text-center">
                                            @if($post->document === NULL)
                                                No file
                                            @else
                                                <a href="{{$post->document}}" class="notification">
                                                    Attached file
                                                </a>
                                            @endif
                                        </div>
                                        <div class="text-center">
                                            <label for="file">
                                                Attach file to post:
                                            </label>
                                            <input type="file" id="document" name="file" class="w-90">
                                        </div>
                                    </div>
                                    <div class="mt-4">
                                        <button class="create-post-btn">Post</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @section('custom-js')
                <script type="text/javascript" src="{!! asset('js/jquery.js') !!}"></script>
                <script type="text/javascript" src="{!! asset('js/multiselect.min.js') !!}"></script>
                <script type="text/javascript" src="{!! asset('js/posts-create-ajax.js') !!}"></script>
                @can('admin')
                    <script type="text/javascript" src="{!! asset('js/multiselect.min.js') !!}"></script>
                    <script type="application/javascript">
                        $(document).ready(function () {
                            $(".mul-select").select2({
                                placeholder: "Select", //placeholder
                                tags: true,
                                tokenSeparator: ['/', ',', ',', ""]
                            })
                        })
                    </script>
                @endcan
                <script type="application/javascript" src="{!! asset('js/multiselect-dis.js') !!}"></script>
            @endsection

        </div>
    </div>
</x-app-layout>
@section('custom-js')
    <script src="{!! asset('js/jquery.js') !!}"></script>
    <script src="{!! asset('js/multiselect.min.js') !!}"></script>
    @can('admin')
        <script src="{!! asset('js/posts-create-ajax.js') !!}"></script>
        <script src="{!! asset('js/multiselect.min.js') !!}"></script>
        <script>
            // $(document).ready(function () {
            //     $(".mul-select").select2({
            //         placeholder: "Select", //placeholder
            //         tags: true,
            //         tokenSeparator: ['/', ',', ',', ""]
            //     })
            // })
            $(document).ready(function() {
                $('.mul-select').select2();
                $('#specialities').select2().val({{ json_encode($post->specialities()->get()->pluck('id')->toArray()) }}).trigger('change');
            });

        </script>
    @endcan
    <script src="{!! asset('js/multiselect-dis.js') !!}"></script>
@endsection
