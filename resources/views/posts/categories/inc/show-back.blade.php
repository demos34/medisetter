<div class="mx-auto my-4 discussion-post-post">
    <div class="discussion-author-wrapper">
        <img class="discussion-author-avatar" src="{{$post->author->user->profile->avatar}}" alt="">
        <p class="discussion-author-name">{{$post->author->user->name}}</p>
        @if(implode(',', $post->author->user->roles()->get()->pluck('role')->toArray()) === 'Administrator')
            <p class="discussion-author-role">
                {{implode(',', $post->author->user->roles()->get()->pluck('role')->toArray())}}
            </p>
        @endif
        <div class="date-picker">
            Date: <strong>{{$post->created_at}}</strong>
        </div>
        <div class="discussion-icons-wrapper-post">
            <div class="comments">
                <span><i class="material-icons-outlined activity-icon">comment</i></span>
                <span>{{$post->comments->count()}}</span>
            </div>
            <div class="views">
                <span><i class="material-icons-outlined activity-icon">remove_red_eye</i></span>
                <span>{{$post->views->count()}}</span>
            </div>
            <div class="likes">
                <span><i class="material-icons-outlined activity-icon">thumb_up_alt</i></span>
                <span>{{$post->postReactions->where('reaction_id', 1)->count()}}</span>
            </div>
        </div>
    </div>
    <div class="discussion-post-wrapper">
        <div class="news-articles-attached-users-specialities">
            <div>
                <strong>
                    @foreach($post->specialities as $speciality)
                        <span>
                            <a href="">
                                | {{$speciality->en_name}} |
                            </a>
                        </span>
                    @endforeach
                </strong>
            </div>
            <div>
                <strong>
                    @foreach($post->users as $user)
                        <span>
                            <a href="">
                                | {{$user->name}} |
                            </a>
                        </span>
                    @endforeach
                </strong>
            </div>
        </div>

        <div class="d-flex justify-content-between px-5">
            <div class="p-1 w-50 text-justify">
                <strong>
                    Title:
                </strong>
            </div>
            <div class="w-25">
                <a href="{{$post->video}}" target="_">
                    <span class="material-icons">
                       live_tv
                    </span>
                </a>
            </div>
            <div class="w-25">
                <a href="{{$post->document}}" target="_blank">
                    <span class="material-icons">
                        assignment
                    </span>
                </a>
            </div>
        </div>
        <div class="text-justify px-5 font-italic">
            <strong>
                {{$post->title}}
            </strong>
        </div>
        <div class="d-flex justify-content-center">
            <img src="{{$post->image}}" alt="{{$post->image}}">
        </div>
        <div class="discussion-post-body ">
            <div class="p-3">
                @foreach(explode(PHP_EOL, $post->body) as $paragraph)
                    <p>
                        {{$paragraph}}
                    </p>
                @endforeach
            </div>
            <div class="discussion-post-reaction">
                <div class="post-reaction-wrapper">
                    @if(Auth::check())
                        <div class="post-reaction">
                            @foreach($reacts as $react)
                                <a href="{{route('posts-reactions', [$post->slug , $react->name])}}">
                                    <img src="{{$react->image}}" alt="{{$react->image}}">
                                </a>
                                <strong class="my-auto">
                                    {{$post->postReactions->where('reaction_id', $react->id)->count()}}
                                </strong>
                            @endforeach
                        </div>
                        <div class="share m-auto">
                            <button class="btn btn-light" data-modal-target="#modal">
                                Share
                            </button>
                        </div>
                    @else
                        <strong class="text-center">You must be sign in to react or share!</strong>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@section('custom-js')
    <script src="{!! asset('js/modal.js') !!}"></script>
@endsection
