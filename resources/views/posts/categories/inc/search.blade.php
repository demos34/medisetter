<div class="mx-auto discussion-post-post">
    <div class="discussion-author-wrapper discussion-author-wrapper-mobile">
        <a href="{{route('user-view', $post->author->user_id)}}" class="w-100">
            <img class="discussion-author-avatar" src="{{$post->author->user->profile->avatar}}" alt="">
        </a>
        <p class="discussion-author-name">{{$post->author->user->name}}</p>
        @if(implode(',', $post->author->user->roles()->get()->pluck('role')->toArray()) === 'Administrator')
            <p class="discussion-author-role">
                {{implode(',', $post->author->user->roles()->get()->pluck('role')->toArray())}}
            </p>
        @endif
        <div class="date-picker">
            DATE: <strong>{{$post->created_at}}</strong>
        </div>
        <div class="discussion-icons-wrapper-post">
            <div class="comments">
                <span><i class="material-icons-outlined activity-icon">comment</i></span>
                <span>{{$post->comments->count()}}</span>
            </div>
            <div class="views">
                <span><i class="material-icons-outlined activity-icon">remove_red_eye</i></span>
                <span>{{$post->views->count()}}</span>
            </div>
            <div class="likes">
                <span><i class="material-icons-outlined activity-icon">thumb_up_alt</i></span>
                <span>{{$post->postReactions->where('reaction_id', 1)->count()}}</span>
            </div>
        </div>
        <div class="text-center my-1">
            <a href="{{route('categories-show', $post->category->slug)}}">
                <button class="btn btn-outline-primary">
                    Back
                </button>
            </a>
        </div>
        @can('admin')
            <div class="d-flex justify-content-between mx-2">
                <div class="mx-1">
                    <a href="{{route('categories-posts-edit', $post->slug)}}">
                        <button class="btn btn-outline-primary">
                            Edit
                        </button>
                    </a>
                </div>
                <div class="mx-1">
                    <form action="{{route('posts-delete', $post->slug)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-outline-danger" onclick="return confirm('Are you sure?')">
                            Delete
                        </button>
                    </form>
                </div>
            </div>
            <div class="d-flex justify-content-between my-2">
                <div class="mx-1">
                    <div class="mx-1">
                        <a href="{{route('analyze-details', $post->slug)}}">
                            <button class="btn btn-outline-secondary">
                                View analyze
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        @endcan
        @if(Auth::check() && $post->author->user_id == Auth::user()->id)
            <div class="d-flex justify-content-between mx-2 my-2">
                <div>
                    @can('admin')
                        <span>
                            Author edit->
                        </span>
                    @endcan
                    <a href="{{route('categories-posts-edit-author', $post->slug)}}">
                        <button class="btn btn-outline-primary">
                            Edit
                        </button>
                    </a>
                </div>
            </div>
            <div class="mx-auto my-2">
                <div>
                    <span>Age range: </span>
                    <br>
                    <span>From:</span>
                    @if($post->from_age === NULL)
                        <strong>N/A</strong>
                    @else
                        <strong>{{$post->from_age}}</strong>
                    @endif
                    <span>To:</span>

                    @if($post->to_age === NULL)
                        <strong>N/A</strong>
                    @else
                        <strong>{{$post->to_age}}</strong>
                    @endif
                </div>
                <div class="mt-1">
                    <span>Location: </span>
                    @if($post->location === NULL)
                        <strong>N/A</strong>
                    @else
                        <strong>{{$post->location}}</strong>
                    @endif
                </div>
            </div>
            <div class="mx-auto my-2">
                <div class="my-1 mx-1">
                    <span>
                        Total notified users (tagged by age range) when post was created:
                    </span>
                    <strong>
                        {{$post->userAgeLocations->where('flag', 'age')->count()}}
                    </strong>
                </div>
                <div class="my-1 mx-1">
                    <span>
                        Total notified users (tagged by location) when post was created:
                    </span>
                    <strong>
                        {{$post->userAgeLocations->where('flag', 'location')->count()}}
                    </strong>
                </div>
            </div>
        @endif
    </div>
    <div class="discussion-post-wrapper">
        <div class="news-articles-attached-users-specialities">
            <div>
                <strong>
                    @foreach($post->specialities as $speciality)
                        <span>
                            <a href="">
                                | {{$speciality->en_name}} |
                            </a>
                        </span>
                    @endforeach
                </strong>
            </div>
            <div>
                <div class="d-flex justify-content-end">
                    @foreach($post->users as $user)
                        <div class="w-15 text-center">
                            <a href="{{route('user-view', $user->id)}}">
                                <img src="{{$user->avatar}}" alt="{{$user->avatar}}" class="rounded-circle w-40">
                                <small>
                                    {{$user->name}}
                                </small>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
            <div>
                @if($post->code_id !== NULL)
                    <strong>
                        <u>
                            CODE
                        </u>
                    </strong>
                    <br>
                    <strong>
                        {{$post->code->code}}
                    </strong>
                @endif
            </div>
        </div>
        <div>
            @if($post->event && $dateForHumans !== null && $hoursForHumans !== null)
                <div class="row mx-auto px-5 my-3">
                    <div class="col-md-6">
                        <span>
                            <u>
                                Date:
                            </u>
                        </span>
                        <strong>
                            {{$dateForHumans}},
                        </strong>
                        <span>
                            <u>
                                Time:
                            </u>
                        </span>
                        <strong>
                            {{$hoursForHumans}}.
                        </strong>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            @endif
        </div>

        <div class="d-flex justify-content-between px-5">
            <div class="p-1 w-50 text-justify">
                <strong>
                    Title:
                </strong>
            </div>
            <div class="w-25">
                @if($post->video !== NULL)
                    {{--                    <span class="material-icons">--}}
                    {{--                       live_tv--}}
                    {{--                    </span>--}}
                    {{--                @else--}}
                    <a href="{{$post->video}}" target="_blank">
                    <span class="material-icons">
                       live_tv
                    </span>
                    </a>
                @endif
            </div>
            <div class="w-25">
                @if($post->document !== NULL)
                    {{--                    <span class="material-icons">--}}
                    {{--                        assignment--}}
                    {{--                    </span>--}}
                    {{--                @else--}}
                    <a href="{{$post->document}}" target="_blank">
                    <span class="material-icons">
                        assignment
                    </span>
                    </a>
                @endif
            </div>
        </div>
        <div class="text-justify px-5 font-italic">
            <strong>
                {{$post->title}}
            </strong>
        </div>
        <div class="d-flex justify-content-center">
            <img src="{{$post->image}}" alt="{{$post->image}}">
        </div>
        <div class="discussion-post-body ">
            <div class="p-3">
                @foreach(explode(PHP_EOL, $post->body) as $paragraph)
                    <p>
                        {{$paragraph}}
                    </p>
                @endforeach
            </div>
            @if(Auth::check())
                @if(implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) !== 'Enterprise')
                    <div class="discussion-post-reaction">
                        <div class="post-reaction-wrapper">
                            @can('verify')
                                <react :post="{{$post}}" :reacts="{{$reacts}}" :user="{{Auth::user()}}"></react>
                                <div class="share m-auto">
                                    <button class="notification" data-modal-target="#modal">
                                <span class="material-icons">
                                    share
                                </span>
                                    </button>
                                </div>
                            @else
                                <strong class="text-center">Your account is not verified yet!</strong>
                            @endcan
                        </div>
                    </div>
                @endif
            @else
                <strong class="text-center">You must be logged in to react or share!</strong>
            @endif
        </div>
    </div>
</div>
@section('custom-js')
    <script src="{!! asset('js/modal.js') !!}"></script>
@endsection
