<div class="post-details">

    <section class="post-author">
        <a class="back-btn" href="{{route('categories-show', $post->category->slug)}}">
            <i class="post-option-icon fas fa-arrow-left"></i>
        </a>

        <a href="{{route('user-view', $post->author->user_id)}}">
            <img class="author-avatar" src="{{$post->author->user->profile->avatar}}" alt="">
        </a>

        <p class="author-name">{{$post->author->user->name}}</p>
        @if(implode(',', $post->author->user->roles()->get()->pluck('role')->toArray()) === 'Administrator')
        <p class="author-role">
            ({{implode(',', $post->author->user->roles()->get()->pluck('role')->toArray())}})
        </p>
        @endif
        <div class="date-picker">
            Date: <strong>{{$post->created_at}}</strong>
        </div>

        <div class="post-reactions">
            <div class="comments">
                <span><i class="far fa-comment-dots"></i></span>
                <span>{{$post->comments->count()}}</span>
            </div>
            <div class="views">
                <span><i class="far fa-eye"></i></span>
                <span>{{$post->views->count()}}</span>
            </div>
            <div class="likes">
                <span><i class="far fa-thumbs-up"></i></span>
                <span>{{$post->postReactions->where('reaction_id', 1)->count()}}</span>
            </div>
        </div>

        <div class="post-options">
            @if(Auth::user()->id === $post->author->user_id)
                <a href="{{route('categories-posts-edit', $post->slug)}}">
                    <i class="post-option-icon fas fa-edit"></i>
                </a>
                <form class="delete-form" action="{{route('posts-delete', $post->slug)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="delete-form-button">
                        <i class="post-option-icon fas fa-trash-alt"></i>
                    </button>
                </form>
                @if(implode(',', Auth::user()->roles()->get()->pluck('id')->toArray()) != 4)
                <a href="{{route('analyze-details', $post->slug)}}">
                    <i class="post-option-icon fas fa-chart-bar"></i>
                </a>
                @endif
            @else
                @can('admin')
                    <a href="{{route('categories-posts-edit', $post->slug)}}">
                        <i class="post-option-icon fas fa-edit"></i>
                    </a>
                    <form class="delete-form" action="{{route('posts-delete', $post->slug)}}" method="post">
                        @csrf
                        @method('DELETE')
                        <i class="post-option-icon fas fa-trash-alt"></i>
                    </form>
                    <a href="{{route('analyze-details', $post->slug)}}">
                        <i class="post-option-icon fas fa-chart-bar"></i>
                    </a>
                @endcan
            @endif
        </div>
    </section>

    <section class="post-content">
        <div class="top-post-data">
            <div class="post-specialities">
                <strong>
                    @foreach($post->specialities as $speciality)
                    <a href="">
                        | {{$speciality->en_name}} |
                    </a>
                    @endforeach
                </strong>
            </div>

            <div class="enterprise-users">
                @foreach($post->users as $user)
                <a href="{{route('user-view', $user->id)}}">
                    <img src="{{$user->avatar}}" alt="{{$user->avatar}}" class="enterprise-avatar">
                </a>
                @endforeach
            </div>
        </div>

        @if($post->code_id !== NULL)
        <div class="post-code">
            <strong>Code: </strong>
            <strong class="code-value">{{$post->code->code}}</strong>
        </div>
        @endif

        @if($post->event && $dateForHumans !== null && $hoursForHumans !== null)
        <div>
            <strong>
                Date:
            </strong>
            <strong class="date-value">
                {{$dateForHumans}},
            </strong>
            <br>
            <strong>
                Time:
            </strong>
            <strong class="date-value">
                {{$hoursForHumans}}.
            </strong>
        </div>
        @endif

        @if($post->video !== NULL || $post->document !== NULL)
        <div class="post-attachments">
            @if($post->video !== NULL)
            <a href="{{$post->video}}" target="_blank">
                <i class="fab fa-youtube"></i>
            </a>
            @endif
            @if($post->document !== NULL)
            <a href="{{$post->document}}" target="_blank">
                <i class="fas fa-paste"></i>
            </a>
            @endif
        </div>
        @endif

        <section class="post-text">
            <h2 class="post-heading">{{$post->title}}</h2>

            <img class="post-image" src="{{$post->image}}" alt="{{$post->image}}">

            <div class="post-paragraphs">
                @foreach(explode(PHP_EOL, $post->body) as $paragraph)
                <p>
                    {{$paragraph}}
                </p>
                @endforeach
            </div>
        </section>

        @if(Auth::check())
        @if(implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) !== 'Enterprise')
        <section class="post-reactions-bottom">
            <div class="reactions-container">
                @can('verify')

                <react :post="{{$post}}" :reacts="{{$reacts}}" :user="{{Auth::user()}}"></react>

                @else
                <strong class="text-center">Your account is not verified yet!</strong>
                @endcan
            </div>

            <button class="show-button" data-modal-target="#modal">
                Comment and share
                <i class="fas fa-share-alt"></i>
            </button>
        </section>
        @endif
        @else
        <strong class="text-center">You must be logged in to react or share!</strong>
        @endif

    </section>
</div>

@if(Auth::check())
    @can('verify')
        <comments :post="{{$post}}" :user="{{Auth::user()}}"></comments>
    @else
        <strong class="text-center">Your account is not verified yet!</strong>
    @endcan
@else
    <div class="d-flex justify-content-center">
        <div class="bordered-1px-primary-colour w-50 text-center">
            You must be logged in to post a comment!
        </div>
    </div>
@endif
@section('custom-js')
    <script src="{!! asset('js/modal.js') !!}"></script>
@endsection
