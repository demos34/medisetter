<div class="category-body-wrapper">
    <div class="category-subcategory-wrapper">
        <div class="category-subcategory-title">
            browse by speciality
        </div>
        <div class="category-subcategory-body">
            @foreach($post->category->subcategories as $item)
                <div>
                    <a href="{{route('subcategories-show', $item->slug)}}">
                        {{$item->name}}
                    </a>
                </div>
            @endforeach
        </div>
    </div>
    <div class="post-wrapper">
        <div class="d-flex justify-content-center my-2 border-bottom">
            <h2>
                {{$post->title}}
            </h2>
        </div>
        <div class="row post-author-wrapper">
            <div class="col-md-2">
                <img class="post-author-image" src="/storage/helpers/profile.png" alt="profile-image">
            </div>
            <div class="col-md-1 my-auto">
                <strong>
                    {{$post->author->user->name}}
                </strong>
            </div>
            <div class="col-md-4 text-uppercase my-auto">
                <strong>
                    date: {{$post->created_at}}
                </strong>
            </div>
            <div class="col-md-2 my-auto">
                @if($post->video != NULL)
                    <a href="{{$post->video}}" target="_blank">
                        <strong>
                            Attached video
                        </strong>
                    </a>
                @endif
            </div>
            <div class="col-md-2 my-auto">
                @if($post->document != NULL)
                    <a href="{{$post->document}}" target="_blank">
                        <strong>
                            Attached file
                        </strong>
                    </a>
                @endif
            </div>
            <div class="col-md-1 my-auto">
                @if($post->code != NULL)
                    <strong>
                        <u>
                            CODE
                        </u>
                    </strong>
                    <br>
                    <strong>
                        {{$post->code->code}}
                    </strong>
                @endif
            </div>
        </div>
        @if($post->event)
            <div class="row mx-auto px-5 my-3">
                <div class="col-md-6">
                    <strong class="text-uppercase">
                        <u>
                            Start date:
                        </u>
                    </strong>
                    <strong>
                        {{$post->event->start_date}}
                    </strong>
                </div>
                <div class="col-md-6">
                    <strong class="text-uppercase">
                        <u>
                            End date:
                        </u>
                    </strong>
                    <strong>
                        {{$post->event->end_date}}
                    </strong>
                </div>
            </div>
        @endif

        <div class="post-body-wrapper">
            <div class="m-auto">
                <div>
                    <img src="{{$post->image}}" alt="{{$post->image}}">
                </div>
            </div>

            <div class="mt-3">
                @foreach(explode(PHP_EOL, $post->body) as $paragraph)
                    <p>
                        {{$paragraph}}
                    </p>
                @endforeach
            </div>
        </div>
        <div class="post-reaction-wrapper">
            @if(Auth::check())
            <div class="post-reaction">
                @foreach($reacts as $react)
                    <a href="{{route('posts-reactions', [$post->slug , $react->name])}}">
                        <img src="{{$react->image}}" alt="{{$react->image}}">
                    </a>
                    <strong class="my-auto">
                        Total: {{$post->postReactions->where('reaction_id', $react->id)->count()}}
                    </strong>
                @endforeach
            </div>
            <div class="total-views m-auto">
                Total views: {{$views->count}}
            </div>
            <div class="share m-auto">
                <button class="btn btn-light" data-modal-target="#modal">
                    share
                </button>
            </div>
            @else
                <strong class="text-center">You must be sign in to react or share!</strong>
            @endif
        </div>
    </div>
</div>
@section('custom-js')
    <script src="{!! asset('js/modal.js') !!}"></script>
@endsection
