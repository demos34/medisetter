@section('custom-css')
    <link rel="stylesheet" href="{{ asset('css/modal.css') }}">
@endsection
@section('spec')
    <spec :category="{{$post->category}}"></spec>
@endsection
@section('search')
    <div class="nav-search-bar">
        <span class="material-icons">
          search
        </span>
    </div>
@endsection
<x-app-layout>
    <div class="main-category-body">
        <div class="w-100 mx-auto">
            <div class="custom-modal" id="modal">
                <div class="custom-modal-header">
                    <div class="custom-modal-title">
                        Do you want to share this post?
                    </div>
                    <button data-close-button class="custom-close-button">
                        &times;
                    </button>
                </div>
                <div class="custom-modal-body">
                    <form action="{{route('posts-share', $post->slug)}}"
                          method="post">
                        @csrf
                        @method('PATCH')
                        <label for="title">Say something about it:</label>
                        <br>
                        <input class="form-control my-2" id="title" name="title">
                        <div class="text-center my-2">
                            <button class="btn btn-primary">Share!</button>
                        </div>

                    </form>
                </div>
            </div>
            <div id="custom-overlay" class="w-100">
                @if($post->category->id == 5)
                    @include('posts.categories.inc.search-' . $post->category->slug)
                @else
                    @include('posts.categories.inc.search')
                @endif
            </div>
        </div>
    </div>
</x-app-layout>
@section('custom-js')
    <script src="{!! asset('js/modal.js') !!}"></script>
@endsection
