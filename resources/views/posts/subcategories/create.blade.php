<x-app-layout>
    <div class="main-category-body">
        <div class="overflow-hidden">
            <div class="text-center mb-12">
                <strong class="category-title" id="sub">
                    {{$current->category}}
                </strong>
                <strong class="category-title" id="{{$current->slug}}">
                </strong>
                <br>
                <strong class="subcategory-title" id="{{$currentSub->id}}">
                    {{$currentSub->name}}
                </strong>
            </div>
                @include('posts.subcategories.inc.create-form')
        </div>
    </div>
</x-app-layout>
