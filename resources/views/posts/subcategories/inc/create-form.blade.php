<div class="category-body-wrapper">
    <div class="w-80 d-flex justify-content-center">
        <form action="{{route('posts-store')}}" method="post" enctype="multipart/form-data" class="w-100 form-group create-posts-form">
            @csrf
            @method('PUT')
            <div class="w-100">
                <div class="post-create-wrapper">
                    <div>
                        <label for="categories">
                            Categories
                        </label>
                        <select class="select-css text-uppercase" name="categories" id="categories">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}" @if($category->id == $current->id) selected
                                        id="selected-cat" @endif>
                                    {{$category->$cat}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div>
                        <label for="subcategories">
                            Subcategories
                        </label>
                        <select class="select-css mul-select" name="subcategories[]" id="subcategories" multiple="true">
                        </select>
                    </div>
                </div>
                <div class="post-create-wrapper">
                    <div>
                        <label for="users">
                            Enterprise users
                        </label>
                        <select class="select-css mul-select" name="users[]" id="users" multiple="true">
                            @foreach($users as $user)
                                <option value="{{$user->id}}">
                                    {{$user->name}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="d-none" id="codes">
                        <label for="selected-codes">
                            Codes
                        </label>
                        <select class="select-css text-uppercase" name="" id="selected-codes">
                            <option value="0">
                                ---- Please select code -----
                            </option>
                            @foreach($codes as $code)
                                <option value="{{$code->id}}">
                                    {{$code->code}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="post-create-wrapper" id="event-dates">
                    <div>
                        <label for="start_date">
                            Event start date
                        </label>
                        <br>
                        <input name="" type="datetime-local" id="start_date" autocomplete>
                    </div>
                    <div>
                        <label for="end_date">
                            Event end date
                        </label>
                        <br>
                        <input name="" type="datetime-local" id="end_date" autocomplete>
                    </div>
                </div>
                <div class="create-posts-body">
                    <div class="create-posts-body-title">
                        <strong>
                            create new post
                        </strong>
                    </div>
                    <div class="create-posts-inputs">
                        <div>
                            <img class="create-posts-body-image" src="/storage/helpers/profile.png" alt="">
                            <strong>
                                {{Auth::user()->name}}
                            </strong>
                        </div>
                        <div class="create-posts-textarea-wrapper">
                            <div class="w-80 mx-auto">
                                <label for="title">
                                    Post title:
                                </label>
                                <input id="title"
                                       class="create-posts-title-input @error('title') is-invalid @enderror"
                                       name="title"
                                       value="{{ old('title') }}"
                                       required autocomplete="title">
                                @error('title')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="">
                                <label for="body">
                                </label>
                            </div>
                            <div class="">
                            <textarea id="body"
                                      class="create-posts-textarea @error('body') is-invalid @enderror"
                                      name="body"
                                      required autocomplete="body">{{ old('body') }}</textarea>
                                @error('body')
                                <br>
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="">
                            </div>
                        </div>
                    </div>
                    <div class="create-posts-files-wrapper">
                        <div class="create-posts-files">
                            <div>
                                <input type="file" id="image" name="image">
                            </div>
                            <div>
                                <input type="text" id="youtube" name="youtube">
                            </div>
                            <div>
                                <input type="file" id="document" name="file">
                            </div>
                            <div class="">
                                <button id class="add-button">Post</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script src="{!! asset('js/jquery.js') !!}"></script>
<script src="{!! asset('js/posts-create-ajax.js') !!}"></script>
<script src="{!! asset('js/multiselect.min.js') !!}"></script>
<script>
    $(document).ready(function () {
        $(".mul-select").select2({
            placeholder: "Select", //placeholder
            tags: true,
            tokenSeparator: ['/', ',', ',', ""]
        })
    })
</script>
