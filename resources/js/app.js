require('./bootstrap');

window.Vue = require('vue')
Vue.component('category', require('./components/Category').default);
Vue.component('comments', require('./components/Comments').default);
Vue.component('chat-counter', require('./components/ChatCounter').default);
Vue.component('spec', require('./components/Speciality').default);
Vue.component('notif', require('./components/Notification').default);
Vue.component('react', require('./components/Reaction').default);
Vue.component('search', require('./components/Search').default);
// Vue.component('messenger', require('./components/messenger/Messenger').default);
Vue.component('new-nav', require('./components/new-view/Nav').default);
Vue.component('new-no-login', require('./components/new-view/Not-logged').default);

//chat app

Vue.component('chat-app', require('./components/chat/ChatApp').default);


new Vue({
    el: "#app"
})

