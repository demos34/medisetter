<?php

namespace App\Events;

use App\Models\User;
use App\Models\User\Notification;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NotificationCount implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $notification;

    public $user;
    /**
     * @var Notification
     */

    /**
     * Create a new event instance.
     *
     * @param Notification $notification
     * @param User $user
     */
    public function __construct(Notification $notification, User $user)
    {
        $this->notification = $notification;
        $this->user = $user;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('countNotify.' . $this->notification->to->user_id);
    }

    public function broadcastWith()
    {
        return [
            "user" => $this->user,
            "notification" => $this->notification,
        ];
    }

    public function broadcastAs()
    {
        return 'private-unviewed-count';
    }
}
