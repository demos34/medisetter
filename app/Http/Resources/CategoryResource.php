<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\App;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'en_category' => $this->en_category,
            'vi_category' => $this->vi_category,
            'slug' => $this->slug,
            'subcategories' => $this->subcategories,
            'locale' => $this->prefix(),
        ];
    }


}
