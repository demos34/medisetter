<?php

namespace App\Http\Controllers\Api;

use App\Events\NewNotification;
use App\Events\NotificationCount;
use App\Http\Controllers\Controller;
use App\Http\Resources\CommentResource;
use App\Models\Category\Category;
use App\Models\Post\Comment;
use App\Models\Post\Post;
use App\Models\Post\PostReaction;
use App\Models\Post\Reaction;
use App\Models\User;
use App\Models\User\Notification;
use App\Models\User\To;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function index(Post $post)
    {
        return CommentResource::collection(Comment::where('post_id', $post->id)->where('is_visible', 1)->orderBy('created_at', 'desc')->get());
    }

    public function  profile()
    {
        return Auth::user()->profile->avatar;
    }


    public function store(Request $request, Post $post)
    {
//        return response()->json([$request->validate(['body' => 'required|min:3|max:5000'])]);


        $data = $request->validate(
            [
                'body' => 'required|min:3|max:5000',
                'user_id' => 'required',
            ]
        );
        return Comment::create(
            [
                'body' => $data['body'],
                'is_visible' => true,
                'post_id' => $post->id,
                'user_id' => $data['user_id'],
            ]
        );
    }

    public function react(Request $request, Post $post, Reaction $reaction)
    {
        if($post && $reaction)
        {
            $notifType = $reaction->id + 1;

            if($postReaction = PostReaction::where('post_id', $post->id)->where('user_id', Auth::user()->id)->first()){
                if($postReaction->reaction_id === $reaction->id){
                    $postReaction->delete();
                    $msg = self::msg('warning', 'You get back your vote!');
                } else {
                    $postReaction->update(
                        [
                            'reaction_id' => $reaction->id,
                        ]
                    );
                    self::notify($request->user(), $post->author->user->to->id, $post->id, $notifType);
                    $msg = self::msg('success', 'You reacted with ' . $reaction->en_name);
                }
            } else {
                PostReaction::create(
                    [
                        'post_id' => $post->id,
                        'reaction_id' => $reaction->id,
                        'user_id' => Auth::user()->id,
                    ]
                );

                self::notify($request->user(), $post->author->user->to->id, $post->id, $notifType);
                $msg = self::msg('success', 'You reacted with ' . $reaction->en_name);
            }
        } else {
            $msg = self::msg('alert', 'Ooops! Something is wrong!');
        }
        return response()->json($msg);
    }

    public function reactCount(Post $post, Reaction $reaction)
    {
        return response()->json($post->postReactions->where('reaction_id', $reaction->id)->count());
    }

    public function search(Request $request)
    {
        $data = $request->validate(
            [
                'categoryId' => 'exists:categories,id',
                'searchContent' => 'required|min:1',
            ]
        );
        $posts = Post::with('category')
                    ->where('category_id', (int)$data['categoryId'])
                    ->where('title', 'LIKE', '%' . $data['searchContent'] . '%')
                    ->where('body', 'LIKE', '%' . $data['searchContent'] . '%')
                    ->get();
        $category = Category::findOrFail($data['categoryId']);
        return view('categories.categories.search')->with(
            [
                'posts' => $posts,
                'category' => $category,
            ]
        );
    }

    public function myReact(Request $request, Post $post, Reaction $reaction)
    {
        if($post->postReactions->where('user_id', $request->user()->id)->count() > 0){
            if($post->postReactions->where('user_id', $request->user()->id)[0]->reaction_id == $reaction->id){
                return response()->json(true);
            } else {
                return response()->json(false);
            }
        } else {
            return response()->json(false);
        }
    }

    public function myBind(Request $request, Post $post)
    {
        if($post->postReactions->where('user_id', $request->user()->id)->count() > 0){
            return response()->json($post->postReactions->where('user_id', $request->user()->id)[0]->reaction_id);
        } else {
            return response()->json(100);
        }
    }

    protected static function notify($authUser, $toId, $postId, $typeId)
    {
        $notification = Notification::create(
            [
                'from_id' => $authUser->from->id,
                'to_id' => $toId,
                'post_id' => $postId,
                'notification_type_id' => $typeId,
                'user_id' => $authUser->id,
                'is_viewed' => false,
            ]
        );

        $tos = To::where('id', $toId)->first();

        $receiver = User::where('id', $tos->user_id)->first();

        broadcast(new NewNotification($notification));

        broadcast(new NotificationCount($notification, $receiver));
    }

    protected static function msg($session, $message)
    {
        return $message;
    }
}
