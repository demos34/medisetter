<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Models\Category\Category;
use App\Repositories\Administrator\AdminRepositoryInterface;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        return CategoryResource::collection(Category::with('subcategories')->where('is_visible', 1)->get());
    }

    public function spec(Category $category)
    {
        return response()->json($category->specialities->sortBy('en_name'));
    }
}
