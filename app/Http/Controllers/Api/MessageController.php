<?php

namespace App\Http\Controllers\Api;

use App\Events\MessageCount;
use App\Events\NewMessage;
use App\Events\NotificationCount;
use App\Http\Controllers\Controller;
use App\Models\Message\Message;
use App\Models\User;
use App\Models\User\Contact;
use App\Models\User\From;
use App\Models\User\Notification;
use App\Models\User\To;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{

    public function get(Request $request)
    {

        $contacts = $request->user()->contacts()->with(['user'])->get();

        $unreadIds = Message::select(DB::raw('`from_id` as sender_id, count(`from_id`) as messages_count'))
            ->where('to_id', $request->user()->to->id)
            ->where('is_read', false)
            ->groupBy('from_id')
            ->get();

        $contacts = $contacts->map(function ($contact) use ($unreadIds){
            $contactUnread = $unreadIds->where('sender_id', $contact->user->from->id)->first();

            $contact->unread = $contactUnread ? $contactUnread->messages_count : 0;

            return $contact;
        });

        return response()->json($contacts);
    }

    public function getMessagesFor(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);
        $from = From::where('user_id', $contact->user_id)->first();
        $to = To::where('user_id', $contact->user_id)->first();
        $fromId = $from->id;
        $toId = $to->id;
        Message::where('from_id', $fromId)->update(
            [
                'is_read' => true,
            ]
        );

        $messages = Message::where(function ($query) use ($request, $id, $toId) {
                        $query->where('from_id', $request->user()->from->id);
                        $query->where('to_id', $toId);
                    })->orWhere(function ($query) use ($request, $id, $fromId) {
                        $query->where('from_id', $fromId);
                        $query->where('to_id', $request->user()->to->id);
                    })
                    ->orderBy('created_at')
                    ->get();

        return response()->json($messages);
    }

    public function getDefaultContact(Request $request)
    {
        $defaultUser = User::where('email', 'admintest@test.test')->first();
        return $defaultUser->contact()
            ->with('user')
            ->with('user.from')
            ->get();
    }

    public function getDefaultMessages(Request $request)
    {
        $defaultUser = User::where('email', 'admintest@test.test')->first();
        $defaultContact = $defaultUser->contact()->with(['user'])->get();
        return $defaultContact;
    }

    public function send(Request $request)
    {
        $toId = To::where('user_id', $request->contact_id)->first();

        $receiver = User::where('id', $request->contact_id)->first();

        $message = Message::create(
            [
                'from_id' => $request->user()->from->id,
                'to_id' => $toId->id,
                'message' => $request->text,
            ]
        );

        broadcast(new NewMessage($message));

        broadcast(new MessageCount($message, $receiver));

        return response()->json($message);
    }

    public function count(Request $request)
    {
        $count = Message::where('to_id', $request->user()->to->id)
            ->where('is_read', 0)
            ->count();

        return response()->json($count);
    }

    public function getAvatar(Request $request)
    {
        return response()->json($request->user()->profile->avatar);
    }

    public function notification(Request $request)
    {
        $notif = Notification::with('user')->with('post')
            ->orderByDesc('created_at')
            ->where('to_id', $request->user()->to->id)
            ->limit(20)
            ->get();
//        $notif = $request->user()->to->notifications()->with('user')->sortByDesc('created_at')->take(5);
        return response()->json($notif);
    }

    public function notificationView(Request $request)
    {
        foreach ($request->user()->to->notifications as $notification)
        {
            $notification->update(
                [
                    'is_viewed' => true,
                ]
            );
            $receiver = User::where('id', $request->user()->id)->first();

            broadcast(new NotificationCount($notification, $receiver));
        }


    }

    public function notificationCount(Request $request)
    {
        $notif = Notification::where('to_id', $request->user()->to->id)
            ->where('is_viewed', 0)->count();
        return response()->json($notif);
    }

    public function readMessage(User $user)
    {
        $from = From::where('user_id', $user->id)->first();
        Message::where('from_id', $from->id)->update(
            [
                'is_read' => true,
            ]
        );
    }
}
