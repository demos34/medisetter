<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Admin\Analyze;
use App\Models\Admin\AnalyzeLogin;
use App\Models\Admin\Login;
use App\Models\Category\Speciality;
use App\Models\Degree\Degree;
use App\Models\Post\Post;
use App\Models\Post\PostReaction;
use App\Models\Post\Share;
use App\Models\User;
use App\Models\View\View;
use App\Repositories\Administrator\AnalyzeRepositoryInterface;
use App\Repositories\Traffic\TrafficRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class AnalyzeController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var AnalyzeRepositoryInterface
     */
    private $analyzeRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                AnalyzeRepositoryInterface $analyzeRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->analyzeRepository = $analyzeRepository;
    }

    public function index()
    {
        $action = 'ui-index';
        $this->trafficRepository->getRemoteAddress($action);
        if(Auth::user()->author === NULL){
            $posts = null;
        } else {
            $posts = Auth::user()->author->posts;
        }
        $enterprise = Auth::user()->posts;
        $users = User::all();
        $currentDate = Carbon::now();
        $past24 = User::whereDate('created_at', '>' ,$currentDate->subDay())->count();
        $pastWeek = User::whereDate('created_at', '>' ,$currentDate->subWeek())->count();
        $past24Logins = Login::whereDate('created_at', '>' ,$currentDate->subDay())->count();
        $pastWeekLogins = Login::whereDate('created_at', '>' ,$currentDate->subWeek())->count();
        $pastMonth = Login::whereDate('created_at', '>' ,$currentDate->subMonth())->count();
        $analyzes = AnalyzeLogin::where('user_id', Auth::user()->id)->get();

        $totalViews = $this->analyzeRepository->getAllMyViews();
        $totalReacts = $this->analyzeRepository->getAllMyReactions();
        $totalShares = $this->analyzeRepository->getAllMyShares();
        $specialities = Speciality::all();
        $degrees = Degree::all();
        $name = App::getLocale() . '_name';

        return view('users.users.dashboard')->with(
            [
                'page' => 'analyze',
                'posts' => $posts,
                'enterprise' => $enterprise,
                'users' => $users,
                'past24' => $past24,
                'pastWeek' => $pastWeek,
                'last24Logins' => $past24Logins,
                'lastWeekLogins' => $pastWeekLogins,
                'pastMonth' => $pastMonth,
                'analyzes' => $analyzes,
                'totalViews' => $totalViews,
                'totalReacts' => $totalReacts,
                'totalShares' => $totalShares,
                'specialities' => $specialities,
                'degrees' => $degrees,
                'name' => $name,
            ]
        );


    }

    public function analyzeDetails(Post $post)
    {
        $ids = [];
        foreach ($post->users as $postUser){
            array_push($ids, $postUser->id);
        }
        $administrators = User::with('roles')->whereHas('Roles', function ($q){
            $q->whereIn('role', ['Administrator', 'Developer']);
        })->get();
        $adminIds = [];
        foreach ($administrators as $administrator){
            array_push($adminIds, $administrator->id);
        }
        $ids = array_merge($ids, $adminIds);

        if (Auth::user()->id != $post->author->user_id){
            if(in_array(Auth::user()->id, $ids) === FALSE) {
                return redirect()->back()->with('alert', 'You can get details only for your posts!');
            }
        }
        if($post->is_deleted === 1){
            return redirect()->back()->with('alert', 'This post is not available!');
        }
        $currentDate = Carbon::now();
        $analyzes = Analyze::where('post_id', $post->id)->where('user_id', Auth::user()->id)->get();
        $past24 = View::where('post_id', $post->id)->whereDate('created_at', '>' ,$currentDate->subDay())->count();
        $pastWeek = View::where('post_id', $post->id)->whereDate('created_at', '>' ,$currentDate->subWeek())->count();
        $past24Reacts = PostReaction::where('post_id', $post->id)->whereDate('created_at', '>' ,$currentDate->subDay())->count();
        $pastWeekReacts = PostReaction::where('post_id', $post->id)->whereDate('created_at', '>' ,$currentDate->subWeek())->count();

        $specialities = Speciality::all();
        $degrees = Degree::all();
        $name = App::getLocale() . '_name';
        return view('users.users.dashboard')->with(
            [
                'page' => 'analyze-details',
                'post' => $post,
                'analyzes' => $analyzes,
                'past24' => $past24,
                'pastWeek' => $pastWeek,
                'past24Reacts' => $past24Reacts,
                'pastWeekReacts' => $pastWeekReacts,
                'specialities' => $specialities,
                'degrees' => $degrees,
                'name' => $name,
            ]
        );
    }

    public function store(Request $request, Post $post)
    {
        $ids = [];
        foreach ($post->users as $postUser){
            array_push($ids, $postUser->id);
        }
        $administrators = User::with('roles')->whereHas('Roles', function ($q){
            $q->whereIn('role', ['Administrator', 'Developer']);
        })->get();
        $adminIds = [];
        foreach ($administrators as $administrator){
            array_push($adminIds, $administrator->id);
        }
        $ids = array_merge($ids, $adminIds);

        if (Auth::user()->id != $post->author->user_id){
            if(in_array(Auth::user()->id, $ids) === FALSE) {
                return redirect()->back()->with('alert', 'You can get details only for your posts!');
            }
        }
//        if (Auth::user()->id != $post->author->user_id){
//            return redirect()->back()->with('alert', 'You can get details only for your posts!');
//        }
        if($post->is_deleted === 1){
            return redirect()->back()->with('alert', 'This post is not available!');
        }
        $analyze = $this->analyzeRepository->createSchema($request, $post);
        return redirect()->route('analyze-show', $analyze->id)->with('success', 'Success');
    }

    public function show(Analyze $analyze)
    {
        $views = View::where('post_id', $analyze->post_id)->whereBetween('created_at', [$analyze->start, $analyze->end])->get();
        $shares = Share::where('post_id', $analyze->post_id)->whereBetween('created_at', [$analyze->start, $analyze->end])->count();
        $reacts = PostReaction::where('post_id', $analyze->post_id)->whereBetween('created_at', [$analyze->start, $analyze->end])->get();

        //ne e gotovo!!!! trqbva da se dobavi userite da otgovarqt na tezi, koito sa react ili view

        $ageUsers = $this->analyzeRepository->getUsersInAgeRange($analyze);
        $locationUsers = $this->analyzeRepository->getUsersInLocation($analyze);
        $ageLocationUsers = $this->analyzeRepository->getUsersInAgeRangeAndLocation($analyze);
        $name = App::getLocale() . '_name';

        return view('users.users.dashboard')->with(
            [
                'page' => 'analyze-show',
                'analyze' => $analyze,
                'views' => $views,
                'shares' => $shares,
                'reacts' => $reacts,
                'ageUsers' => $ageUsers,
                'locationUsers' => $locationUsers,
                'ageLocationUsers' => $ageLocationUsers,
                'name' => $name,
            ]
        );
    }

    public function storeLogin(Request $request)
    {
        $analyzeLogin = $this->analyzeRepository->createLoginSchema($request);
        return redirect()->route('analyze-show-login', $analyzeLogin->id)->with('success', 'Success');
    }

    public function showLogin(AnalyzeLogin $analyzeLogin)
    {
        $loginUsers = Login::whereBetween('created_at', [$analyzeLogin->start, $analyzeLogin->end])->count();
//
        return view('users.users.dashboard')->with(
            [
                'page' => 'analyze-show-login',
                'analyze' => $analyzeLogin,
                'users' => $loginUsers,
            ]
        );
    }

    public function destroy(Analyze $analyze)
    {
        $postSlug = $analyze->post->slug;
        $analyze->delete();
        return redirect()->route('analyze-details', $postSlug);
    }

    public function destroyLogin(AnalyzeLogin $analyzeLogin)
    {
        $analyzeLogin->delete();
        return redirect()->route('analyze');
    }

    public function attach(Post $post)
    {
        if (Auth::user()->id != $post->author->user_id){
            return redirect()->back()->with('alert', 'You can get manage only your posts!');
        }

        $post->update(
            [
                'is_attached' => true,
            ]
        );

        return redirect()->route('analyze')->with('succes', 'Post was successfully dettached');

    }

    public function dettach(Post $post)
    {
        if (Auth::user()->id != $post->author->user_id){
            return redirect()->back()->with('alert', 'You can get manage only your posts!');
        }

        $post->update(
            [
                'is_attached' => false,
            ]
        );

        return redirect()->route('analyze')->with('succes', 'Post was successfully dettached');

    }

    public function showDettached()
    {
        $posts = Auth::user()->author->posts;

        return view('users.users.dashboard')->with(
            [
                'page' => 'analyze-show-dettached',
                'posts' => $posts,
            ]
        );
    }
}
