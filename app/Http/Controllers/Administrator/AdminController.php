<?php

namespace App\Http\Controllers\Administrator;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Models\Category\Speciality;
use App\Models\Category\Subcategory;
use App\Models\Code\Code;
use App\Models\Degree\Degree;
use App\Models\Degree\University;
use App\Models\Post\Post;
use App\Models\Post\Reaction;
use App\Models\User;
use App\Repositories\Administrator\AdminRepositoryInterface;
use App\Repositories\Traffic\TrafficRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var AdminRepositoryInterface
     */
    private $adminRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                AdminRepositoryInterface $adminRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->adminRepository = $adminRepository;
    }

    public function index($page)
    {
        $action = 'ui-index';
        $this->trafficRepository->getRemoteAddress($action);
        $categories = $this->adminRepository->getAllCategories();
        $subcategories = $this->adminRepository->getAllSubCategories();
        $roles = $this->adminRepository->getAllRoles();
        $specialities = $this->adminRepository->getAllSpecialities();
        $universities = $this->adminRepository->getAllUniversities();
        $degrees = $this->adminRepository->getAllDegrees();
        $codes = $this->adminRepository->getAllCodes();
        $name = App::getLocale() . '_name';
        $cat = App::getLocale() . '_category';
        $softdeletedPosts = Post::where('is_deleted', 1)->get();

        return view('users.users.dashboard')->with(
            [
                'page' => $page,
                'categories' => $categories,
                'subcategories' => $subcategories,
                'roles' => $roles,
                'specialities' => $specialities,
                'universities' => $universities,
                'degrees' => $degrees,
                'name' => $name,
                'cat' => $cat,
                'codes' => $codes,
                'deletedPosts' => $softdeletedPosts,
            ]
        );
    }

    public function roles()
    {
        $action = 'ui-roles';
        $this->trafficRepository->getRemoteAddress($action);

        $roles = $this->adminRepository->getAllRoles();

        return view('administrator.roles')->with(
            [
                'roles' => $roles,
            ]
        );
    }

    public function categories()
    {
        $action = 'ui-categories';
        $this->trafficRepository->getRemoteAddress($action);

        $categories = $this->adminRepository->getAllCategories();

        return view('administrator.categories')->with(
            [
                'categories' => $categories,
            ]
        );
    }

    public function storeCategory(Request $request)
    {
        $action = 'ui-store-category';
        $this->trafficRepository->getRemoteAddress($action);
        $msg = $this->adminRepository->insertNewRowCategory($request);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function showCategory(Category $category)
    {
        $action = 'ui-show-category';
        $this->trafficRepository->getRemoteAddress($action);
        return view('users.users.dashboard')->with(
            [
                'page' => 'category',
                'category' => $category,
            ]
        );
    }

    public function updateCategory(Request $request, Category $category)
    {
        $action = 'ui-update-category';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->updateCategory($request, $category);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function changeVisibleStatus(Category $category)
    {
        $action = 'ui-change-category';
        $this->trafficRepository->getRemoteAddress($action);
        $status = $this->adminRepository->changeVisibility($category);

        return redirect()->back()->with('success', 'The status is change to ' . $status);
    }

    public function deleteCategory(Category $category)
    {
        $action = 'ui-delete-category';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->deleteCategory($category);
        return redirect()->route('admin-index', 'admin')->with($msg['session'], $msg['message']);
    }

    public function subcategories()
    {
        $action = 'ui-subcategories';
        $this->trafficRepository->getRemoteAddress($action);

        $subcategories = $this->adminRepository->getAllSubCategories();

        return view('administrator.subcategories')->with(
            [
                'subcategories' => $subcategories,
            ]
        );
    }

    public function storeSubcategories(Request $request)
    {
        $action = 'ui-store-subcategory';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->insertNewRowSubcategory($request);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function showSubcategory(Subcategory $subcategory)
    {
        $action = 'ui-show-subcategory';
        $this->trafficRepository->getRemoteAddress($action);
        return view('users.users.dashboard')->with(
            [
                'page' => 'subcategory',
                'subcategory' => $subcategory,
            ]
        );
    }

    public function updateSubcategories (Request $request, Subcategory $subcategory)
    {
        $action = 'ui-update-subcategory';
        $this->trafficRepository->getRemoteAddress($action);
        $msg = $this->adminRepository->updateSubcategories($request, $subcategory);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function deleteSubcategory(Subcategory $subcategory)
    {
        $action = 'ui-delete-subcategory';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->deleteSubcategory($subcategory);
        return redirect()->route('admin-index', 'admin')->with($msg['session'], $msg['message']);
    }

    public function storeSpecialities(Request $request)
    {
        $action = 'ui-store-specialties';
        $this->trafficRepository->getRemoteAddress($action);
        $msg = $this->adminRepository->insertNewRowSpeciality($request);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function showSpeciality(Speciality $speciality)
    {
        $action = 'ui-show-specialties';
        $this->trafficRepository->getRemoteAddress($action);

        return view('users.users.dashboard')->with(
            [
                'page' => 'speciality',
                'speciality' => $speciality,
            ]
        );
    }

    public function  updateSpecialities(Request $request, Speciality $speciality)
    {
        $action = 'ui-update-specialties';
        $this->trafficRepository->getRemoteAddress($action);
        $msg = $this->adminRepository->updateSpeciality($request, $speciality);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function  deleteSpeciality(Speciality $speciality)
    {
        $action = 'ui-delete-specialties';
        $this->trafficRepository->getRemoteAddress($action);
        $msg = $this->adminRepository->deleteSpeciality($speciality);
        return redirect()->route('admin-index', 'admin')->with($msg['session'], $msg['message']);
    }

    public function storeUniversities(Request $request)
    {
        $action = 'ui-store-university';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->insertNewRowUniversity($request);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function showUniversity(University $university)
    {
        $action = 'ui-show-university';
        $this->trafficRepository->getRemoteAddress($action);
        return view('users.users.dashboard')->with(
            [
                'page' => 'university',
                'university' => $university,
            ]
        );
    }

    public function updateUniversities (Request $request, University $university)
    {
        $action = 'ui-update-university';
        $this->trafficRepository->getRemoteAddress($action);
        $msg = $this->adminRepository->updateUniversity($request, $university);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function deleteUniversity(University $university)
    {
        $action = 'ui-delete-university';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->deleteUniversity($university);
        return redirect()->route('admin-index', 'admin')->with($msg['session'], $msg['message']);
    }

    public function storeDegrees(Request $request)
    {
        $action = 'ui-store-degree';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->insertNewDegree($request);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function showDegree(Degree $degree)
    {
        $action = 'ui-show-degree';
        $this->trafficRepository->getRemoteAddress($action);
        return view('users.users.dashboard')->with(
            [
                'page' => 'degree',
                'degree' => $degree,
            ]
        );
    }

    public function updateDegrees(Request $request, Degree $degree)
    {
        $action = 'ui-update-degree';
        $this->trafficRepository->getRemoteAddress($action);
        $msg = $this->adminRepository->updateDegree($request, $degree);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function deleteDegree(Degree $degree)
    {
        $action = 'ui-delete-degree';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->deleteDegree($degree);
        return redirect()->route('admin-index', 'admin')->with($msg['session'], $msg['message']);
    }

    public function viewUsers()
    {
        $action = 'ui-view-users';
        $this->trafficRepository->getRemoteAddress($action);

        $users = $this->adminRepository->getAllUsers();
        $admins = $this->adminRepository->getAllAdministrators();
        $enterprises = $this->adminRepository->getAllEnterprises();

        return view('users.users.dashboard')->with(
            [
                'page' => 'users',
                'users' => $users,
                'admins' => $admins,
                'enterprises' => $enterprises,
            ]
        );
    }

    public function viewOneUser(User $user)
    {
        $action = 'ui-view-users';
        $this->trafficRepository->getRemoteAddress($action);

        $roles = $this->adminRepository->getAllRoles();

        return view('users.users.dashboard')->with(
            [
                'page' => 'details',
                'user' => $user,
                'roles' => $roles,
            ]
        );
    }

    public function changeRoles(Request $request, User $user)
    {
        $action = 'ui-change-roles';
        $this->trafficRepository->getRemoteAddress($action);
        $msg = $this->adminRepository->changeRole($request, $user);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function getLang()
    {
        $locale = $this->adminRepository->getLang();
        return response()->json($locale);
    }


    public function storeCodes(Request $request)
    {
        $action = 'ui-store-code';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->insertNewCode($request);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function showCode(Code $code)
    {
        $action = 'ui-show-code';
        $this->trafficRepository->getRemoteAddress($action);
        return view('users.users.dashboard')->with(
            [
                'page' => 'code',
                'code' => $code,
            ]
        );
    }

    public function updateCodes(Request $request, Code $code)
    {
        $action = 'ui-update-degree';
        $this->trafficRepository->getRemoteAddress($action);
        $msg = $this->adminRepository->updateCode($request, $code);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function deleteCode(Code $code)
    {
        $action = 'ui-delete-degree';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->adminRepository->deleteCode($code);
        return redirect()->route('admin-index', 'admin')->with($msg['session'], $msg['message']);
    }

    public function restoreSoftDeletedPost(Post $post)
    {
        $post->update(
            [
                'is_deleted' => false,
            ]
        );
        return redirect()->back()->with('success', 'Success');
    }

    public function showSoftDeletedPost(Post $post)
    {
        $reacts = Reaction::all();
        return view('posts.admin.show')->with(
            [
                'post' => $post,
                'reacts' => $reacts,
            ]
        );
    }

    public function hardDestroy(Post $post)
    {
        $post->delete();
        return redirect()->route('admin-index', 'admin')->with('success','Success');
    }

    public function hardDestroyAll()
    {
        Post::where('is_deleted', 1)->delete();
        return redirect()->route('admin-index', 'admin')->with('success','Success');
    }



    public function cat()
    {


        $cat = Category::all();
        $spec = Speciality::all();
        foreach ($cat as $item) {
            foreach ($spec as $value){
                $item->specialities()->attach($value->id);
            }
        }
    }

    public function verify()
    {
        $users = User::all();
        foreach ($users as $user)
        {
            $user->update(
                [
                    'is_verified' => true,
                ]
            );
        }
        return redirect()->back()->with('success', 'All users are verified now!');
    }

    public function verifyUser(User $user)
    {
        $user->update(
            [
                'is_verified' => true,
            ]
        );

        return redirect()->back()->with('success', 'User was successfully verified!');
    }

    public function createEnterprise()
    {
        return view('users.users.administrator.enterprise');
    }

    public function storeEnterprise(Request $request)
    {
        $user = $this->adminRepository->storeEnterprise($request);
        return redirect()->route('admin-view-users')->with('success', 'New Enterprise user, with name: ' . $user->name . ' is created!');
    }
}
