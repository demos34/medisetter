<?php

namespace App\Http\Controllers\Categories;

use App\Http\Controllers\Controller;
use App\Models\Category\Subcategory;
use App\Repositories\Category\SubcategoryRepositoryInterface;
use App\Repositories\Traffic\TrafficRepositoryInterface;
use Illuminate\Http\Request;

class SubcategoriesController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var SubcategoryRepositoryInterface
     */
    private $subcategoryRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                SubcategoryRepositoryInterface $subcategoryRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->subcategoryRepository = $subcategoryRepository;
    }

    public function  show(Subcategory $subcategory)
    {
        $action = 'ui-index';
        $this->trafficRepository->getRemoteAddress($action);

        return view('categories.subcategories.show')->with(
            [
                'subcategory' => $subcategory,
            ]
        );
    }
}
