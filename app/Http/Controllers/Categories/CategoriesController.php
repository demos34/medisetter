<?php

namespace App\Http\Controllers\Categories;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\Traffic\TrafficRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                CategoryRepositoryInterface $categoryRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $action = 'ui-index';
        $this->trafficRepository->getRemoteAddress($action);

        $categories = $this->categoryRepository->getAllCategories();

        return view('categories.categories.index')->with(
            [
                'categories' => $categories,
            ]
        );
    }

    public function show(Category $category)
    {
        $action = 'ui-index';
        $this->trafficRepository->getRemoteAddress($action);

        if(!Auth::check()){
            return redirect()->route('categories-not-log', $category->slug);
        }

        if(implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) == 'Enterprise'){
            return redirect()->route('analyze');
        }

        if($category->id > 6){
            return redirect()->back();
        }

        return view('categories.categories.show')->with(
            [
                'category' => $category,
            ]
        );
    }

    public function isNotLoggedUser(Category $category)
    {
        $news = Category::findOrFail(1);
        $drug = Category::findOrFail(2);
        $events = Category::findOrFail(4);
        $disc = Category::findOrFail(5);
        $paid = Category::findOrFail(6);

        $eventPost = $events->posts->sortByDesc('created_at')->first();

        $newsPost = $this->categoryRepository->getLeadPosts($news);
        $drugPost = $this->categoryRepository->getLeadPosts($drug);
        $discPost = $this->categoryRepository->getLeadPosts($disc);
        $eventPost = $events->posts->sortByDesc('created_at')->first();
        $careerPost = $this->categoryRepository->getLeadPosts($paid);

        return view('categories.categories.not-logged')->with(
            [
                'news' => $news,
                'drug' => $drug,
                'event' => $events,
                'discussion' => $disc,
                'career' => $paid,
                'newsPost' => $newsPost,
                'drugPost' => $drugPost,
                'discussionPost' => $discPost,
                'eventPost' => $eventPost,
                'careerPost' => $careerPost,
            ]
        );
    }
}
