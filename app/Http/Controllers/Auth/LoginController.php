<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Admin\Login;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller{
    public function authenticate(Request $request){
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // if success login
            Login::create(
                [
                    'user_id' => Auth::user()->id,
                ]
            );
            if(Auth::user()->is_verified === 1){
                if(implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) == 'Enterprise'){
                    return redirect()->route('analyze');
                }
                return redirect()->route('home');
            } else {
                return redirect()->route('user-not-verified');
            }

            //return redirect()->intended('/details');
        }
        // if failed login
        return redirect('login');
    }
}
