<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Message\Message;
use App\Models\User;
use App\Models\User\Contact;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ContactsController extends Controller
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(UserRepositoryInterface $userRepository)
    {

        $this->userRepository = $userRepository;
    }
    public function  index()
    {
        return view('users.messages.index');
    }

    public function get()
    {
        $contacts = Auth::user()->contacts()->with('user')->get();

        return response()->json($contacts);
    }

    public function getMessagesFor($id)
    {

        $messages = Message::where('from_id', $id)->orWhere('to_id', $id)->get();

        return response()->json($messages);
    }

    public function send(Request $request)
    {
        $message = Message::create(
            [
                'from_id' => auth()->user()->id,
                'to_id' => $request->contact_id,
                'message' => $request->text,
            ]
        );

        return response()->json($message);
    }

    public function  addToContacts(User $user)
    {
        $this->userRepository->addToContacts($user);
        return redirect()->route('msg');
    }


//    public function token()
//    {
//        $token = bin2hex(openssl_random_pseudo_bytes(40));
//        echo $token . PHP_EOL . strlen($token);
//
//        foreach (User::all() as $user)
//        {
//            $user->update(
//                [
//                    'api_token' => bin2hex(openssl_random_pseudo_bytes(40)),
//                ]
//            );
//        }
//    }


}
