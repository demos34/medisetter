<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Models\Post\Post;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\Traffic\TrafficRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                CategoryRepositoryInterface $categoryRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->categoryRepository = $categoryRepository;
    }

    public function index()
    {
        $action = 'ui-index';
        $this->trafficRepository->getRemoteAddress($action);

        $news = Category::findOrFail(1);
        $drug = Category::findOrFail(2);
        $events = Category::findOrFail(4);
        $disc = Category::findOrFail(5);
        $paid = Category::findOrFail(6);

        $eventPost = $events->posts->sortByDesc('created_at')->first();

        if (Auth::check()) {

            if(implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) == 'Enterprise'){
                return redirect()->route('analyze');
            }
            $newsNewslettersPosts = $this->categoryRepository->getNewsLettersPost($news, Auth::user());
            $drugNewslettersPosts = $this->categoryRepository->getNewsLettersPost($drug, Auth::user());
            $discNewslettersPosts = $this->categoryRepository->getNewsLettersPost($disc, Auth::user());
            $eventsNewslettersPosts = $this->categoryRepository->getNewsLettersPost($events, Auth::user());
            $paidNewslettersPosts = $this->categoryRepository->getNewsLettersPost($paid, Auth::user());

            $lastNewsPost = $this->categoryRepository->getLastPostFromCategory($news);
            $lastDrugPost = $this->categoryRepository->getLastPostFromCategory($drug);
            $lastDiscPost = $this->categoryRepository->getLastPostFromCategory($disc);
            $lastPaidPost = $this->categoryRepository->getLastPostFromCategory($paid);
            return view('home.index')->with(
                [
                    'news' => $news,
                    'drug' => $drug,
                    'event' => $events,
                    'discussion' => $disc,
                    'career' => $paid,
                    'newsPosts' => $newsNewslettersPosts,
                    'drugPosts' => $drugNewslettersPosts,
                    'discussionPosts' => $discNewslettersPosts,
                    'eventPosts' => $eventsNewslettersPosts,
                    'careerPosts' => $paidNewslettersPosts,
                    'newsLast' => $lastNewsPost,
                    'drugLast' => $lastDrugPost,
                    'discussionLast' => $lastDiscPost,
                    'eventLast' => $eventPost,
                    'careerLast' => $lastPaidPost,
                ]
            );

        }

        $newsPost = $this->categoryRepository->getLeadPosts($news);
        $drugPost = $this->categoryRepository->getLeadPosts($drug);
        $discPost = $this->categoryRepository->getLeadPosts($disc);
        $eventPost = $events->posts->sortByDesc('created_at')->first();
        $careerPost = $this->categoryRepository->getLeadPosts($paid);

        return view('home.home')->with(
            [
                'news' => $news,
                'drug' => $drug,
                'event' => $events,
                'discussion' => $disc,
                'career' => $paid,
                'newsPost' => $newsPost,
                'drugPost' => $drugPost,
                'discussionPost' => $discPost,
                'eventPost' => $eventPost,
                'careerPost' => $careerPost,
            ]
        );
    }

    public function layout()
    {
        return view('new-view.layout');
    }

    public function noLog()
    {
        return view('new-view.views.nologin');
    }

    public function newLogin()
    {
        return view('new-view.views.login');
    }

    public function newsFeed()
    {
        return view('new-view.views.news-feed');
    }
}
