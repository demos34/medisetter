<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Models\Category\Speciality;
use App\Models\Post\Event;
use App\Models\Post\Post;
use App\Models\User;
use App\Models\User\CareerInterested;
use App\Models\User\CareerParticipate;
use App\Models\User\Interested;
use App\Models\User\Participate;
use App\Repositories\Traffic\TrafficRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                UserRepositoryInterface $userRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $action = 'ui-index';
        $this->trafficRepository->getRemoteAddress($action);

        $page = 'overview';

        return view('users.users.dashboard')->with(
            [
                'page' => $page,
            ]
        );
    }

    public function redirectDashboard($page)
    {
        $action = 'ui-redirect';
        $this->trafficRepository->getRemoteAddress($action);

        $specialities = $this->userRepository->getAllSpecialities();
        $universities = $this->userRepository->getAllUniversities();
        $degrees = $this->userRepository->getAllEducation();
        $name = App::getLocale() . '_name';

        return view('users.users.dashboard')->with(
            [
                'page' => $page,
                'specialities' => $specialities,
                'universities' => $universities,
                'degrees' => $degrees,
                'name' => $name,
            ]
        );
    }

    public function  updateUser(Request $request, $page)
    {
        $action = 'ui-update';
        $this->trafficRepository->getRemoteAddress($action);

       $this->userRepository->updateProfile($request);
       return redirect()->back()->with('success', 'Profile is successfully updated!');
    }

    public function  updateUniversity(Request $request)
    {
        if(implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) == 'Enterprise'){
            return redirect()->route('analyze');
        }
        $action = 'ui-update';
        $this->trafficRepository->getRemoteAddress($action);

        $update = $this->userRepository->updateUniversity($request);
        if($update === FALSE){
            $msg = [
                'session' => 'alert',
                'message' => 'Please try again!',
            ];
        } else {
            $msg = [
                'session' => 'success',
                'message' => 'Your status is successfully updated!',
            ];
        }
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function updateSpeciality(Request $request)
    {
        if(implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) == 'Enterprise'){
            return redirect()->route('analyze');
        }
        $action = 'ui-update-speciality';
        $this->trafficRepository->getRemoteAddress($action);

        $update = $this->userRepository->updateSpeciality($request);
        if($update === FALSE){
            $msg = [
                'session' => 'alert',
                'message' => 'Please try again!',
            ];
        } else {
            $msg = [
                'session' => 'success',
                'message' => 'Your status is successfully updated!',
            ];
        }
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function updateOrganisation(Request $request)
    {
        $this->userRepository->updateOrganisation($request);
        $msg = [
            'session' => 'success',
            'message' => 'Your status is successfully updated!',
        ];
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function viewUser(User $user)
    {
        $action = 'ui-view-user';
        $this->trafficRepository->getRemoteAddress($action);

        if ($user->api_token === Auth::user()->api_token){
            $page = 'overview';

            return view('users.users.dashboard')->with(
                [
                    'page' => $page,
                ]
            );
        }



        if(Auth::user()->is_verified === 0) {
            return view('users.user.not-verify');
        }


        return view('users.users.view')->with('user', $user);
    }

    public function follow(User $user)
    {
        $action = 'ui-view-user';
        $this->trafficRepository->getRemoteAddress($action);

        if(Auth::user()->is_verified === 0) {
            return view('users.user.not-verify');
        }
        $msg = $this->userRepository->followUser($user);

        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function searchUser(Request $request)
    {
        $action = 'ui-view-user';
        $this->trafficRepository->getRemoteAddress($action);
        if ($this->userRepository->searchUser($request) === FALSE){
            return redirect()->back()->with('alert', 'There is no such user here!');
        }
        $users = $this->userRepository->searchUser($request);
        return view('users.users.dashboard')->with(
            [
                'page' => 'users',
                'users' => $users,
            ]
        );
    }

    public function notVerifiedYet()
    {
        if (Auth::user()->is_verified == 1){
            return redirect()->route('home');
        }
        return view('users.user.not-verify');
    }

    public function changeAge(Request $request)
    {
        if(implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) == 'Enterprise'){
            return redirect()->route('analyze');
        }
        $data = $request->validate(
            [
                'age' => 'required|integer',
            ]
        );
        Auth::user()->profile->update(
            [
                'age' => $data['age'],
            ]
        );
        return redirect()->back()->with('success', 'Successfully!');
    }

    public function changeUsersLocation(Request $request)
    {
        if(implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) == 'Enterprise'){
            return redirect()->route('analyze');
        }
        $data = $request->validate(
            [
                'im_from' => 'required|string|min:5|max:100',
            ]
        );
        Auth::user()->profile->update(
            [
                'location' => $data['im_from'],
            ]
        );
        return redirect()->back()->with('success', 'Successfully!');
    }

    public function myEvents()
    {

        $me = Auth::user();
        $now = date('Y-m-d G:i:s');
        $role = implode(',' , $me->roles()->get()->pluck('role')->toArray());

//        if($me->author === NULL){
//            return redirect()->back()->with('alert', 'You havent posts yet!');
//        }
//        if($role != 'Developer' && $role != 'Administrator'){
//            if($me->author->posts->count() < 0) {
//                return redirect()->back()->with('alert', 'You havent posts yet!');
//            }
//            if ($me->author->posts()->with('event')->whereHas('event', function ($q) use ($now){
//                    $q->where('post_id', '<>', '0');
//                })->count() < 1) {
//                return redirect()->back()->with('alert', 'You havent events yet!');
//            }
//        }

        if($role == 'Developer' || $role == 'Administrator'){
            if($me->author !== NULL){
                $allUpcomingEvents = Post::with('event')
                    ->whereHas('event', function ($q) use ($now){
                        $q->where('start_date', '>', $now);
                    })
                    ->where('author_id', '<>', $me->author->id)
                    ->get();

                $allPastEvents = Post::with('event')
                    ->whereHas('event', function ($q) use ($now){
                        $q->where('start_date', '<', $now);
                    })
                    ->where('author_id', '<>', $me->author->id)
                    ->get();
            } else {
                $allUpcomingEvents = Post::with('event')
                    ->whereHas('event', function ($q) use ($now){
                        $q->where('start_date', '>', $now);
                    })
                    ->get();

                $allPastEvents = Post::with('event')
                    ->whereHas('event', function ($q) use ($now){
                        $q->where('start_date', '<', $now);
                    })
                    ->get();
            }
        } else {
            $allUpcomingEvents = null;
            $allPastEvents = null;
        }

        $upcomingInterested = $me->interesteds()->with('event')->with('event.post')
            ->whereHas('event', function ($q) use ($now) {
                $q->where('start_date', '>', $now);
            })
            ->get()
            ->sortBy('event.start_date');
        $pastInterested = $me->interesteds()->with('event')
            ->whereHas('event', function ($q) use ($now) {
                $q->where('start_date', '<', $now);
            })
            ->get()
            ->sortBy('event.start_date');

        $upcomingParticipate = $me->participates()->with('event')
            ->whereHas('event', function ($q) use ($now) {
                $q->where('start_date', '>', $now);
            })
            ->get()
            ->sortBy('event.start_date');
        $pastParticipate = $me->participates()->with('event')
            ->whereHas('event', function ($q) use ($now) {
                $q->where('start_date', '<', $now);
            })
            ->get()
            ->sortBy('event.start_date');
        if($me->author !== NULL){
            $postedUpcomingEvents = $me->author->posts()->with('event')
                ->whereHas('event', function ($q) use ($now){
                    $q->where('start_date', '>', $now);
                })
                ->get()
                ->sortByDesc('event.id');;
            $postedPastEvents = $me->author->posts()->with('event')
                ->whereHas('event', function ($q) use ($now){
                    $q->where('start_date', '<', $now);
                })
                ->get()
                ->sortByDesc('event.id');
        } else {
            $postedUpcomingEvents = null;
            $postedPastEvents = null;
        }


        return view('users.user.my-events')->with(
            [
                'upcomingInts' => $upcomingInterested,
                'upcomingParts' => $upcomingParticipate,
                'pastInts' => $pastInterested,
                'pastParts' => $pastParticipate,
                'me' => $me,
                'postedUpcomingEvents' => $postedUpcomingEvents,
                'postedPastEvents' => $postedPastEvents,
                'allUpcomingEvents' => $allUpcomingEvents,
                'allPastEvents' => $allPastEvents,
            ]
        );
    }

    public function listAllUsers($type, Post $post)
    {
        if($type == 'interested'){
            $users = Interested::where('event_id', $post->event->id)->get();
        } elseif ($type == 'participate'){
            $users = Participate::where('event_id', $post->event->id)->get();
        } elseif ($type == 'career-interested'){
            $users = CareerInterested::where('post_id', $post->id)->get();
        } elseif ($type == 'career-participate'){
            $users = CareerParticipate::where('post_id', $post->id)->get();
        } else {
            return redirect()->back();
        }
        $type = ucfirst(strtolower($type));

        return view('users.user.list')->with(
            [
                'users' => $users,
                'post' => $post,
                'type' => $type,
            ]
        );
    }

    public function myCareer()
    {
        $me = Auth::user();
        $role = implode(',' , $me->roles()->get()->pluck('role')->toArray());


//        if($me->author->posts->count() < 0) {
//            return redirect()->back()->with('alert', 'You havent posts yet!');
//        }
//        if($role != 'Developer' && $role != 'Administrator'){
//            if ($me->author->posts()->with('event')->whereHas('event', function ($q) use ($now){
//                    $q->where('post_id', '<>', '0');
//                })->count() < 1) {
//                return redirect()->back()->with('alert', 'You havent events yet!');
//            }
//        }
        if($role == 'Developer' || $role == 'Administrator') {
            if($me->author !== NULL) {
                $allCareers = Post::where('category_id', 6)
                    ->where('author_id', '<>', $me->author->id)
                    ->get();
            } else {
                $allCareers = Post::where('category_id', 6)
                    ->get();
            }
        } else {
            $allCareers = null;
        }

        return view('users.user.my-careers')->with(
            [
                'me' => $me,
                'allCareers' => $allCareers,
            ]
        );
    }
}
