<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Models\Role\Role;
use App\Models\User;
use App\Models\User\Contact;
use App\Repositories\Traffic\TrafficRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MessagesController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                UserRepositoryInterface $userRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->userRepository = $userRepository;
        $this->middleware('auth');
    }

//    public function index()
//    {
//        $action = 'ui-index';
//        $this->trafficRepository->getRemoteAddress($action);
//
//        return view('users.messages.index');
//    }

    public function index()
    {
        $action = 'ui-index';
        $this->trafficRepository->getRemoteAddress($action);

        return view('users.users.dashboard')->with(
            [
                'page' => 'message',
            ]
        );
    }

    public function attachContacts()
    {
        $action = 'ui-index';
        $this->trafficRepository->getRemoteAddress($action);

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('contact_user')->truncate();
        DB::table('contacts')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $users = User::all();
        $administrators = Role::with('users')->where('role', 'Administrator')->first();
        foreach ($users as $user) {
            Contact::create(
                [
                    'user_id' => $user->id,
                ]
            );
        }
        $contacts = Contact::all();
        foreach ($administrators->users as $administrator) {
            $administrator->contacts()->attach($contacts->where('user_id', '<>', $administrator->id));
        }


        return view('users.users.dashboard')->with(
            [
                'page' => 'message',
            ]
        );
    }
}
