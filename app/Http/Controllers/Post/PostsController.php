<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Models\Category\Category;
use App\Models\Category\Speciality;
use App\Models\Category\Subcategory;
use App\Models\Post\Post;
use App\Models\Post\Reaction;
use App\Models\Post\Share;
use App\Models\User\CareerInterested;
use App\Models\User\CareerParticipate;
use App\Models\User\Interested;
use App\Models\User\Participate;
use App\Repositories\Post\PostRepositoryInterface;
use App\Repositories\Traffic\TrafficRepositoryInterface;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    /**
     * @var TrafficRepositoryInterface
     */
    private $trafficRepository;
    /**
     * @var PostRepositoryInterface
     */
    private $postRepository;

    public function __construct(TrafficRepositoryInterface $trafficRepository,
                                PostRepositoryInterface $postRepository)
    {

        $this->trafficRepository = $trafficRepository;
        $this->postRepository = $postRepository;
    }

    public function index()
    {
        $action = 'ui-index';
        $this->trafficRepository->getRemoteAddress($action);
        return redirect()->back();
    }

    public function createFromCategories(Category $category)
    {
        $action = 'ui-create-cat';
        $this->trafficRepository->getRemoteAddress($action);

        $users = $this->postRepository->getAllEnterpriseUsers();
        $codes = $this->postRepository->getAllCodes();
        $categories = $this->postRepository->getAllCategories();
        $specialities = $this->postRepository->getAllSpecialities();
        $name = App::getLocale() . '_name';
        $cat = App::getLocale() . '_category';

        return view('posts.categories.create')->with(
            [
                'categories' => $categories,
                'current' => $category,
                'users' => $users,
                'codes' => $codes,
                'specialities' => $specialities,
                'name' => $name,
                'cat' => $cat,
            ]
        );
    }

    public function editAuthor(Post $post)
    {
        if ($post->author->user_id != Auth::user()->id) {
            return redirect()->back();
        }
        $category = Category::where('id', $post->category_id)->first();
        $users = $this->postRepository->getAllEnterpriseUsers();
        $codes = $this->postRepository->getAllCodes();
        $categories = $this->postRepository->getAllCategories();
        $specialities = $this->postRepository->getAllSpecialities();
        $name = App::getLocale() . '_name';
        $cat = App::getLocale() . '_category';
        $json = json_encode($post->specialities()->get()->pluck('id')->toArray());


        return view('posts.categories.edit-author')->with(
            [
                'categories' => $categories,
                'current' => $category,
                'users' => $users,
                'codes' => $codes,
                'specialities' => $specialities,
                'name' => $name,
                'cat' => $cat,
                'post' => $post,
                'json' => $json
            ]
        );
    }

    public function edit(Post $post)
    {
        $category = Category::where('id', $post->category_id)->first();
        $users = $this->postRepository->getAllEnterpriseUsers();
        $codes = $this->postRepository->getAllCodes();
        $categories = $this->postRepository->getAllCategories();
        $specialities = $this->postRepository->getAllSpecialities();
        $name = App::getLocale() . '_name';
        $cat = App::getLocale() . '_category';
        $json = json_encode($post->specialities()->get()->pluck('id')->toArray());


        return view('posts.categories.edit')->with(
            [
                'categories' => $categories,
                'current' => $category,
                'users' => $users,
                'codes' => $codes,
                'specialities' => $specialities,
                'name' => $name,
                'cat' => $cat,
                'post' => $post,
                'json' => $json
            ]
        );
    }

    public function update(Request $request, Post $post)
    {
        $this->postRepository->updatePost($request, $post);
        return redirect()->route('posts-show', $post->slug);
    }

    public function updateAuthor(Request $request, Post $post)
    {
        if ($post->author->user_id != Auth::user()->id) {
            return redirect()->back();
        }
        $this->postRepository->updatePost($request, $post);
        return redirect()->route('posts-show', $post->slug);
    }

    public function createFromDiscussion(Category $category)
    {
        $action = 'ui-create-discussion';
        $this->trafficRepository->getRemoteAddress($action);

        if (implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) == 'Enterprise') {
            return redirect()->route('analyze');
        }

        $users = $this->postRepository->getAllEnterpriseUsers();
        $codes = $this->postRepository->getAllCodes();
        $categories = $this->postRepository->getAllCategories();
        $specialities = $this->postRepository->getAllSpecialities();
        $name = App::getLocale() . '_name';
        $cat = App::getLocale() . '_category';

        return view('posts.categories.discussion-create')->with(
            [
                'categories' => $categories,
                'current' => $category,
                'users' => $users,
                'codes' => $codes,
                'specialities' => $specialities,
                'name' => $name,
                'cat' => $cat,
            ]
        );
    }

    public function createFromSubcategories(Subcategory $subcategory)
    {
        $action = 'ui-create-subcat';
        $this->trafficRepository->getRemoteAddress($action);

        $users = $this->postRepository->getAllEnterpriseUsers();
        $codes = $this->postRepository->getAllCodes();
        $categories = $this->postRepository->getAllCategories();
        $current = $this->postRepository->getCurrentCategory($subcategory);
        $name = App::getLocale() . '_name';
        $cat = App::getLocale() . '_category';

        return view('posts.subcategories.create')->with(
            [
                'categories' => $categories,
                'current' => $current,
                'users' => $users,
                'codes' => $codes,
                'currentSub' => $subcategory,
                'name' => $name,
                'cat' => $cat,
            ]
        );
    }

    public function subcategories(Request $request, $id)
    {
        $action = 'ui-subcategories-ajax';
        $this->trafficRepository->getRemoteAddress($action);

        if ($request->ajax()) {
            return response()->json([
                'subcategories' => Subcategory::where('category_id', $id)->get()
            ]);
        }
    }

    public function store(Request $request)
    {
        $action = 'ui-store';
        $this->trafficRepository->getRemoteAddress($action);

        $post = $this->postRepository->storePost($request);

        return redirect()->route('posts-show', $post->slug);
    }

    public function storeDiscussion(Request $request)
    {

        if (implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) == 'Enterprise') {
            return redirect()->route('analyze');
        }
        $action = 'ui-store-discussion';
        $this->trafficRepository->getRemoteAddress($action);

        // always $request['categories'] will be with ID = 5 -> discussion;
        $request->merge(
            [
                'categories' => "5",
            ]
        );
        $post = $this->postRepository->storePost($request);

        return redirect()->route('posts-show', $post->slug);
    }

    public function show(Post $post)
    {
        $action = 'ui-show';
        $this->trafficRepository->getRemoteAddress($action);

        if (Auth::user()->is_verified === 0) {
            return view('users.user.not-verify');
        }

        if (implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) == 'Enterprise') {
            $ids = [];
            foreach ($post->users as $postUser) {
                array_push($ids, $postUser->id);
            }
            if (in_array(Auth::user()->id, $ids) === FALSE) {
                return redirect()->route('analyze');
            }
        }

        if ($post->event === NULL) {
            $dateForHumans = null;
            $hoursForHumans = null;
        } else {
            $start = $post->event->start_date;
            $end = $post->event->end_date;
            $carbonStart = Carbon::parse($start);
            $carbonEnd = Carbon::parse($end);
            $dateForHumans = $carbonStart->format('d M') . ' - ' . $carbonEnd->format('d M Y');
            $hoursForHumans = $carbonStart->format('H:i') . ' to ' . $carbonEnd->format('H:i');
        }

        if ($post->is_deleted === 1) {
            return redirect()->back()->with('alert', 'This post is not available!');
        }

        $views = $this->postRepository->views($post);
        $reacts = $this->postRepository->getAllReactions();

        return view('posts.categories.show')->with(
            [
                'post' => $post,
                'views' => $views,
                'reacts' => $reacts,
                'dateForHumans' => $dateForHumans,
                'hoursForHumans' => $hoursForHumans,
            ]
        );
    }

    public function reaction(Post $post, Reaction $reaction)
    {
        if ($post->is_deleted === 1) {
            return redirect()->back()->with('alert', 'This post is not available!');
        }
        if (implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) == 'Enterprise') {
            return redirect()->route('analyze');
        }
        $action = 'ui-reaction';
        $this->trafficRepository->getRemoteAddress($action);

        $msg = $this->postRepository->react($post, $reaction);
        return redirect()->back()->with($msg['session'], $msg['message']);
    }

    public function share(Request $request, Post $post)
    {

        if (implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) == 'Enterprise') {
            return redirect()->route('analyze');
        }
        if ($post->is_deleted === 1) {
            return redirect()->back()->with('alert', 'This post is not available!');
        }
        $action = 'ui-share';
        $this->trafficRepository->getRemoteAddress($action);

        $new = $this->postRepository->share($request, $post);

        return redirect()->route('posts-show', $new->slug)->with('success', 'You shared successfully post!');


    }

    public function showSpecialitiesPosts(Category $category, Speciality $speciality)
    {
        $action = 'ui-spec';
        $this->trafficRepository->getRemoteAddress($action);
        if (implode(',', Auth::user()->roles()->get()->pluck('role')->toArray()) == 'Enterprise') {
            return redirect()->route('analyze');
        }

        $speciality->posts->where('category_id', $category->id);

        if ($category->id > 6) {
            return redirect()->back();
        }

        return view('categories.spec.show')->with(
            [
                'category' => $category,
                'speciality' => $speciality,
            ]
        );

    }

    public function destroy(Post $post)
    {
        $me = Auth::user();
        $role = (int)implode(',', $me->roles()->get()->pluck('id')->toArray());
        if($role === 1 || $role === 2 || (int)$post->author->user_id === (int)$me->id) {
            $post->update(
                [
                    'is_deleted' => true,
                ]
            );
            return redirect()->route('categories-show', $post->category->slug)->with('success', 'Post is successfully deleted!');
        } else {
            return redirect()->back();
        }
    }

    public function search(Request $request)
    {
        $data = $request->validate(
            [
                'category' => 'exists:categories,id',
                'search' => 'required|min:1',
            ]
        );
        $posts = Post::with('category')
            ->where('title', 'LIKE', '%' . $data['search'] . '%')
            ->orWhere('body', 'LIKE', '%' . $data['search'] . '%')
            ->where('category_id', (int)$data['category'])
            ->get();
        $category = Category::findOrFail($data['category']);
        return view('categories.categories.search')->with(
            [
                'posts' => $posts,
                'category' => $category,
            ]
        );
    }

    public function eventInterested(Post $post)
    {
        if ($post->event) {
            $me = Auth::user();
            if ($me->interesteds->where('event_id', $post->event->id)->count() > 0) {
                $value = Interested::where('user_id', $me->id)
                    ->where('event_id', $post->event->id)
                    ->first();
                $value->delete();
                return redirect()->back()->with('success', 'You are unsubscribed as Interested');
            } else {
                Interested::create(
                    [
                        'event_id' => $post->event->id,
                        'user_id' => $me->id,
                    ]
                );
                return redirect()->back()->with('success', 'You are subscribed in as Interested');
            }
        } else {
            return redirect()->back();
        }
    }

    public function eventParticipate(Post $post)
    {
        if ($post->event) {
            $me = Auth::user();
            if ($me->participates->where('event_id', $post->event->id)->count() > 0) {
                $value = Participate::where('user_id', $me->id)
                    ->where('event_id', $post->event->id)
                    ->first();
                $value->delete();
                return redirect()->back()->with('success', 'You are unsubscribed as Participant');
            } else {
                Participate::create(
                    [
                        'event_id' => $post->event->id,
                        'user_id' => Auth::user()->id,
                    ]
                );

                if ($me->interesteds->where('event_id', $post->event->id)->count() > 0) {
                    $value = Interested::where('user_id', $me->id)
                        ->where('event_id', $post->event->id)
                        ->first();
                    $value->delete();
                }
                return redirect()->back()->with('success', 'You are subscribed in as Participant');
            }
        } else {
            return redirect()->back();
        }
    }

    public function careerInterested(Post $post)
    {
        if ($post->category->id === 6) {
            $me = Auth::user();
            if ($me->careerInteresteds->where('post_id', $post->id)->count() > 0) {
                $value = CareerInterested::where('user_id', $me->id)
                    ->where('post_id', $post->id)
                    ->first();
                $value->delete();
                return redirect()->back()->with('success', 'You are unsubscribed as Interested');
            } else {
                CareerInterested::create(
                    [
                        'post_id' => $post->id,
                        'user_id' => $me->id,
                    ]
                );
                return redirect()->back()->with('success', 'You are subscribed in as Interested');
            }
        } else {
            return redirect()->back();
        }
    }

    public function careerParticipate(Post $post)
    {
        if ($post->category->id === 6) {
            $me = Auth::user();
            if ($me->careerParticipates->where('post_id', $post->id)->count() > 0) {
                $value = CareerParticipate::where('user_id', $me->id)
                    ->where('post_id', $post->id)
                    ->first();
                $value->delete();
                return redirect()->back()->with('success', 'You are unsubscribed as Participant');
            } else {
                CareerParticipate::create(
                    [
                        'post_id' => $post->id,
                        'user_id' => $me->id,
                    ]
                );


                if ($me->careerInteresteds->where('post_id', $post->id)->count() > 0) {
                    $value = CareerInterested::where('user_id', $me->id)
                        ->where('post_id', $post->id)
                        ->first();
                    $value->delete();
                }
                return redirect()->back()->with('success', 'You are subscribed in as Participant');
            }
        } else {
            return redirect()->back();
        }
    }
}
