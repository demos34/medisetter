<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use MongoDB\Driver\Session;
use Illuminate\Support\Facades\Lang;

class LanguageMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
//        $response = $next($request);
        if(session()->has('locale')){
            app()->setLocale(session('locale'));
//            Lang::setLocale(session('locale'));
        }
        else {
            app()->setLocale(config('app.locale'));
//            Lang::setLocale(config('app.locale'));
        }
        return $next($request);
//        return $response;
    }
}
