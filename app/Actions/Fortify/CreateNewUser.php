<?php

namespace App\Actions\Fortify;

use App\Events\NewNotification;
use App\Events\NotificationCount;
use App\Models\Role\Role;
use App\Models\User;
use App\Models\User\Contact;
use App\Models\User\From;
use App\Models\User\Notification;
use App\Models\User\Profile;
use App\Models\User\To;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
        ])->validate();

        $user = User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
            'api_token' => bin2hex(openssl_random_pseudo_bytes(40)),
        ]);

        self::userCreating($user);

        return $user;
    }

    protected static function userCreating(User $user)
    {
        $user->roles()->attach(4);
        $user->update(
            [
                'avatar' => '/storage/helpers/profile.png',
            ]
        );
        Profile::create(
            [
                'user_id' => $user->id,
                'avatar' => '/storage/helpers/profile.png',
            ]
        );
        To::create(
            [
                'user_id' => $user->id,
            ]
        );

        From::create(
            [
                'user_id' => $user->id,
            ]
        );
        $contact = Contact::create(
            [
                'user_id' => $user->id,
            ]
        );

        $administrators = Role::with('users')->where('role', 'Administrator')->first();
        foreach ($administrators->users as $administrator){
            $administrator->contacts()->attach($contact->id);
            $user->contacts()->attach($administrator->id);


            $notification = Notification::create(
                [
                    'from_id' => $user->from->id,
                    'to_id' => $administrator->to->id,
                    'notification_type_id' => 10,
                    'user_id' => $user->id,
                    'is_viewed' => false,
                ]
            );

            broadcast(new NewNotification($notification));

            broadcast(new NotificationCount($notification, $administrator));
        }
    }
}
