<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    protected $redirectTo = '/user/login-message';

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin-dev', function ($user){
            return $user->hasAnyRoles(['Administrator', 'Developer']);
        });

        Gate::define('admin-enterprise', function ($user){
            return $user->hasAnyRoles(['Administrator', 'Developer', 'Enterprise']);
        });

        Gate::define('admin', function ($user){
            return $user->hasAnyRoles(['Administrator', 'Developer']);
        });

        Gate::define('admin-generic', function ($user){
            return $user->hasAnyRoles(['Administrator', 'Developer', 'Generic User']);
        });

        Gate::define('dev', function ($user){
            return $user->hasRole('Developer');
        });

        Gate::define('verify', function ($user){
            if($user->is_verified == 1){
                return true;
            } else {
                return false;
            }
        });
    }
}
