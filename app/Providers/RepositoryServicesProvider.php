<?php

namespace App\Providers;

use App\Repositories\Administrator\AdminRepository;
use App\Repositories\Administrator\AdminRepositoryInterface;
use App\Repositories\Administrator\AnalyzeRepository;
use App\Repositories\Administrator\AnalyzeRepositoryInterface;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Category\CategoryRepositoryInterface;
use App\Repositories\Category\SubcategoryRepository;
use App\Repositories\Category\SubcategoryRepositoryInterface;
use App\Repositories\Post\PostRepository;
use App\Repositories\Post\PostRepositoryInterface;
use App\Repositories\Traffic\TrafficRepository;
use App\Repositories\Traffic\TrafficRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(AdminRepositoryInterface::class, AdminRepository::class);
        $this->app->singleton(AnalyzeRepositoryInterface::class, AnalyzeRepository::class);
        $this->app->singleton(CategoryRepositoryInterface::class, CategoryRepository::class);
        $this->app->singleton(PostRepositoryInterface::class, PostRepository::class);
        $this->app->singleton(SubcategoryRepositoryInterface::class, SubcategoryRepository::class);
        $this->app->singleton(TrafficRepositoryInterface::class, TrafficRepository::class);
        $this->app->singleton(UserRepositoryInterface::class, UserRepository::class);
    }
}
