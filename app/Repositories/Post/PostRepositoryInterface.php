<?php

namespace App\Repositories\Post;

use App\Models\Category\Subcategory;
use App\Models\Post\Post;
use App\Models\Post\Reaction;
use Illuminate\Http\Request;

interface PostRepositoryInterface
{
    public function getAll();

    public function  getAllCodes();

    public function getAllEnterpriseUsers();

    public function  getAllCategories();

    public function  getAllSpecialities();

    public function  getCurrentCategory(Subcategory $subcategory);

    public function  getCategory(Subcategory $subcategory);

    public function storePost(Request $request);

    public function updatePost(Request $request, Post $post);

    public function views(Post $post);

    public function getAllReactions();

    public function  react(Post $post, Reaction $reaction);

    public function share(Request $request, Post $post);
}
