<?php


namespace App\Repositories\Post;


use App\Events\NewNotification;
use App\Events\NotificationCount;
use App\Models\Admin\AnalyzePost;
use App\Models\Category\Category;
use App\Models\Category\Speciality;
use App\Models\Category\Subcategory;
use App\Models\Code\Code;
use App\Models\Post\Comment;
use App\Models\Post\Event;
use App\Models\Post\Post;
use App\Models\Post\PostReaction;
use App\Models\Post\Reaction;
use App\Models\Post\Share;
use App\Models\Post\UserAgeLocation;
use App\Models\Role\Role;
use App\Models\User;
use App\Models\User\Author;
use App\Models\User\Notification;
use App\Models\User\Primary;
use App\Models\User\Secondary;
use App\Models\User\To;
use App\Models\View\Total;
use App\Models\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Self_;

class PostRepository implements PostRepositoryInterface
{
    public function  getAll()
    {
        return Post::all();
    }

    public function  getAllCodes()
    {
        return Code::all();
    }

    public function getAllEnterpriseUsers()
    {

        return Role::where('role', 'Enterprise')->first()->users()->get();
    }

    public function getAllCategories()
    {
        return Category::all();
    }

    public function  getAllSpecialities()
    {
        return Speciality::all();
    }

    public function  getCurrentCategory(Subcategory $subcategory)
    {
        return Category::findOrfail($subcategory->category->id);
    }

    public function getCategory(Subcategory $subcategory)
    {
        return Category::firstOrFail($subcategory->category_id);
    }

    public function storePost(Request $request)
    {
        $from = self::validateFromAge($request);
        $to = self::validateToAge($request);
        $location = self::validatedLocation($request);
        $ageUsers = self::getUsersInAgeRange($from, $to);
        $locationUsers = self::getUsersInLocation($location);
        $mergedArray = self::getUsersId($ageUsers, $locationUsers);

        $author = self::checkAuthor();
        $validated = self::validation($request);
        $imageName = self::storeImage($request, $validated);
        $fileName = self::storeDocument($request, $validated);

        $post = self::createPost($validated, $imageName, $fileName, $author->id);

        $post->update(
            [
                'from_age' => $from['from_age'],
                'to_age' => $to['to_age'],
                'location' => $location['location'],
            ]
        );

        //notify users in range
        if(count($mergedArray) > 0){
            self::notifyUsersInAgeRangeAndLocation($mergedArray);
        }

        //fill table
        if($ageUsers !== NULL) {
            self::insertRowsInUserAgeLocationsTable($ageUsers, 'age', $post->id);
        }
        if($locationUsers !== NULL) {
            self::insertRowsInUserAgeLocationsTable($locationUsers, 'location', $post->id);
        }


        AnalyzePost::create(
            [
                'post_id' => $post->id,
                'is_in' => true,
            ]
        );
        //notify followers of attached enterprise users
        if($validated['users'] !== NULL){
            foreach ($validated['users'] as $enterpriseId){
                $enterpriseUser = User::findOrFail((int)$enterpriseId);
                self::notify($enterpriseUser->to->id, $post->id, 1);
                if(isset($enterpriseUser->followers)){
                    $followers = $enterpriseUser->followers;
                    foreach ($followers as $follower){
                        self::notify($follower->user->to->id, $post->id, 5);
                    }
                }
            }
        }


        //notify primary and secondary notif
        if($validated['specialities'] !== NULL){
            foreach ($validated['specialities'] as $specialityId){
                $primary = Primary::where('speciality_id', (int)$specialityId)->first();
                $secondary = Secondary::where('speciality_id', (int)$specialityId)->first();
                if(isset($primary)){
                    self::notify($primary->user->to->id, $post->id, 7);
                }
                if(isset($secondary)){
                    self::notify($primary->user->to->id, $post->id, 8);
                }
            }
        }

        return $post;
    }

    public function updatePost(Request $request, Post $post)
    {

        $author = self::checkAuthor();
        $validated = self::validation($request);
        if($request->has('image')) {
            self::deleteImages($post);
            $imageName = self::storeImage($request, $validated);
        } else {
            $imageName = $post->image;
        }
         if($request->has('file')) {
            self::deleteFiles($post);
             $fileName = self::storeDocument($request, $validated);
        } else {
             $fileName = $post->document;
         }

        if($validated['codes'] === NULL && $post->code_id !== NULL) {
            $code = Code::findOrFail($post->code_id);
            $post->update(
                [
                    'code_id' => null,
                ]
            );
            $code->delete();
        } elseif($validated['codes'] === NULL && $post->code_id === NULL) {
            $post->update(
                [
                    'code_id' => $validated['codes'],
                ]
            );
        }  else {
            if($post->code_id === NULL)
            {
                $code = Code::create(
                    [
                        'code' => $validated['codes'],
                    ]
                );
                $post->update(
                    [
                        'code_id' => $code->id,
                    ]
                );
            } else {
                $post->code->update(
                    [
                        'code' => $validated['codes'],
                    ]
                );
            }
        }
        $post->update(
            [
                'title' => $validated['title'],
                'body' => $validated['body'],
                'category_id' => $validated['categories'],
                'image' => $imageName,
                'video' => $validated['youtube'],
                'document' => $fileName,
                'author_id' => $author->id,
            ]
        );

        if ($validated['categories'] == 4){
            $post->event->update(
                [
                    'start_date' => $validated['start_date'],
                    'end_date' => $validated['end_date'],
                ]
            );
        }

        $post->subcategories()->sync($validated['subcategories']);
        $post->users()->sync($validated['users']);
        $post->specialities()->sync($validated['specialities']);
    }


    public function views(Post $post)
    {
        if (Auth::check()){
            $userId = Auth::user()->id;
        } else {
            $userId = NULL;
        }
        View::create(
            [
                'post_id' => $post->id,
                'user_id' => $userId,
            ]
        );
        $total = Total::where('post_id', $post->id)->first();
        if (!isset($total->id)){
            $total = Total::create(
                [
                    'post_id' => $post->id,
                    'count' => 1,
                ]
            );
        } else {
            $total->update(
                [
                    'count' => ($total->count + 1),
                ]
            );
        }

        return $total;
    }

    public function getAllReactions()
    {
        return Reaction::all();
    }

    public function  react(Post $post, Reaction $reaction)
    {
        if($post && $reaction)
        {
            $notifType = $reaction->id + 1;

            if($postReaction = PostReaction::where('post_id', $post->id)->where('user_id', Auth::user()->id)->first()){
                if($postReaction->reaction_id === $reaction->id){
                    $postReaction->delete();
                    $msg = self::msg('warning', 'You get back your vote!');
                } else {
                    $postReaction->update(
                        [
                            'reaction_id' => $reaction->id,
                        ]
                    );
                    self::notify($post->author->user->to->id, $post->id, $notifType);
                    $msg = self::msg('success', 'You reacted with ' . $reaction->en_name);
                }
            } else {
                PostReaction::create(
                    [
                        'post_id' => $post->id,
                        'reaction_id' => $reaction->id,
                        'user_id' => Auth::user()->id,
                    ]
                );

                self::notify($post->author->user->to->id, $post->id, $notifType);
                $msg = self::msg('success', 'You reacted with ' . $reaction->en_name);
            }
        } else {
            $msg = self::msg('alert', 'Ooops! Something is wrong!');
        }
        return $msg;
    }

    public function share(Request $request, Post $post)
    {
        $title = self::validateTitle($request);
        if(Auth::user()->author === NULL){
            $author = Author::create(
                [
                    'user_id' => Auth::user()->id,
                ]
            );
            $authorId = $author->id;
        } else{
            $authorId = Auth::user()->author->id;
        }
        $share = Share::create(
            [
                'title' => $title['title'],
                'post_id' => $post->id,
            ]
        );


        $newPost = Post::create(
            [
                'title' => $post->title,
                'body' => $post->body,
                'slug' => NULL,
                'category_id' => 5,
                'image' => $post->image,
                'video' => $post->video,
                'document' => $post->document,
                'code_id' => $post->code_id,
                'author_id' => $authorId,
                'is_shared' => true,
                'share_id' => $share->id,
            ]
        );

        if ($request->title === NULL){
            Comment::create(
                [
                    'body' => 'Shared by ' . Auth::user()->name,
                    'is_visible' => true,
                    'post_id' => $post->id,
                    'user_id' => Auth::user()->id,
                ]
            );
        } else {
            Comment::create(
                [
                    'body' => 'Shared with title: ' . $title['title'] . 'by ' . Auth::user()->name,
                    'is_visible' => true,
                    'post_id' => $post->id,
                    'user_id' => Auth::user()->id,
                ]
            );

            Comment::create(
                [
                    'body' => $title['title'],
                    'is_visible' => true,
                    'post_id' => $newPost->id,
                    'user_id' => Auth::user()->id,
                ]
            );
        }



        $newPost->update(
            [
                'slug' => Str::slug($newPost->title . '-' . $newPost->id),
            ]
        );

        return $newPost;
    }

    protected static function createPost($validated, $imageName, $fileName, $author_id)
    {
        if($validated['codes'] !== NULL) {
            $code = Code::create(
                [
                    'code' => $validated['codes'],
                ]
            );
            $codeId = $code->id;
        } else {
            $codeId = NULL;
        }
        $post = Post::create(
            [
                'title' => $validated['title'],
                'body' => $validated['body'],
                'slug' => 'NULL',
                'category_id' => $validated['categories'],
                'image' => $imageName,
                'video' => $validated['youtube'],
                'document' => $fileName,
                'code_id' => $codeId,
                'author_id' => $author_id,
                'is_attached' => 1,
            ]
        );

        if ($validated['categories'] == 4){
            Event::create(
                [
                    'post_id' => $post->id,
                    'start_date' => $validated['start_date'],
                    'end_date' => $validated['end_date'],
                ]
            );
        }

        $post->subcategories()->attach($validated['subcategories']);
        $post->users()->attach($validated['users']);
        $post->specialities()->attach($validated['specialities']);

        $post->update(
            [
                'slug' => Str::slug($post->title . '-' . $post->id),
            ]
        );

        return $post;
    }

    protected static function storeImage(Request $request, $validated)
    {

        if($request->has('image')){
            $file = $validated['image']->getClientOriginalName();
            $fileName = pathinfo($file, PATHINFO_FILENAME);
            $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
            $date = date('Y-m-d-H-i-s');
            $fileFullName = $date . '-' . $fileName . '.' . $fileExtension;
            $imagePath = '/storage/posts/images/' . $fileFullName;
            $request->image->storeAs('posts/images', $fileFullName, 'public');
        } else {
            $imagePath = '/storage/custom-nav/logo.png';
        }

        return $imagePath;
    }

    protected static function storeDocument(Request $request, $validated)
    {
        if($request->has('file')){
            $file = $validated['file']->getClientOriginalName();
            $fileName = pathinfo($file, PATHINFO_FILENAME);
            $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
            $date = date('Y-m-d-H-i-s');
            $fileSlug = $date . '-' . $fileName;
            $fileFullName = Str::slug($fileSlug) . '.' . $fileExtension;
            $filePath = '/storage/posts/files/' . $fileFullName;
            $request->file->storeAs('posts/files', $fileFullName, 'public');
        } else {
            $filePath = NULL;
        }

        return $filePath;
    }

    protected static function checkAuthor()
    {
        if (Author::where('user_id', Auth::user()->id)->first() != NULL){
            return Author::where('user_id', Auth::user()->id)->first();
        } else {
            return Author::create(
                [
                    'user_id' => Auth::user()->id,
                ]
            );
        }
    }

    protected static function validation(Request $request)
    {
        $validated = self::validateStoreRequest($request);
        $validated = self::checkForImagesVideosFiles($request, $validated);
        $validated = self::checkSubcategories($request, $validated);
        $validated = self::checkSpecialities($request, $validated);
        $validated = self::checkCodesAndStartAndEndDates($request, $validated);

        return self::checkEnterpriseUsers($request, $validated);
    }

    protected static function checkSpecialities(Request $request, $validated)
    {
        if($request->has('specialities')){
            $specialities = $request->validate(
                [
                    'specialities' => "array|nullable|required|exists:specialities,id|exists:category_speciality,speciality_id,category_id,{$request->get('categories')}",
                ]
            );
        } else {
            $specialities = ['specialities' => NULL,];
        }

        return array_merge($validated, $specialities);
    }

    protected static function checkSubcategories(Request $request, $validated)
    {
        if($request->has('subcategories')){
            $subcategories = $request->validate(
                [
                    'subcategories' => "required|exists:subcategories,id,category_id,{$request->get('categories')}",
                ]
            );
        } else {
            $subcategories = ['subcategories' => NULL,];
        }

        return array_merge($validated, $subcategories);
    }

    protected static function checkEnterpriseUsers(Request $request, $validated)
    {
        if($request->has('users')){
            $users = $request->validate(
                [
                    'users' => 'array|nullable|exists:users,id',
                ]
            );
        } else {
            $users = ['users' => NULL,];
        }

        return array_merge($validated, $users);
    }

    protected static function checkCodesAndStartAndEndDates(Request $request, $validated)
    {

        $code = [
            'codes' => NULL,
        ];
        if($request->has('codes')){
            $code = $request->validate(
                [
                    'codes' => 'required|min:2|max:10'
                ]
            );
        }
        $date = [
            'start_date' => NULL,
            'end_date' => NULL,
        ];

        if($request->has('start_date') && $request->has('end_date')){
            if($request->has('start_date') != NULL && $request->has('end_date') != NUll)
            $date = $request->validate(
                [
                    'start_date' => 'required|date',
                    'end_date' => 'required|date|after:start_date',
                ]
            );
        }

        if((int)$validated['subcategories'] !== 4){
            $code = [
                'codes' => NULL,
            ];
        }


        $validated = array_merge($validated, $code);
        return array_merge($validated, $date);

    }

    protected static function checkForImagesVideosFiles(Request $request, $validated)
    {
        if($request->has('youtube')){
            $youtube = $request->validate(
                [
                    'youtube' => 'nullable|string',
                ]
            );
        } else {
            $youtube = ['youtube' => NULL,];
        }
        if ($request->has('file')){
            $file = $request->validate(
                [
                    'file' => 'nullable|file|max:50000|mimes:pdf,zip,doc,docx,xls,xlsx',
                ]
            );
        } else {
            $file = ['file' => NULL,];
        }
        if ($request->has('image')){
            $image = $request->validate(
                [
                    'image' => 'nullable|image',
                ]
            );
        } else {
            $image = ['image' => NULL,];
        }
        $checkedTube = array_merge($validated, $youtube);
        $checkedFile = array_merge($checkedTube, $file);
        return array_merge($checkedFile, $image);

    }

    protected static function validateStoreRequest(Request $request)
    {
        return $request->validate(
            [
                'title' => 'required|min:3|max:100|string',
                'body' => 'required|min:10|max:5000',
                'categories' => 'required|exists:categories,id',
            ]
        );
    }

    protected static function validateTitle(Request $request)
    {
        if ($request->title === NULL){
            $title = [
                'title' => NULL,
            ];
        } else {
            $title = $request->validate(
                [
                    'title' => 'string',
                ]
            );
        }

        return $title;
    }

    protected static function msg($session, $message)
    {
        return [
            'session' => $session,
            'message' => $message,
        ];
    }

    protected function deleteImages(Post $post)
    {
        $originalImage = $post->image;
        $array = explode('/', $originalImage);
        $fileName = $array['4'];
        $path = '/public/' . $array['2'] . '/' . $array['3'] . '/' . $fileName;
        Storage::delete($path);
    }

    protected function deleteFiles(Post $post)
    {
        if($post->document != NULL) {
            $originalImage = $post->document;
            $array = explode('/', $originalImage);
            $fileName = $array['4'];
            $path = '/public/' . $array['2'] . '/' . $array['3'] . '/' . $fileName;
            Storage::delete($path);
        }
    }

    protected static function validateFromAge(Request $request)
    {
        if($request->has('from_age')){
            if($request->from_age === NULL || (int)$request->from_age < 0){
                $spec =
                    [
                        'from_age' => NULL,
                    ];
            } else {
                $spec = $request->validate(
                    [
                        'from_age' => 'required|min:1|integer'
                    ]
                );
            }

        } else {
            $spec =
                [
                    'from_age' => NULL,
                ];
        }
        return $spec;
    }

    protected static function validateToAge(Request $request)
    {
        if($request->has('to_age')){
            if($request->to_age === NULL || (int)$request->to_age < 0){
                $spec =
                    [
                        'to_age' => NULL,
                    ];
            } else {
                $spec = $request->validate(
                    [
                        'to_age' => 'required|min:0|integer'
                    ]
                );
            }

        } else {
            $spec =
                [
                    'to_age' => NULL,
                ];
        }
        return $spec;
    }

    protected static function validatedLocation(Request $request)
    {
        if($request->has('location')){
            if($request->location === NULL){
                $spec =
                    [
                        'location' => NULL,
                    ];
            } else {
                $spec = $request->validate(
                    [
                        'location' => 'required|min:5|max:50'
                    ]
                );
            }
        } else {
            $spec =
                [
                    'location' => NULL,
                ];
        }
        return $spec;
    }

    protected static function getFromAge($from)
    {
        if($from['from_age'] === NULL || (int)$from['from_age'] < 0){
            $fromAge = 0;
        } elseif((int)$from['from_age'] > 110){
            $fromAge = 110;
        } else {
            $fromAge = $from['from_age'];
        }

        return $fromAge;
    }

    protected static function getToAge($to)
    {
        if($to['to_age'] === NULL || (int)$to['to_age'] > 150 || $to['to_age'] < 0){
            $toAge = 150;
        } else {
            $toAge = $to['to_age'];
        }

        return $toAge;
    }

    protected static function getUsersInAgeRange($from, $to)
    {
        $fromAge = self::getFromAge($from);
        $toAge = self::getToAge($to);

        if ($from['from_age'] !== NULL || $to['to_age'] !== NULL){
            $ageUsers = User::with('profile')
                ->whereHas('profile', function($q) use($fromAge, $toAge){
                    $q->where('age', '>' , $fromAge);
                    $q->where('age', '<', $toAge);
                })
                ->get();
        } else {
            $ageUsers = NULL;
        }

        return $ageUsers;
    }

    protected static function getUsersInLocation($location)
    {
        if ($location['location'] !== NULL){
            $locationUsers = User::with('profile')
                ->whereHas('profile', function($q) use($location){
                    $q->where('location', 'LIKE' , '%' . $location['location'] . '%');
                })
                ->get();
        } else {
            $locationUsers = NULL;
        }

        return $locationUsers;
    }

    protected static function getUsersId($ageUsers, $locationUsers)
    {
        $usersId = [];
        if($ageUsers !== NULL){
            foreach ($ageUsers as $ageUser){
                array_push($usersId,$ageUser->id);
            }
        }
        if($locationUsers !== NULL){
            foreach ($locationUsers as $locationUser){
                array_push($usersId,$locationUser->id);
            }
        }

        $mergedArray = [];

        foreach ($usersId as $value){
            if(!in_array($value, $mergedArray)){
                array_push($mergedArray, $value);
            }
        }

        return $mergedArray;
    }

    protected static function notifyUsersInAgeRangeAndLocation($usersId)
    {
        foreach ($usersId as $id){
            $user = User::findOrFail($id);
            self::notify($user->to->id, 1, 1);
        }
    }

    protected static function insertRowsInUserAgeLocationsTable($users, $flag, $postId)
    {
        foreach ($users as $user){
            UserAgeLocation::create(
                [
                    'post_id' => $postId,
                    'user_id' => $user->id,
                    'flag' => $flag,
                ]
            );
        }
    }

    protected static function notify($toId, $postId, $typeId)
    {
        $notification = Notification::create(
            [
                'from_id' => Auth::user()->from->id,
                'to_id' => $toId,
                'post_id' => $postId,
                'notification_type_id' => $typeId,
                'user_id' => Auth::user()->id,
                'is_viewed' => false,
            ]
        );

        $tos = To::where('id', $toId)->first();

        $receiver = User::where('id', $tos->user_id)->first();

        broadcast(new NewNotification($notification));

        broadcast(new NotificationCount($notification, $receiver));
    }

}
