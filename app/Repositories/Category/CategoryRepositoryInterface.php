<?php

namespace App\Repositories\Category;

use App\Models\Category\Category;
use App\Models\User;

interface CategoryRepositoryInterface
{
    public function getAllCategories();

    public function getLeadPosts(Category $category);

    public function getNewsLettersPost(Category $category, User $user);

    public function getPostsForLoggedUser(Category $category, User $user);

    public function getLastPostFromCategory(Category $category);
}
