<?php

namespace App\Repositories\Category;

interface SubcategoryRepositoryInterface
{
    public function getAll();

}
