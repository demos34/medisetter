<?php
namespace App\Repositories\Category;


use App\Models\Category\Category;
use App\Models\Post\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class CategoryRepository implements CategoryRepositoryInterface
{
    public function getAllCategories()
    {
        return Category::all();
    }

    public function getLeadPosts(Category $category)
    {
        $count = 0;
        $sum = 0;
        $feed = [];
        foreach ($category->posts->where('is_deleted', 0) as $post){
            $count = $post->shares->count() + $post->views->count() + $post->postReactions->count();
            if ($sum < $count){
                $sum = $count;
                $feed = [
                    'post_id' => $post->id,
                    'count' => $sum
                ];
            }
            $count = 0;
        }

        if(!empty($feed)){
            $newPost = Post::findOrFail($feed['post_id']);
        } else {
            $newPost = $category->posts->sortByDesc('created_at')->first();
        }

        return $newPost;
    }

    public function getNewsLettersPost(Category $category, User $user)
    {
        $posts = Post::with('author')
            ->with('specialities')
            ->with('category')
            ->where('category_id', $category->id)
            ->get()
            ->sortByDesc('created_at');

        if($user->primary !== NULL){
            $pPost = Post::with('author')
                ->with('specialities')
                ->with('category')
                ->where('category_id', $category->id)
                ->get()
                ->sortBy(function($q) use ($user){
                    $q->where('speciality_id', $user->primary->speciality->id);
                })
                ->sortByDesc('created_at');
            $posts = $posts->merge($pPost);
        }
        if($user->secondary !== NULL){
            $sPost = Post::with('author')
                ->with('specialities')
                ->with('category')
                ->where('category_id', $category->id)
                ->get()
                ->sortBy(function($q) use ($user){
                    $q->where('speciality_id', $user->primary->speciality->id);
                })
                ->sortByDesc('created_at');
            $posts = $posts->merge($sPost);
        }

        if ($user->follows->count() > 0){
            foreach ($user->follows as $follow){
                $uPost = Post::with('author')
                    ->with('specialities')
                    ->with('category')
                    ->where('category_id', $category->id)
                    ->get()
                    ->sortBy(function($q) use ($follow){
                        $q->where('user_id', $follow->author->id);
                    })
                    ->sortByDesc('created_at');

                $posts = $posts->merge($uPost);
            }
        }

        return $posts->take(2);

//        $posts = Post::with('author')
//                    ->with('specialities')
//                    ->with('category')
//                    ->where('category_id', $category->id)
//                    ->whereHas('specialities',function (Builder $q) use ($user){
//                        $q->whereIn('specialities.id', [$user->primary->speciality_id, $user->secondary->speciality_id]);
//                    })
//                    ->get()
//                    ->sortBy(function($q) use ($user){
//                        $q->where('user_id', $user->author->id);
//                        $q->whereIn('speciality_id', [$user->primary->speciality->id, $user->secondary->speciality->id]);
//                    })
//                    ->sortByDesc('created_at');
    }

    public function getPostsForLoggedUser(Category $category, User $user)
    {
        // TODO: Implement getPostsForLoggedUser() method.
    }

    public function getLastPostFromCategory(Category $category)
    {
        return $category->posts->sortByDesc('created_at')->first();
    }
}
