<?php


namespace App\Repositories\Category;


use App\Models\Category\Subcategory;

class SubcategoryRepository implements SubcategoryRepositoryInterface
{
    public function  getAll()
    {
        return Subcategory::all();
    }
}
