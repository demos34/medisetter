<?php

namespace App\Repositories\User;

use App\Models\User;
use Illuminate\Http\Request;

interface UserRepositoryInterface
{
    public function updateProfile(Request $request);

    public function getAllUniversities();

    public function getAllSpecialities();

    public function getAllEducation();

    public function updateUniversity(Request $request);

    public function updateSpeciality(Request $request);

    public function followUser(User $user);

    public function addToContacts(User $user);

    public function updateOrganisation(Request $request);

    public function searchUser(Request $request);
}
