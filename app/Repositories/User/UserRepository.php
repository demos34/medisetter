<?php


namespace App\Repositories\User;

use App\Models\Category\Organisation;
use App\Models\Category\Speciality;
use App\Models\Degree\Degree;
use App\Models\Degree\OtherUniversity;
use App\Models\Degree\University;
use App\Models\User;
use App\Models\User\Follow;
use App\Models\User\From;
use App\Models\User\Primary;
use App\Models\User\Secondary;
use App\Models\User\To;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use phpDocumentor\Reflection\Types\Self_;

class UserRepository implements UserRepositoryInterface
{
    public function updateProfile(Request $request)
    {
        if($request->has('name')){
            self::updateName($request);
        }
        if($request->has('email')){
            self::updateEmail($request);
        }
        if ($request->has('age')) {
            self::updateAge($request);
        }
        if ($request->has('im_from')) {
            self::updateLocation($request);
        }
        if ($request->has('image')) {
            self::updateAvatar($request);
        }
        $this->updateUniversity($request);
        $this->updateSpeciality($request);
        $this->updateOrganisation($request);
    }

    public function getAllUniversities()
    {
        return University::all();
    }

    public function getAllSpecialities()
    {
        return Speciality::all();
    }

    public function getAllEducation()
    {
        return Degree::all();
    }

    public function updateUniversity(Request $request)
    {
        if ($request->has('is_student')) {
            if ((int)$request->is_student === 1) {
                self::updateIsStudent(1);
                if($request->degrees != 'null') {
                    $degrees = self::validateDegree($request);
                    self::updateDegree($degrees);
                }
                if ($request->has('other_uni')) {
                    if ($request->has('other_university')) {
                        $validated = self::validateOtherUniversity($request);
                        return self::updateOtherUniversity($validated);
                    } else {
                        return false;
                    }
                } else {
                    $validated = self::validateUniversityFromList($request);
                    return self::updateUniversityFromList($validated);
                }
            } else {
                return false;
            }
        } else {
            Auth::user()->degrees()->detach();
            Auth::user()->universities()->detach();
            if (isset(Auth::user()->otherUniversity)) {
                Auth::user()->otherUniversity->delete();
            }
            self::updateIsStudent(0);
        }
    }

    public function updateSpeciality(Request $request)
    {
        if ($request->primary !== "null") {
            $validatedPrimary = self::validateSpeciality($request, 'primary');
            if (self::updateUsersSpeciality($validatedPrimary, 'primary') === FALSE) {
                return false;
            };
        } else {
            Auth::user()->primary()->delete();
        }
        if ($request->secondary !== "null") {
            $validatedSecondary = self::validateSpeciality($request, 'secondary');
            if (self::updateUsersSpeciality($validatedSecondary, 'secondary') === FALSE) {
                return false;
            }
        } else {
            Auth::user()->secondary()->delete();
        }
        return true;
    }

    public function followUser(User $user)
    {
        $me = Auth::user();
        if (!isset($me->follower)) {
            $meFollower = $me->follower()->create();
        } else {
            $meFollower = $me->follower;
        }
        if ($meFollower->users()->where('user_id', $user->id)->count() > 0) {
            $meFollower->users()->detach($user->id);
            $msg = self::msg('success', 'Now you not follow ' . $user->name);
        } else {
            $meFollower->users()->attach($user->id);
            $msg = self::msg('success', 'Now you follow ' . $user->name);
        }
        return $msg;
    }

    public function addToContacts(User $user)
    {
        if (Auth::user()->contact === NULL) {
            $meContact = Auth::user()->contact()->create();
        } else {
            $meContact = Auth::user()->contact;
        }

        if ($user->contact === NULL) {
            $heContact = $user->contact()->create();
        } else {
            $heContact = $user->contact;
        }

        if (Auth::user()->from === NULL) {
            From::create(
                [
                    'user_id' => Auth::user()->id,
                ]
            );
        }

        if (Auth::user()->to === NULL) {
            To::create(
                [
                    'user_id' => Auth::user()->id,
                ]
            );
        }

        $validateOne = DB::table('contact_user')
            ->where('contact_id', $heContact->id)
            ->where('user_id', Auth::user()->id)
            ->first();


        $validateTwo = DB::table('contact_user')
            ->where('contact_id', $meContact->id)
            ->where('user_id', $user->id)
            ->first();

        if ($validateOne === NULL && $validateTwo === NULL) {
            $heContact->users()->attach(Auth::user()->id);
            $meContact->users()->attach($user->id);
        }
    }

    public function updateOrganisation(Request $request)
    {
        $validated = $request->validate(
            [
                'organisation' => 'required|min:3|max:150',
                'location' => 'required|min:3|max:150',
            ]
        );

        if (Auth::user()->organisation != NULL) {
            Auth::user()->organisation()->update(
                [
                    'name' => $validated['organisation'],
                    'location' => $validated['location'],
                ]
            );
        } else {
            Organisation::create(
                [
                    'name' => $validated['organisation'],
                    'location' => $validated['location'],
                    'user_id' => Auth::user()->id,
                ]
            );
        }
    }

    public function searchUser(Request $request)
    {
        if ($request->username === NULL) {
            return false;
        }
        $validated = $request->validate(
            [
                'username' => 'required|min:1|max:50'
            ]
        );
        return User::where('name', 'like', '%' . $validated['username'] . '%')->get();

    }

    protected static function updateName(Request $request)
    {
        $me = Auth::user();
        $validated = self::validateName($request);
        $me->update(
            [
                'name' => $validated['name'],
            ]
        );
    }

    protected static function validateName(Request $request)
    {
        return $request->validate(
            [
                'name' => 'required|min:2|max:50',
            ]
        );
    }

    protected static function updateEmail(Request $request)
    {
        $me = Auth::user();
        if($request->email !== $me->email){
            $validated = self::validateEmail($request);
            $me->update(
                [
                    'email' => $validated['email'],
                ]
            );
        }
    }

    protected static function validateEmail(Request $request)
    {
        return $request->validate(
            [
                'email' => 'required|min:3|max:100|unique:users',
            ]
        );
    }

    protected static function validateSpeciality(Request $request, $key)
    {
        return $request->validate(
            [
                $key => 'required|exists:specialities,id'
            ]
        );
    }

    protected static function updateUsersSpeciality($validated, $key)
    {
        if (isset(Auth::user()->$key)) {
            Auth::user()->$key->update(
                [
                    'speciality_id' => $validated[$key],
                ]
            );
        }
        if ($key == 'primary') {
            Primary::create(
                [
                    'speciality_id' => $validated[$key],
                    'user_id' => Auth::user()->id,
                ]
            );
        } else {
            Secondary::create(
                [
                    'speciality_id' => $validated[$key],
                    'user_id' => Auth::user()->id,
                ]
            );
        }
    }

    protected static function updateDegree($validated)
    {
        if (Auth::user()->degrees->count() > 0) {
            Auth::user()->degrees()->sync($validated);
        } else {
            Auth::user()->degrees()->attach($validated);
        }
    }

    protected static function updateIsStudent($condition)
    {
//        if (Auth::user()->universities->count() > 0){
//            Auth::user()->universities()->detach();
//        }
//        if (Auth::user()->degrees->count() > 0){
//            Auth::user()->degrees()->detach();
//        }
//        if(isset(Auth::user()->otherUniversity)){
//            Auth::user()->otherUniversity->delete();
//        }
        Auth::user()->profile->update(
            [
                'is_student' => $condition,
            ]
        );
    }

    protected static function updateOtherUniversity($validated)
    {
        if (Auth::user()->universities->count() > 0) {
            Auth::user()->universities()->detach();
        }

        if (isset(Auth::user()->otherUniversity)) {
            Auth::user()->otherUniversity->update(
                [
                    'name' => $validated['other_university'],
                ]
            );
        } else {
            $other = OtherUniversity::create(
                [
                    'name' => $validated['other_university'],
                    'user_id' => Auth::user()->id,
                ]
            );
            $other->update(
                [
                    'slug' => Str::slug($other->name . '-' . $other->id)
                ]
            );
        }
    }

    protected static function updateUniversityFromList($validated)
    {
        if (Auth::user()->universities->count() > 0) {
            Auth::user()->universities()->sync($validated);
        } else {
            Auth::user()->universities()->attach($validated);
        }

        if (isset(Auth::user()->otherUniversity)) {
            Auth::user()->otherUniversity->delete();
        }
    }

    protected static function validateOtherUniversity(Request $request)
    {
        return $request->validate(
            [
                'other_university' => 'required|min:5|max:100|string',
            ]
        );
    }

    protected static function validateUniversityFromList(Request $request)
    {
        return $request->validate(
            [
                'university' => 'required|exists:universities,id'
            ]
        );
    }

    protected static function validateDegree(Request $request)
    {
        return $request->validate(
            [
                'degrees' => 'required|exists:degrees,id'
            ]
        );
    }

    protected static function updateAvatar(Request $request)
    {
        $validated = self::validateAvatar($request);
        return self::storeAvatar($request, $validated);
    }

    protected static function updateLocation(Request $request)
    {
        $data = $request->validate(
            [
                'im_from' => 'required|string|min:5|max:100',
            ]
        );
        Auth::user()->profile->update(
            [
                'location' => $data['im_from'],
            ]
        );
    }

    protected static function updateAge(Request $request)
    {
        $data = $request->validate(
            [
                'age' => 'required|integer',
            ]
        );
        Auth::user()->profile->update(
            [
                'age' => $data['age'],
            ]
        );
    }

    protected static function validateAvatar(Request $request)
    {
        return $request->validate(
            [
                'image' => 'image',
            ]
        );
    }

    protected static function storeAvatar(Request $request, $validated)
    {
        $oldImage = Auth::user()->profile->avatar;
        $checkImage = explode('/', $oldImage);
        if ($checkImage['2'] != 'helpers'){
            self::deleteImage();
        }

        $file = $validated['image']->getClientOriginalName();
        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $fileExtension = pathinfo($file, PATHINFO_EXTENSION);
        $date = date('Y-m-d-H-i-s');
        $fileFullName = $date . '-' . $fileName . '.' . $fileExtension;
        $imagePath = '/storage/users/' . $fileFullName;
        $request->image->storeAs('users', $fileFullName, 'public');
        self::profile()->update(
            [
                'avatar' => $imagePath
            ]
        );
        Auth::user()->update(
            [
                'avatar' => $imagePath,
            ]
        );
    }

    protected static function deleteImage()
    {
        $originalImage = self::profile()->avatar;
        $array = explode('/', $originalImage);
        $fileName = $array['3'];
        $path = '/public/' . $array['2'] . '/' . $fileName;
        Storage::delete($path);
    }

    protected static function profile()
    {
        return Auth::user()->profile;
    }

    protected static function msg($session, $msg)
    {
        return [
            'session' => $session,
            'message' => $msg,
        ];
    }

}
