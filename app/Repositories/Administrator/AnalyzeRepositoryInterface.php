<?php

namespace App\Repositories\Administrator;

use App\Models\Admin\Analyze;
use App\Models\Post\Post;
use Illuminate\Http\Request;

interface AnalyzeRepositoryInterface
{
    public function createSchema(Request $request, Post $post);

    public function createLoginSchema(Request $request);

    public function getAllMyViews();

    public function getAllMyReactions();

    public function getAllMyShares();

    public function getUsersInAgeRange(Analyze $analyze);

    public function getUsersInLocation(Analyze $analyze);

    public function getUsersInAgeRangeAndLocation(Analyze $analyze);
}
