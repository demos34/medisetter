<?php


namespace App\Repositories\Administrator;


use App\Actions\Fortify\PasswordValidationRules;
use App\Events\NewNotification;
use App\Events\NotificationCount;
use App\Models\Category\Category;
use App\Models\Category\Organisation;
use App\Models\Category\Speciality;
use App\Models\Category\Subcategory;
use App\Models\Code\Code;
use App\Models\Degree\Degree;
use App\Models\Degree\University;
use App\Models\Role\Role;
use App\Models\User;
use App\Models\User\Contact;
use App\Models\User\From;
use App\Models\User\Notification;
use App\Models\User\Profile;
use App\Models\User\To;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AdminRepository implements AdminRepositoryInterface
{
    use PasswordValidationRules;

    public function getAllUsers()
    {
        return User::all();
    }

    public function getAllAdministrators()
    {
        return User::with('roles')
            ->whereHas('roles', function ($q){
                $q->where('role', 'Administrator');
            })
            ->get();
    }

    public function getAllEnterprises()
    {
        return User::with('roles')
            ->whereHas('roles', function ($q){
                $q->where('role', 'Enterprise');
            })
            ->get();
    }

    public function storeEnterprise(Request $request)
    {
        $data = $request->validate(
            [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => $this->passwordRules(),
                'location' => ['required', 'string', 'max:255'],
            ]
        );

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'api_token' => bin2hex(openssl_random_pseudo_bytes(40)),
            'is_verified' => true,
        ]);

        self::userCreating($data['location'], $user);


        return $user;
    }

    public function getAllRoles()
    {
        return Role::all();
    }

    public function changeRole(Request $request, User $user)
    {
        $validatedRole = $request->validate(
            [
                'role' => 'required|exists:roles,id',
            ]
        );

       $user->roles()->sync($validatedRole['role']);


        $user->contact->users()->detach();
        $user->contacts()->detach();
        $contact = $user->contact;

        if((int)$validatedRole['role'] == 1 || (int)$validatedRole['role'] == 2){
            $users = User::all();
            foreach ($users as $item) {
                if($user->id != $item->id){
                    $item->contacts()->attach($contact->id);
                    $user->contacts()->attach($item->id);
                }
            }
        } else {
            $administrators = Role::with('users')->where('role', 'Administrator')->first();
            foreach ($administrators->users as $administrator) {
                $administrator->contacts()->attach($contact->id);
                $user->contacts()->attach($administrator->id);
            }
        }

       return [
           'session' => 'success',
           'message' => 'You changed role to ' . implode(',', $user->roles()->get()->pluck('role')->toArray()),
       ];
    }

    public function getAllCategories()
    {
        return Category::all();
    }

    public function insertNewRowCategory(Request $request)
    {
        $validated = self::validateRequestStoreNewCategory($request);
        if (self::storeCategory($validated)) {
            $msg = self::msg('success', 'New category was successfully added!');
        } else {
            $msg = self::msg('alert', 'Oops! Something was wrong!');
        }

        return $msg;
    }

    public function changeVisibility(Category $category)
    {
        if ($category->is_visible === 0) {
            $category->update(
                [
                    'is_visible' => true,
                ]
            );
            $status = 'Visible!';
        } else {
            $category->update(
                [
                    'is_visible' => false,
                ]
            );
            $status = 'not visible!';
        }
        return $status;
    }

    public function updateCategory(Request $request, Category $category)
    {
        $validated = self::validateCategoryForUpdate($request, $category);
        $category->update(
            [
                'en_category' => $validated['category'],
                'vi_category' => $validated['vi_category'],
            ]
        );

        return self::msg('success', 'The category is successfully updated!');
    }


    public function deleteCategory(Category $category)
    {
        $category->delete();
        return self::msg('success', 'This category was successfully deleted!');
    }

    public function getAllSubCategories()
    {
        return Subcategory::all();
    }

    public function insertNewRowSubcategory(Request $request)
    {
//        $type = (int)$request->radio;
        $validated = self::validateRequestStoreNewSubcategory($request);
//        if($type == 1 ){
//            $msg = self::storeSubcategoryNewsEvents($validated);
//        } elseif ( $type == 2){
        $msg = self::storeSubcategoryCareerOpportunity($validated);
//        } else {
//            $msg = self::msg('alert', 'Please enter a valid Category!');
//        }

        return $msg;
    }

    public function updateSubcategories(Request $request, Subcategory $subcategory)
    {
        $validated = self::validateSubcategoryForUpdate($request);
        $subcategory->update(
            [
                'en_name' => $validated['name'],
                'vi_name' => $validated['vi_name'],
            ]
        );
        return self::msg('success', 'The subcategory is successfully updated!');
    }

    public function deleteSubcategory(Subcategory $subcategory)
    {
        $subcategory->delete();
        return self::msg('success', 'This subcategory was successfully deleted!');
    }

    public function getAllSpecialities()
    {
        return Speciality::all();
    }

    public function insertNewRowSpeciality(Request $request)
    {
        $validated = self::validatedSpecialities($request);
        $speciality = Speciality::create(
            [
                'en_name' => $validated['spec'],
                'vi_name' => $validated['vi_spec'],
            ]
        );
        $speciality->update(
            [
                'slug' => Str::slug($speciality->en_name . '-' . $speciality->id)
            ]
        );
        return self::msg('success', 'The new speciality is successfully added!');
    }

    public function updateSpeciality(Request $request, Speciality $speciality)
    {
        $validated = self::validatedSpecialities($request);
        $speciality->update(
            [
                'en_name' => $validated['spec'],
                'vi_name' => $validated['vi_spec'],
            ]
        );
        return self::msg('success', 'The speciality is successfully updated!');
    }

    public function deleteSpeciality(Speciality $speciality)
    {
        $speciality->delete();
        return self::msg('success', 'This speciality was successfully deleted!');
    }


    public function getAllUniversities()
    {
        return University::all();
    }

    public function insertNewRowUniversity(Request $request)
    {
        $validated = self::validateUniversity($request);
        $university = University::create(
            [
                'en_name' => $validated['uni'],
                'vi_name' => $validated['vi_uni'],
            ]
        );

        $university->update(
            [
                'slug' => Str::slug($university->en_name . '-' . $university->id),
            ]
        );
        return self::msg('success', 'The University was successfully added!');
    }

    protected static function validateUniversity(Request $request)
    {
        return $request->validate(
            [
                'uni' => 'required|min:3|max:100',
                'vi_uni' => 'required|min:3|max:100',
            ]
        );
    }

    public function updateUniversity(Request $request, University $university)
    {
        $validated = self::validateUniversity($request);
        $university->update(
            [
                'en_name' => $validated['uni'],
                'vi_name' => $validated['vi_uni'],
            ]
        );
        return self::msg('success', 'The University was successfully updated!');
    }

    public function deleteUniversity(University $university)
    {
        $university->delete();
        return self::msg('success', 'This University was successfully deleted!');
    }

    public function getAllDegrees()
    {
        return Degree::all();
    }

    public function insertNewDegree(Request $request)
    {
        $validated = self::validatedDegree($request);
        $degree = Degree::create(
            [
                'en_name' => $validated['deg'],
                'vi_name' => $validated['vi_deg'],
            ]
        );

        $degree->update(
            [
                'slug' => Str::slug($degree->en_name . '-' . $degree->id),
            ]
        );

        return self::msg('success', 'The new Degree type was successfully added!');
    }

    public function updateDegree(Request $request, Degree $degree)
    {
        $validated = self::validatedDegree($request);
        $degree->update(
            [
                'en_name' => $validated['deg'],
                'vi_name' => $validated['vi_deg'],
            ]
        );
        return self::msg('success', 'The Degree type was successfully updated!');
    }

    public function deleteDegree(Degree $degree)
    {
        $degree->delete();
        return self::msg('success', 'This Degree type was successfully deleted!');
    }

    public function getLang()
    {
        return App::getLocale();
    }

    public function  getAllCodes()
    {
        return Code::all();
    }

    public function insertNewCode(Request $request)
    {
       $validated = self::validateCode($request);
       Code::create(
            [
                'code' => $validated['code'],
            ]
        );

        return self::msg('success', 'The new Code was successfully added!');
    }

    public function updateCode(Request $request, Code $code)
    {
        $validated = self::validateCode($request);
        $code->update(
            [
                'code' => $validated['code'],
            ]
        );
        return self::msg('success', 'The Code was successfully updated!');
    }

    public function deleteCode(Code $code)
    {
        $code->delete();
        return self::msg('success', 'This Code was successfully deleted!');
    }

    protected static function validateRequestStoreNewCategory(Request $request)
    {
        return $request->validate(
            [
                'category' => 'required|min:5|max:50|unique:categories',
                'vi_category' => '',
            ]
        );
    }

    protected static function validateRequestStoreNewSubcategory(Request $request)
    {
        return $request->validate(
            [
                'name' => 'required|min:2|max:50|',
                'vi_name' => 'required|min:2|max:50|',
            ]
        );
    }


    protected static function storeCategory($validated)
    {
        return Category::create(
            [
                'en_category' => $validated['category'],
                'vi_category' => $validated['vi_category'],
                'slug' => self::createSlug($validated['category']),
                'is_visible' => false,
            ]
        );
    }

    protected static function storeSubcategoryNewsEvents($validated)
    {
        if (self::storeSubcategoryEvents($validated)
            && self::storeSubcategoryNews($validated)
            && self::storeSubcategoryDiscussion($validated)) {
            $msg = self::msg('success', 'Subcategory was successfully added');
        } else {
            $msg = self::msg('alert', 'Please enter a valid data!');
        }

        return $msg;
    }

    protected static function storeSubcategoryEvents($validated)
    {
        $slug = self::createSlug($validated['name'] . '-4');
        return Subcategory::create(
            [
                'name' => $validated['name'],
                'category_id' => 4,
                'slug' => $slug,
            ]
        );
    }

    protected static function storeSubcategoryNews($validated)
    {
        $slug = self::createSlug($validated['name'] . '-1');
        return Subcategory::create(
            [
                'name' => $validated['name'],
                'category_id' => 1,
                'slug' => $slug,
            ]
        );
    }

    protected static function storeSubcategoryDiscussion($validated)
    {
        $slug = self::createSlug($validated['name'] . '-5');
        return Subcategory::create(
            [
                'name' => $validated['name'],
                'category_id' => 5,
                'slug' => $slug,
            ]
        );
    }

    protected static function storeSubcategoryCareerOpportunity($validated)
    {
        $slug = self::createSlug($validated['name'] . '-6');
        if (Subcategory::create(
            [
                'en_name' => $validated['name'],
                'vi_name' => $validated['vi_name'],
                'category_id' => 6,
                'slug' => $slug,
            ]
        )) {
            $msg = self::msg('success', 'Subcategory was successfully added');
        } else {
            $msg = self::msg('alert', 'Please enter a valid data!');
        }
        return $msg;
    }

    protected static function validateCategoryForUpdate(Request $request, Category $category)
    {
        if ($request->category == $category->category && $request->vi_category == $category->vi_category) {
            $validated = $request->validate(
                [
                    'category' => 'required|min:5|max:100',
                    'vi_category' => ''
                ]
            );
        } elseif ($request->category !== $category->category && $request->vi_category !== $category->vi_category) {
            $validated = $request->validate(
                [
                    'category' => 'required|min:5|max:100|unique:categories',
                    'vi_category' => ''
                ]
            );
        } elseif ($request->category == $category->category && $request->vi_category !== $category->vi_category) {
            $validated = $request->validate(
                [
                    'category' => 'required|min:5|max:100',
                    'vi_category' => ''
                ]
            );
        } elseif ($request->category !== $category->category && $request->vi_category == $category->vi_category) {
            $validated = $request->validate(
                [
                    'category' => 'required|min:5|max:100|unique:categories',
                    'vi_category' => ''
                ]
            );
        }

        return $validated;
    }

    protected static function validateSubcategoryForUpdate(Request $request)
    {
        return $request->validate(
            [
                'name' => 'required|min:5|max:100',
                'vi_name' => 'required|min:5|max:100',
            ]
        );
    }

    protected static function validatedSpecialities(Request $request)
    {
        return $request->validate(
            [
                'spec' => 'required|min:5|max:100',
                'vi_spec' => 'required|min:5|max:100',
            ]
        );

    }

    protected static function validatedDegree(Request $request)
    {
        return $request->validate(
            [
                'deg' => 'required|min:3|max:100',
                'vi_deg' => 'required|min:3|max:100',
            ]
        );
    }


    protected static function validateCode(Request $request)
    {
        return $request->validate(
            [
                'code' => 'required|min:2|max:100',
            ]
        );
    }

    protected static function createSlug($title)
    {
        return $slug = Str::slug($title, '-');
    }

    protected static function msg($session, $message)
    {
        return [
            'session' => $session,
            'message' => $message,
        ];
    }

    protected static function userCreating($location, User $user)
    {
        $user->roles()->attach(3);
        $user->update(
            [
                'avatar' => '/storage/helpers/profile.png',
            ]
        );
        Profile::create(
            [
                'user_id' => $user->id,
                'avatar' => '/storage/helpers/profile.png',
                'location' => $location,
            ]
        );
        To::create(
            [
                'user_id' => $user->id,
            ]
        );

        From::create(
            [
                'user_id' => $user->id,
            ]
        );
        $contact = Contact::create(
            [
                'user_id' => $user->id,
            ]
        );

        Organisation::create(
            [
                'name' => $user->name,
                'location' => $user->profile->location,
                'user_id' =>$user->id,
            ]
        );

        $administrators = Role::with('users')->where('role', 'Administrator')->first();
        foreach ($administrators->users as $administrator){
            $administrator->contacts()->attach($contact->id);
            $user->contacts()->attach($administrator->id);


            $notification = Notification::create(
                [
                    'from_id' => $user->from->id,
                    'to_id' => $administrator->to->id,
                    'notification_type_id' => 10,
                    'user_id' => $user->id,
                    'is_viewed' => false,
                ]
            );

            broadcast(new NewNotification($notification));

            broadcast(new NotificationCount($notification, $administrator));
        }
    }
}
