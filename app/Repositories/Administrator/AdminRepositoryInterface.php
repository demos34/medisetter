<?php

namespace App\Repositories\Administrator;

use App\Models\Category\Category;
use App\Models\Category\Speciality;
use App\Models\Category\Subcategory;
use App\Models\Code\Code;
use App\Models\Degree\Degree;
use App\Models\Degree\University;
use App\Models\User;
use Illuminate\Http\Request;

interface AdminRepositoryInterface
{
    public function getAllUsers();

    public function getAllAdministrators();

    public function getAllEnterprises();

    public function storeEnterprise(Request $request);

    public function getAllRoles();

    public function changeRole(Request $request, User $user);

    public function getAllCategories();

    public function insertNewRowCategory(Request $request);

    public function changeVisibility(Category $category);

    public function updateCategory(Request $request, Category $category);

    public function deleteCategory(Category $category);

    public function getAllSubCategories();

    public function insertNewRowSubcategory(Request $request);

    public function updateSubcategories(Request $request, Subcategory $subcategory);

    public function deleteSubcategory(Subcategory $subcategory);

    public function getAllSpecialities();

    public function insertNewRowSpeciality(Request $request);

    public function updateSpeciality(Request $request, Speciality $speciality);

    public function deleteSpeciality(Speciality $speciality);

    public function getAllUniversities();

    public function insertNewRowUniversity(Request $request);

    public function updateUniversity(Request $request, University $university);

    public function deleteUniversity(University $university);

    public function getAllDegrees();

    public function insertNewDegree(Request $request);

    public function updateDegree(Request $request, Degree $degree);

    public function deleteDegree(Degree $degree);

    public function getLang();

    public function getAllCodes();

    public function insertNewCode(Request $request);

    public function updateCode(Request $request, Code $code);

    public function deleteCode(Code $code);

}
