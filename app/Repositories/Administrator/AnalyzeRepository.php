<?php
namespace App\Repositories\Administrator;



use App\Models\Admin\Analyze;
use App\Models\Admin\AnalyzeLogin;
use App\Models\Category\Speciality;
use App\Models\Post\Post;
use App\Models\User;
use App\Models\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnalyzeRepository implements AnalyzeRepositoryInterface
{
    public function createSchema(Request $request, Post $post)
    {
        $validated = self::validatedSchema($request);

        return Analyze::create(
            [
                'user_id' => Auth::user()->id,
                'post_id' => $post->id,
                'title' => $validated['name'],
                'start' => $validated['start_date'],
                'end' => $validated['end_date'],
                'from_age' => $validated['from_age'],
                'to_age' => $validated['to_age'],
                'location' => $validated['location'],
                'speciality_id' => $validated['speciality'],
                'degree_id' => $validated['degrees'],
            ]
        );
    }

    public function createLoginSchema(Request $request)
    {
        $validated = self::validated($request);

        return AnalyzeLogin::create(
            [
                'user_id' => Auth::user()->id,
                'title' => $validated['name'],
                'start' => $validated['start_date'],
                'end' => $validated['end_date'],
            ]
        );
    }

    public function getAllMyViews()
    {
        $sum = 0;
        if(Auth::user()->author !== NULL){
            foreach(Auth::user()->author->posts as $post){
                $sum = $sum + (int)$post->views->count();
            }
        }
        return $sum;
    }

    public function getAllMyReactions()
    {
        $sum = 0;
        if(Auth::user()->author !== NULL){
            foreach(Auth::user()->author->posts as $post){
                $sum = $sum + (int)$post->postReactions->count();
            }
        }
        return $sum;
    }

    public function getAllMyShares()
    {
        $sum = 0;
        if(Auth::user()->author !== NULL){
            foreach(Auth::user()->author->posts as $post){
                $sum = $sum + (int)$post->shares->count();
            }
        }
        return $sum;
    }



    public function getUsersInAgeRange(Analyze $analyze)
    {
        $usersId = self::getUsersId($analyze);
        $from = self::getFromAge($analyze);
        $to = self::getToAge($analyze);
        return User::with('profile')
            ->whereIn('id', $usersId)
            ->whereHas('profile', function($q) use($from, $to){
                $q->where('age', '>' , $from);
                $q->where('age', '<', $to);
            })
            ->get();
    }

    public function getUsersInLocation(Analyze $analyze)
    {
        $usersId = self::getUsersId($analyze);
        return User::with('profile')
            ->whereIn('id', $usersId)
            ->whereHas('profile', function($q) use($analyze){
                $q->where('location', 'LIKE' , '%'. $analyze->location . '%');
            })
            ->get();
    }

    public function getUsersInAgeRangeAndLocation(Analyze $analyze)
    {

        $usersId = self::getUsersId($analyze);
        $from = self::getFromAge($analyze);
        $to = self::getToAge($analyze);
        return User::with('profile')
            ->whereIn('id', $usersId)
            ->whereHas('profile', function($q) use($analyze, $from, $to){
                $q->where('age', '>' , $from);
                $q->where('age', '<', $to);
                $q->where('location', 'LIKE' , '%'. $analyze->location . '%');
            })
            ->get();
    }

    protected static function validated(Request $request)
    {
        return $request->validate(
            [
                'name' => 'required|min:2|max:50',
                'start_date' => 'required|date',
                'end_date' => 'required|date|after:start_date',
            ]
        );
    }


    protected static function validatedSchema(Request $request)
    {
        $validated = self::validated($request);
        $spec = self::validatedSpec($request);
        $education = self::validatedEducation($request);
        $location = self::validatedLocation($request);
        $from = self::validateFromAge($request);
        $to = self::validateToAge($request);
        if((int)$from['from_age'] > (int)$to['to_age']){
            $from_age = [
                'from_age' => $to['to_age'],
            ];
            $to_age = [
                'to_age' => $from['from_age'],
            ];
        } else {
            $from_age = [
                'from_age' => $from['from_age'],
            ];
            $to_age = [
                'to_age' => $to['to_age'],
            ];
        }

        $validated = array_merge($validated, $spec);
        $validated = array_merge($validated, $education);
        $validated = array_merge($validated, $location);
        $validated = array_merge($validated, $from_age);
        $validated = array_merge($validated, $to_age);

        return $validated;
    }

    protected static function validateFromAge(Request $request)
    {
        if($request->has('from_age')){
            if($request->from_age === NULL){
                $spec =
                    [
                        'from_age' => NULL,
                    ];
            } else {
                $spec = $request->validate(
                    [
                        'from_age' => 'required|min:1|integer'
                    ]
                );
            }

        } else {
            $spec =
                [
                    'from_age' => NULL,
                ];
        }
        return $spec;
    }

    protected static function validateToAge(Request $request)
    {
        if($request->has('to_age')){
            if($request->to_age === NULL){
                $spec =
                    [
                        'to_age' => NULL,
                    ];
            } else {
                $spec = $request->validate(
                    [
                        'to_age' => 'required|min:1|integer'
                    ]
                );
            }

        } else {
            $spec =
                [
                    'to_age' => NULL,
                ];
        }
        return $spec;
    }

    protected static function validatedLocation(Request $request)
    {
        if($request->has('location')){
            if($request->location === NULL){
                $spec =
                    [
                        'location' => NULL,
                    ];
            } else {
                $spec = $request->validate(
                    [
                        'location' => 'required|min:5|max:50'
                    ]
                );
            }
        } else {
            $spec =
                [
                    'location' => NULL,
                ];
        }
        return $spec;
    }

    protected static function validatedSpec(Request $request)
    {
        if($request->has('speciality')){
            if($request->speciality == 'all' || $request->speciality == '0'){
                $spec =
                    [
                        'speciality' => NULL,
                    ];
            } else {
                $spec = $request->validate(
                    [
                        'speciality' => 'exists:specialities,id',
                    ]
                );
            }
        } else {
            $spec =
                [
                    'speciality' => NULL,
                ];
        }
        return $spec;
    }

    protected static function validatedEducation(Request $request)
    {
        if($request->has('degrees')){
            if($request->degrees == 'all' || $request->degrees == '0'){
                $spec =
                    [
                        'degrees' => NULL,
                    ];
            } else {
                $spec = $request->validate(
                    [
                        'degrees' => 'exists:degrees,id',
                    ]
                );
            }
        } else {
            $spec =
                [
                    'degrees' => NULL,
                ];
        }
        return $spec;
    }

    protected static function getUsersId(Analyze $analyze)
    {
        $views = View::where('post_id', $analyze->post_id)->whereBetween('created_at', [$analyze->start, $analyze->end])->get();
        $nonGuestViews = $views->where('user_id', '<>', NULL);
        $usersId = [];
        foreach ($nonGuestViews as $nonGuestView){
            array_push($usersId,$nonGuestView->user_id);
        }

        $mergedArray = [];

        foreach ($usersId as $value){
            if(!in_array($value, $mergedArray)){
                array_push($mergedArray, $value);
            }
        }

       return $mergedArray;
    }

    protected static function getFromAge(Analyze $analyze)
    {
        if($analyze->from_age === NULL || $analyze->from_age < 0){
            $from = 0;
        } elseif($analyze->from_age > 110){
            $from = 110;
        } else {
            $from = $analyze->from_age;
        }

        return $from;
    }

    protected static function getToAge(Analyze $analyze)
    {
        if($analyze->to_age === NULL || $analyze->to_age > 150){
            $to = 150;
        } elseif ($analyze->to_age < 0) {
            $to = 0;
        } else {
            $to = $analyze->to_age;
        }

        return $to;
    }
}
