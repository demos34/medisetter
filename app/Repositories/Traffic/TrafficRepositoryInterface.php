<?php
namespace App\Repositories\Traffic;

interface TrafficRepositoryInterface
{
    public function getRemoteAddress($action);
}
