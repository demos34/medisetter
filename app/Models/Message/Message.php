<?php

namespace App\Models\Message;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function from()
    {
        return $this->belongsTo('App\Models\User\From');
    }

    public function to()
    {
        return $this->belongsTo('App\Models\User\To');
    }
}
