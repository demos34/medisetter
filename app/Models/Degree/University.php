<?php

namespace App\Models\Degree;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class University extends Model
{
    use HasFactory;

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }


    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
