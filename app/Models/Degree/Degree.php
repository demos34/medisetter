<?php

namespace App\Models\Degree;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Degree extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
