<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class From extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function messages()
    {
        return $this->hasMany('App\Models\Message\Message');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\User\Notification');
    }
}
