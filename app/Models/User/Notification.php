<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function notificationType()
    {
        return $this->belongsTo('App\Models\User\NotificationType');
    }

    public function to()
    {
        return $this->belongsTo('App\Models\User\To');
    }

    public function from()
    {
        return $this->belongsTo('App\Models\User\From');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function post()
    {
        return $this->belongsTo('App\Models\Post\Post');
    }
}
