<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NotificationType extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function notifications()
    {
        return $this->hasMany('App\Models\User\Notification');
    }
}
