<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Interested extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function event()
    {
        return $this->belongsTo('App\Models\Post\Event');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
