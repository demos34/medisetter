<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    use HasFactory;

    protected $guarded = [];


    public function primaries()
    {
        return $this->hasMany('App\Models\User\Primary');
    }

    public function secondaries()
    {
        return $this->hasMany('App\Models\User\Secondary');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category\Category');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Models\Post\Post');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
