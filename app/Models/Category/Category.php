<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Category extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function subcategories()
    {
        return $this->hasMany('App\Models\Category\Subcategory');
    }

    public function posts()
    {
        return $this->hasMany('App\Models\Post\Post');
    }

    public function specialities()
    {
        return $this->belongsToMany('App\Models\Category\Speciality');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function prefix()
    {
        return App::getLocale() . '_category';
    }
}
