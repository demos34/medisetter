<?php

namespace App\Models\Category;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo('App\Models\Category\Category');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Models\Post\Post');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
