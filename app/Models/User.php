<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'api_token',
        'avatar',
        'is_verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    public function logins()
    {
        return $this->hasMany('App\Models\Admin\Login');
    }
    public function analyzeLogins()
    {
        return $this->hasMany('App\Models\Admin\AnalyzeLogin');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\User\Profile');
    }

    public function posts()
    {
        return $this->belongsToMany('App\Models\Post\Post');
    }

    public function author()
    {
        return $this->hasOne('App\Models\User\Author');
    }

    public function postReactions()
    {
        return $this->hasMany('App\Models\Post\PostReaction');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Post\Comment');
    }

    public function from()
    {
        return $this->hasOne('App\Models\User\From');
    }

    public function to()
    {
        return $this->hasOne('App\Models\User\To');
    }

    public function contact()
    {
        return $this->hasOne('App\Models\User\Contact');
    }

    public function  contacts()
    {
        return $this->belongsToMany('App\Models\User\Contact');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role\Role');
    }

    public function primary()
    {
        return $this->hasOne('App\Models\User\Primary');
    }

    public function secondary()
    {
        return $this->hasOne('App\Models\User\Secondary');
    }

    public function universities()
    {
        return $this->belongsToMany('App\Models\Degree\University');
    }

    public function otherUniversity()
    {
        return $this->hasOne('App\Models\Degree\OtherUniversity');
    }

    public function degrees()
    {
        return $this->belongsToMany('App\Models\Degree\Degree');
    }

    public function follows()
    {
        return $this->hasMany('App\Models\User\Follow');
    }

    public function follower()
    {
        return $this->hasOne('App\Models\User\Follower');
    }

    public function followers()
    {
        return $this->belongsToMany('App\Models\User\Follower');
    }

    public function organisation()
    {
        return $this->hasOne('App\Models\Category\Organisation');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\User\Notification');
    }

    public function shares()
    {
        return $this->hasMany('App\Models\View\View');
    }

    public function userAgeLocations()
    {
        return $this->hasMany('App\Models\Post\UserAgeLocation');
    }

    public function interesteds()
    {
        return $this->hasMany('App\Models\User\Interested');
    }

    public function participates()
    {
        return $this->hasMany('App\Models\User\Participate');
    }

    public function careerInteresteds()
    {
        return $this->hasMany('App\Models\User\CareerInterested');
    }

    public function careerParticipates()
    {
        return $this->hasMany('App\Models\User\CareerParticipate');
    }

    public function hasAnyRoles($roles)
    {
        if($this->roles()->whereIn('role', $roles)->first()) {
            return true;
        }
        return false;
    }

    public function hasRole($role)
    {
        if($this->roles()->where('role', $role)->first()) {
            return true;
        }
        return false;
    }

}
