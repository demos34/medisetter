<?php

namespace App\Models\View;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class View extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function post()
    {
        return $this->belongsTo('App\Models\Post\Post');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
