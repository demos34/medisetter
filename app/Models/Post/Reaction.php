<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reaction extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function postReactions()
    {
        return $this->hasMany('App\Models\Post\PostReaction');
    }

    public function getRouteKeyName()
    {
        return 'en_name';
    }
}
