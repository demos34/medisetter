<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
    use HasFactory;

    protected $guarded = [];

    //which is shared (source post)
    public function post()
    {
        return $this->belongsTo('App\Models\Post\Post');
    }

    //shared post (which is generated from source post)
    public function posts()
    {
        return $this->hasMany('App\Models\Post\Post');
    }
}
