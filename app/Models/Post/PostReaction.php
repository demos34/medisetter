<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostReaction extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function reactions()
    {
        return $this->belongsTo('App\Models\Post\Reaction');
    }

    public function posts()
    {
        return $this->belongsTo('App\Models\Post\Post');
    }

    public function users()
    {
        return $this->belongsTo('App\Models\User');
    }
}
