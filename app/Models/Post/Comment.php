<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected function post()
    {
        return $this->belongsTo('App\Models\Post\Post');
    }

    protected function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
