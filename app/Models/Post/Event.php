<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function post()
    {
        return $this->belongsTo('App\Models\Post\Post');
    }

    public function interesteds()
    {
        return $this->hasMany('App\Models\User\Interested');
    }

    public function participates()
    {
        return $this->hasMany('App\Models\User\Participate');
    }
}
