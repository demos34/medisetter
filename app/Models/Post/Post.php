<?php

namespace App\Models\Post;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo('App\Models\Category\Category');
    }

    public function author()
    {
        return $this->belongsTo('App\Models\User\Author');
    }

    public function code()
    {
        return $this->belongsTo('App\Models\Code\Code');
    }

    public function analyzePost()
    {
        return $this->hasOne('App\Models\Admin\AnalyzePost');
    }

    public function subcategories()
    {
        return $this->belongsToMany('App\Models\Category\Subcategory');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    public function event()
    {
        return $this->hasOne('App\Models\Post\Event');
    }

    public function views()
    {
        return $this->hasMany('App\Models\View\View');
    }

    public function total()
    {
        return $this->hasOne('App\Models\View\Total');
    }

    public function postReactions()
    {
        return $this->hasMany('App\Models\Post\PostReaction');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Post\Comment');
    }

    //which is shared (source post)

    public function shares()
    {
        return $this->hasMany('App\Models\Post\Share');
    }

    public function specialities()
    {
        return $this->belongsToMany('App\Models\Category\Speciality');
    }

    //shared post (which is generated from source post)
    public function share()
    {
        return $this->belongsTo('App\Models\Post\Share');
    }

    public function notifications()
    {
        return $this->hasMany('App\Models\User\Notification');
    }

    public function userAgeLocations()
    {
        return $this->hasMany('App\Models\Post\UserAgeLocation');
    }

    public function careerInteresteds()
    {
        return $this->hasMany('App\Models\User\CareerInterested');
    }

    public function careerParticipates()
    {
        return $this->hasMany('App\Models\User\CareerParticipate');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
