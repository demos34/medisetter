let otherUni = document.getElementById('other-uni');
let universities = document.getElementById('universities-div');
let inputOtherUni = document.getElementById('other-university');
let student = document.getElementById('is-student');
let otherDiv = document.getElementById('other-div');
let degrees = document.getElementById('degrees-div');

hideUniversities(universities);
hideOther(inputOtherUni);
hideDegrees();
hideOtherCheck(otherDiv)

if (student.checked){
    showUniversities(universities);
    hideOther(inputOtherUni);
    showOtherCheck(otherDiv);
    showDegrees();
}
if (otherUni.checked){
    hideUniversities(universities);
    showOther(inputOtherUni);
}

otherUni.addEventListener('change', function() {
    if (this.checked) {
        hideUniversities(universities);
        showOther(inputOtherUni);
    } else {
        showUniversities(universities);
        hideOther(inputOtherUni);
    }
});

student.addEventListener('change', function (){
   if (this.checked){
       showUniversities(universities);
       hideOther(inputOtherUni);
       showOtherCheck(otherDiv);
       showDegrees();
   } else {
       hideUniversities(universities);
       hideOther(inputOtherUni);
       hideOtherCheck(otherDiv);
       hideDegrees();
   }
});

function hideUniversities(uni){
    uni.classList.add('d-none');
}

function showUniversities(uni, other){
    uni.classList.remove('d-none');
}

function showOther(other){
    other.classList.remove('d-none');
}

function hideOther(other){
    other.classList.add('d-none');
}

function hideOtherCheck(check){
    check.classList.add('d-none');
}

function showOtherCheck(check){
    check.classList.remove('d-none');
}

function hideDegrees(){
    degrees.classList.add('d-none');
}

function showDegrees(){
    degrees.classList.remove('d-none');
}
