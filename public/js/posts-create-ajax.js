const categories = document.getElementById('categories');
const sub = document.getElementById('subcategories');
const categoryId = parseInt(document.getElementById('selected-cat').value);
const codes = document.getElementById('codes');
const events = document.getElementById('event-dates');
const idCodes = document.getElementById('selected-codes');
const startDate = document.getElementById('start_date');
const endDate = document.getElementById('end_date');
const subDiv = document.getElementById('sub-div');


addSubcategoryItem(categoryId);
hideEvents();
// hideSub();

if (document.getElementById('events')){
    showEvents();
} else if (document.getElementById('career-paid-opportunity')){
    hideCode()
    // showSub();
}


categories.addEventListener("click", e => {
    let options = categories.querySelectorAll("option");
    let count = options.length;
    if (typeof (count) === "undefined" || count < 2) {
        addSubcategoryItem(e.target.value);
        hideCode();
        hideEvents();
        // hideSub()
    }
});

categories.addEventListener("change", e => {
    addSubcategoryItem(e.target.value);
    hideCode();
    hideEvents();
    // hideSub()
    if(parseInt(e.target.value) === 6 ){
        hideCode()
    } else if (parseInt(e.target.value) === 4 ){
        showEvents()
        hideCode()
    }
});

sub.addEventListener("change", e => {
   if (parseInt(e.target.value) === 4){
       showCodes();
   } else {
       hideCode()
   }
});

function addSubcategoryItem(id) {
    $.ajax({
        url: `/posts/` + id + `/subcategories`,
        success: data => {
            for (i = sub.length - 1; i >= 0; i--) {
                sub.options[i] = null;
            }
            data.subcategories.forEach(subcategories => {
                    if (document.getElementById('sub')){
                        let currentSub = document.getElementsByClassName('subcategory-title')[0];
                        let currentSubId = parseInt(currentSub.id);
                        let subId = parseInt(subcategories.id);
                        if (subId === currentSubId ) {
                            $('#subcategories').append(`<option value="${subcategories.id}" selected>${subcategories.en_name}</option>`)
                        } else {
                            $('#subcategories').append(`<option value="${subcategories.id}">${subcategories.en_name}</option>`)
                        }
                    } else if(parseInt(subcategories.id) === 4) {
                        $('#subcategories').append(`<option value="${subcategories.id}" id="job">${subcategories.en_name}</option>`)
                    } else {
                        $('#subcategories').append(`<option value="${subcategories.id}">${subcategories.en_name}</option>`)
                    }
                }
            )
        }
    })
}

function removeSubcategoryItem() {
    for (i = 1; i <= sub.length; i++) {
        sub.remove(i);
    }
}

function hideCode(){
    codes.classList.add('d-none');
    idCodes.name = "";
}

function showCodes(){
    codes.classList.remove('d-none');
    idCodes.name = "codes";
}


function hideEvents(){
    events.style.display = 'none';
    startDate.name = "";
    endDate.name = "";

}

function showEvents(){
    events.style.display = 'grid';
    startDate.name = "start_date";
    endDate.name = "end_date";
}

// function hideSub(){
//     subDiv.classList.add('d-none');
// }
//
// function showSub(){
//     subDiv.classList.remove('d-none');
//     subDiv.classList.add('d-flex');
// }

$(document).ready(function() {
    $(".mul-select").select2({
        placeholder: "Select", //placeholder
        tags: true,
        tokenSeparator: ['/',',',',',""]
    })
})
