const openModalButtons = document.querySelectorAll('[data-modal-target]');
const closeModalButtons = document.querySelectorAll('[data-close-button]');
const overlay = document.getElementById('custom-overlay');
const modalFullOverlay = document.getElementById('modal-full-overlay');

console.log(overlay)

openModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        console.log(123);
        const modal = document.querySelector(button.dataset.modalTarget);
        openModal(modal);
    })
})

closeModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = button.closest('.custom-modal')
        closeModal(modal);
    })
})

function openModal(modal){
    if (modal == null) return;
    modal.classList.add('custom-modal-active');
    overlay.classList.add('custom-active');
    modalFullOverlay.classList.add('modal-full-overlay-active');
}

function closeModal(modal){
    if (modal == null) return;
    modal.classList.remove('custom-modal-active');
    overlay.classList.remove('custom-active');
    modalFullOverlay.classList.remove('modal-full-overlay-active');
}
