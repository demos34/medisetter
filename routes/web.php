<?php

use App\Http\Controllers\Administrator\AdminController;
use App\Http\Controllers\Administrator\AnalyzeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Categories\CategoriesController;
use App\Http\Controllers\Categories\SubcategoriesController;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Post\PostsController;
use App\Http\Controllers\User\ContactsController;
use App\Http\Controllers\Users\MessagesController;
use App\Http\Controllers\Users\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|users-update
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
 * ISO-639-1 Lang codes -> Vietnamese code is vi
 */



Route::get('/lang/{locale}', function ($locale) {
    if (! in_array($locale, ['en', 'vi'])) {
        $locale = 'en';
    }

    session()->put('locale', $locale);
    return redirect()->back();
});

//new login
Route::post('logged_in', [LoginController::class, 'authenticate'])->name('new-login');

//Route::get('/', function () {
//    return view('welcome');
//});

/*
 * new layouts
 */

Route::get('/new/layout', [HomeController::class, 'layout'])
    ->name('new-layout')
    ->middleware('can:dev');
Route::get('/new/log-out', [HomeController::class, 'noLog'])
    ->name('new-layout-logout')
    ->middleware('can:dev');
Route::get('/new/log-in', [HomeController::class, 'newLogin'])
    ->name('new-layout-in')
    ->middleware('can:dev');
Route::get('/new/news-feed', [HomeController::class, 'newsFeed'])
    ->name('new-layout-news-feed')
    ->middleware('can:dev');

/*
 * Admin controller
 */



Route::get('/admin/for/cat', [AdminController::class, 'cat'])->middleware('can:admin');
Route::get('/admin/verify/all', [AdminController::class, 'verify'])->name('verify-all-users')->middleware('can:admin');


Route::get('/admin/get/lang', [AdminController::class, 'getLang'])
    ->name('admin-lang');

Route::get('/admin/{page}', [AdminController::class, 'index'])
    ->name('admin-index')->middleware('can:admin');

Route::get('/admin/roles', [AdminController::class, 'roles'])
    ->name('admin-roles')->middleware('can:admin');

Route::get('/admin/categories', [AdminController::class, 'categories'])
    ->name('admin-categories')->middleware('can:admin');

Route::put('/admin/categories', [AdminController::class, 'storeCategory'])
    ->name('admin-categories-store')->middleware('can:admin');

Route::put('/admin/categories/update/{category}', [AdminController::class, 'updateCategory'])
    ->name('admin-categories-update')->middleware('can:admin');

Route::get('/admin/category/{category}', [AdminController::class, 'showCategory'])
    ->name('admin-show-category')->middleware('can:admin');

Route::get('/admin/category/is-visible/{category}', [AdminController::class, 'changeVisibleStatus'])
    ->name('admin-visible-category')->middleware('can:admin');

Route::delete('/admin/categories/{category}', [AdminController::class, 'deleteCategory'])
    ->name('admin-categories-delete')->middleware('can:admin');

Route::get('/admin/subcategories', [AdminController::class, 'subcategories'])
    ->name('admin-subcategories')->middleware('can:admin');

Route::get('/admin/subcategory/{subcategory}', [AdminController::class, 'showSubcategory'])
    ->name('admin-show-subcategory')->middleware('can:admin');

Route::put('/admin/subcategories', [AdminController::class, 'storeSubcategories'])
    ->name('admin-subcategories-store')->middleware('can:admin');

Route::put('/admin/subcategories/{subcategory}', [AdminController::class, 'updateSubcategories'])
    ->name('admin-subcategories-update')->middleware('can:admin');

Route::delete('/admin/subcategories/{subcategory}', [AdminController::class, 'deleteSubcategory'])
    ->name('admin-subcategories-delete')->middleware('can:admin');



Route::get('/admin/specialities/{speciality}', [AdminController::class, 'showSpeciality'])
    ->name('admin-show-speciality')->middleware('can:admin');

Route::put('/admin/specialities', [AdminController::class, 'storeSpecialities'])
    ->name('admin-specialities-store')->middleware('can:admin');

Route::put('/admin/specialities/{speciality}', [AdminController::class, 'updateSpecialities'])
    ->name('admin-specialities-update')->middleware('can:admin');

Route::delete('/admin/specialities/{speciality}', [AdminController::class, 'deleteSpeciality'])
    ->name('admin-specialities-delete')->middleware('can:admin');


Route::get('/admin/universities/{university}', [AdminController::class, 'showUniversity'])
    ->name('admin-show-university')->middleware('can:admin');

Route::put('/admin/universities', [AdminController::class, 'storeUniversities'])
    ->name('admin-universities-store')->middleware('can:admin');

Route::put('/admin/universities/{university}', [AdminController::class, 'updateUniversities'])
    ->name('admin-universities-update')->middleware('can:admin');

Route::delete('/admin/universities/{university}', [AdminController::class, 'deleteUniversity'])
    ->name('admin-universities-delete')->middleware('can:admin');

Route::get('/admin/degrees/{degree}', [AdminController::class, 'showDegree'])
    ->name('admin-show-degree')->middleware('can:admin');

Route::put('/admin/degrees', [AdminController::class, 'storeDegrees'])
    ->name('admin-degrees-store')->middleware('can:admin');

Route::put('/admin/degrees/{degree}', [AdminController::class, 'updateDegrees'])
    ->name('admin-degrees-update')->middleware('can:admin');

Route::delete('/admin/degrees/{degree}', [AdminController::class, 'deleteDegree'])
    ->name('admin-degrees-delete')->middleware('can:admin');

Route::get('/admin/codes/{code}', [AdminController::class, 'showCode'])
    ->name('admin-show-code')->middleware('can:admin');

Route::put('/admin/codes', [AdminController::class, 'storeCodes'])
    ->name('admin-codes-store')->middleware('can:admin');

Route::put('/admin/codes/{code}', [AdminController::class, 'updateCodes'])
    ->name('admin-codes-update')->middleware('can:admin');

Route::delete('/admin/codes/{code}', [AdminController::class, 'deleteCode'])
    ->name('admin-codes-delete')->middleware('can:admin');



Route::get('/admin/users/view', [AdminController::class, 'viewUsers'])
    ->name('admin-view-users')->middleware('can:admin');

Route::get('/admin/users/{user}/one', [AdminController::class, 'viewOneUser'])
    ->name('admin-view-one-user')->middleware('can:admin');

Route::post('/admin/roles/change/{user}', [AdminController::class, 'changeRoles'])
    ->name('admin-change-roles')->middleware('can:admin');

Route::get('/admin/posts/restore/{post}', [AdminController::class, 'restoreSoftDeletedPost'])
    ->name('admin-posts-restore')->middleware('can:admin');

Route::get('/admin/posts/show/{post}', [AdminController::class, 'showSoftDeletedPost'])
    ->name('admin-posts-deleted-show')->middleware('can:admin');

Route::delete('/admin/posts/delete/{post}', [AdminController::class, 'hardDestroy'])
    ->name('admin-posts-hard-delete')->middleware('can:admin');

Route::delete('/admin/posts/all/delete', [AdminController::class, 'hardDestroyAll'])
    ->name('admin-posts-hard-delete-all')->middleware('can:admin');


Route::get('/admin/enterprise/create', [AdminController::class, 'createEnterprise'])
    ->name('admin-create-enterprise')->middleware('can:admin');
Route::post('/admin/enterprise/', [AdminController::class, 'storeEnterprise'])
    ->name('admin-store-enterprise')->middleware('can:admin');

//verify user

Route::get('/admin/user/verify/{user}', [AdminController::class, 'verifyUser'])
    ->name('admin-user-verify')->middleware('can:admin');

/*
 * analyze
 */

Route::get('/analyze', [AnalyzeController::class, 'index'])
    ->name('analyze')->middleware('can:admin-enterprise');

Route::get('/analyze/details/{post}', [AnalyzeController::class, 'analyzeDetails'])
    ->name('analyze-details')->middleware('can:admin-enterprise');

Route::post('/analyze/create/{post}', [AnalyzeController::class, 'store'])
    ->name('analyze-create')->middleware('can:admin-enterprise');

Route::get('/analyze/show/{analyze}', [AnalyzeController::class, 'show'])
    ->name('analyze-show')->middleware('can:admin-enterprise');

Route::delete('/analyze/show/{analyze}', [AnalyzeController::class, 'destroy'])
    ->name('analyze-delete')->middleware('can:admin-enterprise');

Route::post('/analyze/login/create', [AnalyzeController::class, 'storeLogin'])
    ->name('analyze-create-login')->middleware('can:admin');

Route::get('/analyze/login/{analyzeLogin}', [AnalyzeController::class, 'showLogin'])
    ->name('analyze-show-login')->middleware('can:admin');

Route::delete('/analyze/login/{analyzeLogin}', [AnalyzeController::class, 'destroyLogin'])
    ->name('analyze-delete-login')->middleware('can:admin');

Route::get('/analyze/dettach/{post}', [AnalyzeController::class, 'dettach'])
    ->name('analyze-dettach')->middleware('can:admin-enterprise');

Route::get('/analyze/attach/{post}', [AnalyzeController::class, 'attach'])
    ->name('analyze-attach')->middleware('can:admin-enterprise');

Route::get('/analyze/show-dettached', [AnalyzeController::class, 'showDettached'])
    ->name('analyze-show-dettached')->middleware('can:admin-enterprise');

/*
 * users
 */

Route::get('/user/login-message', [UsersController::class, 'notVerifiedYet'])
    ->name('user-not-verified')->middleware('auth');

Route::get('/users', [UsersController::class, 'index'])
    ->name('users')->middleware('auth');
Route::get('/users/{page}', [UsersController::class, 'redirectDashboard'])
    ->name('users-page')->middleware('auth');
Route::put('/users/{page}', [UsersController::class, 'updateUser'])
    ->name('users-update')->middleware('auth');
Route::patch('/users/university', [UsersController::class, 'updateUniversity'])
    ->name('users-university-update')->middleware('auth');
Route::post('/users/speciality', [UsersController::class, 'updateSpeciality'])
    ->name('users-speciality-update')->middleware('auth');
Route::post('/users/organisation', [UsersController::class, 'updateOrganisation'])
    ->name('users-organisation-update')->middleware('auth');
Route::post('/users/search', [UsersController::class, 'searchUser'])
    ->name('users-search')->middleware('auth');
Route::post('/users/age', [UsersController::class, 'changeAge'])
    ->name('users-age')->middleware('auth');
Route::post('/users/where-in', [UsersController::class, 'changeUsersLocation'])
    ->name('users-where-in')->middleware('auth');


Route::get('/users/view/{user}', [UsersController::class, 'viewUser'])
    ->name('user-view')->middleware('auth');
Route::get('/users/follow/{user}', [UsersController::class, 'follow'])
    ->name('user-follow')->middleware('auth')->middleware('can:verify');

//my events
Route::get('/user/my/events', [UsersController::class, 'myEvents'])
    ->name('users-my-events')->middleware('auth')->middleware('can:verify');
Route::get('/user/my/events/{type}/{post}', [UsersController::class, 'listAllUsers'])
    ->name('users-list-events')->middleware('can:admin')->middleware('can:verify');

//my career & paid opp

Route::get('/user/my/career', [UsersController::class, 'myCareer'])
    ->name('users-my-career')->middleware('auth')->middleware('can:verify');


/*
 * dash
 */


Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/', [HomeController::class, 'index'])->name('home');


/*
 * categories
 */

//Route::get('/categories', [CategoriesController::class, 'index'])->name('categories-index');
Route::get('/categories/{category}', [CategoriesController::class, 'show'])
    ->name('categories-show');
Route::get('/categories/not-logged/{category}', [CategoriesController::class, 'isNotLoggedUser'])
    ->name('categories-not-log');



/*
 * subcategories
 */

//Route::get('/subcategories', [SubcategoriesController::class, 'index'])->name('subcategories-index');
Route::get('/subcategories/{subcategory}', [SubcategoriesController::class, 'show'])->name('subcategories-show');


/*
 * posts
 */
Route::get('/posts', [PostsController::class, 'index'])->name('posts-index');

Route::post('/posts/search', [PostsController::class, 'search'])->name('posts-search');

Route::get('/posts/{post}', [PostsController::class, 'show'])->name('posts-show')
    ->middleware('auth');

Route::get('/categories/posts/create/{category}', [PostsController::class, 'createFromCategories'])
    ->name('categories-posts-create')->middleware('can:admin');

Route::get('/categories/posts/{post}/edit', [PostsController::class, 'edit'])
    ->name('categories-posts-edit')->middleware('can:admin');

Route::get('/categories/posts/{post}/author', [PostsController::class, 'editAuthor'])
    ->name('categories-posts-edit-author');

Route::patch('/categories/posts/{post}', [PostsController::class, 'update'])
    ->name('categories-posts-update')->middleware('can:admin');


Route::patch('/categories/posts/{post}/update', [PostsController::class, 'updateAuthor'])
    ->name('categories-posts-update-author');

Route::get('/discussion/posts/create/{category}', [PostsController::class, 'createFromDiscussion'])
    ->name('discussion-posts-create')->middleware('auth')->middleware('can:verify');

Route::put('/posts', [PostsController::class, 'store'])
    ->name('posts-store')->middleware('can:admin');

Route::put('/discussion', [PostsController::class, 'storeDiscussion'])
    ->name('discussion-store')->middleware('auth')->middleware('can:verify');

Route::get('/subcategories/posts/create/{subcategory}', [PostsController::class, 'createFromSubcategories'])
    ->name('subcategories-posts-create')->middleware('can:admin');

Route::get('/posts/{id}/subcategories', [PostsController::class, 'subcategories'])
    ->name('subcategories-ajax')->middleware('can:admin');

Route::get('/posts/react/{post}/{reaction}',  [PostsController::class, 'reaction'])
    ->name('posts-reactions')->middleware('auth')->middleware('can:verify');

Route::delete('/posts/destroy/{post}',  [PostsController::class, 'destroy'])
    ->name('posts-delete')->middleware('auth');

//events subscribing

Route::get('/posts/event/interested/{post}',  [PostsController::class, 'eventInterested'])
    ->name('posts-event-interested')->middleware('auth')->middleware('can:verify');
Route::get('/posts/event/participate/{post}',  [PostsController::class, 'eventParticipate'])
    ->name('posts-event-participate')->middleware('auth')->middleware('can:verify');

//career subscribing


Route::get('/posts/career/interested/{post}',  [PostsController::class, 'careerInterested'])
    ->name('posts-career-interested')->middleware('auth')->middleware('can:verify');
Route::get('/posts/career/participate/{post}',  [PostsController::class, 'careerParticipate'])
    ->name('posts-career-participate')->middleware('auth')->middleware('can:verify');



//specialities
Route::get('/sub/{category}/speciality/{speciality}', [PostsController::class, 'showSpecialitiesPosts'])->name('posts-spec');


//sharing logic
Route::patch('/posts/share/{post}',  [PostsController::class, 'share'])
    ->name('posts-share')->middleware('auth')->middleware('can:verify');

//message
Route::get('/messages',  [MessagesController::class, 'index'])
    ->name('msg')->middleware('auth');

//message
Route::get('/messages/attach',  [MessagesController::class, 'attachContacts'])
    ->name('attach')->middleware('can:admin');


//contacts
//Route::get('/contacts/token', [ContactsController::class, 'token'])->name('token');
Route::get('/contacts', [ContactsController::class, 'index'])->name('contacts')->middleware('auth');
Route::get('/get', [ContactsController::class, 'get'])->name('get')->middleware('auth');
Route::get('/conversation/{id}', [ContactsController::class, 'getMessagesFor'])->name('getMsg')->middleware('auth');
Route::post('/conversation/send', [ContactsController::class, 'send'])->name('send')->middleware('auth');
Route::get('/users/send/{user}', [ContactsController::class, 'addToContacts'])->name('add-contacts')->middleware('can:admin');


