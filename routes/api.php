<?php

use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\CommentController;
use App\Http\Controllers\Api\MessageController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/categories', [CategoryController::class, 'index'])->name('api-category');

Route::get('/spec/{category}', [CategoryController::class, 'spec'])->name('api-speciality');

Route::get('/comments/{post}', [CommentController::class, 'index'])->name('api-comments');

Route::middleware('auth:api')->get('/user', [CommentController::class, 'profile'])->name('api-user-profile');

Route::post('/comments/{post}', [CommentController::class, 'store'])->name('api-comments-post');

//message



Route::middleware('auth:api')->get('/react/{post}/{reaction}', [CommentController::class, 'react'])->name('api-react-post');
Route::middleware('auth:api')->get('/my/{post}/{reaction}', [CommentController::class, 'myReact'])->name('api-my-react-post');
Route::middleware('auth:api')->get('/bind-my/{post}', [CommentController::class, 'myBind'])->name('api-my-react-post');
Route::middleware('auth:api')->get('/react/count/{post}/{reaction}', [CommentController::class, 'reactCount'])->name('api-count-reacts-post');
Route::middleware('auth:api')->get('/get', [MessageController::class, 'get'])->name('api-get');
Route::middleware('auth:api')->get('/conversation/{id}', [MessageController::class, 'getMessagesFor'])->name('api-getMsg');
Route::middleware('auth:api')->get('/getDefault', [MessageController::class, 'getDefaultContact'])->name('api-getDefault');
Route::middleware('auth:api')->get('/getDefaultMessages', [MessageController::class, 'getDefaultMessages'])->name('api-getDefault-msg');
Route::middleware('auth:api')->post('/conversation/send', [MessageController::class, 'send'])->name('api-send');
Route::middleware('auth:api')->get('/count', [MessageController::class, 'count'])->name('api-count');
Route::middleware('auth:api')->get('/users/avatar', [MessageController::class, 'getAvatar'])->name('api-avatar');
Route::middleware('auth:api')->get('/messages/read/{user}', [MessageController::class, 'readMessage'])->name('api-read-msg');


Route::middleware('auth:api')->get('/notifications', [MessageController::class, 'notification'])->name('notif');
Route::middleware('auth:api')->get('/notifications/view', [MessageController::class, 'notificationView'])->name('notif');
Route::middleware('auth:api')->get('/notifications/count', [MessageController::class, 'notificationCount'])->name('notif');

Route::middleware('auth:api')->post('/search', [CommentController::class, 'search'])->name('search');
