<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToAnalyzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analyzes', function (Blueprint $table) {
            $table->integer('from_age')->nullable();
            $table->integer('to_age')->nullable();
            $table->string('location')->nullable();
            $table->unsignedBigInteger('degree_id')->nullable();
            $table->unsignedBigInteger('speciality_id')->nullable();

            $table->foreign('degree_id')->references('id')->on('degrees')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('speciality_id')->references('id')->on('specialities')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analyzes', function (Blueprint $table) {
            $table->dropForeign('degree_id');
            $table->dropForeign('speciality_id');
            $table->dropColumn('from_age');
            $table->dropColumn('to_age');
            $table->dropColumn('location');
            $table->dropColumn('degree_id');
            $table->dropColumn('speciality_id');
        });
    }
}
