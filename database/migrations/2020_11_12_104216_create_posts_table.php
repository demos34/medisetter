<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->longText('body');
            $table->string('slug')->unique()->nullable();
            $table->string('image')->nullable();
            $table->string('video')->nullable();
            $table->string('document')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('code_id')->nullable();
            $table->unsignedBigInteger('author_id');
            $table->boolean('is_shared')->default(0);
            $table->unsignedBigInteger('share_id')->nullable();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')
                ->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreign('code_id')->references('id')->on('codes')
                ->cascadeOnUpdate()->cascadeOnDelete();
            $table->foreign('author_id')->references('id')->on('authors')
                ->cascadeOnUpdate()->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
