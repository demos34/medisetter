<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('from_id');
            $table->unsignedBigInteger('to_id');
            $table->unsignedBigInteger('post_id')->nullable();
            $table->timestamps();

            $table->foreign('from_id')->references('id')->on('froms')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('to_id')->references('id')->on('tos')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
            $table->foreign('post_id')->references('id')->on('posts')
                ->cascadeOnUpdate()
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
