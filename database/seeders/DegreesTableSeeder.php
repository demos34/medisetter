<?php

namespace Database\Seeders;

use App\Models\Degree\Degree;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DegreesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Degree::truncate();
        DB::table('subcategories')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Degree::create(['en_name' => 'General practitioner','vi_name'=>'Bác sĩ đa khoa','slug'=>1,]);
        Degree::create(['en_name' => 'Residency','vi_name'=>'Bác sĩ nội trú','slug'=>2,]);
        Degree::create(['en_name' => 'Specialist Level I','vi_name'=>'Bác sĩ chuyên khoa I','slug'=>3,]);
        Degree::create(['en_name' => 'Specialist Level II','vi_name'=>'Bác sĩ chuyên khoa II','slug'=>4,]);
        Degree::create(['en_name' => 'Academia-researcher','vi_name'=>'Hàn lâm nghiên cứu gia','slug'=>5,]);
        Degree::create(['en_name' => 'Masters','vi_name'=>'Thạc sĩ','slug'=>6,]);
        Degree::create(['en_name' => 'Doctorate','vi_name'=>'Tiến sĩ','slug'=>7,]);
        Degree::create(['en_name' => 'Associate professor','vi_name'=>'Phó giáo sư','slug'=>8,]);
        Degree::create(['en_name' => 'Professor','vi_name'=>'Giáo sư','slug'=>9,]);

    }
}
