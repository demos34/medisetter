<?php

namespace Database\Seeders;

use App\Models\Degree\University;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UniversitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        University::truncate();
        DB::table('subcategories')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        University::create(['en_name' => 'Ton Duc Thang University','vi_name'=>'Đại Học Tôn Đức Thắng','location'=>'HCMC','slug'=>1,]);
        University::create(['en_name' => 'Ho Chi Minh City University of Medicine and Pharmacy','vi_name'=>'Đại Học Y Dược TPHCM','location'=>'HCMC','slug'=>2,]);
        University::create(['en_name' => 'Institute of Medical Military','vi_name'=>'Học Viện Quân Y','location'=>'HCMC','slug'=>3,]);
        University::create(['en_name' => 'Pham Ngoc Thach University of Medicine','vi_name'=>'Đại Học Y Khoa Phạm Ngọc Thạch','location'=>'HCMC','slug'=>4,]);
        University::create(['en_name' => 'Nguyen Tat Thanh University','vi_name'=>'Đại Học Nguyễn Tất Thành','location'=>'HCMC','slug'=>5,]);
        University::create(['en_name' => 'Ha Noi Medical University','vi_name'=>'Đại Học Y Hà Nội','location'=>'Hanoi','slug'=>6,]);
        University::create(['en_name' => 'Hanoi National University','vi_name'=>'Đại học Quốc Gia Hà Nội','location'=>'Hanoi','slug'=>7,]);
        University::create(['en_name' => 'Hue University of Medicine and Pharmacy','vi_name'=>'Đại Học Y Dược – Đại Học Huế','location'=>'Hanoi','slug'=>8,]);
        University::create(['en_name' => 'Vietnam University Of Traditional Medicine','vi_name'=>'Học Viện Y Dược Học Cổ Truyền Việt Nam','location'=>'Hanoi','slug'=>9,]);
        University::create(['en_name' => 'Hanoi University of Science & Technology','vi_name'=>'Đại Học Bách Khoa Hà Nội','location'=>'Hanoi','slug'=>10,]);
        University::create(['en_name' => 'Thai Binh University of Medicine and Pharmacy','vi_name'=>'Đại Học Y Dược Thái Bình','location'=>'Thai Binh','slug'=>11,]);
        University::create(['en_name' => 'Danang University of Medical Technology and Pharmacy','vi_name'=>'Đại học Kỹ thuật Y Dược Đà Nẵng','location'=>'Danang','slug'=>12,]);
        University::create(['en_name' => 'Danang University','vi_name'=>'Đại Học Đà Nẵng','location'=>'Danang','slug'=>13,]);
        University::create(['en_name' => 'Can Tho University of Medicine and Pharmacy','vi_name'=>'Đại Học Y Dược Cần Thơ','location'=>'Can Tho','slug'=>14,]);
        University::create(['en_name' => 'Hai Phong University of Medicine and Pharmacy','vi_name'=>'Đại Học Y Dược Hải Phòng','location'=>'Hai Phong','slug'=>15,]);
        University::create(['en_name' => 'Thai Nguyen University of Medicine and Pharmacy','vi_name'=>'Đại Học Y Dược – Đại Học Thái Nguyên','location'=>'Thai Nguyen','slug'=>16,]);
        University::create(['en_name' => 'Vinh Medical University','vi_name'=>'Đại Học Y Khoa Vinh','location'=>'Vinh','slug'=>17,]);
        University::create(['en_name' => 'Hai Duong Medical Technical University','vi_name'=>'Đại Học Kỹ Thuật Y Tế Hải Dương','location'=>'Hai Duong','slug'=>18,]);
        University::create(['en_name' => 'Tay Nguyen University','vi_name'=>'Đại Học Tây Nguyên','location'=>'Dak Lak','slug'=>19,]);
        University::create(['en_name' => 'Hanoi College of Medicine and Pharmacy','vi_name'=>'Cao đẳng Y Dược Hà Nội','location'=>'Hanoi','slug'=>20,]);
        University::create(['en_name' => 'Pasteur College of Medicine and Pharmacy','vi_name'=>'Cao đẳng Y Dược Pasteur','location'=>'Hanoi','slug'=>21,]);
        University::create(['en_name' => 'Pham Ngoc Thach Medical College','vi_name'=>'Cao đẳng Y Khoa Phạm Ngọc Thạch','location'=>'Hanoi','slug'=>22,]);
        University::create(['en_name' => 'Bach Mai Medical College','vi_name'=>'Cao đẳng Y tế Bạch Mai','location'=>'Hanoi','slug'=>23,]);
        University::create(['en_name' => 'Hanoi Medical College','vi_name'=>'Cao đẳng Y tế Hà Nội','location'=>'Hanoi','slug'=>24,]);
        University::create(['en_name' => 'Ha Dong Medical College','vi_name'=>'Cao đẳng Y tế Hà Đông','location'=>'Hanoi','slug'=>25,]);
        University::create(['en_name' => 'Yersin College of Medicine and Pharmacy','vi_name'=>'Cao đẳng Y Dược Yersin','location'=>'Hanoi','slug'=>26,]);
        University::create(['en_name' => 'Dang Van Ngu Medical College','vi_name'=>'Cao đẳng Y tế Đặng Văn Ngữ','location'=>'Hanoi','slug'=>27,]);
        University::create(['en_name' => 'Hanoi College of Public Health','vi_name'=>'Cao đẳng Y tế cộng đồng Hà Nội','location'=>'Hanoi','slug'=>28,]);
        University::create(['en_name' => 'College of Medical Millitary 1','vi_name'=>'Cao đẳng Quân Y 1','location'=>'Hanoi','slug'=>29,]);
        University::create(['en_name' => 'Saigon College of Medicine and Pharmacy','vi_name'=>'Cao đẳng Y Dược Sài Gòn','location'=>'HCMC','slug'=>30,]);
        University::create(['en_name' => 'Saigon Gia Dinh College','vi_name'=>'Cao đẳng Sài Gòn Gia Định','location'=>'HCMC','slug'=>31,]);
        University::create(['en_name' => 'Hong Duc College of Medicine and Pharmacy','vi_name'=>'Cao đẳng Y Dược Hồng Đức','location'=>'HCMC','slug'=>32,]);
        University::create(['en_name' => 'Dai Viet Saigon College','vi_name'=>'Cao đẳng Đại Việt Sài Gòn','location'=>'HCMC','slug'=>33,]);
        University::create(['en_name' => 'Phu Tho College of Medicine and Pharmacy','vi_name'=>'Cao đẳng Y Dược Phú Thọ','location'=>'Phu Tho','slug'=>34,]);
        University::create(['en_name' => 'Hai Dương Medical College','vi_name'=>'Cao đẳng Y tế Hải Dương','location'=>'Hai Duong','slug'=>35,]);
        University::create(['en_name' => 'Bac Ninh Medical College','vi_name'=>'Cao đẳng Y tế Bắc Ninh','location'=>'Bac Ninh','slug'=>36,]);
        University::create(['en_name' => 'Thai Nguyen Medical College','vi_name'=>'Cao đẳng Y tế Thái Nguyên','location'=>'Thai Nguyen','slug'=>37,]);
        University::create(['en_name' => 'Quang Ninh Medical College','vi_name'=>'Cao đẳng Y tế Quảng Ninh','location'=>'Quang Nin','slug'=>38,]);
        University::create(['en_name' => 'Thanh Hoa Medical College','vi_name'=>'Cao đẳng Y tế Thanh Hóa','location'=>'Thanh Hoa','slug'=>39,]);
        University::create(['en_name' => 'Thai Binh Medical College','vi_name'=>'Cao đẳng Y tế Thái Bình','location'=>'Thai Binh','slug'=>40,]);
        University::create(['en_name' => 'Hung Yen Medical College','vi_name'=>'Cao đẳng Y tế Hưng Yên','location'=>'Hung Yen','slug'=>41,]);
        University::create(['en_name' => 'Lang Son Medical College','vi_name'=>'Cao đẳng Y tế Lạng Sơn','location'=>'Lang Son','slug'=>42,]);
        University::create(['en_name' => 'Son La Medical College','vi_name'=>'Cao đẳng Y tế Sơn La','location'=>'Son La','slug'=>43,]);
        University::create(['en_name' => 'Ninh Binh Medical College','vi_name'=>'Cao đẳng Y tế Ninh Bình','location'=>'Ninh Binh','slug'=>44,]);
        University::create(['en_name' => 'Can Tho Medical College','vi_name'=>'Cao đẳng Y tế Cần Thơ','location'=>'Can Tho','slug'=>45,]);
        University::create(['en_name' => 'Quang Nam Medical College','vi_name'=>'Cao đẳng Y tế Quảng Nam','location'=>'Quang Nam','slug'=>46,]);
        University::create(['en_name' => 'Tue Tinh College of Medicine and Pharmacy','vi_name'=>'Cao đẳng Y Dược Tuệ Tĩnh','location'=>'Nghe An','slug'=>47,]);
        University::create(['en_name' => 'Tue Tinh College of Medicine and Pharmacy','vi_name'=>'Cao đẳng Y Dược Tuệ Tĩnh','location'=>'Ha Tinh','slug'=>48,]);
        University::create(['en_name' => 'Hue Medical College','vi_name'=>'Cao đẳng Y tế Huế','location'=>'Hue','slug'=>49,]);
        University::create(['en_name' => 'Ha Tinh Medical College','vi_name'=>'Cao đẳng Y tế Hà Tĩnh','location'=>'Ha Tinh','slug'=>50,]);
        University::create(['en_name' => 'Binh Dinh Medical College','vi_name'=>'Cao đẳng Y tế Bình Định','location'=>'Binh Dinh','slug'=>51,]);
        University::create(['en_name' => 'Dong Nai Medical College','vi_name'=>'Cao đẳng Y tế Đồng Nai','location'=>'Dong Nai','slug'=>52,]);
        University::create(['en_name' => 'Binh Duong Medical College','vi_name'=>'Cao đẳng Y tế Bình Dương','location'=>'Binh Duong','slug'=>53,]);
        University::create(['en_name' => 'Ca Mau Medical College','vi_name'=>'Cao đẳng Y tế Cà Mau','location'=>'Ca Mau','slug'=>54,]);
        University::create(['en_name' => 'Tien Giang Medical College','vi_name'=>'Cao đẳng Y tế Tiền Giang','location'=>'Tien Giang','slug'=>55,]);
        University::create(['en_name' => 'Dong Thap Medical College','vi_name'=>'Cao đẳng Y tế Đồng Tháp','location'=>'Đong Thap','slug'=>56,]);
        University::create(['en_name' => 'Khanh Hoa Medical College','vi_name'=>'Cao đẳng Y tế Khánh Hòa','location'=>'Khanh Hoa','slug'=>57,]);
        University::create(['en_name' => 'Lam Dong Medical College','vi_name'=>'Cao đẳng Y tế Lâm Đồng','location'=>'Lam Đong','slug'=>58,]);
        University::create(['en_name' => 'Binh Duong Medical College','vi_name'=>'Cao đẳng y tế Bình Dương','location'=>'Binh Duong','slug'=>59,]);
        University::create(['en_name' => 'Bac Lieu Medical College','vi_name'=>'Cao đẳng y tế Bạc Liêu','location'=>'Bac Lieu','slug'=>60,]);
        University::create(['en_name' => 'Binh Thuan Medical College','vi_name'=>'Cao đẳng Y tế Bình Thuận','location'=>'Binh Thuan','slug'=>61,]);
        University::create(['en_name' => 'Binh Phuoc Medical College','vi_name'=>'Cao đẳng Y tế Bình Phước','location'=>'Binh Phuoc','slug'=>62,]);
        University::create(['en_name' => 'Kien Giang Medical College','vi_name'=>'Cao đẳng Y tế Kiên Giang','location'=>'Kien Giang','slug'=>63,]);
        University::create(['en_name' => 'Tra Vinh Medical College','vi_name'=>'Cao đẳng Y tế Trà Vinh','location'=>'Tra Vinh','slug'=>64,]);
        University::create(['en_name' => 'Phu Yen Medical College','vi_name'=>'Cao đẳng Y tế Phú Yên','location'=>'Phu Yen','slug'=>65,]);

    }
}
