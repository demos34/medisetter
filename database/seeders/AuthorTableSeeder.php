<?php

namespace Database\Seeders;

use App\Models\Post\Post;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AuthorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Post::truncate();
        DB::table('authors')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        foreach (User::all() as $user) {
            $user->author()->create();
        }
    }
}
