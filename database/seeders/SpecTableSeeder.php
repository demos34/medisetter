<?php

namespace Database\Seeders;

use App\Models\Category\Speciality;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SpecTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Speciality::truncate();
        DB::table('subcategories')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Speciality::create(['en_name' => 'General Surgery','vi_name'=>'Ngoại tổng quát','slug'=>1,]);
        Speciality::create(['en_name' => 'Orthopedic Surgery','vi_name'=>'Phẫu thuật chỉnh hình','slug'=>2,]);
        Speciality::create(['en_name' => 'Organ Transplantation','vi_name'=>'Ghép cơ quan','slug'=>3,]);
        Speciality::create(['en_name' => 'Vascular Surgery','vi_name'=>'Phẫu thuật mạch máu','slug'=>4,]);
        Speciality::create(['en_name' => 'Ophthalmologic Surgery','vi_name'=>'Phẫu thuật mắt','slug'=>5,]);
        Speciality::create(['en_name' => 'Urology','vi_name'=>'Phẫu thuật tiết niệu','slug'=>6,]);
        Speciality::create(['en_name' => 'Paediatric Surgery','vi_name'=>'Phẫu thuật nhi','slug'=>7,]);
        Speciality::create(['en_name' => 'Otolaryngologic Surgery','vi_name'=>'Tai mũi họng','slug'=>8,]);
        Speciality::create(['en_name' => 'Neurosurgery','vi_name'=>'Phẫu thuật thần kinh','slug'=>9,]);
        Speciality::create(['en_name' => 'Cardio-thoracic Surgery','vi_name'=>'Phẫu thuật tim - lồng ngực','slug'=>10,]);
        Speciality::create(['en_name' => 'Otolaryngology / Head and Neck Surgery','vi_name'=>'Phẫu thuật Đầu mặt cổ','slug'=>11,]);
        Speciality::create(['en_name' => 'Plastic Surgery','vi_name'=>'Phẫu thuật thẩm mỹ','slug'=>12,]);
        Speciality::create(['en_name' => 'Internal Medicine','vi_name'=>'Nội tổng quát','slug'=>13,]);
        Speciality::create(['en_name' => 'Pulmonology / Respiratory','vi_name'=>'Hô hấp','slug'=>14,]);
        Speciality::create(['en_name' => 'Cardiology','vi_name'=>'Tim mạch','slug'=>15,]);
        Speciality::create(['en_name' => 'Gastroenterology','vi_name'=>'Tiêu hoá','slug'=>16,]);
        Speciality::create(['en_name' => 'Endocrinology','vi_name'=>'Nội tiết','slug'=>17,]);
        Speciality::create(['en_name' => 'Hematology','vi_name'=>'Huyết học','slug'=>18,]);
        Speciality::create(['en_name' => 'Urology & Neprology','vi_name'=>'Tiết niệu & Thận học','slug'=>19,]);
        Speciality::create(['en_name' => 'Musculoskeletal','vi_name'=>'Cơ xương khớp','slug'=>20,]);
        Speciality::create(['en_name' => 'Infectious Disease','vi_name'=>'Bệnh truyền nhiễm','slug'=>21,]);
        Speciality::create(['en_name' => 'Oncology','vi_name'=>'Ung thư','slug'=>22,]);
        Speciality::create(['en_name' => 'Allergy & Immunology','vi_name'=>'Dị ứng & Miễn dịch','slug'=>23,]);
        Speciality::create(['en_name' => 'Rheumatology','vi_name'=>'Thấp khớp','slug'=>24,]);
        Speciality::create(['en_name' => 'Nutrition','vi_name'=>'Dinh dưỡng','slug'=>25,]);
        Speciality::create(['en_name' => 'Obstetrics & Gynecology','vi_name'=>'Sản phụ khoa','slug'=>26,]);
        Speciality::create(['en_name' => 'Dermatology','vi_name'=>'Da liễu','slug'=>27,]);
        Speciality::create(['en_name' => 'Tropical Disease','vi_name'=>'Bệnh nhiệt đới','slug'=>28,]);
        Speciality::create(['en_name' => 'Anesthesia & Recovery','vi_name'=>'Gây mê hồi sức','slug'=>29,]);
        Speciality::create(['en_name' => 'Ondoto-Stomatology','vi_name'=>'Răng hàm mặt','slug'=>30,]);
        Speciality::create(['en_name' => 'Ophthalmology','vi_name'=>'Nhãn khoa','slug'=>31,]);
        Speciality::create(['en_name' => 'Orthopedics','vi_name'=>'Chỉnh hình','slug'=>32,]);
        Speciality::create(['en_name' => 'ENT','vi_name'=>'Tai mũi họng','slug'=>33,]);
        Speciality::create(['en_name' => 'Paediatrics & Neonatology','vi_name'=>'Nhi & Sơ sinh','slug'=>34,]);
        Speciality::create(['en_name' => 'General Medicine','vi_name'=>'Đa khoa','slug'=>35,]);
        Speciality::create(['en_name' => 'Traditional Medicine','vi_name'=>'Y học cổ truyền','slug'=>36,]);
        Speciality::create(['en_name' => 'Emergency Medicine','vi_name'=>'Y học cấp cứu','slug'=>37,]);
        Speciality::create(['en_name' => 'Neurology','vi_name'=>'Nội thần kinh','slug'=>38,]);
        Speciality::create(['en_name' => 'Physiotherapy & Rehabilitation','vi_name'=>'Vật lý trị liệu & Phục hồi chức năng','slug'=>39,]);
        Speciality::create(['en_name' => 'Plastic Surgery','vi_name'=>'Phẫu thuật thẩm mỹ','slug'=>40,]);
        Speciality::create(['en_name' => 'Psychiatry','vi_name'=>'Tâm thần','slug'=>41,]);
        Speciality::create(['en_name' => 'Radiotherapy','vi_name'=>'Xạ trị','slug'=>42,]);
        Speciality::create(['en_name' => 'Nuclear Medicine','vi_name'=>'Y học hạt nhân','slug'=>43,]);
        Speciality::create(['en_name' => 'Preventive Medicine','vi_name'=>'Y học dự phòng','slug'=>44,]);
        Speciality::create(['en_name' => 'Veterinary Medicine','vi_name'=>'Thú y','slug'=>45,]);
        Speciality::create(['en_name' => 'Diagnostic Imaging','vi_name'=>'Chẩn đoán hình ảnh','slug'=>46,]);
        Speciality::create(['en_name' => 'Andrology','vi_name'=>'Nam khoa','slug'=>47,]);
        Speciality::create(['en_name' => 'Molecular Biology','vi_name'=>'Sinh học phân tử','slug'=>48,]);
        Speciality::create(['en_name' => 'Geriatrics','vi_name'=>'Lão khoa','slug'=>49,]);
        Speciality::create(['en_name' => 'Dentistry','vi_name'=>'Nha khoa','slug'=>50,]);
        Speciality::create(['en_name' => 'Functional Exploration','vi_name'=>'Thăm dò chức năng','slug'=>51,]);
        Speciality::create(['en_name' => 'Intensive care & Toxicology','vi_name'=>'Hồi sức tích cực & Chống độc','slug'=>52,]);
        Speciality::create(['en_name' => 'Nuclear Medicine','vi_name'=>'Y học hạt nhân','slug'=>53,]);
        Speciality::create(['en_name' => 'Psychology','vi_name'=>'Tâm lý','slug'=>54,]);
        Speciality::create(['en_name' => 'Maternity','vi_name'=>'Sản','slug'=>55,]);
        Speciality::create(['en_name' => 'Pharmacy','vi_name'=>'Dược','slug'=>56,]);
        Speciality::create(['en_name' => 'Pathology','vi_name'=>'Giải phẫu bệnh','slug'=>57,]);
        Speciality::create(['en_name' => 'Biochemistry','vi_name'=>'Hoá sinh','slug'=>58,]);
        Speciality::create(['en_name' => 'Microbiology','vi_name'=>'Vi sinh','slug'=>59,]);
        Speciality::create(['en_name' => 'Infection Control','vi_name'=>'Chống nhiễm khuẩn','slug'=>60,]);
        Speciality::create(['en_name' => 'Endoscopy','vi_name'=>'Nội soi','slug'=>61,]);
        Speciality::create(['en_name' => 'Sports Medicine','vi_name'=>'Y học thể thao','slug'=>62,]);
    }
}
