<?php

namespace Database\Seeders;

use App\Models\User\NotificationType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\Type;

class NotificationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        NotificationType::truncate();
        DB::table('notification_types')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        NotificationType::create(
            [
                'type' => 'attach',
            ]
        );
        NotificationType::create(
            [
                'type' => 'like',
            ]
        );
        NotificationType::create(
            [
                'type' => 'love',
            ]
        );
        NotificationType::create(
            [
                'type' => 'support',
            ]
        );
        NotificationType::create(
            [
                'type' => 'follow',
            ]
        );
        NotificationType::create(
            [
                'type' => 'comment',
            ]
        );
        NotificationType::create(
            [
                'type' => 'primary',
            ]
        );
        NotificationType::create(
            [
                'type' => 'secondary',
            ]
        );
        NotificationType::create(
            [
                'type' => 'follow-author',
            ]
        );
        NotificationType::create(
            [
                'type' => 'login',
            ]
        );
    }
}
