<?php

namespace Database\Seeders;

use App\Models\Category\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Category::truncate();
        DB::table('categories')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Category::create(
            [
                'en_category' => 'news & articles',
                'vi_category' => 'vi news & articles',
                'slug' => 'news-articles',
                'is_visible' => 1,
            ]
        );
        Category::create(
            [
                'en_category' => 'drug info & updates',
                'vi_category' => 'vi drug info & updates',
                'slug' => 'drug-info-updates',
                'is_visible' => 1,
            ]
        );
        Category::create(
            [
                'en_category' => 'test your knowledge',
                'vi_category' => 'vi test your knowledge',
                'slug' => 'test-your-knowledge',
                'is_visible' => 0,
            ]
        );
        Category::create(
            [
                'en_category' => 'events',
                'vi_category' => 'vi events',
                'slug' => 'events',
                'is_visible' => 1,
            ]
        );
        Category::create(
            [
                'en_category' => 'discussion',
                'vi_category' => 'vi discussion',
                'slug' => 'discussion',
                'is_visible' => 1,
            ]
        );
        Category::create(
            [
                'en_category' => 'career & paid opportunity',
                'vi_category' => 'vi career & paid opportunity',
                'slug' => 'career-paid-opportunity',
                'is_visible' => 1,
            ]
        );
    }
}
