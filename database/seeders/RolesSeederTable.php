<?php

namespace Database\Seeders;

use App\Models\Role\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Role::truncate();
        DB::table('role_user')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Role::create(
            [
                'role' => 'Administrator'
            ]
        );
        Role::create(
            [
                'role' => 'Developer'
            ]
        );
        Role::create(
            [
                'role' => 'Enterprise'
            ]
        );
        Role::create(
            [
                'role' => 'Generic User'
            ]
        );


    }
}
