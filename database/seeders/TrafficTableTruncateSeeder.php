<?php

namespace Database\Seeders;

use App\Models\Traffic\Traffic;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TrafficTableTruncateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Traffic::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
