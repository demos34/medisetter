<?php

namespace Database\Seeders;

use App\Models\Degree\Degree;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->call(DegreesTableSeeder::class);
        $this->call(ReactionTableSeeder::class);
        $this->call(RolesSeederTable::class);
        $this->call(SpecTableSeeder::class);
        $this->call(SubcategoriesTableSeeder::class);
        $this->call(TrafficTableTruncateSeeder::class);
        $this->call(UniversitiesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AuthorTableSeeder::class);
        $this->call(PostsTableSeeder::class);
    }
}
