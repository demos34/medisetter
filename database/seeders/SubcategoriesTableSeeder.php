<?php

namespace Database\Seeders;

use App\Models\Category\Subcategory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubcategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Subcategory::truncate();
        DB::table('subcategories')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Subcategory::create(
            [
                'en_name' => 'Surveys',
                'vi_name' => 'vi Surveys',
                'slug' => 'surveys-6',
                'category_id' => 6,
            ]
        );
        Subcategory::create(
            [
                'en_name' => 'Clinical Studies',
                'vi_name' => 'vi Clinical Studies',
                'slug' => 'clinical-studies-6',
                'category_id' => 6,
            ]
        );
        Subcategory::create(
            [
                'en_name' => 'Products preview',
                'vi_name' => 'vi Products preview',
                'slug' => 'products-preview-6',
                'category_id' => 6,
            ]
        );
        Subcategory::create(
            [
                'en_name' => 'Job',
                'vi_name' => 'vi Job',
                'slug' => 'jobs-6',
                'category_id' => 6,
            ]
        );
    }
}
