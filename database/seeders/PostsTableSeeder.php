<?php

namespace Database\Seeders;

use App\Models\Category\Category;
use App\Models\Post\Post;
use App\Models\User;
use Faker\Provider\Lorem;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Post::truncate();
        DB::table('posts')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        User\Author::all()->each(function (User\Author $author) {
            foreach (Category::all() as $category) {
                $title = Lorem::word() . '-' . $category->id;
                $author->posts()->create(
                    [
                        'title' => $title,
                        'body' => Lorem::words(50, true),
                        'slug' => Str::slug($title),
                        'category_id' => $category->id,
                        'image' => '/storage/posts/images/no-img.png'
                    ],
                );
            }
        });
    }
}
