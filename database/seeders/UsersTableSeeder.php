<?php

namespace Database\Seeders;

use App\Models\Role\Role;
use App\Models\User;
use App\Models\User\Contact;
use App\Models\User\From;
use App\Models\User\Profile;
use App\Models\User\To;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::table('users')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $administratorRole = Role::where('role', 'Administrator')->first();

        $dev = User::create(
            [
                'name' => 'Inso',
                'email' => 'ins0mniac1van@gmail.com',
                'password' => Hash::make('123123123'),
                'api_token' => bin2hex(openssl_random_pseudo_bytes(40)),
                'avatar' => '/storage/helpers/profile.png',
                'is_verified' => true,
            ]
        );

        $user = User::create(
            [
                'name' => 'Teya',
                'email' => 'georgieva.doroteqq@gmail.com',
                'password' => Hash::make('123123123'),
                'api_token' => bin2hex(openssl_random_pseudo_bytes(40)),
                'avatar' => '/storage/helpers/profile.png',
                'is_verified' => true,
            ]
        );

        $devContact = Contact::create(
            [
                'user_id' => $dev->id,
            ]
        );

        $userContact = Contact::create(
            [
                'user_id' => $user->id,
            ]
        );

        $dev->contacts()->attach($devContact->id);
        $user->contacts()->attach($userContact->id);

        Profile::create(
            [
                'user_id' => $dev->id,
                'avatar' => '/storage/helpers/profile.png',
            ]
        );

        Profile::create(
            [
                'user_id' => $user->id,
                'avatar' => '/storage/helpers/profile.png',
            ]
        );

        To::create(
            [
                'user_id' => $dev->id,
            ]
        );


        To::create(
            [
                'user_id' => $user->id,
            ]
        );

        From::create(
            [
                'user_id' => $dev->id,
            ]
        );
        From::create(
            [
                'user_id' => $user->id,
            ]
        );

        $dev->roles()->attach($administratorRole->id);
        $user->roles()->attach($administratorRole->id);
    }
}
