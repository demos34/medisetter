<?php

namespace Database\Seeders;

use App\Models\Post\Reaction;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReactionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Reaction::truncate();
        DB::table('reactions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        Reaction::create(
            [
                'en_name' => 'like',
                'vi_name' => 'vi-like',
                'image' => '/storage/reactions/like.svg',
            ]
        );
        Reaction::create(
            [
                'en_name' => 'love',
                'vi_name' => 'vi-love',
                'image' => '/storage/reactions/love.svg',
            ]
        );
        Reaction::create(
            [
                'en_name' => 'support',
                'vi_name' => 'vi-support',
                'image' => '/storage/reactions/support.svg',
            ]
        );
    }
}
